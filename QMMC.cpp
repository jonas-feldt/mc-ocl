/*
 * File:   QMMC.cpp
 * Author: Jonas Feldt
 * 
 * Version: 2015-11-30
 */

#include <iostream>
#include <future>
#include <random>
#include <string>
#include <fstream>
#include <cmath>
#include <functional>

#include "QMMC.h"
#include "jonas_io_utils.h"
#include "System.h"
#include "Run.h"
#include "ChainResults.h"
#include "math_utils.h"
#include "geom_utils.h"

#include "options.h" // Header with all run options.


/**
 * Constructor
 */
QMMC::QMMC (
      const std::string base_name,
      const Run *run,
      const int num_devices,
      const int num_chains)
 : m_base_name (base_name),
   num_devices (num_devices),
   num_chains (num_chains)
{
   // works on a copy to not modify the original run object
   m_run = new Run(*run);
}



/**
 * Destructor
 */
QMMC::~QMMC () {
   delete m_run;
}





/**
 * Runs the QM Metropolis MC simulation.
 *
 * @param systems
 *
 * @return a vector of vectors of the final systems (=systems[num_devices][num_chains])
 *
 */
std::vector<std::vector<System*>> QMMC::run (std::vector<std::vector<System*>> &systems)
{
   vvd energies_old (systems.size(), vd(systems.begin()->size()));

   for (unsigned int i = 0; i < m_run->num_devices; ++i) {
      for (unsigned int j = 0; j < m_run->num_chains; ++j) {
         energies_old[i][j] = run_molpro (
               "initial-" + std::to_string(i) + "-" + std::to_string(j),
               systems[i][j]);
         VERBOSE_LVL_INIT("\tQM energy: " << energies_old[i][j] << std::endl);
      }
   }

   return run (systems, energies_old);
}




/**
 * Runs the QM Metropolis MC simulation.
 *
 * @param system
 *
 * @return a vector of vectors of the final systems (=systems[n_device][n_chain])
 */
std::vector<std::vector<System*>> QMMC::run (System *system)
{

   double energy_old = run_molpro ("initial", system);

   VERBOSE_LVL_INIT("[INIT] System atoms  =" << system->natom << std::endl);    
   VERBOSE_LVL_INIT("[INIT]     QM energy = " << energy_old << std::endl);

   // prepares data for multi-chain jobs
   vvd energies_old (num_devices, vd(num_chains)); // total energies of the last step
   std::vector<std::vector<System*>> systems;

   for (unsigned int i = 0; i < m_run->num_devices; ++i) {
      std::vector<System*> tmpS;
      tmpS.reserve(m_run->num_chains);
      for (unsigned int j = 0; j < m_run->num_chains; ++j) {
         tmpS.push_back(system);
         energies_old[i][j] = energy_old;
      }
      systems.push_back(tmpS);
   }

   return run (systems, energies_old);
}




/**
 * Runs the QM Metropolis MC simulation.
 *
 * @param systems
 * @param energies_old
 *
 * @return a vector of vectors of the final systems (=systems[num_devices][num_chains])
 */
std::vector<std::vector<System*>> QMMC::run (
      std::vector<std::vector<System*>> &systems, 
      const vvd &energies_old)
{
   // Adjust maxmoves according to number of chains
   m_run->maxmoves = m_run->maxmoves / (num_devices * num_chains);
   VERBOSE_LVL_WARN("[INIT] Set Maxmoves per Chain: " << m_run->maxmoves << std::endl);

   // seeds PRNG
   random_engine engine;
   std::uniform_int_distribution<int> dist (
         std::numeric_limits<int>::min(),
         std::numeric_limits<int>::max());
   if (not m_run->has_seed_path) engine.seed(m_run->seed);

   // Starts num_devices * num_chains tasks
   std::vector<std::vector<std::future<std::tuple<ChainResults*, System*>>>> futures;
   futures.reserve(systems.size());
   int index = 0;
   for (unsigned int i = 0; i < systems.size(); ++i) {

      std::vector<std::future<std::tuple<ChainResults*, System*>>> tmp;
      tmp.reserve(systems.begin()->size());
      for (unsigned int j = 0; j < systems.begin()->size(); ++j) {

         int seed = 0;
         if (not m_run->has_seed_path) seed = dist(engine);

         std::packaged_task<std::tuple<ChainResults*, System*> ()> task(std::bind(
                &QMMC::run_chain, this, systems[i][j], index, seed,
                m_base_name, energies_old[i][j]));
         tmp.push_back(task.get_future());
         std::thread(move(task)).detach();
         index++;
      }
      futures.push_back(std::move(tmp));
   }

   // collects results
   std::vector<std::vector<System*>> final_systems (m_run->num_devices, std::vector<System*>(m_run->num_chains));
   std::vector<ChainResults*> results;
   for (unsigned int i = 0; i < systems.size(); ++i) {
      for (unsigned int j = 0; j < systems.begin()->size(); ++j) {
         std::tuple<ChainResults*, System*> result = futures[i][j].get();
         results.push_back (std::get<0> (result));
         final_systems[i][j] = std::get<1> (result); 
      }
   }

   ChainResults::print_results (m_base_name + ".out", results);

   for (ChainResults *result : results) delete result;

   return final_systems;
}




std::tuple<ChainResults *, System*> QMMC::run_chain(
      System *system_start,
      const int chain_id,
      const int seed,
      std::string basename,
      double energy_old)
{

   // Prints the header for the energy file
   FILE *energy_file = nullptr;
   FILE *exen_file = nullptr;
   std::string xyz_file;
   if (m_run->print_every != 0) {
      energy_file = std::fopen (
            (m_base_name + "-energy-" + std::to_string (chain_id) + ".out").c_str(), "w");
      fprintf (energy_file, "%10s %10s %20s %10s %20s %10s\n",
           "Steps", "Accepted", "E_qm", "dE_qm", "mE_qm", "sE_qm"); 
      xyz_file = m_base_name + "-" + std::to_string (chain_id) + ".xyz";
   }
   if (m_run->print_energy != 0) {
      exen_file = std::fopen (
            (m_base_name + "-exen-" + std::to_string (chain_id) + ".out").c_str(), "w");
      fprintf (exen_file, "%10s %10s %20s %10s %20s %10s\n",
           "Steps", "Accepted", "E_qm", "dE_qm", "mE_qm", "sE_qm"); 
   }

   // Inits systems for the simulation
   System *system = new System (*system_start);

   // Inits variables
   double e_old         = energy_old;
   double old_mean      = 0.0;
   double old_variance  = 0.0;
   int accepted_steps   = 0;
   double stepmax       = m_run->stepmax;
   int adj_steps        = 0;
   int adj_acc_steps    = 0;

   // Inits random engine
   random_engine rnd_engine;
   if (m_run->has_seed_path) {
      std::string file = m_run->seed_path + "/" + std::to_string(chain_id) + ".random";
      VERBOSE_LVL_INIT("[INIT] reading file " << file << std::endl);
      std::ifstream ifile (file, std::ios::binary);
      ifile >> rnd_engine;
      VERBOSE_LVL_INIT("[INIT] loading random state " << rnd_engine() << std::endl);
   } else {
      rnd_engine.seed(seed);
   }

   // Inits distributions for the random numbers
   std::uniform_int_distribution<int> dist_atom (0, system_start->natom - 1);
   std::uniform_real_distribution<double> dist_two          (-1.0, 1.0);
   std::uniform_real_distribution<_FPOINT_S_> dist_boltzman ( 0.0, 1.0);

   auto dist_step = new std::uniform_real_distribution<_FPOINT_S_> ( 0.0, stepmax);

   const std::string chain_id_s = std::to_string (chain_id);

   // QM MC steps
   for (unsigned int step = 0; step < m_run->maxmoves; ++step) {

      int random_atom = dist_atom (rnd_engine);

      double old_x = system->rx[random_atom];
      double old_y = system->ry[random_atom];
      double old_z = system->rz[random_atom];

      // translation
      double rnd1;
      double rnd2;
      double s_rnd_12;
      do {
         rnd1 = dist_two (rnd_engine);
         rnd2 = dist_two (rnd_engine); 
         s_rnd_12 = rnd1 * rnd1 + rnd2 * rnd2;
      } while (s_rnd_12 >= 1.0);

      double trans_x = 2 * rnd1 * sqrt (1 - s_rnd_12);
      double trans_y = 2 * rnd2 * sqrt (1 - s_rnd_12);
      double trans_z = 1 - 2 * s_rnd_12;

      double random_step = (*dist_step) (rnd_engine); 
      system->rx[random_atom] += random_step * trans_x;
      system->ry[random_atom] += random_step * trans_y;
      system->rz[random_atom] += random_step * trans_z;

      double energy = run_molpro (chain_id_s + "-" + std::to_string(step), system);
      double de = energy - e_old;

      if (de < 0.0 ||
            boltzmann_factor (m_run->temperature, de) > dist_boltzman(rnd_engine)) {

         accepted_steps++;
         adj_acc_steps++;
         adj_steps++;

         e_old = energy;

      } else {

         adj_steps++;

         // restore previous configuration
         system->rx[random_atom] = old_x;
         system->ry[random_atom] = old_y;
         system->rz[random_atom] = old_z;

         de = 0.0;
      }

      // Init Method
      if (step == 0) {

         old_mean = e_old; 
         old_variance = 0; 

      } else {
        
         double new_mean     = old_mean + (e_old - old_mean) / (step + 1);
         double new_variance = old_variance + (e_old - old_mean) * (e_old - new_mean);
      
         old_mean     = new_mean; 
         old_variance = new_variance; 
      }

      // IO
      if (m_run->print_every != 0 && (step % m_run->print_every) == 0) {
         fprintf (energy_file, "%10i %10i %20.3f %10.5f %20.3f %10.5f\n",
               step, accepted_steps, e_old, de, old_mean, std::sqrt(old_variance / (step + 1)));
         system->save_xyz (xyz_file);
      }
      if (m_run->print_energy != 0 && (step % m_run->print_energy) == 0) {
         fprintf (exen_file, "%10i %10i %20.3f %10.5f %20.3f %10.5f\n",
               step, accepted_steps, e_old, de, old_mean, std::sqrt(old_variance / (step + 1)));
      }

      // adjust step or theta
      if (m_run->adjust_max_step != 0 && step != 0 && (step % m_run->adjust_max_step) == 0)
      {
         double tmp = stepmax;
         stepmax = adjust_stepmax (adj_steps, adj_acc_steps, stepmax, m_run->acceptance_ratio);
         if ((std::abs (tmp - stepmax) / tmp) > 0.05) // more than 5% diff
         {
            adj_steps = 0;
            adj_acc_steps = 0;
         }
         delete dist_step;
         dist_step = new std::uniform_real_distribution<_FPOINT_S_> (0.0, stepmax);
      }

   } // end MC steps

   // saves random number generator state
   std::ofstream ofile (basename + "/" + std::to_string(chain_id) + ".random", std::ios::binary);
   ofile << rnd_engine; 

   delete dist_step;
   
   if (m_run->print_every  != 0) std::fclose (energy_file);
   if (m_run->print_energy != 0) std::fclose (exen_file  );

   // computes final step or theta
   double new_step = m_run->stepmax;
   if (m_run->adjust_max_step != 0)
   {
     new_step = stepmax;
   }

   ChainResults *results = new ChainResults (
         old_mean, 0.0, old_variance, 0.0, new_step, m_run->maxmoves,
         accepted_steps);

   return std::make_tuple (results, const_cast<System*> (system));
}






double QMMC::run_molpro (
      std::string filename,
      System *system) const
{
   write_system (m_run->grid_input, m_base_name,
         filename, system);
   molpro_lattice (m_base_name, filename, m_run->molpro_exe, m_run->molpro_exe_cores); 
   const double e = read_energy_molpro_jf (m_base_name, filename);
   const std::string tmp = m_base_name + std::string("/") + filename;
   std::remove((tmp + std::string(".inp")).c_str());
   std::remove((tmp + std::string(".out")).c_str());
   std::remove((tmp + std::string(".cosmo_out")).c_str());
   return e;
}









