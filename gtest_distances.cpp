
#include <string>
#include <vector>

#include "gtest/gtest.h"
#include "System.h"
#include "distance_matrix.h"
#include "io_utils.h"
#include "typedefs.h"

TEST (DistanceInterface, DistancesMatrix) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   // XXX cannot reset this because the DistanceCall points to the initial distance_matrix
   //system->distances = new DistanceMatrix ();
   //system->distances->SetContext (system);
   //system->UpdateDistances ();

   DistanceCall GetDistance = system->distances->GetDelegate ();
   // XXX diagonal not used anymore
   //EXPECT_EQ (GetDistance (0, 0), 0);
   //EXPECT_EQ (GetDistance (1, 1), 0);
   EXPECT_EQ (GetDistance (1, 0), 1.0);
   EXPECT_EQ (GetDistance (1, 0), GetDistance (0, 1));

   system->rz[0] = -1.0;
   EXPECT_EQ (GetDistance (1, 0), 1.0);
   system->UpdateDistances ();
   EXPECT_EQ (GetDistance (1, 0), 2.0);

   delete system;
}


TEST (DistanceInterface, DistanceCall) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   //system->distances = new DistanceMatrix ();
   //system->distances->SetContext (system);
   //system->UpdateDistances ();

   DistanceCall GetDistance = system->GetDistance;

   // XXX diagonal not used anymore
   //EXPECT_EQ (GetDistance (0, 0), 0);
   //EXPECT_EQ (GetDistance (1, 1), 0);
   EXPECT_EQ (GetDistance (1, 0), 1.0);
   EXPECT_EQ (GetDistance (1, 0), GetDistance (0, 1));

   system->rz[0] = -1.0;
   EXPECT_EQ (GetDistance (1, 0), 1.0);
   system->UpdateDistances ();
   EXPECT_EQ (GetDistance (1, 0), 2.0);

   System *other = new System (*system);
   DistanceCall OtherGetDistance = other->GetDistance;
   EXPECT_EQ (OtherGetDistance (1, 0), 2.0);

   delete other;
   delete system;
}
