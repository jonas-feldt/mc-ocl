/**
 *
 * File: geom_utils.cpp
 * Author: Jonas Feldt, João Oliveira
 *
 * Version: 2013-07-02
 *
 */

#include <math.h>
#include <iostream>

#include "System.h"
#include "math_utils.h"

using namespace std;


/**
 * Calculates the distance betweeen atom i and atom j
 *
 * @param rxi coordinates of atom i
 * @param ryi coordinates of atom i
 * @param rzi coordinates of atom i
 * @param rxj coordinates of atom j
 * @param ryj coordinates of atom j
 * @param rzj coordinates of atom j
 */
double compute_distance (double rxi, double ryi, double rzi, double rxj, double ryj, double rzj) {

   double diffx = rxi - rxj;
   double diffy = ryi - ryj;
   double diffz = rzi - rzj;

   double distsq = diffx * diffx + diffy * diffy + diffz * diffz;
   return sqrt (distsq);
}



/**
 * Calculates the minimal distance between the atoms i and j in a periodic
 * system. Expects coordinates in reduced units.
 *
 * @param rxi coordinates of atom i
 * @param ryi coordinates of atom i
 * @param rzi coordinates of atom i
 * @param rxj coordinates of atom j
 * @param ryj coordinates of atom j
 * @param rzj coordinates of atom j
 */
double compute_minimal_distance (double rxi, double ryi, double rzi,
        double rxj, double ryj, double rzj) {

   double diffx = rxi - rxj;
   double diffy = ryi - ryj;
   double diffz = rzi - rzj;

   // converts to minimal distances
   diffx -= static_cast<int> (floor (diffx + 0.5));
   diffy -= static_cast<int> (floor (diffy + 0.5));
   diffz -= static_cast<int> (floor (diffz + 0.5));

   double distsq = diffx * diffx + diffy * diffy + diffz * diffz;
   return sqrt (distsq);
}



/**
 * Calculates the minimal difference vector between the atoms i and j in a
 * periodic system. Expects coordinates in reduced units.
 *
 * @param rxi coordinates of atom i
 * @param ryi coordinates of atom i
 * @param rzi coordinates of atom i
 * @param rxj coordinates of atom j
 * @param ryj coordinates of atom j
 * @param rzj coordinates of atom j
 */
void compute_minimal_diff (double rxi, double ryi, double rzi,
        double rxj, double ryj, double rzj, double *scr) {

   scr[0] = rxi - rxj;
   scr[1] = ryi - ryj;
   scr[2] = rzi - rzj;

   // converts to minimal distances
   scr[0] -= static_cast<int> (floor (scr[0] + 0.5));
   scr[1] -= static_cast<int> (floor (scr[1] + 0.5));
   scr[2] -= static_cast<int> (floor (scr[2] + 0.5));

}

double value (double *v) {
   return sqrt(pow(v[0], 2) + pow(v[1], 2) + pow(v[2], 2));
}

double scalar_product (double *v1, double *v2) {
   return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}

double scalar_product (
      double rxi, double ryi, double rzi,
      double rxj, double ryj, double rzj)
{
   return rxi * rxj + ryi * ryj + rzi * rzj;
}


/* 
 * Computes the angle i-j-k. Expects coordinates in reduced units.
 */
double compute_minimal_angle (
      double rxi, double ryi, double rzi,
      double rxj, double ryj, double rzj,
      double rxk, double ryk, double rzk)
{
   double *scr1 = new double[3];
   double *scr2 = new double[3];
   
   compute_minimal_diff (rxi, ryi, rzi, rxj, ryj, rzj, scr1); // i-j
   compute_minimal_diff (rxk, ryk, rzk, rxj, ryj, rzj, scr2); // k-j

   double angle = acos(scalar_product(scr1, scr2) / (value(scr1) * value(scr2)));

   delete[] scr1;
   delete[] scr2;

   return angle;
}



/**
 * Translates an atom with a displacement of random_trans_num in a certain
 * direction (ex_trans,ey_trans,ez_trans). Overwrites the current coordinates
 * with the result.
 *
 * @param random_trans_num value of the displacement
 * @param ex_trans x coordinate from the unitary vector responsible for the direction of the translation
 * @param ey_trans y coordinate from the unitary vector responsible for the direction of the translation
 * @param ez_trans z coordinate from the unitary vector responsible for the direction of the translation
 * @param rx atom coordinate before translation
 * @param ry atom coordinate before translation
 * @param rz atom coordinate before translation
 */
void translation (double &random_trans_num,
        double &ex_trans, double &ey_trans, double &ez_trans,
        double &rx, double &ry, double &rz) {

   rx += (random_trans_num * ex_trans);
   ry += (random_trans_num * ey_trans);
   rz += (random_trans_num * ez_trans);
}

void translation(double random_trans_num,
      double ex_trans, double ey_trans, double ez_trans,
      double *rx, double *ry, double *rz, const int atom)
{

   rx[atom] += (random_trans_num * ex_trans);
   ry[atom] += (random_trans_num * ey_trans);
   rz[atom] += (random_trans_num * ez_trans);
}



/**
 * Rotation of an atom, the result overwrites the current coordinates.
 *
 * @param theta rotation angle
 * @param random_1 first random number needed to create a unitary vector
 * @param random_2 second random number needed to create a unitary vector
 * @param random_12 product of random_1 and random_2 needed to create a unitary vector
 * @param rx_old atom coordinate before rotation
 * @param ry_old atom coordinate before rotation
 * @param rz_old atom coordinate before rotation
 */
void rotate (double &theta, double &random_1, double &random_2,
        double &s_random_12, double &rx_old, double &ry_old, double &rz_old) {

   // First generate the Random unit vector v=(ex,ey,ez)
   double ex = 0;
   double ey = 0;
   double ez = 0;

   ex = 2.0 * random_1 * sqrt (1.0 - s_random_12);
   ey = 2.0 * random_2 * sqrt (1.0 - s_random_12);
   ez = 1.0 - 2.0 * s_random_12;

   /*
    *  The Next step is to generate the QUATERNION, the random 4D unitery vector  q = [q0, q1, q2, q3]
    *  q is created as q = [cos( angle / 2), ex sin( angle / 2), ey sin( angle / 2), ez sin( angle / 2)]
    */

   double q0 = 0;
   double q1 = 0;
   double q2 = 0;
   double q3 = 0;

   q0 = cos (theta / 2.0);
   q1 = ex * sin (theta / 2.0);
   q2 = ey * sin (theta / 2.0);
   q3 = ez * sin (theta / 2.0);

   double rx_new = 0;
   double ry_new = 0;
   double rz_new = 0;

   rx_new = rx_old * (1.0 - 2 * (q2 * q2) - 2.0 * (q3 * q3)) + ry_old * (2.0 * (q1 * q2) - 2.0 * (q0 * q3)) + rz_old * (2.0 * (q1 * q3) + 2.0 * (q0 * q2));
   ry_new = rx_old * (2.0 * (q2 * q1) + 2.0 * (q0 * q3)) + ry_old * (1.0 - 2.0 * (q3 * q3) - 2.0 * (q1 * q1)) + rz_old * (2.0 * (q2 * q3) - 2.0 * (q0 * q1));
   rz_new = rx_old * (2.0 * (q3 * q1) - 2.0 * (q0 * q2)) + ry_old * (2.0 * (q3 * q2) + 2.0 * (q0 * q1)) + rz_old * (1.0 - 2.0 * (q1 * q1) - 2.0 * (q2 * q2));

   rx_old = rx_new;
   ry_old = ry_new;
   rz_old = rz_new;
}



/**
 * Computes the center of mass for one molecule.
 *
 * @param id_mol id of the molecule
 * @param system current system
 * @param rx_center variable to save the coordinates of the center of mass
 * @param ry_center variable to save the coordinates of the center of mass
 * @param rz_center variable to save the coordinates of the center of mass
 */
void center_mass(int id_mol, System *system,
        double &rx_center, double &ry_center, double &rz_center){

   double *atom_mass = system->atom_mass;
   int natom         = system->natom;
   int *molecule     = system->molecule;
   double *rx        = system->rx;
   double *ry        = system->ry;
   double *rz        = system->rz;

   double molecular_mass = 0.0;
   rx_center = 0.0;
   ry_center = 0.0;
   rz_center = 0.0;

   for (int i = 0; i < natom; i++) {
      if (molecule[i] == id_mol) {

         rx_center += rx[i] * atom_mass[i];
         ry_center += ry[i] * atom_mass[i];
         rz_center += rz[i] * atom_mass[i];

         molecular_mass += atom_mass[i];
      }
   }

   double rec = 1.0 / molecular_mass;
   rx_center *= rec;
   ry_center *= rec;
   rz_center *= rec;
}




/**
 * Computes the normalized (|v| = 1) dipole moment of the given molecule.
 *
 * @param id_mol id of the molecule
 * @param system current system
 * @param charges
 * @param x variable to save the coordinates of the center of mass
 * @param y variable to save the coordinates of the center of mass
 * @param z variable to save the coordinates of the center of mass
 */
void dipole (int id_mol, System *system, const double *charges,
        double &x, double &y, double &z){

   int natom         = system->natom;
   int *molecule     = system->molecule;
   double *rx        = system->rx;
   double *ry        = system->ry;
   double *rz        = system->rz;

   x = 0.0;
   y = 0.0;
   z = 0.0;

   for (int i = 0; i < natom; i++) {
      if (molecule[i] == id_mol) {
         x += rx[i] * charges[i];
         y += ry[i] * charges[i];
         z += rz[i] * charges[i];
      }
   }
   double scale = 1.0 / sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
   x *= scale;
   y *= scale;
   z *= scale;
}




/*
 * Computes the center of mass of the specified molecule. Then moves the
 * complete system so that the center of mass is in the origin.
 *
 */
void move_into_center_mass(int id_mol, System *system) {
   double cx, cy, cz;
   center_mass(id_mol, system, cx, cy, cz);
   for (unsigned int i = 0; i < system->natom; i++) {
      system->rx[i] -= cx;
      system->ry[i] -= cy;
      system->rz[i] -= cz;
   }
}



/**
 * Adjusts maximum displacement.
 *
 * @param n_moves number of steps
 * @param accepted_steps number of accepted steps
 * @param max_disp maximum displacement
 * @param accept_percent percentage of accepted configurations (in a decimal format  0.50 = to 50 % of accepted configurations)
 * @return max_disp value of the new maximum displacement
 *
 */
double adjust_stepmax (int n_moves, int accepted_steps, double max_disp,
        double accept_percent) {

   double ratio = (double) accepted_steps / (double) n_moves;
   double scale = ratio / accept_percent;
   max_disp *= scale;
   return max_disp;
}



/**
 * Copies the coordinates of the moved molecule from the first system to the
 * copy. Assumes same indices for the coordinates in both systems. 
 *
 * @param system
 * @param copy
 * @param changed_molecule id of the changed molecule
 */
void copy_coordinates(System *system, System *copy, int changed_molecule) {
   for(int pos = 0 ; pos < system->molecule2atom[changed_molecule].natoms ; pos++ ){
      int i = system->molecule2atom[changed_molecule].atom_id[pos];
      copy->rx[i] = system->rx[i];
      copy->ry[i] = system->ry[i];
      copy->rz[i] = system->rz[i];
   }
}

