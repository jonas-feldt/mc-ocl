/*
 * File:   TrajectoryAnalyzerPeriodic.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-05-06
 */

#include <fstream>
#include <iostream>

#include "TrajectoryAnalyzerPeriodic.h"
#include "io_utils.h"



TrajectoryAnalyzerPeriodic::TrajectoryAnalyzerPeriodic (
        const std::string trajectory_file,
        const int first_frame,
        const int last_frame,
        const std::string output_file,
        const double box_dim,
        const unsigned int num_chains)
   : TrajectoryAnalyzer (
         trajectory_file,
         first_frame,
         last_frame,
         output_file,
         num_chains),
   system (nullptr),
   box_dim(box_dim)
{
}




TrajectoryAnalyzerPeriodic::~TrajectoryAnalyzerPeriodic ()
{
   delete system;
}


void TrajectoryAnalyzerPeriodic::read_system (std::string tinker_file)
{
   system = read_tinker_periodic_system (tinker_file, box_dim);
}


void TrajectoryAnalyzerPeriodic::run ()
{

   const int num = system->natom;
   std::string line;

   for (unsigned int i = 0; i < num_chains; ++i) {
      std::string tmp = trajectory_file + "_" + std::to_string (i);
      std::cout << "Reading file " << tmp << std::endl;
      std::ifstream trajectory (tmp.c_str());
      for (int i = 0; i < first_frame - 1; ++i) { // neglects first_frame systems
         for (int j = 0; j < num; ++j) {
            std::getline (trajectory, line); // discard lines
         }
      }

      for (int i = 0; i < steps_file; ++i) {
         read_periodic_frame (trajectory, system);
         analyze ();
      }
   }

   finalize();
   save_output ();
}
