/**
 *
 * File: ChainResults.h
 * Author: Jonas Feldt
 *
 * Version: 2014-10-22
 *
 */

#ifndef CHAINRESULTS_H
#define CHAINRESULTS_H

#include <vector>
#include <string>

class ChainResults {

public:

   ChainResults(
         double mean_energy,
         double mean_interaction,
         double std_dev_energy,
         double std_dev_interaction,
         double stepmax,
         int steps,
         int accepted_steps);

   ~ChainResults();

   static void print_results (std::string file, std::vector<ChainResults*> results);

   // results
   const double mean_energy;
   const double mean_interaction;

   const double std_dev_energy;
   const double std_dev_interaction;

   const double stepmax;

   const int steps;
   const int accepted_steps;
   
protected:
private:
};

#endif // CHAINRESULTS_H
