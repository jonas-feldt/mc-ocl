/*
 * File: distance_vector_periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2017-07-18
 *
 * Concrete implementation of the distances template which is used in Systems.
 */

#ifndef DISTANCE_VECTOR_PERIODIC_H
#define DISTANCE_VECTOR_PERIODIC_H

#include "distance_vector.h"

class DistanceVectorPeriodic : public DistanceVector
{

   public:

      void ComputeDistances () override;
      void ComputeDistances (unsigned int molecule) override;
      DistanceVectorPeriodic *clone () const override;

};


#endif
