/*
 * File:    OPLS_switcher_molecule.h
 * Author:  Jonas Feldt
 *
 */

#ifndef OPLS_SWITCHER_MOLECULE_H
#define OPLS_SWITCHER_MOLECULE_H

#include <vector>
#include <cstdio>

#include "Abstract_Switcher.h"

class OPLS;
class System;
class Run;
struct Molecule_db;

class OPLS_switcher_molecule : public Abstract_Switcher {

   public:

      OPLS_switcher_molecule (
            const System *system, Run *run, const std::vector<double> &factors);

      virtual ~OPLS_switcher_molecule();

      virtual void switch_params (const int unsigned frame, OPLS *opls, FILE *fout) override;

   private:

      const int molecule;
      const std::vector<Molecule_db> molecule2atom;
      const std::vector<int> system_types;

      const std::vector<int> atom_types;
      const std::vector<double> epsilons;
      const std::vector<double> sigmas;
      const std::vector<double> charges;
      const std::vector<double> factors;

};

#endif /* OPLS_SWITCHER_MOLECULE_H */
