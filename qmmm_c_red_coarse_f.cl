/**
 *
 * File: qmmm_c_red_coarse_d.cl
 * Author: Sebastiao Miranda
 *
 * Version: 2014-03-09
 *
 */
 
 /**
 * This reduction kernel allows tunning
 * the level of coarseness
 * 
 */

#pragma OPENCL EXTENSION cl_khr_fp64: enable

__kernel
void qmmm_c_red_coarse(

   // Memory buffer local to the work-group
   __local float *local_mem,
   
   // True size of the vector we wish to reduce.
   // (The vector comes in padded to be multiple of local_size)
   int true_vect_size,
   
   // Number of values this work-item will reduce
   int m_en_values,
   
   // Variable for collecting work-group energy result
   __global float *result_wg_energy
   
   ){
    
   __private int     local_id     ;
   __private int     global_id    ;
   __private int     local_size   ;
   __private float   p_energy =0.0;
    
   //Get id's
   local_id    = get_local_id(0);
   global_id   = get_global_id(0);
   local_size  = get_local_size(0);
      
      
   // if (global_id == 0){
      // for(int i=0; i<true_vect_size; i++){
         // p_energy+=result_wg_energy[i];
      // }
      // //printf("COARSE %lf\n",p_energy);
      // result_wg_energy[0] = p_energy;
   // }
        
// ORIGINAL VERSION:
      
   if(global_id < true_vect_size){
  
      //Collect previous energy results to private memory
      //and accumulate them.
      for(int i=0; i<m_en_values; i++){
         if(global_id*m_en_values+i<true_vect_size){
            p_energy+=result_wg_energy[global_id*m_en_values+i];         
         }
      }
      //Write energy to local memory
      local_mem[local_id] = p_energy;
      
   }else{
      local_mem[local_id] = 0;   
   }
   
   barrier(CLK_LOCAL_MEM_FENCE);
   
   //Now all work-items of the same work group must accumulate their energy.
   for(int offset = local_size/2; offset > 0 ;offset >>= 1){
      if(local_id < offset){
         local_mem[local_id] = local_mem[local_id + offset] + local_mem[local_id];
      }
      barrier(CLK_LOCAL_MEM_FENCE);
   }

   //Work item with id 0 writes the final result of the work-group
   if (local_id == 0) {
      //printf("KERN COARSE %lf\n",local_mem[0]);
      result_wg_energy[get_group_id(0)] = local_mem[0];
   } 
   
}
	