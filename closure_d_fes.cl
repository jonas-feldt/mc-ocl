/**
 *
 * File: closure_d.cl
 * Author: Sebastiao Miranda
 *
 * Version: 2014-02-25
 *
 */
  
#include "options.h"

/**
 * Equivalent pseudo-calls in Reference PMC CYCLE: 
 *
 *
 * energy_new  = qm_energy       + energy_charges  +
 *               energy_vdw_qmmm + change_vdw_qmmm +
 *               energy_mm       + change_mm;
 *    
 * energy_diff = energy_new - energy_old;
 *
 * if (energy_diff <= 0.0) {
 *
 *    number_accepted++;
 *
 *    'update Reference'
 *    
 *    qm_energy       += energy_charges;
 *    energy_vdw_qmmm += change_vdw_qmmm;
 *    energy_mm       += change_mm;
 *    energy_old      = energy_new;
 *
 * }else{
 *    
 *    'boltzman distribution'
 *
 *    if ' boltzman ' {
 *          
 *       number_accepted++;
 *
 *       'update Reference'
 *       
 *       qm_energy       += energy_charges;
 *       energy_vdw_qmmm += change_vdw_qmmm;
 *       energy_mm       += change_mm;
 *       energy_old      = energy_new;
 *          
 *    }else{
 *       'do nothing'
 *    }
 *    
 * }
 *
 *
 * 'Save Steps - If save_every'
 * 
 *
 *
 * 
 */
#pragma OPENCL EXTENSION cl_khr_fp64: enable

#define BOLTZMAN_KB 8.3144621 / 1000.0

inline double boltzmann_factor (double temp_K, double delta_energy) {
   // Thermodynamic beta, were beta = 1 / Kb.T
   double beta = 1.0 / (BOLTZMAN_KB * temp_K);
   return exp (-delta_energy * beta);
}

inline double adjust_stepmax (int n_moves, int accepted_steps, double max_disp,
                              double accept_percent) {

   double ratio = (double) accepted_steps / (double) n_moves;
   double diff = ratio - accept_percent;

   if (diff > 0.1) {
      max_disp *= 1.05;
   } else if (diff < -0.1) {
      max_disp *= 0.95;
   }

   return max_disp;
}
__kernel
void closure(
   
      // Scalar arguments
      int step_number, // 0. Arg
      
      // Input energies
      
      __global double *change_minors_energy,    
      //(^^Writen by mm vdwc Kernel                 )
      __global double  *change_qmmm_c_energy,
      //(^^Writen by qmmm c Kernel                  )
      __global double *qm_energy,                
      //(^^Writen by CPU once every Grid Update     )
      __global double *qm_energy_gp,             
      //(^^Writen by CPU once every Grid Update     )
      
      // Persistent energies/data (Kept between PMC CYCLE steps)
      __global double *energy_old, // 5. Arg
      __global double *energy_vdw_qmmm,
      __global double *energy_mm, 
      __global int    *number_accepted, 
      
      // Place to save accepted configurations
      __global double *accepted_config_data,
      __global double *accepted_energy_data, // 10. Arg
         
      // MM molecule old x,y,z
      __global double   *old_x,
      __global double   *old_y,
      __global double   *old_z,
      
      __global int      *mol2atom0,
      
      // Changed MM molecule (Kernel will write here)
      __global int      *changed_molecule, // 15. Arg
      __global int      *changed_molecule_size,
      __global double   *ch_x,
      __global double   *ch_y,
      __global double   *ch_z,
      
      // Constant Total number of atoms.
      __constant int *n_total_atoms, // XXX: Change to scalar parameter?  // 20. Arg
      __constant int *n_total_mol,
   
      // Random List for bolztman acceptance
      __global double *random_boltzman_acc,
      
      // Run Temperature
      __constant double *temperature,
      
      // Update random lists frequency
      __constant int *update_lists_every,

      // means / variance    
      __global double *old_means, // 25. Arg
      __global double *old_variance,
      // factor to scale energy_charges for free energy of solvation
      __constant double *factor_fes

   ){
   
   __private double energy_new         ;
   __private double energy_diff        ;
   __private double random_acc         ;
   __private double energy_charges     ;
   __private double boltz_factor       ;
   __private int    p_random_molecule  ;
   __private int    ch_start_atom_i    ;
   __private int    ch_end_atom        ;
   
   __private int p_update_lists_every     ;
   
   p_update_lists_every = *update_lists_every;
      
      
   // Collect random accept from boltzman list
   random_acc = random_boltzman_acc [step_number%p_update_lists_every];
   
   // Collect random molecule
   
   // Compute new energy
   energy_charges = *factor_fes * ((double)*change_qmmm_c_energy+change_minors_energy[2]);
   
   energy_new  =  *qm_energy       + energy_charges             +
                  *energy_vdw_qmmm + change_minors_energy[1]    +
                  *energy_mm       + change_minors_energy[0];
        
   // Compute energy diff               
   energy_diff = energy_new - *energy_old;
   
   // For Debug - Only works under OpenCL for CPU   
   // printf("[] change_qmmm_c_energy      %lf\n",*change_qmmm_c_energy);
   // printf("[] change_coulomb_nucl      %lf\n",change_minors_energy[2]);
   // printf("[] qm_energy                %lf\n",*qm_energy);
   // printf("[] energy_vdw_qmmm          %lf\n",*energy_vdw_qmmm);
   // printf("[] energy_mm                %lf\n",*energy_mm);
   // printf("[] energy_charges           %lf\n",energy_charges);
   // printf("[] change_qmmm_vdw_energy   %lf\n",change_minors_energy[1]);
   // printf("[] change_mm_vdwc_energy    %lf\n",change_minors_energy[0]);
   // printf("[] energy_new               %lf\n",energy_new);
   // printf("[] energy_old               %lf\n",*energy_old);
   // printf("[] energy_diff              %lf\n",energy_diff);
   
   // Decide Step
   if (energy_diff <= 0.0){

      *number_accepted = *number_accepted+1;
      // Update persistent energies
      *qm_energy       += energy_charges;
      *energy_vdw_qmmm += change_minors_energy[1];
      *energy_mm       += change_minors_energy[0];
      *energy_old       = energy_new;
      
      /*** Update Reference with changed molecule ***/
      p_random_molecule = *changed_molecule;

      // Find out atoms belonging to the molecule
      // end_atom no longer belongs to molecule
      // end_atom-start_atom_i gives molecule size
      if(p_random_molecule<(*n_total_mol)-1){
         ch_start_atom_i = mol2atom0[p_random_molecule];
         ch_end_atom     = mol2atom0[p_random_molecule+1];
      }else{//Last molecule
         ch_start_atom_i = mol2atom0[p_random_molecule];
         ch_end_atom     = *n_total_atoms;
      }
      
      // To acess the old coordinates of the changed molecule, the value
      // ch_start_atom_i must be added to index j.
      for (int j = 0; j < (ch_end_atom-ch_start_atom_i); j++) { //changed MM molecule
      
         old_x[j+ch_start_atom_i] = ch_x[j];
         old_y[j+ch_start_atom_i] = ch_y[j];
         old_z[j+ch_start_atom_i] = ch_z[j];
         
      }
   
   }else{
      
      // Boltzman distribution
      boltz_factor = boltzmann_factor (*temperature, energy_diff);     

      // For Debug - Only works under OpenCL for CPU   
      //printf("[] boltz_factor %lf > random_acc %lf? \n",boltz_factor,random_acc);
      
      if (boltz_factor > random_acc) {
         
         *number_accepted = *number_accepted+1;
         // Update persistent energies
         *qm_energy       += energy_charges;
         *energy_vdw_qmmm += change_minors_energy[1];
         *energy_mm       += change_minors_energy[0];
         *energy_old       = energy_new;
         
         /*** Update Reference with changed molecule ***/
         p_random_molecule = *changed_molecule;

         // Find out atoms belonging to the molecule
         // end_atom no longer belongs to molecule
         // end_atom-start_atom_i gives molecule size
         if(p_random_molecule<(*n_total_mol)-1){
            ch_start_atom_i = mol2atom0[p_random_molecule];
            ch_end_atom     = mol2atom0[p_random_molecule+1];
         }else{//Last molecule
            ch_start_atom_i = mol2atom0[p_random_molecule];
            ch_end_atom     = *n_total_atoms;
         }
         
         // To acess the old coordinates of the changed molecule, the value
         // ch_start_atom_i must be added to index j.
         for (int j = 0; j < (ch_end_atom-ch_start_atom_i); j++) { //changed MM molecule
         
            old_x[j+ch_start_atom_i] = ch_x[j];
            old_y[j+ch_start_atom_i] = ch_y[j];
            old_z[j+ch_start_atom_i] = ch_z[j];

         }
      }
   } 
   
   double interaction_energy = *energy_old - *qm_energy_gp - *energy_mm;
   double energy_o  = (double) *energy_old;
   double new_means[2];
   double new_variance[2]; 
   // Init Method
   // 0 index is for energy_old
   // 1 index is for interaction_energy
   if(step_number==0){
      old_means[0] = energy_o; 
      old_means[1] = interaction_energy; 
      old_variance[0] = 0; 
      old_variance[1] = 0; 
   }else{
     
      //update means
      new_means[0] = old_means[0] + (energy_o - old_means[0])/(step_number+1);
      new_means[1] = old_means[1] + (interaction_energy - old_means[1])/(step_number+1);
      //update variance
      new_variance[0] = old_variance[0] + (energy_o - old_means[0])*(energy_o-new_means[0]);
      new_variance[1] = old_variance[1] + (interaction_energy - old_means[1])*(interaction_energy-new_means[1]);
   
      old_means[0] = new_means[0]; 
      old_means[1] = new_means[1]; 
      old_variance[0] = new_variance[0]; 
      old_variance[1] = new_variance[1]; 
   }
   
   // Save Steps
   #ifndef AVOID_SAVING
   if (/*step_number!=0 &&*/ (step_number % COLLECT_EVERY) == 0) {
          
      int natom = *n_total_atoms;
      
      // Jump to correct memory address
      accepted_config_data += ((step_number/COLLECT_EVERY)%N_SAVE_SYSTEMS)*(N_ENERGY_SAVE_PARAMS+natom*3);                
      
      /*** Save System Energy ***/

      double interaction_energy = *energy_old - *qm_energy_gp - *energy_mm;
            
      *accepted_config_data = (double)step_number;
      accepted_config_data++;//increment pointer         

      *accepted_config_data = (double)*number_accepted;
      accepted_config_data++;//increment pointer             
      
      *accepted_config_data = (double)*energy_old;
      accepted_config_data++;//increment pointer   
      
      *accepted_config_data = (double)*energy_mm;
      accepted_config_data++;//increment pointer   
      
      *accepted_config_data = (double)interaction_energy;
      accepted_config_data++;//increment pointer  

      *accepted_config_data = (double)energy_charges;
      accepted_config_data++;//increment pointer 
      
      *accepted_config_data = (double)change_minors_energy[1];
      accepted_config_data++; //increment pointer 
      
      *accepted_config_data = (double)change_minors_energy[0];
      accepted_config_data++; //increment pointer 
      
      /*** Save System Configuration ***/
      int a = 0;
      
      // TODO / XXX : 
      //
      // This is not a viable solution in terms of performance.
      //
      // Either use something like requesting assync copybuf from CPU
      // Or make an assync work group copy to speedup memory writing.
      //
      //
      while(a<natom){
      
         *accepted_config_data = (double)old_x[a];
         accepted_config_data++;//increment pointer 
         
         *accepted_config_data = (double)old_y[a];
         accepted_config_data++;//increment pointer 
         
         *accepted_config_data = (double)old_z[a];
         accepted_config_data++;//increment pointer 
         
         a++;
      } 
   }
   #endif

   #ifdef EXTRA_ENERGY
   // Save extra energies
   if ((step_number % COLLECT_EVERY_ENERGY) == 0) {

      // Jump to correct memory address
      accepted_energy_data += ((step_number/COLLECT_EVERY_ENERGY)%N_SAVE_ENERGY) * N_ENERGY_SAVE_PARAMS;

      /*** Save System Energy ***/

      double interaction_energy = *energy_old - *qm_energy_gp - *energy_mm;
            
      *accepted_energy_data = (double)step_number;
      accepted_energy_data++;//increment pointer         

      *accepted_energy_data = (double)*number_accepted;
      accepted_energy_data++;//increment pointer             
      
      *accepted_energy_data = (double)*energy_old;
      accepted_energy_data++;//increment pointer   
      
      *accepted_energy_data = (double)*energy_mm;
      accepted_energy_data++;//increment pointer   
      
      *accepted_energy_data = (double)interaction_energy;
      accepted_energy_data++;//increment pointer  

      *accepted_energy_data = (double)energy_charges;
      accepted_energy_data++;//increment pointer 
      
      *accepted_energy_data = (double)change_minors_energy[1];
      accepted_energy_data++; //increment pointer 
      
      *accepted_energy_data = (double)change_minors_energy[0];
      accepted_energy_data++; //increment pointer 
      
   }
   #endif
}
