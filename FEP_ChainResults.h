/**
 *
 * File: FEP_ChainResults.h
 * Author: Jonas Feldt
 *
 * Version: 2014-10-22
 *
 */

#ifndef FEP_CHAINRESULTS_H
#define FEP_CHAINRESULTS_H

#include <vector>
#include <string>

#include "ChainResults.h"

class FEP_ChainResults : public ChainResults {

public:

   FEP_ChainResults(
         double mean_energy,
         double mean_interaction,
         double mean_free_energy,
         double std_dev_energy,
         double std_dev_interaction,
         double std_dev_free_energy,
         double stepmax,
         int steps,
         int accepted_steps);

   ~FEP_ChainResults();

   static void print_results (std::string file, std::vector<FEP_ChainResults*> results);

   // results
   const double mean_free_energy;
   const double std_dev_free_energy;
   
protected:
private:
};

#endif // FEP_CHAINRESULTS_H
