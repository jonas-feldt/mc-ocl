/*
 * File:   RDF.h
 * Author: Jonas Feldt
 *
 * Version: 2014-02-17
 */

#ifndef RDF_H
#define	RDF_H

#include "TrajectoryAnalyzer.h"


class RDF : public TrajectoryAnalyzer {
public:
   RDF (std::string trajectory_file,
         int first_ID, int second_ID, double delta, int max_bin, int first_frame,
         int last_frame, std::string output_file, const unsigned int num_chains);
   virtual ~RDF();

protected:
   virtual void analyze();
   virtual void finalize();
   virtual void save_output();


private:
   int first;
   int second;
   int max_bin;
   double delta;
   long *histogram;
   double *normalized;


};

#endif	/* RDF_H */

