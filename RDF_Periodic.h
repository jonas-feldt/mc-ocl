/*
 * File:   RDF_Periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2014-02-18
 */

#ifndef RDF_PERIODIC_H
#define	RDF_PERIODIC_H

#include "TrajectoryAnalyzerPeriodic.h"


class RDF_Periodic : public TrajectoryAnalyzerPeriodic {
public:
   RDF_Periodic(std::string trajectory_file,
         int first_ID, int second_ID, double delta, int max_bin,
         int first_frame, int last_frame, std::string output_file,
         double box_dim, const unsigned int num_chains);
   virtual ~RDF_Periodic() override;

protected:
   virtual void analyze() override;
   virtual void finalize() override;
   virtual void save_output() override;

   int first;
   int second;
   double delta;
   int max_bin;

private:

   long *histogram;
   double *normalized;

};

#endif	/* RDF_PERIODIC_H */

