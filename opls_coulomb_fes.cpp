/*
 * File:   opls_coulomb_fes.cpp
 * Author: Jonas Feldt
 *
 * Version: 2017-11-02
 */


#include "OPLS_Periodic.h"
#include "opls_coulomb_fes.h"
#include "System_Periodic.h"
#include "Run_Periodic.h"
#include "Constants.h"



OPLSCoulombFES::OPLSCoulombFES (
         const System *system,
         Run_Periodic *run,
         double fes_factor)
   : OPLS_Periodic (system, run),
   fes_factor (fes_factor)
{
}



double OPLSCoulombFES::energy_coulomb (DistanceCall GetDistance) const
{
   return energy_coulomb_mm (GetDistance) + energy_coulomb_qmmm (GetDistance);
}



double OPLSCoulombFES::energy_coulomb (
      DistanceCall GetDistance,
      DistanceCall GetDistance_old,
      int changed_molecule) const
{
   return energy_coulomb_mm (GetDistance, GetDistance_old, changed_molecule) +
      energy_coulomb_qmmm (GetDistance, GetDistance_old, changed_molecule);
}




double OPLSCoulombFES::energy_coulomb_qmmm (DistanceCall GetDistance) const
{
   double energy = 0.0;
   int start = molecule2atom[0].natoms;
   for(int pos = 0; pos < start; pos++){
      int i = molecule2atom[0].atom_id[pos];
      for (int j = start; j < natom; j++) {
         
         double dist = GetDistance (i, j);
         if (dist < cutoff_coulomb) {
            double qs = charge[i] * charge[j];
            energy += qs * (1.0 / dist - cutoff_coulomb_rec +
                    cutoff_coulomb_rec_sq * (dist - cutoff_coulomb));
         }
      }
   }
   energy *= fes_factor * COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}



double OPLSCoulombFES::energy_coulomb_qmmm (
      DistanceCall GetDistance,
      DistanceCall GetDistance_old,
      int changed_molecule) const
{
   double energy = 0.0;
   for(int pos = 0 ; pos < molecule2atom[0].natoms; pos++) {
      int i = molecule2atom[0].atom_id[pos];
      for (int pos2 = 0; pos2 < molecule2atom[changed_molecule].natoms; pos2++) {
         int j = molecule2atom[changed_molecule].atom_id[pos2];

         double dist = GetDistance (i, j);
         double dist_old = GetDistance_old (i, j);
         if (dist < cutoff_coulomb) {
            double qs = charge[i] * charge[j];
            if (dist_old < cutoff_coulomb) {
               energy += qs * (1.0 / dist - 1.0 / dist_old +
                       cutoff_coulomb_rec_sq * (dist - dist_old));
            } else { // dist < cutoff && dist_old > cutoff
               energy += qs * (1.0 / dist - cutoff_coulomb_rec +
                       cutoff_coulomb_rec_sq * (dist - cutoff_coulomb));
            }
         } else if (dist_old < cutoff_coulomb) { // dist > cutoff && dist_old < cutoff
            double qs = charge[i] * charge[j];   // subtracts old value
            energy -= qs * (1.0 / dist_old - cutoff_coulomb_rec +
                       cutoff_coulomb_rec_sq * (dist_old - cutoff_coulomb));

            // dist < cutoff && dist_old < cutoff --> always 0
         }
      }
   }
   energy *= fes_factor * COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}

