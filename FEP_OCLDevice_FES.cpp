/**
 *
 * File: FEP_OCLDevice_FES.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-10-30
 *
 */

#include "FEP_OCLDevice_FES.h"
#include "FEP_OCLDevice.h"


/*
 * Constructor
 */
FEP_OCLDevice_FES::FEP_OCLDevice_FES (
                        cl::Device device,
                        cl::Context *context,
                        int *ignite_isReady,
                        std::mutex *ignite_mutex,
                        std::condition_variable *ignite_slave,
                        std::condition_variable *ignite_master,
                        int n_pre_steps,
                        int nkernel,
                        int max_chains,
                        System_Periodic* systemA,
                        const OPLS_Periodic* opls,
                        const OPLS_Periodic* oplsB,
                        Run_Periodic* run,
                        _FPOINT_S_ temperature,
                        int ndevices,
                        bool extra_energy_file)
   : FEP_OCLDevice (
         device,
         context,
         ignite_isReady,
         ignite_mutex,
         ignite_slave,
         ignite_master,
         n_pre_steps,
         nkernel,
         max_chains,
         systemA,
         opls,
         oplsB,
         run,
         temperature,
         ndevices,
         extra_energy_file),
   factor_fes_reference (run->fes_factor_reference),
   factor_fes_target (run->fes_factor_target)
{
}




/*
 * Destructor
 */
FEP_OCLDevice_FES::~FEP_OCLDevice_FES ()
{
}




/**
 * Creates kernels, compiles them, sets their ranges, creates chain-shared
 * buffer and sets chain-shared arguments for the kernels.
 */
void FEP_OCLDevice_FES::setup_pmc (std::string kernel_file_closure)
{
   // calls base class implementation
   FEP_OCLDevice::setup_pmc (kernel_file_closure);

   int ncbytes = 0;

   try {

      //
      // constant memory
      //

      //Cutoffs
      buffer_fes_reference = cl::Buffer(*(m_p_context),
            CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
            sizeof (double), // Just one number
            &factor_fes_reference);

      ncbytes += sizeof(double);

      //Cutoffs
      buffer_fes_target = cl::Buffer(*(m_p_context),
            CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
            sizeof (double), // Just one number
            &factor_fes_target);

      ncbytes += sizeof(double);

      // sets additional const argument for the closure kernel
      m_KERNEL_closure->m_kernel.setArg (32, buffer_fes_reference);
      m_KERNEL_closure->m_kernel.setArg (33, buffer_fes_target);


   } catch (const cl::Error &error) {
      std::cout << "FEP_OCLDevice_FES::setup_pmc Error: " << error.what() << "(" << error.err() << ")" << std::endl;
      throw error;
   }

   VERBOSE_LVL_SETUP2 ("[SETU] Device " << m_device_index << " Constant Mem Requested (bytes):" << ncbytes << std::endl);
}



