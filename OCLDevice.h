/**
 *
 * File: OCLDevice.h
 * Author: Sebastiao Miranda, Jonas Feldt
 *
 * Version: 2015-01-06
 *
 */

#ifndef OCLDEVICE_H_INCLUDED
#define OCLDEVICE_H_INCLUDED

#define __CL_ENABLE_EXCEPTIONS

#include <mutex>
#include <thread>
#include <vector>

#include <condition_variable>
#include "options.h"
#include "geom_utils.h"

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

#include "OCLKernel.h"
#include "OCLChain.h"
#include "Charge_Grid.h"
#include "OPLS_Periodic.h"
#include "Run_Periodic.h"
#include "System.h"
#include "math_utils.h"
#include "typedefs.h"

class OCLmanager;

class OCLDevice{
   
public:   
   
   // Keep track of the last device index
   static int lastDevice;
   
   OCLDevice(
             cl::Device device,
             cl::Context *context,
             int *ignite_isReady,
             std::mutex *ignite_mutex,
             std::condition_variable *ignite_slave,
             std::condition_variable *ignite_master,
             #ifdef LEGACY_MULTI_DEVICE
             //Variables for barrier synchronizaton
             //, energy bcast TODO: add performance bcast
             _FPOINT_G_              *a_v_bcast_qmmm_c,
             _FPOINT_S_              *a_v_bcast_mm,
             _FPOINT_S_              *a_v_bcast_qmmm_vdw,
             _FPOINT_S_              *a_v_bcast_qmmm_c_nucl,
                       
             std::mutex              *a_p_barrier_mutex,
             std::condition_variable *a_p_barrier_cond,
             int                     *a_p_barrier_count,
             int                      a_m_barrier_max,
             double                  *m_bal,
             #ifdef BALANCING
             OCLmanager *m_ocl,
             #endif  
             #endif
             int n_pre_steps, 
             int nkernel,
             int max_chains, 
             System_Periodic* system, 
             const OPLS_Periodic* opls, 
             Run_Periodic* run, 
             _FPOINT_S_ temperature,
             int num_devices,
             bool extra_energy_file);
                        
   virtual ~OCLDevice();
      
   //// Device object code
   //
   //
       
   //Create the command queue for this device
   void createQueue();
   
   //Launch the thread for this device management
   void launch_thread();
   void join_thread();
   
   //Add chain to device
   void add_chain(OCLChain *oclchain);   
                       
   //Prepare random number lists
   void prepare_randomness();
   
   //Generate mol2atom struct
   void build_mol2atom0();
   
   //Sets up Kernels and buffers for PMC Cycle
   virtual void setup_pmc(std::string kernel_file_closure);
   
   // Write init values of grids and lattices to gpu
   void first_writes();
    
   // Updates GPU profiler
   void update_profiler(int nevents);
   
   // Print Host-Site profiling
   void print_profiling(); 
   
   // Print GPU profiling   
   void print_gpu_profiling();

   // get final systems
   vs get_final_systems();

   // the final output
   std::vector<ChainResults*> get_results();
 
   //Device thread
   std::thread m_thread;

   
   #ifdef LEGACY_MULTI_DEVICE
   #ifdef BALANCING
   OCLmanager *m_ocl;
      #ifdef BALANCING_PROBE

         double* m_tulock_probe ;    
         double* m_tpmc_probe   ;    
         double* m_prev_offset  ;    

      #endif
   #endif
   #endif
   
   //Pointers to opls and run prameters  
   const OPLS_Periodic  *m_opls ;
   Run_Periodic         *m_run  ;
   
   //Member OpenCL Kernels
   OCLKernel *m_KERNEL_monte_carlo;          //MC step
   OCLKernel *m_KERNEL_qmmm_c;               //Coulomb qmmm (GRID)
   OCLKernel *m_KERNEL_qmmm_c_red_coarse;    //Coarser version of reduction
   OCLKernel *m_KERNEL_mm_vdwc;              //Energy_vdw_mm + energy_coulomb_mm (+ onthefly dists)
   OCLKernel *m_KERNEL_mm_vdwc_red_coarse;   //OCLKernel m_KERNEL_mm_vdwc_coarse_red
   OCLKernel *m_KERNEL_qmmm_tasks;           //energy qmmm_vdw + qmmm_c_nuclei
   OCLKernel *m_KERNEL_closure;              //Decision step
   
   // Command queue for this device
   cl::CommandQueue *m_queue;
   
   // OpenCL device object
   cl::Device m_device;
   
   // Pointer to Context
   cl::Context* m_p_context;
      
   // System pointer do get system constants
   System* m_const_system;
   
   //Sync with chain thread
   //wf molpro launch sync: 
   int                     *v_wf_flag_launch ;
   std::mutex              *v_wf_mutex_launch;
   std::condition_variable *v_wf_slave_launch;
   //wf molpro wait for finish:
   int                     *v_wf_flag_finish ;
   std::mutex              *v_wf_mutex_finish;
   std::condition_variable *v_wf_master_finish;
   
   // Dependencies for next monte caro
   std::vector<cl::Event> m_mc_dependencies;
 
   // Kernel Event vectors
   cl::Event *m_EVENT_monte_carlo;       
   cl::Event *m_EVENT_qmmm_c;          
   cl::Event *m_EVENT_qmmm_c_red_coarse;
   cl::Event *m_EVENT_mm_vdwc;        
   cl::Event *m_EVENT_mm_vdwc_red_coarse;   
   cl::Event *m_EVENT_qmmm_tasks;        
   cl::Event *m_EVENT_closure; 
   
   //Device index
   int m_device_index;
   #ifdef LEGACY_MULTI_DEVICE
      double *m_bal;    
   #endif
   
   #ifdef BASE_PROFILING
      // prove vector to collect global time data
      std::vector<double> v_micro_probe;
   #endif

    //Associated Chains
   std::vector<OCLChain*> m_chains;
   int m_nchains;
   
   int m_maxmoves;

protected:
   
   //// Pmc logic code
   //
   
   //Core pmc management code
   void pmc_core(int chain, int inner_step, int outer_step, int memory_offset);
   
   //Device adjustments (e.g. lists, stepmax, wf launches)
   void pmc_adjustments(int chain, int last_inner_step, int outer_step);
   
   //Main pmc device loop logic (calls pmc core)
   void pmc_loop();
   
   //Ask chain thread for wf update
   void launch_wf_update(int chain);
   
   //wait for wf update
   void wait_wf_update(int chain);
   //non-blocking version
   bool check_wf_update(int chain);

   //Change arguments to switch chain
   virtual void setChainArgs(int chain);
   
   //update device after wf updates
   void update_device(int chain, int inner_step);
   
   //Launches a PMC Cycle step
   virtual void launch_pmc_cycle(int inner_step, int outer_step, int memory_offset);

   //Launch kernel functions
   void launch_mc          (std::vector<cl::Event> *in_deps, cl::Event *out_dep, int step);
   void launch_qmmm_c      (std::vector<cl::Event> *in_deps, OCLKernel *qmmm_c,
         OCLKernel *qmmm_c_reduction, cl::Event *out_dep, cl::Event *qmmm_c_out_dep);
   void launch_mm_vdwc     (std::vector<cl::Event> *in_deps, cl::Event *out_dep, int memory_offset);
   virtual void launch_qmmm_tasks (std::vector<cl::Event> *in_deps, cl::Event *out_dep);
   void launch_closure     (std::vector<cl::Event> *in_deps, cl::Event *out_dep, int step);

   //Host vector for mol2atom structure
   int *m_vect_mol2atom0;

   // number of devices
   int m_ndevices;
   
   //Synchronization variables
   //
   
   //Sync with master thread
   int        *m_ignite_isReady              ;
   std::mutex *m_ignite_mutex                ;
   std::condition_variable *m_ignite_slave   ;
   std::condition_variable *m_ignite_master  ;
     
   //Profiling vars
   #ifdef BASE_PROFILING
      double t_launch_pmc_acc,t_launch_pmc_end,t_launch_pmc_start;
      int    n_launch_pmc;
      double t_save_acc,t_save_end,t_save_start;
      int    n_save;
      double t_lastsave_acc,t_lastsave_end,t_lastsave_start;
      int    n_lastsave;
      double t_update_acc,t_update_end,t_update_start;
      int    n_update;
      double t_get_ref,t_get_ref_end,t_get_ref_start;
      int    n_get_ref;
      double t_wf_adjust,t_wf_adjust_end,t_wf_adjust_start;
      int    n_wf_adjust;
      double t_stepmax,t_stepmax_end,t_stepmax_start;
      int    n_stepmax;
      double t_outer,t_outer_end,t_outer_start;
      int    n_outer;
      double t_pmc_iter_acc,t_pmc_iter_end,t_pmc_iter_start;
      int    n_pmc_iter;
    
      double **t_pmc_chunk      ; 
      double **t_pmc_chunk_end  ; 
      double **t_pmc_chunk_start;
      int    **n_pmc_chunk      ;
   #endif   
      
   int m_config_data_size;
   int m_n_ocl_prof      ; //profile samples so far
   int m_outer_steps     ; // outer steps (number of wfus)
   int m_inner_steps     ; // inner steps (number of wfus)
   int allocate_events;
   double m_theta_max    ;
   int m_wf_adjust       ;
 
   // Opencl device buffers
   // (shared across chains)
                
   //gets first atom of molecule
   cl::Buffer  m_mol2atom0;     

   //atom mass, atomic numbers, some consts
   cl::Buffer m_mass ;     
   cl::Buffer m_atomic_numbers ;     
   cl::Buffer m_charge ;                                                             
   cl::Buffer m_molecule;                              
   cl::Buffer m_n_total_mols ;        
   cl::Buffer m_n_total_atoms ;     
                         
   //Cutoffs
   cl::Buffer m_cutoff_coulomb_rec ;   
   cl::Buffer m_cutoff_coulomb_rec_sq ;
   cl::Buffer m_cutoff_coulomb ;       
   cl::Buffer m_cutoff_vdw ;

   double m_value_cutoff_coulomb;
   double m_value_cutoff_vdw;
   
   bool has_extra_energy_file;
                            
   //OPLS vectors
   cl::Buffer m_epsilon ;              
   cl::Buffer m_sigma ;    
  
   //Other consts
   cl::Buffer m_temperature; 
   cl::Buffer m_update_lists_every; 
  
   /*  Auxiliary variables  */
   
   _FPOINT_S_ m_d_temperature;   
   int m_n_pre_steps;
   
   #ifndef SET_FPOINT_G_DOUBLE
      // float cutoffs for float-qmmmc mode
      float cutoff_coulomb_rec_f   ;
      float cutoff_coulomb_rec_sq_f;
      float cutoff_coulomb_f       ;
      float cutoff_coulomb_sq_f    ;
      float *v_charge_f            ;

      cl::Buffer m_charge_f;
      #ifndef HALF_FIXED
         cl::Buffer m_cutoff_coulomb_rec_f;   
         cl::Buffer m_cutoff_coulomb_rec_sq_f;
         cl::Buffer m_cutoff_coulomb_f; 
      #else
         _FIXED_ cutoff_coulomb_rec_q   ;
         _FIXED_ cutoff_coulomb_rec_sq_q;
         _FIXED_ cutoff_coulomb_q       ;
         _FIXED_ cutoff_coulomb_sq_q    ;

         cl::Buffer m_cutoff_coulomb_rec_q;   
         cl::Buffer m_cutoff_coulomb_rec_sq_q;
         cl::Buffer m_cutoff_coulomb_q;  
         cl::Buffer m_cutoff_coulomb_sq_q;  
      #endif
   #endif

   #ifdef LEGACY_MULTI_DEVICE
      //Variables for barrier synchronizaton
      //, energy bcast TODO: add performance bcast
      _FPOINT_G_              *v_bcast_qmmm_c;
      _FPOINT_S_              *v_bcast_mm;
      _FPOINT_S_              *v_bcast_qmmm_vdw;
      _FPOINT_S_              *v_bcast_qmmm_c_nucl;
      std::mutex              *p_barrier_mutex;
      std::condition_variable *p_barrier_cond;
      int                     *p_barrier_count;
      int                      m_barrier_max;
      int                      m_barrier_index;
      
      cl::Event* m_EVENT_write_part_qmmm_c;
      cl::Event* m_EVENT_read_part_qmmm_c;
      cl::Event* m_EVENT_write_part_minors;
      cl::Event* m_EVENT_read_part_minors;
      /*cl::Event* m_EVENT_write_part_qmmm_c_nucl;
      cl::Event* m_EVENT_read_part_qmmm_c_nucl;
      cl::Event* m_EVENT_write_part_qmmm_vdw;
      cl::Event* m_EVENT_read_part_qmmm_vdw;*/
      
      unsigned long m_PROFL_write_part_qmmm_c;
      unsigned long m_PROFL_read_part_qmmm_c;
      unsigned long m_PROFL_write_part_minors;
      unsigned long m_PROFL_read_part_minors;
      /*unsigned long m_PROFL_write_part_qmmm_c_nucl;
      unsigned long m_PROFL_read_part_qmmm_c_nucl;
      unsigned long m_PROFL_write_part_qmmm_vdw;
      unsigned long m_PROFL_read_part_qmmm_vdw;*/
      unsigned long m_PROFL_barrier_comn;

      #ifdef BASE_PROFILING
         double t_mbarrier_acc,t_mbarrier_end,t_mbarrier_start;
         int    n_mbarrier;
         
         double t_ulock_acc,t_ulock_end,t_ulock_start;
         int    n_ulock;

      #endif

   #endif

   // Variables for kernel profiling 
   unsigned long m_PROFL_monte_carlo;       
   unsigned long m_PROFL_qmmm_c;          
   unsigned long m_PROFL_qmmm_c_red_coarse;
   unsigned long m_PROFL_mm_vdwc;        
   unsigned long m_PROFL_mm_vdwc_red_coarse;   
   unsigned long m_PROFL_qmmm_tasks;        
   unsigned long m_PROFL_closure;    
   unsigned long m_PROFL_pmc_cycle;    

};
#endif //OCLDEVICE_H_INCLUDED
