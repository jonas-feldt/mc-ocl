/*
 * File:   PMC_Periodic_Dual.h
 * Author: Jonas Feldt
 *
 * Version: 2017-02-10
 */

#ifndef PMC_PERIODIC_DUAL_H
#define	PMC_PERIODIC_DUAL_H

#include <string>

#include "typedefs.h"
#include "options.h"

class System_Periodic;
class Run_Periodic;
class OPLS_Periodic;
class Run;
class Pipe_Host;
class Dual_OCLmanager;

/**
 * PMC with periodic boundary conditions and a second density.
 */
class PMC_Periodic_Dual {
public:
   PMC_Periodic_Dual(Run_Periodic *run);
   virtual ~PMC_Periodic_Dual();
   vvs run(vvs &systems,
         Run_Periodic *run,
         OPLS_Periodic *opls,
         std::string traject_file,
         int num_devices,
         int num_chains);
   vvs run(vvs &systems,
         Run_Periodic *run,
         OPLS_Periodic *opls,
         std::string traject_file,
         int num_devices,
         int num_chains,
         double energy_qm_gp);

   double get_energy_qm_gp();

private:
   Run_Periodic *m_run;
   double m_energy_qm_gp;
};

#endif	/* PMC_PERIODIC_DUAL_H */

