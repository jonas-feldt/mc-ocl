/**
 *
 * File: qmmm_c_nfi_optD.cl
 * Author: Sebastiao Miranda
 *
 * Version: 2014-02-25
 *
 */
#pragma OPENCL EXTENSION cl_khr_fp64: enable

#define E2_ANG_TO_KJMOL 1389.354867249


// Those are set from options.h:
//#define N 28 //bits in the fraction
//#define A_N  0xF0000000
//#define N_05 0x08000000
//#define N_1  0x10000000

#define Ne 23 // Number of bits in fraction for polynewton
#define NE_1  0x00800000

typedef long LONG;//should be long! although int might suffice (XXX: Long Long??)
typedef int _FIXED_; // XXX : should be set from options.h

#define FLOAT2FIXED(num_f) (_FIXED_)(num_f * N_1) 
#define FIXED2FLOAT(num_f) ((float)(num_f))/N_1 

#define FIXED2FLOAT_NE(num_f) ((float)(num_f))/NE_1 

#define INV_NE(x) (_FIXED_)((((LONG)NE_1)<<Ne)/(LONG)x)
#define MULT_NE(x,y) (_FIXED_)((((LONG)x)*((LONG)y))>>Ne)
#define N2NE(x) (_FIXED_)(x>>(N-Ne))
               
//Defines for polynewt

#define c0_i   318767    //int c0_i = (int)( 0.038    * (1<<Ne) );
#define c1_i   51816431  //int c1_i = (int)( 6.177    * (1<<Ne) );
#define c2_i   -16554393 //int c2_i = (int)( -63.15   * (1<<(Ne-5)) );
#define c3_i   77882982  //int c3_i = (int)( 297.1    * (1<<(Ne-5)) );
#define half_i 4194304   //int half_i = (int)( 0.5    * (1<<Ne) );

//TODO: Check impact of "inline" in the number of registers
inline _FIXED_ compute_minimal_distance_sq (float rxi, float ryi, float rzi, _FIXED_ rxj_i, _FIXED_ ryj_i, _FIXED_ rzj_i) {

   //Convert float to fixed XXX : Remove after full migration
   int rxi_i = (int)( rxi * N_1);
   int ryi_i = (int)( ryi * N_1);
   int rzi_i = (int)( rzi * N_1);
   
   // int rxj_i = (int)( rxj * N_1);
   // int ryj_i = (int)( ryj * N_1);
   // int rzj_i = (int)( rzj * N_1);

   int dx_i = rxi_i - rxj_i;
   int dy_i = ryi_i - ryj_i;
   int dz_i = rzi_i - rzj_i;

   //equivalent to -=round()
   dx_i -= (dx_i + N_05)&A_N; //>>N)<<N;   
   dy_i -= (dy_i + N_05)&A_N; //>>N)<<N;   
   dz_i -= (dz_i + N_05)&A_N; //>>N)<<N;   
   
   int dx_sq_i =  ( (LONG)dx_i * (LONG)dx_i )>>N;
   int dy_sq_i =  ( (LONG)dy_i * (LONG)dy_i )>>N;
   int dz_sq_i =  ( (LONG)dz_i * (LONG)dz_i )>>N;
       
   return dx_sq_i+dy_sq_i+dz_sq_i;
}

inline float polynewton(_FIXED_ x_i){

   //convert from QN to QNe
   x_i = x_i >> (N-Ne);

   //compute polyfit factors
   
   int c3x_i = (int)(( ((LONG)c3_i )*((LONG)x_i) )>>(Ne-5)); 
   c3x_i     = (int)(( ((LONG)c3x_i)*((LONG)x_i) )>>Ne);
   c3x_i     = (int)(( ((LONG)c3x_i)*((LONG)x_i) )>>Ne);
                                       
   int c2x_i = (int)(( ((LONG)c2_i )*((LONG)x_i) )>>(Ne-5)); 
   c2x_i     = (int)(( ((LONG)c2x_i)*((LONG)x_i) )>>Ne);
                                       
   int c1x_i = (int)(( ((LONG)c1_i )*((LONG)x_i) )>>Ne); 
    
   int res_i = c3x_i + c2x_i + c1x_i + c0_i;
   
   //do some newton-raphson method iterations
   res_i = (int)((((LONG)half_i) * (LONG)(res_i + (int) ((((LONG)x_i)<<Ne)/(LONG)res_i)))>>Ne) ;
   res_i = (int)((((LONG)half_i) * (LONG)(res_i + (int) ((((LONG)x_i)<<Ne)/(LONG)res_i)))>>Ne) ;
   res_i = (int)((((LONG)half_i) * (LONG)(res_i + (int) ((((LONG)x_i)<<Ne)/(LONG)res_i)))>>Ne) ;
   //res_i = (int)((((LONG)half_i) * (LONG)(res_i + (int) ((((LONG)x_i)<<Ne)/(LONG)res_i)))>>Ne) ;
   //res_i = (int)((((LONG)half_i) * (LONG)(res_i + (int) ((((LONG)x_i)<<Ne)/(LONG)res_i)))>>Ne) ;
   //res_i = (int)((((LONG)half_i) * (LONG)(res_i + (int) ((((LONG)x_i)<<Ne)/(LONG)res_i)))>>Ne) ;
   //res_i = (int)((((LONG)half_i) * (LONG)(res_i + (int) ((((LONG)x_i)<<Ne)/(LONG)res_i)))>>Ne) ;
    
   return res_i;//((float)(res_i))/(1<<Ne);

}

__kernel
void qmmm_c(

   // Local Memory
   __local float *scratch,

   // Kernel Scalar Arguments
   // (number of atoms for this work-item)
   int m_grid_points,
   
   // MM molecule old x,y,z
   __global float *old_x,
   __global float *old_y,
   __global float *old_z,
   __global float *charges,

    // Changed MM molecule
   __global int      *changed_molecule,
   __global int      *changed_molecule_size,
   __global float    *ch_x,
   __global float    *ch_y,
   __global float    *ch_z,
   
   // Global buffers with GRID data
   __global _FIXED_   *grid_rx,
   __global _FIXED_   *grid_ry,
   __global _FIXED_   *grid_rz,
   __global float   *grid_charge,
   __global int     *grid_size,
   
   // Work Group energy result
   __global float   *result_wg_energy,
   
   // Run constants
   __constant _FIXED_ *cutoff_coulomb_rec_sq,
   __constant _FIXED_ *cutoff_coulomb_rec,
   __constant _FIXED_ *cutoff,

   // System Constants
   __constant int   *mol2atom0, 
   
   // Squared cutoff for faster branching
   __constant _FIXED_ *cutoff_sq
   ){
   
   __private int    k                 ;
   __private int    global_id         ;
   __private int    local_id          ;
   __private int    local_size        ; 
   __private int    grid_point        ;

   __private _FIXED_  dist_new        ;
   __private _FIXED_  dist_old        ;  
   __private float  dist_new_r        ;
   __private float  dist_old_r        ;   
   __private float  energy = 0.0      ;
   __private float  qs                ;
   __private _FIXED_  this_grid_rx      ;
   __private _FIXED_  this_grid_ry      ;
   __private _FIXED_  this_grid_rz      ; 
   __private float  this_grid_charge  ;
   __private _FIXED_  this_cutoff       ;
   
   __private float  this_ch_old_rx    ;
   __private float  this_ch_old_ry    ;
   __private float  this_ch_old_rz    ;
   __private float  this_ch_old_charge;
    
   // Molecule atoms
   __private int start_atom_i          ;
   __private int end_atom              ;
   
   // Get id's
   local_id  = get_local_id(0);
   global_id  = get_global_id(0);
   local_size = get_local_size(0);
   
   // If this is a valid work-item
   if(global_id < (*grid_size)/m_grid_points){

      // Find Out atoms belonging to the molecule
      start_atom_i = mol2atom0[*changed_molecule]; 

      //For each grid point of this work-item
      for(int i=0;i<m_grid_points;i++){
    
         grid_point = global_id*m_grid_points+i;
         //collect grid data to private memory
         this_grid_rx    = grid_rx    [grid_point];
         this_grid_ry    = grid_ry    [grid_point];
         this_grid_rz    = grid_rz    [grid_point];
         this_grid_charge= grid_charge[grid_point];
       
         this_cutoff     = *cutoff_sq;
         
         // Loop through each changed atom in molecule
         for(int j=0;j<*changed_molecule_size;j++){   
     
            this_ch_old_rx       = old_x    [start_atom_i+j]; 
            this_ch_old_ry       = old_y    [start_atom_i+j]; 
            this_ch_old_rz       = old_z    [start_atom_i+j];
            this_ch_old_charge   = charges  [start_atom_i+j];
            
            dist_new = compute_minimal_distance_sq(ch_x[j],     ch_y[j],     ch_z[j],
                                                   this_grid_rx,this_grid_ry,this_grid_rz);
                                        
            dist_old = compute_minimal_distance_sq(this_ch_old_rx, this_ch_old_ry, this_ch_old_rz,
                                                   this_grid_rx,this_grid_ry,this_grid_rz);
                       
            if (dist_new < this_cutoff && dist_old < this_cutoff) {
               
               _FIXED_ dist_new_sqrt = polynewton(dist_new);
               _FIXED_ dist_old_sqrt = polynewton(dist_old);
               
               qs = -this_ch_old_charge*this_grid_charge;
                   
               float factor = FIXED2FLOAT_NE((INV_NE(dist_new_sqrt) - INV_NE(dist_old_sqrt))+  
                     MULT_NE(N2NE(*cutoff_coulomb_rec_sq),(dist_new_sqrt - dist_old_sqrt)));
               energy += qs * factor;
               
            }else if (dist_new < this_cutoff && dist_old >= this_cutoff) {     
               
               _FIXED_ dist_new_sqrt = polynewton(dist_new);
               _FIXED_ dist_old_sqrt = polynewton(dist_old);
               
               qs = -this_ch_old_charge*this_grid_charge;
               
               float factor = FIXED2FLOAT_NE((INV_NE(dist_new_sqrt) - N2NE(*cutoff_coulomb_rec))+  
                     MULT_NE(N2NE(*cutoff_coulomb_rec_sq),(dist_new_sqrt - N2NE(*cutoff))));
                     
               energy += qs * factor;
              
            }else if (dist_old < this_cutoff) { // dist_new >= cutoff && dist_old < cutoff
             
               _FIXED_ dist_new_sqrt = polynewton(dist_new);
               _FIXED_ dist_old_sqrt = polynewton(dist_old);
               
               qs = -this_ch_old_charge*this_grid_charge;
               
               float factor = FIXED2FLOAT_NE((INV_NE(dist_old_sqrt) - N2NE(*cutoff_coulomb_rec))+  
                     MULT_NE(N2NE(*cutoff_coulomb_rec_sq),(dist_old_sqrt - N2NE(*cutoff))));
                     
               energy += qs * factor;

            }
         }
         // Take grid charge into account   
      }
   }//else, wont contribute to reduction: energy is 0 by default.
     
   //Write private result to local memory
   scratch[local_id] = energy*E2_ANG_TO_KJMOL;
   
   barrier(CLK_LOCAL_MEM_FENCE);
   
   //Now all work-items of the same work group must accumulate their energy.
   for(int offset = local_size/2; offset > 0 ;offset >>= 1){
      if(local_id < offset){
         scratch[local_id] = scratch[local_id + offset] + scratch[local_id];
      }
      barrier(CLK_LOCAL_MEM_FENCE);
   }

   //Work item with id 0 writes the final result of the work-group
   if (local_id == 0) {
      result_wg_energy[get_group_id(0)] = scratch[0];
   } 
   
}
	
