/**
 *
 * File: OCLmanager_FES.h
 * Author: Jonas Feldt
 *
 * Version: 2015-10-05
 *
 */

#ifndef OCLMANAGER_FES_H_INCLUDED
#define OCLMANAGER_FES_H_INCLUDED

#include <string>

#include "options.h"
#include "typedefs.h"
#include "OCLmanager.h"

class System_Periodic;
class OPLS_Periodic;
class Pipe_Host;
class Charge_Grid;
class OCLDevice;

class OCLmanager_FES : public OCLmanager {

public:

   OCLmanager_FES (
               int n_pre_steps,
               int nkernel,
               int ndevices,
               int nchains,
               const vvs &systems,
               const OPLS_Periodic* opls,
               Run_Periodic* run,
               vvg &grid,
               int config_data_size,
               int energy_data_size,
               _FPOINT_S_ temperature,
               double theta_max,
               double stepmax,
               const vvd &energies_qm,
               _FPOINT_S_ energy_qm_gp,
               const vvd &energies_mm,
               const vvd &energies_vdw_qmmm,
               const vvd &energies_old,
               Pipe_Host **hosts,
               std::string basename);

   virtual ~OCLmanager_FES() override;

protected:

   virtual OCLDevice *create_device (const int i) override;

};

#endif //OCLMANAGER_H_INCLUDED
