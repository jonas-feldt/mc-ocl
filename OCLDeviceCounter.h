/**
 *
 * File: OCLDeviceCounter.h
 * Author: Jonas Feldt
 *
 * Version: 2016-12-20
 *
 */
 
#ifndef OCLDEVICECOUNTERH_INCLUDED
#define OCLDEVICECOUNTERH_INCLUDED

class Run;

namespace OCLDeviceCounter {

   int count_devices(Run *run);

}

#endif
