/**
 *
 * File: OCLChain.h
 * Author: Sebastiao Miranda, Jonas Feldt
 *
 * Version: 2015-03-11
 *
 */

#ifndef OCLCHAIN_H_INCLUDED
#define OCLCHAIN_H_INCLUDED

#define __CL_ENABLE_EXCEPTIONS
#include <string>
#include <iostream>
#include <fstream>
#include <thread>
#include <math.h>
#include <mutex>
#include <vector>
#include <condition_variable>
#include <atomic>
#include <random>


#include "options.h"
#include "System_Periodic.h"
#include "Charge_Grid.h"
#include "math_utils.h"
#include "ChainResults.h"
#include "typedefs.h"

#include "Pipe_Host.h"

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include <CL/cl.hpp>
#endif

#define MEAN_ENERGY        0 // indices for the means
#define MEAN_INTERACTION   1

class OCLDevice;

class OCLChain{

public:

   // Keep track of the last chain index
   static int lastChain;

   OCLChain(   System_Periodic *system,
               Charge_Grid *grid,
               Pipe_Host *host,
               _FPOINT_S_ energy_qm,
               _FPOINT_S_ energy_qm_gp,
               _FPOINT_S_ energy_mm,
               _FPOINT_S_ energy_vdw_qmmm,
               _FPOINT_S_ energy_old,
               int n_pre_steps,
               int n_config_data_size,
               int n_energy_data_size,
               int seed,
               double stepmax,
               std::string base_name,
               int num_means,
               int n_energy_save_params,
               std::string print_s,
               bool extra_energy_file,
               bool has_random_state,
               std::string state_file,
               const double dimension_box);

   virtual ~OCLChain();

   //Launch chain thread
   void launch_thread();
   void join_thread();

   //Associate device to chain
   void assoc_device(OCLDevice *device, int chain_index);

   //Setup buffers for chain
   virtual void setup_buffers();

   //Prepare randomness for chain
   void prepare_randomness(double theta_max);

   //Write grid into the device's chain buffers
   virtual void write_grid(int inner_step, bool reduce,
         double dimension_box, double cutoff_coulomb);

   //Write init energies
   virtual void write_init_energies();

   //Write system xyz to device's buffer
   virtual void write_system_lattice();

   //Get reference system respecting to this chain
   void get_reference_system(int inner_step);

   //Get curr number of accepted steps
   int get_current_number_accepted(int inner_step);

   //Update device's random lists
   void update_lists(double theta_max, int inner_step);

   //Update reference energy
   virtual void update_reference_energy(int inner_step);

   //Save accepted system
   void save_acc_systems(int inner_step, int n_reads, int n_reads_energy);

   //Print Pipe Profiling
   void print_profiling();

   //Print last outputs
   virtual ChainResults *get_results (const int steps);

   void save_random_state (std::string file);
   void load_random_state (std::string file, random_engine &engine);

   // Chain thread
   std::thread m_thread;

   // Pointers to host side objects
   System_Periodic   *m_system   ;
   Charge_Grid       *m_grid     ;
   Pipe_Host         *m_host     ;
   OCLDevice         *m_device   ;

   // Variables for controling Kernel parameters
   int m_qmmm_c_grain;
   int m_qmmm_c_localsize;

   // Will point to m_device's queue
   cl::CommandQueue  *m_queue    ;

   // Variabes for IO
   std::string m_trajectory;
   FILE *m_energy_file;
   FILE *m_extra_energy_file;
   bool has_extra_energy_file;
   FILE *m_output_file;
   std::string m_output_string;

   double m_stepmax;
   int m_n_pre_steps;
   int m_chain_index;

   // Profiling vars
   #ifdef BASE_PROFILING
   double t_wlattice;
   double t_wlattice_end;
   double t_wlattice_start;
   int    n_wlattice;
   double t_getgrid;
   double t_getgrid_end;
   double t_getgrid_start;
   int    n_getgrid;
   // prove vector to collect global time data
   std::vector<double> v_micro_probe;
   #endif

   //Buffers that refer only to one chain
   //

   //Reference system data
   cl::Buffer m_old_x ;
   cl::Buffer m_old_y ;
   cl::Buffer m_old_z ;

   #ifndef SET_FPOINT_G_DOUBLE
      cl::Buffer m_old_x_f ;
      cl::Buffer m_old_y_f ;
      cl::Buffer m_old_z_f ;
   #endif

   //Changed Mol data
   cl::Buffer m_changed_molecule ;
   cl::Buffer m_changed_molecule_size ;
   cl::Buffer m_ch_x ;
   cl::Buffer m_ch_y ;
   cl::Buffer m_ch_z ;

   #ifndef SET_FPOINT_G_DOUBLE
     cl::Buffer m_ch_x_f ;
     cl::Buffer m_ch_y_f ;
     cl::Buffer m_ch_z_f ;
   #endif

   //Grid data
   cl::Buffer m_grid_rx ;
   cl::Buffer m_grid_ry ;
   cl::Buffer m_grid_rz ;
   cl::Buffer m_grid_charge ;
   cl::Buffer m_grid_size ;
   cl::Buffer m_sqrt_box_dim ;

   //buffers for energies
   cl::Buffer m_mm_vdwc_energy_col;//reduction buffer
   cl::Buffer m_mm_vdwc_energy_vdw;//reduction buffer
   cl::Buffer m_qmmm_c_energy;
   cl::Buffer m_energy_vdw_qmmm;
   cl::Buffer m_energy_mm;

   cl::Buffer m_minors_energy;
   //cl::Buffer m_mm_vdwc_energy; XXX why commented? 
   //^^^this also include also includes qmmm tasks energy output

   //Host provided energies
   cl::Buffer m_qm_energy; // Also, updated by OpenCL device
   cl::Buffer m_energy_qm_gp;

   //Persistent OpenCL Device data
   cl::Buffer m_energy_old;
   cl::Buffer m_number_accepted;
   cl::Buffer m_accepted_config_data;
   cl::Buffer m_accepted_energy_data;

   //Mean and variance computation
   cl::Buffer m_old_means;
   cl::Buffer m_old_variance;

   // Random Lists
   cl::Buffer m_random_molecule     ;
   cl::Buffer m_rand_rotate_theta   ;
   cl::Buffer m_q0                  ;
   cl::Buffer m_q1                  ;
   cl::Buffer m_q2                  ;
   cl::Buffer m_q3                  ;
   cl::Buffer m_random_trans_num    ;
   cl::Buffer m_rand_trans_ex       ;
   cl::Buffer m_rand_trans_ey       ;
   cl::Buffer m_rand_trans_ez       ;
   cl::Buffer m_random_boltzman_acc ;

   int max_mol_atom;


protected:

   // Pointers to m_device's respective sync variables

   int                     *m_wf_flag_launch ;
   std::mutex              *m_wf_mutex_launch;
   std::condition_variable *m_wf_slave_launch;
   //wf molpro wait for finish:
   int                     *m_wf_flag_finish ;
   std::mutex              *m_wf_mutex_finish;
   std::condition_variable *m_wf_master_finish;

   // Code for chain loop
   virtual void chain_loop();
   virtual void get_means(int dim, double *means, double *variances);

   //vectors for holding float MM reference
   #ifndef SET_FPOINT_G_DOUBLE
      float* v_rx_f;
      float* v_ry_f;
      float* v_rz_f;
   #endif

   // Host side energy variables respecting to this chain
   // (host side _FPOINT_S_'s)
   _FPOINT_S_ m_f_energy_qm        ;
   _FPOINT_S_ m_f_energy_qm_gp     ;
   _FPOINT_S_ m_f_energy_mm        ;
   _FPOINT_S_ m_f_energy_vdw_qmmm  ;
   _FPOINT_S_ m_f_energy_old       ;

   // Buffer to collect accepted systems
   // XXX : This data type is the same used for
   //       collecting printed energies
   _FPOINT_S_ *m_acc_vector;
   _FPOINT_S_ *m_acc_energies;
   int m_config_data_size;
   int m_energy_data_size;

   // For random generation
   random_engine rnd_engine;

   // Random Number lists for pre generation
   //
   int        *m_v_random_molecule     ;
   _FPOINT_S_ *m_v_rand_rotate_theta   ;
   _FPOINT_S_ *m_v_q0                  ;
   _FPOINT_S_ *m_v_q1                  ;
   _FPOINT_S_ *m_v_q2                  ;
   _FPOINT_S_ *m_v_q3                  ;
   _FPOINT_S_ *m_v_random_trans_num    ;
   _FPOINT_S_ *m_v_rand_trans_ex       ;
   _FPOINT_S_ *m_v_rand_trans_ey       ;
   _FPOINT_S_ *m_v_rand_trans_ez       ;
   _FPOINT_S_ *m_v_random_boltzman_acc ;

   // Events
   //

   // Buffer Events
   cl::Event m_EVENT_old_x;
   cl::Event m_EVENT_old_y;
   cl::Event m_EVENT_old_z;

   // Global Grid Buffers Write
   cl::Event m_EVENT_grid_rx;
   cl::Event m_EVENT_grid_ry;
   cl::Event m_EVENT_grid_rz;
   cl::Event m_EVENT_grid_charge;
   cl::Event m_EVENT_grid_size;
   cl::Event m_EVENT_sqrt_box_dim;

   // Energy qm
   cl::Event m_EVENT_qm_energy;
   cl::Event m_EVENT_energy_qm_gp;
   cl::Event m_EVENT_energy_vdw_qmmm;
   cl::Event m_EVENT_energy_mm;
   cl::Event m_EVENT_energy_old;
   cl::Event m_EVENT_get_energy_vdw_qmmm;
   cl::Event m_EVENT_get_energy_mm;

   // Reading confin data
   cl::Event m_EVENT_read_config_data;
   cl::Event m_EVENT_read_ref_x;
   cl::Event m_EVENT_read_ref_y;
   cl::Event m_EVENT_read_ref_z;
   cl::Event m_EVENT_read_number_accepted;

   // Refresh random list events
   cl::Event m_EVENT_random_molecule     ;
   cl::Event m_EVENT_rand_rotate_theta   ;
   cl::Event m_EVENT_q0                  ;
   cl::Event m_EVENT_q1                  ;
   cl::Event m_EVENT_q2                  ;
   cl::Event m_EVENT_q3                  ;
   cl::Event m_EVENT_random_trans_num    ;
   cl::Event m_EVENT_rand_trans_ex       ;
   cl::Event m_EVENT_rand_trans_ey       ;
   cl::Event m_EVENT_rand_trans_ez       ;
   cl::Event m_EVENT_random_boltzman_acc ;

   int m_curr_acc;
   int num_means; // number of mean values
   int m_n_energy_save_params;
   std::string m_print_s;
   
   const double m_dimension_box;
};

#endif //OCLCHAIN_H_INCLUDED

