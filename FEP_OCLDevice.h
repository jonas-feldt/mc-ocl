/**
 *
 * File: FEP_OCLDevice.h
 * Author: Sebastiao Miranda, Jonas Feldt
 *
 * Version: 2015-04-13
 *
 */

#ifndef FEP_OCLDEVICE_H_INCLUDED
#define FEP_OCLDEVICE_H_INCLUDED

#define __CL_ENABLE_EXCEPTIONS

#include <vector>

#include <condition_variable>

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

#include "OCLDevice.h"

class System_Periodic;
class OPLS_Periodic;
class Run_Periodic;
class OCl_Kernel;


class FEP_OCLDevice : public OCLDevice {

public:

   FEP_OCLDevice(
             cl::Device device,
             cl::Context *context,
             int *ignite_isReady,
             std::mutex *ignite_mutex,
             std::condition_variable *ignite_slave,
             std::condition_variable *ignite_master,
             int n_pre_steps,
             int nkernel,
             int max_chains,
             System_Periodic* systemA,
             const OPLS_Periodic* opls,
             const OPLS_Periodic* oplsB,
             Run_Periodic* run,
             _FPOINT_S_ temperature,
             int ndevices,
             bool extra_energy_file);

   virtual ~FEP_OCLDevice();

   //// Device object code

   //Sets up Kernels and buffers for PMC Cycle
   virtual void setup_pmc(std::string kernel_file_closure) override;

   const OPLS_Periodic *m_oplsB;

   //Member OpenCL Kernels
   OCLKernel *m_KERNEL_qmmm_cB;               //Coulomb qmmm (GRID)
   OCLKernel *m_KERNEL_qmmm_c_red_coarseB;    //Coarser version of reduction

   // Kernel Event vectors
   cl::Event *m_EVENT_qmmm_cB;
   cl::Event *m_EVENT_qmmm_c_red_coarseB;

protected:

   //// Pmc logic code

   //Change arguments to switch chain
   virtual void setChainArgs(int chain);

   //Launches a PMC Cycle step
   virtual void launch_pmc_cycle(int inner_step, int outer_step, int memory_offset);

   //Launch kernel functions
   virtual void launch_qmmm_tasks  (std::vector<cl::Event> *in_deps,cl::Event *out_dep);


   // OPLS vectors for system B
   cl::Buffer m_epsilonB;
   cl::Buffer m_sigmaB;
};

#endif //FEP_OCLDEVICE_H_INCLUDED
