/*
 * File:   FEP_Periodic_FES.h
 * Author: Jonas Feldt
 *
 * Version: 2015-10-30
 */

#ifndef FEP_PERIODIC_FES_H
#define	FEP_PERIODIC_FES_H

#include <string>

#include "FEP_Periodic.h"
#include "typedefs.h"
#include "options.h"

class FEP_OCLmanager;
class System_Periodic;
class OPLS_Periodic;
class Pipe_Host;


/**
 * Free Energy Pertubation theory which uses PMC with periodic boundary
 * conditions...
 *
 * TODO
 */
class FEP_Periodic_FES : public FEP_Periodic {

public:

   using FEP_Periodic::FEP_Periodic;
   virtual ~FEP_Periodic_FES() override;

protected:

   virtual FEP_OCLmanager *create_manager(
               int n_pre_steps, 
               int nkernel, 
               int ndevices, 
               int nchains, 
               const vvs &systemsA,
               System_Periodic* systemB,
               OPLS_Periodic* opls, 
               OPLS_Periodic* oplsB,
               Run_Periodic* run, 
               vvg &gridsA,
               vvg &gridsB,
               int config_data_size, 
               int energy_data_size, 
               _FPOINT_S_ temperature,
               double theta_max,
               double stepmax,   
               vvd &energies_qmA,
               vvd &energies_qmB,
               _FPOINT_S_ energy_qm_gp,
               vvd &energies_mmA,
               vvd &energies_vdw_qmmmA,
               vvd &energies_vdw_qmmmB,
               vvd &energies_oldA,
               vvd &energies_oldB,
               Pipe_Host **hosts,
               std::string basename) override;

};

#endif	/* FEP_PERIODIC_FES_H */

