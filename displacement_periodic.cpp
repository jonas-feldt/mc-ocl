/*
 * File:   displacement_periodic.cpp
 * Author: Jonas Feldt
 *
 * Version: 2017-08-14
 */

#include <string>
#include <cmath>
#include <cstdio>
#include <iostream>

#include "System_Periodic.h"
#include "TrajectoryAnalyzerPeriodic.h"
#include "displacement_periodic.h"
#include "geom_utils.h"
#include "io_utils.h"


DisplacementPeriodic::DisplacementPeriodic (
     std::string trajectory_file, int first_frame, int last_frame,
     std::string output_file, const double box_dim, const unsigned int
     num_chains)
   : TrajectoryAnalyzerPeriodic (trajectory_file, first_frame, last_frame,
         output_file, box_dim, num_chains)
{
   result = new double[steps_file];
}


DisplacementPeriodic::~DisplacementPeriodic ()
{
   delete[] result;
   delete[] refx;
   delete[] refy;
   delete[] refz;
}




void DisplacementPeriodic::analyze ()
{
   const int nmols = system->nmol_total;

   // creates center of masses for the first frame, sets result to 0
   if (not has_ref) { 

      refx = new double[nmols];
      refy = new double[nmols];
      refz = new double[nmols];

      for (int i = 0; i < nmols; ++i) {
         center_mass (i, system, refx[i], refy[i], refz[i]);
      }
      has_ref = true;
      result[step] = 0.0;

   // further steps
   } else {

      double d = 0.0;

      double x, y, z;
      for (int i = 0; i < nmols; ++i) {
         center_mass (i, system, x, y, z);
         d += std::pow (compute_minimal_distance (x, y, z, refx[i], refy[i], refz[i]) * box_dim, 2);
      }

      d /= (double) nmols;
      result[step] = d;
   }

   step++;
}



void DisplacementPeriodic::finalize ()
{
   // nothing to do here
}



void DisplacementPeriodic::save_output ()
{
   if (file_exists (output_file)) {
      std::cerr << "Output file already exists: " << output_file << std::endl;
      return;
   }

   FILE *out = fopen (output_file.c_str(), "w");
   fprintf (out, "%10s %10s\n", "step", "r2");
   for (int i = 0; i < steps_file; ++i) {
      fprintf (out, "%10i %10.3f\n", i, result[i]);
   }
   fclose (out);
}



