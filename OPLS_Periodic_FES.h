/*
 * File:   OPLS_Periodic_FES.h
 * Author: Jonas Feldt
 *
 * Version: 2015-10-29
 */

#ifndef OPLS_PERIODIC_FES_H
#define	OPLS_PERIODIC_FES_H

#include <vector>

#include "OPLS_Periodic.h"

class OPLS_Periodic_FES : public OPLS_Periodic {
public:

   OPLS_Periodic_FES (
         const System *system,
         Run_Periodic *run,
         const double factor_fes);
   virtual const double* get_normal_charges() const override;

private:
   std::vector<double> scaled_charges;
};

#endif	/* OPLS_PERIODIC_FES_H */

