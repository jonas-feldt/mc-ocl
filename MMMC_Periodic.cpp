/*
 * File:   MMMC_Periodic.cpp
 * Author: Jonas Feldt
 * 
 * Version: 2015-11-30
 */

#include <iostream>
#include <future>
#include <random>
#include <string>
#include <fstream>
#include <cmath>
#include <functional>

#include "MMMC_Periodic.h"
#include "jonas_io_utils.h"
#include "System_Periodic.h"
#include "Run_Periodic.h"
#include "OPLS_Periodic.h"
#include "ChainResults.h"
#include "geom_utils.h"
#include "typedefs.h"
#include "Swap_Stepper.h"
#include "Rosenbluth_Swap_Stepper.h"
#include "Rotate_Translate_Stepper.h"

#include "options.h" // Header with all run options.


/**
 * Constructor
 */
MMMC_Periodic::MMMC_Periodic (
      const std::string m_base_name,
      const Run_Periodic *run,
      const OPLS_Periodic *opls,
      const int num_devices,
      const int num_chains)
 : m_base_name (m_base_name),
   opls (opls),
   num_devices (num_devices),
   num_chains (num_chains)
{
   // works on a copy to not modify the original run object
   m_run = new Run_Periodic(*run);
}



/**
 * Destructor
 */
MMMC_Periodic::~MMMC_Periodic () {
   delete m_run;
}





/**
 * Runs the MM MC simulation.
 *
 * @param systems
 *
 * @return a vector of vectors of the final systems (=systems[num_devices][num_chains])
 *
 */
vvs MMMC_Periodic::run (vvs &systems)
{
   vvd energies_old (systems.size(), vd(systems.begin()->size()));

   for (unsigned int i = 0; i < m_run->num_devices; ++i) {
      for (unsigned int j = 0; j < m_run->num_chains; ++j) {
         System_Periodic *system = systems[i][j];
         energies_old[i][j] = opls->energy (system->GetDistance);
         VERBOSE_LVL_INIT("\tMM energy: " << energies_old[i][j] << std::endl);
      }
   }

   return run (systems, energies_old);
}




/**
 * Runs the MM MC simulation.
 *
 * @param system
 *
 * @return a vector of vectors of the final systems (=systems[n_device][n_chain])
 */
vvs MMMC_Periodic::run (System_Periodic *system)
{

   // computes initial MM
   double energy_old = opls->energy (system->GetDistance);

   VERBOSE_LVL_INIT("[INIT] System MM atoms=" << system->natom << std::endl);    
   VERBOSE_LVL_INIT("\tMM energy: " << energy_old << std::endl);

   // prepares data for multi-chain jobs, has always 1 device
   vvd energies_old (num_devices, vd(num_chains)); // total energies of the last step
   vvs systems;

   for (unsigned int i = 0; i < m_run->num_devices; ++i) {
      vs tmpS;
      tmpS.reserve(m_run->num_chains);
      for (unsigned int j = 0; j < m_run->num_chains; ++j) {
         tmpS.push_back(system);
         energies_old[i][j] = energy_old;
      }
      systems.push_back(tmpS);
   }

   return run (systems, energies_old);
}




/**
 * Runs the PMC simulation.
 *
 * @param system
 * @param energies_old
 *
 * @return a vector of vectors of the final systems (=systems[num_devices][num_chains])
 */
vvs MMMC_Periodic::run (
      vvs &systems, 
      const vvd &energies_old)
{
   // Adjust maxmoves according to number of chains
   m_run->maxmoves = m_run->maxmoves / (num_devices * num_chains);

   VERBOSE_LVL_WARN("[INIT] Set Maxmoves per Chain: " << m_run->maxmoves << std::endl);
   VERBOSE_LVL_INIT("[INIT] Making directory " << m_base_name << "..."<< std::endl);
   make_directory_jf (m_base_name);

   // seeds PRNG
   random_engine engine;
   std::uniform_int_distribution<int> dist (
         std::numeric_limits<int>::min(),
         std::numeric_limits<int>::max());
   if (not m_run->has_seed_path) engine.seed(m_run->seed);

   // Starts num_devices * num_chains tasks
   std::vector<std::vector<std::future<std::tuple<ChainResults*, System_Periodic*>>>> futures;
   futures.reserve(systems.size());
   int index = 0;
   for (unsigned int i = 0; i < systems.size(); ++i) {

      std::vector<std::future<std::tuple<ChainResults*, System_Periodic*>>> tmp;
      tmp.reserve(systems.begin()->size());
      for (unsigned int j = 0; j < systems.begin()->size(); ++j) {

         int seed = 0;
         if (not m_run->has_seed_path) seed = dist(engine);

         std::packaged_task<std::tuple<ChainResults*, System_Periodic*> ()> task(std::bind(
                &MMMC_Periodic::run_chain, this, systems[i][j], index, seed,
                m_base_name, energies_old[i][j]));
         tmp.push_back(task.get_future());
         std::thread(move(task)).detach();
         index++;
      }
      futures.push_back(std::move(tmp));
   }

   // collects results
   vvs final_systems (m_run->num_devices, vs(m_run->num_chains));
   std::vector<ChainResults*> results;
   for (unsigned int i = 0; i < systems.size(); ++i) {
      for (unsigned int j = 0; j < systems.begin()->size(); ++j) {
         std::tuple<ChainResults*, System_Periodic*> result = futures[i][j].get();
         results.push_back (std::get<0> (result));
         final_systems[i][j] = std::get<1> (result); 
      }
   }

   ChainResults::print_results (m_base_name + ".out", results);

   for (ChainResults *result : results) delete result;

   return final_systems;
}




std::tuple<ChainResults *, System_Periodic*> MMMC_Periodic::run_chain(
      System_Periodic *system_start,
      const int chain_id,
      const int seed,
      std::string basename,
      double energy_old)
{

   // TODO check not to overwrite

   // Prints the header for the energy file
   FILE *energy_file = nullptr;
   FILE *exen_file = nullptr;
   std::string xyz_file;
   if (m_run->print_every != 0) {
      energy_file = std::fopen (
            (m_base_name + "-energy-" + std::to_string (chain_id) + ".out").c_str(), "w");
      fprintf (energy_file, "%10s %10s %20s %10s %20s %10s\n",
           "Steps", "Accepted", "E_mm", "dE_mm", "mE_mm", "sE_mm"); 
      xyz_file = m_base_name + "-" + std::to_string (chain_id) + ".xyz";
   }
   if (m_run->print_energy != 0) {
      exen_file = std::fopen (
            (m_base_name + "-exen-" + std::to_string (chain_id) + ".out").c_str(), "w");
      fprintf (exen_file, "%10s %10s %20s %10s %20s %10s\n",
           "Steps", "Accepted", "E_mm", "dE_mm", "mE_mm", "sE_mm"); 
   }

   // Inits systems for the simulation
   System_Periodic *system_old = new System_Periodic (*system_start);
   System_Periodic *system     = new System_Periodic (*system_start);

   // Inits variables
   double e_old         = energy_old;
   double old_mean      = 0.0;
   double old_variance  = 0.0;
   int accepted_steps   = 0;

   // Inits random engine
   random_engine rnd_engine;
   if (m_run->has_seed_path) {
      std::string file = m_run->seed_path + "/" + std::to_string(chain_id) + ".random";
      VERBOSE_LVL_INIT("[INIT] reading file " << file << std::endl);
      std::ifstream ifile (file, std::ios::binary);
      ifile >> rnd_engine;
      VERBOSE_LVL_INIT("[INIT] loading random state " << rnd_engine() << std::endl);
   } else {
      rnd_engine.seed(seed);
   }

   // TODO first molecule is still frozen
   
   auto swapper = Rosenbluth_Swap_Stepper {&rnd_engine, system, m_run->rosenbluth_trials, m_run};
   auto stepper = Rotate_Translate_Stepper {&rnd_engine, system, m_run};

   // MM MC steps
   unsigned int step;
   for (step = 0; step < m_run->maxmoves; ++step) {

      double de;
      if (m_run->rosenbluth_steps != 0 && (step % m_run->rosenbluth_steps) == 0) // swap step
      {
         swapper.step (system, opls);
         de = swapper.dE (system, system_old, opls);

         if (swapper.is_accepted()) {

            accepted_steps++;
            swapper.accept (system, system_old);
            e_old += de;

         } else {

            swapper.reject (system, system_old);
            de = 0.0;
         }


      } else { // translate & rotate step

         stepper.step(system, opls);
         de = stepper.dE (system, system_old, opls);

         if (stepper.is_accepted()) {

            accepted_steps++;
            stepper.accept (system, system_old);
            e_old += de;

         } else {

            stepper.reject (system, system_old);
            de = 0.0;
         }
      }

      // Init Method
      if (step == 0) {

         old_mean = e_old; 
         old_variance = 0; 

      } else {
        
         double new_mean     = old_mean + (e_old - old_mean) / (step + 1);
         double new_variance = old_variance + (e_old - old_mean) * (e_old - new_mean);
      
         old_mean     = new_mean; 
         old_variance = new_variance; 
      }

      // IO
      if (m_run->print_every != 0 && (step % m_run->print_every) == 0) {
         fprintf (energy_file, "%10i %10i %20.3f %10.5f %20.3f %10.5f\n",
               step, accepted_steps, e_old, de, old_mean, std::sqrt(old_variance / (step + 1)));
         system->save_xyz (xyz_file);
      }
      if (m_run->print_energy != 0 && (step % m_run->print_energy) == 0) {
         fprintf (exen_file, "%10i %10i %20.3f %10.5f %20.3f %10.5f\n",
               step, accepted_steps, e_old, de, old_mean, std::sqrt(old_variance / (step + 1)));
      }

      stepper.update (step);

   } // end MC steps

   // saves random number generator state
   std::ofstream ofile (basename + "/" + std::to_string(chain_id) + ".random", std::ios::binary);
   ofile << rnd_engine; 

   delete system_old;
   
   if (m_run->print_every  != 0) std::fclose (energy_file);
   if (m_run->print_energy != 0) std::fclose (exen_file  );

   // computes final step or theta
   double new_step = m_run->stepmax * m_run->dimension_box;
   if (m_run->adjust_max_step != 0)
   {
     new_step = stepper.stepmax * m_run->dimension_box;
   } else if (m_run->adjust_max_theta != 0) {
     new_step = stepper.thetamax * 180.0 / M_PI;
   } 

   ChainResults *results = new ChainResults (
         old_mean, 0.0, old_variance, 0.0, new_step, m_run->maxmoves,
         accepted_steps);

   return std::make_tuple (results, const_cast<System_Periodic*> (system));
}

















