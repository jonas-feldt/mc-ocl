/*
 * File:   Integrals.cpp
 * Author: Jonas Feldt
 *
 * Version: 2013-10-10
 */

#include <string>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "System.h"
#include "Integrals.h"
#include "c_integrals.h"

#define TOANG 0.529177209 // Molpro 2012 variables
#define TOBOHR 1.0/TOANG

using namespace std;



Integrals::Integrals (string density_file)
{

   init_binom ();
   // IO
   FILE *fp       = fopen(LATT_DAT, "r");
   FILE *den_file = fopen(density_file.c_str (), "r");
   // reads data
   char *line = NULL;
   size_t len = 0;
   long int nbas;
   long int ngrp;
   long int npsh;
   long int ntot;
   if (getline(&line, &len, fp) != -1) nbas = atoi(line);
   else
   {
      cerr << "ERROR: Cannot read " << LATT_DAT << endl;
      exit(-1);
   }
   if (getline(&line, &len, fp) != -1) ngrp = atoi(line);
   else
   {
      cerr << "ERROR: Cannot read " << LATT_DAT << endl;
      exit(-1);
   }
   if (getline(&line, &len, fp) != -1) npsh = atoi(line);
   else
   {
      cerr << "ERROR: Cannot read " << LATT_DAT << endl;
      exit(-1);
   }
   if (getline(&line, &len, fp) != -1) ntot = atoi(line);
   else
   {
      cerr << "ERROR: Cannot read " << LATT_DAT << endl;
      exit(-1);
   }
   iv1      = new double[nbas * nbas];
   density  = new double[nbas * nbas];
   ccs      = new double[ntot];
   icentres = new double[3 * ngrp];
   ixpss    = new double[npsh];
   ccoffs   = new long int[ngrp];
   inshell  = new long int[ngrp];
   infuns   = new long int[ngrp];
   ioffs    = new long int[ngrp];
   igpoffs  = new long int[ngrp];
   ilmins   = new long int[ngrp];
   ilmaxs   = new long int[ngrp];
   incarts  = new long int[ngrp];
   ncont    = new long int[ngrp];

   read_data(fp, ngrp, npsh, inshell, infuns, ioffs, ixpss, igpoffs, icentres, ilmins, ilmaxs, incarts, ncont, ccs, ccoffs);
   read_density(density, den_file, nbas);
   if (line) free(line);

   fclose(den_file);
   fclose(fp);
}




/**
 * Destructor
 */
Integrals::~Integrals () {
   delete[] ccs;
   delete[] icentres;
   delete[] density;
   delete[] iv1;
   delete[] ccoffs;
   delete[] inshell;
   delete[] infuns;
   delete[] ioffs;
   delete[] ixpss;
   delete[] igpoffs;
   delete[] ilmins;
   delete[] ilmaxs;
   delete[] incarts;
   delete[] ncont;
}



/**
 * Computes the contribution of the point charges.
 *
 * @param new_system current system
 * @param old_system last system
 * @param charges partial charges
 * @param molecule_id id of the moved molecule
 * @return energy contribution of the point charges
 */
double Integrals::compute_contribution (System* new_system,
        System *old_system, double* charges, int molecule_id) {

   // Counts number of atoms in molecule
   long int nlat = 0;
   for (unsigned int i = 0; i < new_system->natom; ++i) {
      if (new_system->molecule[i] == molecule_id) nlat++;
   }
   nlat *= 2;

   // initializes arrays for charges and positions
   double *latq = new double[nlat];
   double *latr = new double[3 * nlat];
   long int cnt = 0;
   for (unsigned int i = 0; i < new_system->natom; ++i) {
      if (new_system->molecule[i] == molecule_id) {
         latq[cnt] = charges[i]; // new positions with normal charges
         latr[3 * cnt    ] = new_system->rx[i] * TOBOHR;
         latr[3 * cnt + 1] = new_system->ry[i] * TOBOHR;
         latr[3 * cnt + 2] = new_system->rz[i] * TOBOHR;
         cnt++;
         latq[cnt] = -charges[i]; // old positions with inverted charges
         latr[3 * cnt    ] = old_system->rx[i] * TOBOHR;
         latr[3 * cnt + 1] = old_system->ry[i] * TOBOHR;
         latr[3 * cnt + 2] = old_system->rz[i] * TOBOHR;
         cnt++;
      }
   }

   basis_pt_charges(nbas, ngrp, npsh, inshell, infuns, ioffs, ixpss, igpoffs,
           icentres, ilmins, ilmaxs, incarts, nlat, latq, latr, iv1, ncont, ccs,
           ccoffs);

   // nuclei-charges
   double sum = 0.0;
   for (int i = 0; i < nlat; ++i) {
      for (unsigned int j = 0; j < new_system->natom; ++j) {
         if (new_system->molecule[j] != 0) continue; // continues, 0 = QM molecule
         double dist = sqrt(pow(new_system->rx[j] * TOBOHR - latr[3 * i    ], 2)
                          + pow(new_system->ry[j] * TOBOHR - latr[3 * i + 1], 2)
                          + pow(new_system->rz[j] * TOBOHR - latr[3 * i + 2], 2));
         sum += new_system->atomic_numbers[j] * latq[i] / dist;
      }
   }

   double trace = 0.0;
   for (int i = 0; i < nbas; ++i) {
      for (int j = 0; j < nbas; ++j) {
         trace += iv1[j * nbas + i] * density[i * nbas + j];
      }
   }
   double result = -trace + sum;

   delete[] latq;
   delete[] latr;

   return result;
}
