/**
*
* File : geom_utils.h
* Author : Jonas Feldt, João Oliveira
*
* Version : 2013-07-02
*
*/

#ifndef GEOM_UTILS_H_INCLUDED
#define GEOM_UTILS_H_INCLUDED

#include "math_utils.h"

class System;

double compute_distance (double rxi, double ryi, double rzi, double rxj, double ryj, double rzj);
double compute_minimal_distance (double rxi, double ryi, double rzi, double rxj, double ryj, double rzj);

void translation(double &random_trans_num,double &ex_trans, double &ey_trans, double &ez_trans, double &rx, double &ry, double &rz);
void translation(double random_trans_num, double ex_trans, double ey_trans, double ez_trans, double *rx, double *ry, double *rz, const int atom);
void rotate(double &theta, double &random_1, double &random_2, double &s_random_12, double &rx_old, double &ry_old, double &rz_old);

void copy_coordinates(System *system, System *copy, int changed_molecule);
//void copy_distances(System *system, System *copy, int changed_molecule);

void move_into_center_mass(int id_mol, System *system);
void center_mass(int id_mol, System *system, double &rx_center, double &ry_center, double &rz_center);
double dipole (int id_mol, System *system, const double *charges, double &rx_center, double &ry_center, double &rz_center);
double adjust_stepmax(int n_moves, int accept_steps, double max_disp, double accept_percent);

void compute_minimal_diff (double rxi, double ryi, double rzi,
        double rxj, double ryj, double rzj, double *scr);
double value (double *v);
double scalar_product (double *v1, double *v2);
double scalar_product (double rxi, double ryi, double rzi,
        double rxj, double ryj, double rzj);
double compute_minimal_angle (
      double rxi, double ryi, double rzi,
      double rxj, double ryj, double rzj,
      double rxk, double ryk, double rzk);
#endif // GEOM_UTILS_H_INCLUDED
