import numpy as np
import matplotlib.pyplot as plt

grid = open("./mc-git/mc-ocl/grid.dat","r")

nums = []
for index, line in enumerate(grid):
	if index>100:
		break
	if index!=0:   
		splited = line.split(' ')
                numbers = [num for num in splited if (num != ' ' and num != '')]
		nums.append(float(numbers[3].split('\n')[0]))


x = np.linspace(0,len(nums),len(nums))
fig = plt.figure()
logfig = fig.add_subplot(1,1,1)

logfig.plot(x,nums,'.')
plt.ylim(1.0e-36,1.0e-2)
logfig.set_yscale('log')
plt.show()
