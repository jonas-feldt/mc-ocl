/*
 * File:   mm_fep_periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2017-07-18
 */

#ifndef MMFEPPERIODIC_H
#define	MMFEPPERIODIC_H

#include <string>

#include "typedefs.h"

class Run_Periodic;
class OPLS_Periodic;
class System_Periodic;
class ChainResults;

/**
 * MM MC with periodic boundary conditions
 */
class MMFEPPeriodic {
public:

   MMFEPPeriodic(
         const std::string base_name,
         const Run_Periodic *run,
         const OPLS_Periodic *oplsA,
         const OPLS_Periodic *oplsB,
         const int num_devices,
         const int num_chains);
   virtual ~MMFEPPeriodic();

   vvs run(System_Periodic *system);

   vvs run(vvs &systems);

private:

   const std::string m_base_name;
   Run_Periodic *m_run;
   const OPLS_Periodic *oplsA;
   const OPLS_Periodic *oplsB;
   const int num_devices;
   const int num_chains;

   vvs run (vvs &systems,
         const vvd &energies_oldA,
         const vvd &energies_oldB);

   std::tuple<ChainResults *, System_Periodic *> run_chain(
         System_Periodic *system_start,
         const int chain_id,
         const int seed,
         std::string basename,
         double energy_oldA,
         double energy_oldB);

};

#endif	/* MMFEPPERIODIC_H */

