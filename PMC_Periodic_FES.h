/*
 * File:   PMC_Periodic_FES.h
 * Author: Jonas Feldt
 *
 * Version: 2015-11-06
 */

#ifndef PMC_PERIODIC_FES_H
#define	PMC_PERIODIC_FES_H

#include <string>

#include "PMC_Periodic.h"
#include "typedefs.h"

class Run_Periodic;
class Run;
class OPLS_Periodic;
class System_Periodic;


class PMC_Periodic_FES : public PMC_Periodic {

public:

   using PMC_Periodic::PMC_Periodic;
   virtual ~PMC_Periodic_FES () override;

protected:

   virtual OCLmanager* create_manager (
      int n_pre_steps, 
      int nkernel, 
      int ndevices, 
      int nchains, 
      const vvs &systems,
      const OPLS_Periodic* opls, 
      Run_Periodic* run, 
      vvg &grid, 
      int config_data_size, 
      int energy_data_size, 
      _FPOINT_S_ temperature,
      double theta_max,
      double stepmax,   
      const vvd &energies_qm, 
      _FPOINT_S_ energy_qm_gp,
      const vvd &energies_mm,
      const vvd &energies_vdw_qmmm,
      const vvd &energies_old,
      Pipe_Host **hosts,
      std::string basename) override;

};

#endif	/* PMC_PERIODIC_FES_H */

