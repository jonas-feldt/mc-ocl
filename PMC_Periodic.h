/*
 * File:   PMC_Periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2015-01-06
 */

#ifndef PMC_PERIODIC_H
#define	PMC_PERIODIC_H

#include <string>

#include "typedefs.h"

class Run_Periodic;
class Run;
class OPLS_Periodic;
class System_Periodic;
class OCLmanager;
class Pipe_Host;


/**
 * PMC with periodic boundary conditions, update of the wavefunction that uses
 * the wavefunction of the first system as starting point. The cutoff for the
 * electrostatic between QM/MM is at the moment solely based on the distance of
 * the corresponding atoms.
 */
class PMC_Periodic {
public:

   PMC_Periodic(
         const std::string base_name,
         const Run_Periodic *run,
         const OPLS_Periodic *opls,
         const int num_devices,
         const int num_chains);
   virtual ~PMC_Periodic();

   vvs run(System_Periodic *system);

   vvs run(vvs &systems, const double energy_qm_gp);

   vvs run(vvs &systems);

   static double qm_lattice_updates_wf (
         const unsigned int device,
         const unsigned int chain,
         System_Periodic *system,
         const OPLS_Periodic *opls,
         std::string base_name,
         const Run *run);
   static double qm_gas_phase_updates_wf (
         System_Periodic *system,
         const std::string base_name,
         const Run *run);
   double get_energy_qm_gp();

protected:
   
   virtual OCLmanager* create_manager (
      int n_pre_steps, 
      int nkernel, 
      int ndevices, 
      int nchains, 
      const vvs &systems,
      const OPLS_Periodic* opls, 
      Run_Periodic* run, 
      vvg &grid, 
      int config_data_size, 
      int energy_data_size, 
      _FPOINT_S_ temperature,
      double theta_max,
      double stepmax,   
      const vvd &energies_qm, 
      _FPOINT_S_ energy_qm_gp,
      const vvd &energies_mm,
      const vvd &energies_vdw_qmmm,
      const vvd &energies_old,
      Pipe_Host **hosts,
      std::string basename);

private:
   double m_energy_qm_gp;

   const std::string m_base_name;
   Run_Periodic *m_run;
   const OPLS_Periodic *opls;
   const int num_devices;
   const int num_chains;

   vvs run (
         vvs &systems, 
         const double energy_qm_gp,
         vvg &grids,
         const vvd &energies_mm,
         const vvd &energies_old,
         const vvd &energies_vdw_qmmm,
         const vvd &energies_qm);

   vvg qm_lattice(
         vvs &systems,
         vvd &energies_mm,
         vvd &energies_old,
         vvd &energies_vdw_qmmm,
         vvd &energies_qm);

   double energy_gp(System_Periodic *system);

   #ifdef BASE_PROFILING 
      double t_mc_acc=0,t_mc_end=0,t_mc_start=0;
      int    n_mc=0;
      double t_oclinit_acc=0,t_oclinit_end=0,t_oclinit_start=0;
      int    n_oclinit=0;
      double t_firstmol=0,t_firstmol_end=0,t_firstmol_start=0;
      int    n_firstmol=0;
   #endif
};

#endif	/* PMC_PERIODIC_H */

