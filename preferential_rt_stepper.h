
/*
 * File:   preferential_rt_stepper.h
 * Author: Jonas Feldt
 *
 * Version: 2017-08-15
 */

#pragma once

#include "Rotate_Translate_Stepper.h"
#include "typedefs.h"

class System;
class OPLS;
class Run;

class PreferentialRTStepper : public Rotate_Translate_Stepper
{
   public:

      PreferentialRTStepper (random_engine *rnd_engine, System *system,
            Run *run);

      void step (System *system, const OPLS *opls) override;
      void accept (System *system, System *system_old) override;
      bool is_accepted() override;

   private:

      double *weights;
      double weights_sum;
      double weights_sum_new;
      double weight_new;
      double sx, sy, sz;
};

