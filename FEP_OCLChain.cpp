/**
 *
 * File: FEP_OCLChain.cpp
 * Author: Jonas Feldt 
 *
 * Version: 2015-03-11
 *
 */

#include<iostream>
#include<iomanip>
#include "FEP_OCLChain.h"
#include "OCLChain.h"
#include "FEP_OCLDevice.h"
#include "OCLDevice.h"
#include "Constants.h"

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

#define FREE_ENERGY 2 // index for mean values


/*
 * Constructor, creates a copy of system A & B and grid A & B.
 */
FEP_OCLChain::FEP_OCLChain(
                     System_Periodic *systemA, 
                     System_Periodic *systemB, 
                     Charge_Grid *gridA, 
                     Charge_Grid *gridB, 
                     Pipe_Host *hostA, 
                     Pipe_Host *hostB, 
                     _FPOINT_S_ energy_qmA, 
                     _FPOINT_S_ energy_qmB, 
                     _FPOINT_S_ energy_qm_gp,
                     _FPOINT_S_ energy_mmA,
                     _FPOINT_S_ energy_vdw_qmmmA,
                     _FPOINT_S_ energy_vdw_qmmmB,
                     _FPOINT_S_ energy_oldA,
                     _FPOINT_S_ energy_oldB,
                     int n_pre_steps,
                     int config_data_size, 
                     int energy_data_size, 
                     int seed, 
                     double stepmax, 
                     std::string base_name,
                     int num_means,
                     int n_energy_save_params,
                     std::string print_s,
                     bool extra_energy_file,
                     bool has_seed_state,
                     std::string state_file,
                     const double dimension_box)
   : OCLChain(systemA, gridA, hostA, energy_qmA, energy_qm_gp,
         energy_mmA, energy_vdw_qmmmA, energy_oldA,
         n_pre_steps, config_data_size, energy_data_size, seed, stepmax,
         base_name, num_means, n_energy_save_params, print_s,
         extra_energy_file, has_seed_state, state_file, dimension_box)
{
   // Object for pipe communication
   m_hostB = hostB;

   // Chain's system and grid
   m_systemB = new System_Periodic(*systemB);
   m_gridB   = new Charge_Grid(*gridB);
   
   // Chain's energy
   m_f_energy_qmB         = energy_qmB      ;
   m_f_energy_vdw_qmmmB   = energy_vdw_qmmmB;
   m_f_energy_oldB        = energy_oldB     ;      
}



FEP_OCLChain::~FEP_OCLChain(){
   delete m_systemB;
   delete m_gridB  ;
}



/*
 * Communicates with the Pipe Host to get a new grid.
 */
void FEP_OCLChain::chain_loop(){

   while (true){
      // Wait for wf request
      std::unique_lock<std::mutex> ulock(*m_wf_mutex_launch);
         while(*m_wf_flag_launch == 0){
            VERBOSE_LVL_SYNC("[SYNC] Chain thread "<<m_chain_index<<" is waiting for wfu request" << std::endl);
            m_wf_slave_launch->wait(ulock); 
         }                                        
         if (*m_wf_flag_launch == -1) return; // die, unique_lock unlocks mutex
         *m_wf_flag_launch = 0;
      ulock.unlock();   
      
      //Execute wf request (pipe comn)
      
      // Writes System to pipe
      FEP_OCLDevice *device = static_cast<FEP_OCLDevice*> (m_device);
      VERBOSE_LVL_SYNC("[SYNC] Chain thread " << m_chain_index << "A will issue wfu to wf process" << std::endl);
      m_host ->write_lattice_pipe (m_system , device->m_opls);
      VERBOSE_LVL_SYNC("[SYNC] Chain thread " << m_chain_index << "B will issue wfu to wf process" << std::endl);
      m_hostB->write_lattice_pipe (m_system, m_systemB, device->m_oplsB); // QM of B with MM of A and OPLS B
   
      // Read Grids from pipe
      // (also wait for wf process to terminate the wfu)
      VERBOSE_LVL_SYNC("[SYNC] Chain thread " << m_chain_index << "A will read grid" << std::endl);
      m_f_energy_qm  = m_host ->read_grid_pipe (m_grid ); 
      VERBOSE_LVL_SYNC("[SYNC] Chain thread " << m_chain_index << "B will read grid" << std::endl);
      m_f_energy_qmB = m_hostB->read_grid_pipe (m_gridB); 
      
      // Alert master the pipe comn is over
      std::unique_lock<std::mutex> ulock2(*m_wf_mutex_finish);   
         *m_wf_flag_finish = 1;
         m_wf_master_finish->notify_all();
      ulock2.unlock(); 
   }
}




/*
 * Sets up the buffers.
 */
void FEP_OCLChain::setup_buffers(){

   // call base class method to init buffers for A
   OCLChain::setup_buffers();

   // System B Reference
   m_old_xB =                  cl::Buffer(*(m_device->m_p_context),      //Device context
                                  CL_MEM_READ_WRITE,                     //Read and Write(for updating ref)
                                  (m_systemB->natom)*sizeof(_FPOINT_S_)); //Number of atoms
                        
   m_old_yB =                  cl::Buffer(*(m_device->m_p_context),        
                                  CL_MEM_READ_WRITE,                     
                                  (m_systemB->natom)*sizeof(_FPOINT_S_));
                         
   m_old_zB =                  cl::Buffer(*(m_device->m_p_context),         
                                  CL_MEM_READ_WRITE,                     
                                  (m_systemB->natom)*sizeof(_FPOINT_S_));
   
   #ifndef SET_FPOINT_G_DOUBLE
   m_old_x_fB =                cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (m_systemB->natom)*sizeof(_FPOINT_G_));
                        
   m_old_y_fB =                cl::Buffer(*(m_device->m_p_context),        
                                  CL_MEM_READ_WRITE,                     
                                  (m_systemB->natom)*sizeof(_FPOINT_G_));
                         
   m_old_z_fB =                cl::Buffer(*(m_device->m_p_context),         
                                  CL_MEM_READ_WRITE,                     
                                  (m_systemB->natom)*sizeof(_FPOINT_G_));
   #endif

   //The QM grid x,y,z
   m_grid_rxB =                cl::Buffer(*(m_device->m_p_context), 
                                  CL_MEM_READ_ONLY,              // Read Only is from device's prespective
                                  MAX_GRID_SIZE_MULT*(m_gridB->dim)*sizeof(_FPOINT_G_));
        
   m_grid_ryB =                cl::Buffer(*(m_device->m_p_context), 
                                  CL_MEM_READ_ONLY,              
                                  MAX_GRID_SIZE_MULT*(m_gridB->dim)*sizeof(_FPOINT_G_));
   
   m_grid_rzB =                cl::Buffer(*(m_device->m_p_context), 
                                  CL_MEM_READ_ONLY,             
                                  MAX_GRID_SIZE_MULT*(m_gridB->dim)*sizeof(_FPOINT_G_));
   
   m_grid_chargeB =            cl::Buffer(*(m_device->m_p_context), 
                                  CL_MEM_READ_ONLY,             
                                  MAX_GRID_SIZE_MULT*(m_gridB->dim)*sizeof(_FPOINT_G_));                                  
   
   m_grid_sizeB =              cl::Buffer(*(m_device->m_p_context), 
                                  CL_MEM_READ_ONLY,              
                                  sizeof(int));

      
   //(For m_KERNEL_mm_vdwc/_red kernels)
   m_mm_vdwc_energy_vdwB =         cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE, 
                                  (m_device->m_KERNEL_mm_vdwc->m_int_globalRange/       //Global/Local is size of first reduction
                                  m_device->m_KERNEL_mm_vdwc->m_int_localRange)*sizeof(_FPOINT_S_));
                                  
   m_mm_vdwc_energy_colB =         cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE, 
                                 (m_device->m_KERNEL_mm_vdwc->m_int_globalRange/       //Global/Local is size of first reduction
                                  m_device->m_KERNEL_mm_vdwc->m_int_localRange)*sizeof(_FPOINT_S_));
                                  
   m_minors_energyB =              cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE, 
                                  3*sizeof(_FPOINT_S_));//includes mm_vdwc, qmmm_c_nucl and qmmm_vdw                              
   
   m_qmmm_c_energyB  =         cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE, 
                                  (FIX_THIS_MULT*(m_device->m_KERNEL_qmmm_c->m_int_globalRange)/       //Global/Local is size of first reduction
                                  m_device->m_KERNEL_qmmm_c->m_int_localRange)*sizeof(_FPOINT_G_));

   //Host provided Energies
   //This one will be also updated by OpenCL device
   m_qm_energyB =              cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE, 
                                  sizeof(_FPOINT_S_));

   // Init to 0
   _FPOINT_S_ zero = 0;
   m_energy_oldB =             cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, 
                                  sizeof(_FPOINT_S_),
                                  &zero);
      
   m_energy_vdw_qmmmB =        cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE, 
                                  sizeof(_FPOINT_S_));      
                                  
}




/*
 * Writes the grid to the device.
 *
 * Calls the base class function for A and then writes everything that is
 * different for B (e.g. not sqr_box_dim).
 *
 * CL Dependencies:
 * <- m_EVENT_grid_rxB
 * <- m_EVENT_grid_ryB
 * <- m_EVENT_grid_rzB
 * <- m_EVENT_grid_chargeB
 * <- m_EVENT_grid_sizeB
 */
void FEP_OCLChain::write_grid(
      int inner_step,
      bool reduce,
      double dimension_box,
      double cutoff_coulomb){

   OCLChain::write_grid(inner_step, reduce, dimension_box, cutoff_coulomb);
   if (reduce) m_gridB->reduced_coords(dimension_box, cutoff_coulomb);

   try{
   
      std::vector<cl::Event> write_grid_dependencies; 
      if (inner_step > 0) {
         write_grid_dependencies.push_back(m_device->m_EVENT_closure[inner_step]);
      }
      
      cl_int blocking;
      #ifdef FORCE_GRID_CL_TRUE
         blocking = CL_TRUE;
      #else
         blocking = CL_FALSE;
      #endif
     
      // XXX deprecated comment? 
      //The "offset" in the enqueue function is in respect to the target buffer! Kept to 0
      //The origin vector offset is  + m_assoc_device*(grid_size) - Comment only applies for old muli-device

      VERBOSE_LVL_TRANSFERS("[TRAN] Chain " << m_chain_index << "B will write grid to OpenCL device." << std::endl);

      #ifdef HALF_FIXED
             
         // Convert Grid Coordinates To Fixed Point
         // XXX XXX : Attention : This could be moved to another loop (like pipe-read)
         // This has to happen every grid update, make sure of that !         
           
         _FIXED_* x_qB = new _FIXED_[m_gridB->dim];
         _FIXED_* y_qB = new _FIXED_[m_gridB->dim];
         _FIXED_* z_qB = new _FIXED_[m_gridB->dim];
         
         for (int v = 0; v < m_grid->dim; v++) {
            x_q[v] = FLOAT2FIXED(m_grid->x[v]);
            y_q[v] = FLOAT2FIXED(m_grid->y[v]);
            z_q[v] = FLOAT2FIXED(m_grid->z[v]);
         }
         
         m_queue->enqueueWriteBuffer(  m_grid_rxB,                      //Buffer
                                       blocking,                        //blocking
                                       0,                               //offset
                                       (m_gridB->dim)*sizeof(_FIXED_),  //#grid points
                                       x_qB,                            //origin vector
                                       &write_grid_dependencies,        //no dependencies
                                       &m_EVENT_grid_rxB);              //finish event
                                       
         m_queue->enqueueWriteBuffer(  m_grid_ryB,                      //Buffer
                                       blocking,                        //blocking
                                       0,                               //offset
                                       (m_gridB->dim)*sizeof(_FIXED_),  //#grid points
                                       y_qB,                            //origin vector
                                       &write_grid_dependencies,        //no dependencies
                                       &m_EVENT_grid_ryB);              //finish event 
                                       
         m_queue->enqueueWriteBuffer(  m_grid_rzB,                      //Buffer
                                       blocking,                        //blocking
                                       0,                               //offset
                                       (m_gridB->dim)*sizeof(_FIXED_),  //#grid points
                                       z_qB,                            //origin vector
                                       &write_grid_dependencies,        //no dependencies
                                       &m_EVENT_grid_rzB);              //finish event  

         // TODO this was somewhere down but commented, I think it is actually necessary 
         // should be also in base class?
         delete[] x_qB;
         delete[] y_qB;
         delete[] z_qB;
                                       
      #else
         m_queue->enqueueWriteBuffer(  m_grid_rxB,                       //Buffer
                                       blocking,                         //blocking
                                       0,                                //offset
                                       (m_gridB->dim)*sizeof(_FPOINT_G_),//#grid points
                                       m_gridB->x,                       //origin vector
                                       &write_grid_dependencies,         //no dependencies
                                       &m_EVENT_grid_rxB);               //finish event
                                                      
         m_queue->enqueueWriteBuffer(  m_grid_ryB,                       //Buffer
                                       blocking,                         //blocking
                                       0,                                //offset
                                       (m_gridB->dim)*sizeof(_FPOINT_G_),//#grid points
                                       m_gridB->y,                       //origin vector
                                       &write_grid_dependencies,         //no dependencies
                                       &m_EVENT_grid_ryB);               //finish event 
      
         m_queue->enqueueWriteBuffer(  m_grid_rzB,                       //Buffer
                                       blocking,                         //blocking
                                       0,                                //offset
                                       (m_gridB->dim)*sizeof(_FPOINT_G_),//#grid points
                                       m_gridB->z,                       //origin vector
                                       &write_grid_dependencies,         //no dependencies
                                       &m_EVENT_grid_rzB);               //finish event  
      #endif
   
      m_queue->enqueueWriteBuffer(  m_grid_chargeB,                   //Buffer
                                    blocking,                         //blocking
                                    0,                                //offset
                                    (m_gridB->dim)*sizeof(_FPOINT_G_),//#grid points
                                    m_gridB->charges,                 //origin vector
                                    &write_grid_dependencies,         //no dependencies
                                    &m_EVENT_grid_chargeB);           //finish event          

      m_queue->enqueueWriteBuffer(  m_grid_sizeB,                     //Buffer
                                    blocking,                         //blocking
                                    0,                                //offset
                                    sizeof(int),                      //#grid points
                                    &m_gridB->dim,                    //origin vector
                                    &write_grid_dependencies,         //no dependencies
                                    &m_EVENT_grid_sizeB);             //finish event

      // m_sqrt_box_dim is equal for A and B
                                                   
   }catch(cl::Error error){//openCL error catching
      std::cout << "FEP_OCLChain::write_grid Error: "<<error.what()<<"("<<error.err()<<")" << std::endl;
      throw error;
   }
   
   // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_grid_rxB      );
   m_device->m_mc_dependencies.push_back(m_EVENT_grid_ryB      );
   m_device->m_mc_dependencies.push_back(m_EVENT_grid_rzB      );
   m_device->m_mc_dependencies.push_back(m_EVENT_grid_sizeB    );
   m_device->m_mc_dependencies.push_back(m_EVENT_grid_chargeB  );
}




/*
 * Writes the initial energies at the beginning of a PMC cycle to the device.
 *
 * Calls the base class method and then writes the necessary energies for B.
 */
void FEP_OCLChain::write_init_energies(){

   OCLChain::write_init_energies();

   cl_int blocking = CL_FALSE;

   try{
   
      m_queue->enqueueWriteBuffer(  m_qm_energyB,               //Buffer
                                    blocking,                   //blocking defined above
                                    0,                          //offset
                                    sizeof(_FPOINT_S_),         //1 value
                                    &m_f_energy_qmB,            //origin vector
                                    NULL,                       //no dependencies
                                    &m_EVENT_qm_energyB);       //finish event
                                                               
      m_queue->enqueueWriteBuffer(  m_energy_vdw_qmmmB,         //Buffer
                                    blocking,                   //blocking defined above
                                    0,                          //offset
                                    sizeof(_FPOINT_S_),         //1 value
                                    &m_f_energy_vdw_qmmmB,      //origin vector
                                    NULL,                       //no dependencies
                                    &m_EVENT_energy_vdw_qmmmB); //finish event   
      
      m_queue->enqueueWriteBuffer(  m_energy_oldB,              //Buffer
                                    blocking,                   //blocking defined above
                                    0,                          //offset
                                    sizeof(_FPOINT_S_),         //1 value
                                    &m_f_energy_oldB,           //origin vector
                                    NULL,                       //no dependencies
                                    &m_EVENT_energy_oldB);      //finish event   
                                                   
   }catch(cl::Error error){//openCL error catching
      std::cout<<"FEP_OCLChain::write_init_energies Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
   
   // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_qm_energyB      );
   m_device->m_mc_dependencies.push_back(m_EVENT_energy_vdw_qmmmB);
   m_device->m_mc_dependencies.push_back(m_EVENT_energy_oldB     );
}




/*
 * Writes the system to the device. System B contains only the QM molecule.
 *
 * CL-Dependencies:
 * <- m_EVENT_old_x
 * <- m_EVENT_old_y
 * <- m_EVENT_old_z
 * <- m_EVENT_old_xB
 * <- m_EVENT_old_yB
 * <- m_EVENT_old_zB
 */
void FEP_OCLChain::write_system_lattice(){

   OCLChain::write_system_lattice();

   try{
                        
      cl_int blocking;
      #ifdef FORCE_LATTICE_CL_TRUE
         blocking = CL_TRUE;
      #else
         blocking = CL_FALSE;
      #endif
      
      //The "offset" in the enqueue function is in respect to the target buffer! Kept to 0

      m_queue->enqueueWriteBuffer(  m_old_xB,                             //Buffer
                                    blocking,                             //blocking
                                    0,                                    //offset
                                    (m_systemB->natom)*sizeof(_FPOINT_S_),//#atoms
                                    m_systemB->rx,                        //origin vector
                                    NULL,                                 //no dependencies
                                    &m_EVENT_old_xB);                     //finish event
                                                   
                                                   
      m_queue->enqueueWriteBuffer(  m_old_yB,                             //Buffer
                                    blocking,                             //blocking
                                    0,                                    //offset
                                    (m_systemB->natom)*sizeof(_FPOINT_S_),//#atoms
                                    m_systemB->ry,                        //origin vector
                                    NULL,                                 //no dependencies
                                    &m_EVENT_old_yB);                     //finish event

      m_queue->enqueueWriteBuffer(  m_old_zB,                             //Buffer
                                    blocking,                             //blocking
                                    0,                                    //offset
                                    (m_systemB->natom)*sizeof(_FPOINT_S_),//#atoms
                                    m_systemB->rz,                        //origin vector
                                    NULL,                                 //no dependencies
                                    &m_EVENT_old_zB);                     //finish event
      #ifndef SET_FPOINT_G_DOUBLE 

      #if defined(HALF_FIXED) && defined(LIGHT_FIXED)
             
         // Convert Grid Coordinates To Fixed Point
         // XXX XXX : Attention : This could be moved to another loop (like pipe-read)
         // This has to happen every grid update, make sure of that !         
           
         _FIXED_* x_qB = new _FIXED_[m_systemB->natom];
         _FIXED_* y_qB = new _FIXED_[m_systemB->natom];
         _FIXED_* z_qB = new _FIXED_[m_systemB->natom];
         
         for (int v = 0; v < m_systemB->natom; v++){
            x_qB[v] = FLOAT2FIXED(m_systemB->rx[v]);
            y_qB[v] = FLOAT2FIXED(m_systemB->ry[v]);
            z_qB[v] = FLOAT2FIXED(m_systemB->rz[v]);
         }
         
         m_queue->enqueueWriteBuffer(  m_old_x_fB,                           //Buffer
                                       blocking,                             //blocking
                                       0,                                    //offset
                                       (m_systemB->natom)*sizeof(_FPOINT_G_),//#atoms
                                       x_qB,                                 //origin vector
                                       NULL,                                 //no dependencies
                                       NULL);                                //finish event
                                                      
                                                      
         m_queue->enqueueWriteBuffer(  m_old_y_fB,                           //Buffer
                                       blocking,                             //blocking
                                       0,                                    //offset
                                       (m_systemB->natom)*sizeof(_FPOINT_G_),//#atoms
                                       y_qB,                                 //origin vector
                                       NULL,                                 //no dependencies
                                       NULL);                                //finish event

         m_queue->enqueueWriteBuffer(  m_old_z_fB,                           //Buffer
                                       blocking,                             //blocking
                                       0,                                    //offset
                                       (m_systemB->natom)*sizeof(_FPOINT_G_),//#atoms
                                       z_qB,                                 //origin vector
                                       NULL,                                 //no dependencies
                                       NULL);                                //finish event

         // TODO is this needed because it is not in the base class?
         delete[] x_qB;
         delete[] y_qB;
         delete[] z_qB;
      #else
      
         v_rx_fB = new float[m_systemB->natom];
         v_ry_fB = new float[m_systemB->natom];
         v_rz_fB = new float[m_systemB->natom];
         
         for(unsigned int a = 0; a < m_systemB->natom; a++){
            v_rx_fB[a] = (float) m_systemB->rx[a];
            v_ry_fB[a] = (float) m_systemB->ry[a];
            v_rz_fB[a] = (float) m_systemB->rz[a];
         } 
       
         m_queue->enqueueWriteBuffer(  m_old_x_fB,                           //Buffer
                                       blocking,                             //blocking
                                       0,                                    //offset
                                       (m_systemB->natom)*sizeof(_FPOINT_G_),//#atoms
                                       v_rx_fB,                              //origin vector
                                       NULL,                                 //no dependencies
                                       NULL);                                //finish event
                                                      
                                                      
         m_queue->enqueueWriteBuffer(  m_old_y_fB,                           //Buffer
                                       blocking,                             //blocking
                                       0,                                    //offset
                                       (m_systemB->natom)*sizeof(_FPOINT_G_),//#atoms
                                       v_ry_fB,                              //origin vector
                                       NULL,                                 //no dependencies
                                       NULL);                                //finish event

         m_queue->enqueueWriteBuffer(  m_old_z_fB,                           //Buffer
                                       blocking,                             //blocking
                                       0,                                    //offset
                                       (m_systemB->natom)*sizeof(_FPOINT_G_),//#atoms
                                       v_rz_fB,                              //origin vector
                                       NULL,                                 //no dependencies
                                       NULL);                                //finish event
         
         // TODO is this needed because it is not in the base class?
         delete[] v_rx_fB;
         delete[] v_ry_fB;
         delete[] v_rz_fB;
      #endif
      #endif

      //(Charges are written once in constant memory...)
      
   } catch(cl::Error error) {//openCL error catching
      std::cout << "FEP_OCLChain::write_system_lattice Error: " << error.what() << "(" << error.err() << ")" << std::endl;
      throw error;
   }

   // TODO there are no out-dependencies for float/fixed?!

   // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_old_xB);
   m_device->m_mc_dependencies.push_back(m_EVENT_old_yB);
   m_device->m_mc_dependencies.push_back(m_EVENT_old_zB);
}




/*
 * Reads some energies from the device and
 * writes the reference energies to the device.
 *
 * 
 * CL-Dependecies:
 * -> m_EVENT_closure[inner_step]
 *
 * <- m_EVENT_get_energy_vdw_qmmm
 * <- m_EVENT_get_energy_mm
 * <- m_EVENT_qm_energy
 * <- m_EVENT_energy_old
 * <- m_EVENT_get_energy_vdw_qmmmB
 * <- m_EVENT_get_energy_mmB
 * <- m_EVENT_qm_energyB
 * <- m_EVENT_energy_oldB
 */
void FEP_OCLChain::update_reference_energy(int inner_step){
     
   OCLChain::update_reference_energy(inner_step);

   std::vector<cl::Event> update_reference_dependencies; 
   update_reference_dependencies.push_back(m_device->m_EVENT_closure[inner_step]);
   
   cl_int blocking=CL_TRUE;
   
   _FPOINT_S_ ref_energy_vdw_qmmmB = 0;

   try{

      // Get reference's mm and vdw_qmmm
      m_queue->enqueueReadBuffer(m_energy_vdw_qmmmB, 
                                 blocking, 
                                 0, 
                                 sizeof(_FPOINT_S_),
                                 &ref_energy_vdw_qmmmB,
                                 &update_reference_dependencies,
                                 &m_EVENT_get_energy_vdw_qmmmB);

      m_f_energy_oldB = m_f_energy_qmB + ref_energy_vdw_qmmmB + m_f_energy_mm;

      // Write Updated Energies
      VERBOSE_LVL_TRANSFERS("[TRAN] Chain " << m_chain_index << " will update m_f_energy_qmB  = " << m_f_energy_qmB << std::endl);
      VERBOSE_LVL_TRANSFERS("[TRAN] Chain " << m_chain_index << " will update m_f_energy_oldB = " << m_f_energy_oldB << std::endl);

      m_queue->enqueueWriteBuffer(  m_qm_energyB,         //Buffer
                                    blocking,             //blocking defined above
                                    0,                    //offset
                                    sizeof(_FPOINT_S_),   //1 value
                                    &m_f_energy_qmB,      //origin vector
                                    &update_reference_dependencies,            
                                    &m_EVENT_qm_energyB); //finish event  
                                                                        
      m_queue->enqueueWriteBuffer(  m_energy_oldB,        //Buffer
                                    blocking,             //blocking defined above
                                    0,                    //offset
                                    sizeof(_FPOINT_S_),   //1 value
                                    &m_f_energy_oldB,     //origin vector
                                    &update_reference_dependencies,          
                                    &m_EVENT_energy_oldB);//finish event  

   }catch(cl::Error error){//openCL error catching
      std::cout<<"FEP_OCLDevice::write_new_energy Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
   
   // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_get_energy_vdw_qmmmB );     
   m_device->m_mc_dependencies.push_back(m_EVENT_qm_energyB           );    
   m_device->m_mc_dependencies.push_back(m_EVENT_energy_oldB          );   
}



/**
 * Returns the final results.
 */
ChainResults *FEP_OCLChain::get_results (const int steps) {

   const int number_accepted = get_current_number_accepted (0); // 0 is dummy here
   
   double means[num_means];
   double variances[num_means];

   get_means(num_means, means, variances);
   m_queue->finish();

   const double free_energy = -BOLTZMANN_KB * m_device->m_run->temperature * log(means[FREE_ENERGY]);
  
   // computes std dev
   const double sigma_energy      = sqrt(variances[MEAN_ENERGY     ] / m_device->m_maxmoves);
   const double sigma_interaction = sqrt(variances[MEAN_INTERACTION] / m_device->m_maxmoves);
   const double sigma_exp         = sqrt(variances[FREE_ENERGY     ] / m_device->m_maxmoves);
   const double sigma_fe          = BOLTZMANN_KB * m_device->m_run->temperature * sigma_exp / means[FREE_ENERGY];
 
   fclose (m_energy_file); // XXX the scope of these fopen/close close can be reduced 
   fclose (m_output_file);
   if (has_extra_energy_file) fclose (m_extra_energy_file);

   return new FEP_ChainResults(
         means[MEAN_ENERGY], means[MEAN_INTERACTION], free_energy,
         sigma_energy      , sigma_interaction      , sigma_fe,
         m_stepmax * m_dimension_box, steps, number_accepted);
}



