/*
 * File:   Pipe_Host.h
 * Author: Sebastião Miranda, Jonas Feldt
 *
 * Version: 2014-02-25
 */

#ifndef PIPE_HOST_H
#define PIPE_HOST_H

#include <string>
#include <vector>
#include "Charge_Grid.h"
#include "OPLS.h"

#include "options.h"


class Pipe_Host {
public:
   Pipe_Host(std::string base_name,
         std::string molpro);
   virtual ~Pipe_Host();

   void launch_child_process(std::string input);
   void write_lattice_pipe (System *system, const OPLS *opls);
   void write_lattice_pipe (System *system, System *qm_system, const OPLS *opls);
   double read_grid_pipe (Charge_Grid *grid);
   void print_profiling();
   
   #ifdef BASE_PROFILING
      // prove vector to collect global time data
      std::vector<double> v_micro_probe;
   #endif
   
private:

   std::string base_name;
   std::string molpro;
   int pipe_pmc_to_userf[2];
   int pipe_userf_to_pmc[2];
   pid_t pid;

   // Profiling vars
   #ifdef BASE_PROFILING
   double t_new;
   double t_new_end;
   double t_new_start;
   int    n_new;
   double t_delete;
   double t_delete_end;
   double t_delete_start;
   int    n_delete;
   double t_readgrid;
   double t_readgrid_end;
   double t_readgrid_start;
   int    n_readgrid;
   double t_bohr;
   double t_bohr_end;
   double t_bohr_start;
   int    n_bohr;
   #endif

   void write_fd (std::string input, int fd_read, int fd_write);
   void __launch_child_process(std::string input, int *pipe_pmc_to_userf,
           int *pipe_userf_to_pmc);
   int __write_lattice_pipe(int pipe_writefd, int n_qm_atom, int n_total_atom,
           _FPOINT_S_ * rx, _FPOINT_S_ * ry, _FPOINT_S_ * rz,
           const _FPOINT_S_ *charges);
   int __write_lattice_pipe(int pipe_writefd, int n_qm_atom, int n_total_atom,
           _FPOINT_S_ * rx, _FPOINT_S_ * ry, _FPOINT_S_ * rz,
           const _FPOINT_S_ *charges,
           _FPOINT_S_ *qm_rx, _FPOINT_S_ *qmx_ry, _FPOINT_S_ *qm_rz);
   int __read_grid_pipe(int pipe_readfd,
           _FPOINT_G_ * gx, _FPOINT_G_ * gy, _FPOINT_G_ * gz, _FPOINT_G_ * gc,
           double* qm_energy, int *ngrid);
   int __read_size_pipe(int pipe_readfd);

};

#endif	/* PIPE_HOST_H */

