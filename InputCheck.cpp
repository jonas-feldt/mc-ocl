/*
 * File: InputCheck.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-05-08
 */


#include <libconfig.h++>
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <unordered_map>

#include "InputCheck.h"
#include "Run_Periodic.h"
#include "io_utils.h"
#include "OPLS_Switcher.h"
#include "OCLDeviceCounter.h"




InputCheck::InputCheck ()
:  job_map_periodic {
      {"mmmc"     , &InputCheck::check_mmmc}, 
      {"m3c"      , &InputCheck::check_mmmc}, 
      {"pmc"      , &InputCheck::check_pmc}, 
      {"dual-pmc" , &InputCheck::check_dd}, 
      {"dd"       , &InputCheck::check_dd}, 
      {"fep"      , &InputCheck::check_fep}, 
      {"fes"      , &InputCheck::check_fes}, 
      {"mmfes"    , &InputCheck::check_mmfes}, 
      {"mmfep"    , &InputCheck::check_mmfep}, 
      {"rdf"      , &InputCheck::check_rdf}, 
      {"rdfangle" , &InputCheck::check_rdfangle}, 
      {"displacement" , &InputCheck::check_displacement}, 
      {"dipole"   , &InputCheck::check_dipole}, 
   },
   job_map {{"qmmc", &InputCheck::check_qmmc}}
{
}



InputCheck::~InputCheck ()
{
}




void InputCheck::check (const std::string job, const libconfig::Config &config,
      Run_Periodic *run)
{
   try {
      set_defaults (run);
      (this->*job_map_periodic.at(job))(config, run);
   } catch (const std::out_of_range &e) {
      std::cerr << "Unknown periodic job identifier " << job << " found." << std::endl;
      std::exit(EXIT_FAILURE);
   }
}



void InputCheck::check (const std::string job, const libconfig::Config &config,
      Run *run)
{
   try {
      set_defaults (run);
      (this->*job_map.at(job))(config, run);
   } catch (const std::out_of_range &e) {
      std::cerr << "Unknown job identifier " << job << " found." << std::endl;
      std::exit(EXIT_FAILURE);
   }
}

// TODO set defaults for all checks
// TODO have shared check?
// TODO how to handle different Runs? or put more variables into Run?


void InputCheck::check_pmc (const libconfig::Config &config, Run_Periodic *run)
{

   try {

      // Setup
      config.lookupValue ("setup.skip-first", run->skip_first);
      config.lookupValue ("setup.devices", run->num_devices);
      config.lookupValue ("setup.chains" , run->num_chains );
      config.lookupValue ("setup.n-save-systems", run->n_save_systems);

      if (config.exists("io.molpro-exe"))  run->molpro_exe      = config.lookup ("io.molpro-exe").c_str();
      if (config.exists("io.molpro-grid")) run->molpro_grid_exe = config.lookup ("io.molpro-grid").c_str();
      if (config.exists("io.molpro-pipe")) run->molpro_pipe_exe = config.lookup ("io.molpro-pipe").c_str();

      config.lookupValue ("io.molpro-exe-cores", run->molpro_exe_cores);
      config.lookupValue ("io.molpro-grid-cores", run->molpro_grid_cores);

      // device counter
      if (run->num_devices == 0) {
         run->num_devices = OCLDeviceCounter::count_devices(run);
         run->num_chains /= run->num_devices;
      }

      // Method
      run->maxmoves        = config.lookup("method.steps");
      run->wf_adjust       = config.lookup("method.wfu-update");
      run->cutoff_coulomb  = config.lookup("method.coulomb-cutoff"); // periodic
      run->cutoff_vdw      = config.lookup("method.vdw-cutoff");     // periodic

      if (config.exists("io.parameters-type"))
      {
         std::string type = config.lookup("io.parameters-type").c_str();
         if (type == "pmc") {
            read_params_periodic(config.lookup("method.parameters").c_str(), run);
         } else if (type == "tinker") {
            read_params_periodic_tinker (config.lookup("method.parameters").c_str(), run);
         }
      } else {
         read_params_periodic(config.lookup("method.parameters").c_str(), run);
      }

      // Simulation
      run->stepmax         = config.lookup("simulation.max-step");
      run->temperature     = config.lookup("simulation.temperature");
      run->theta_max       = config.lookup("simulation.max-theta");
      run->set_dimension_box(config.lookup("simulation.periodic.dimension")); // cutoffs have to be set before, periodic
      run->adjust_max_step  = 0; // no adjust by default
      run->adjust_max_theta = 0;
      config.lookupValue("simulation.adjust-max-step" , run->adjust_max_step );
      config.lookupValue("simulation.adjust-max-theta", run->adjust_max_theta);

      if (config.exists("simulation.acceptance-ratio")) {
         run->acceptance_ratio = config.lookup("simulation.acceptance-ratio");
         if (run->acceptance_ratio > 1.0 || run->acceptance_ratio < 0.0)
         {
            std::cerr << "Acceptance ratio has to be set between 0.0 and 1.0." << std::endl;
            std::exit(EXIT_FAILURE);
         }
      } else {
         run->acceptance_ratio = 0.5;
         config.lookup("simulation").add("acceptance-ratio", libconfig::Setting::TypeFloat) = 0.5;
      }
      if ((run->adjust_max_step != 0) && (run->adjust_max_theta != 0))
      {
         std::cerr << "Only one of simulation.adjust-max-step and simulation.adjust-max-theta can be set." << std::endl;
         std::exit(EXIT_FAILURE);
      }
      // restart or continue?
      if (not(config.lookupValue("simulation.restart" , run->restart) or 
              config.lookupValue("simulation.continue", run->restart))) {
         // adds default value to configuration
         libconfig::Setting &simulation = config.lookup("simulation");
         libconfig::Setting &setting =
            simulation.add("restart", libconfig::Setting::TypeBoolean);
         setting = false;
      }
      run->geometry = config.lookup("simulation.geometry").c_str();
      if (not std::ifstream(run->geometry).good()) {
         std::cerr << "Cannot read file: " << run->geometry << std::endl;
         std::exit (EXIT_FAILURE);
      }


      // QM
      std::unordered_map<std::string, std::string> qm_settings;
      libconfig::Setting &settings = config.lookup("qm");
      for (int i = 0; i < settings.getLength(); ++i) {
         libconfig::Setting &setting = settings[i];
         std::string tmp;
         switch (setting.getType()) {
            case libconfig::Setting::TypeInt:
               tmp = std::to_string(static_cast<int> (setting));
               break;
            case libconfig::Setting::TypeFloat:
               tmp = std::to_string(static_cast<double> (setting));
               break;
            case libconfig::Setting::TypeString:
               tmp = setting.c_str();
               break;
            default:
               // XXX not supported in some libconfig versions
               //std::cerr << "Unsupported type for " << setting.getPath() << std::endl;
               std::cerr << "Unsupported type for " << setting.c_str() << std::endl;
               std::cerr << "Currently supported types are:"
                            " Int, Float, String." << std::endl;
               std::exit(EXIT_FAILURE);
         }
         std::string key = std::string("%") + setting.getName() + "%";
         qm_settings[key] = tmp;
      }

      // IO
      if (config.exists("io")) {
         run->print_every = config.lookup("io.save-geometry");
         config.lookupValue("io.save-energy", run->print_energy);

         // reads or sets seed
         if (config.exists("io.seed")) {
            // unsigned int or string with path to seed files
            libconfig::Setting &setting = config.lookup("io.seed");
            if (setting.getType() == libconfig::Setting::TypeString) {
               run->seed_path = setting.c_str();
               run->has_seed_path = true;
            } else {
               run->seed = setting;
            }
         } else {
            std::random_device rd; // real random number if available
            std::uniform_int_distribution<int> dist(
                  std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
            run->seed = dist(rd);
            config.lookup("io").add("seed", libconfig::Setting::TypeInt) = run->seed;
         }

         // path to templates
         if (not config.exists("io.pipe-template")) {
            config.lookup("io").add("pipe-template", libconfig::Setting::TypeString) = "pipe.template";
         }
         if (not config.exists("io.grid-template")) {
            config.lookup("io").add("grid-template", libconfig::Setting::TypeString) = "grid.template";
         }
         run->grid_input = read_template(config.lookup("io.grid-template").c_str(), qm_settings);
         run->pipe_input = read_template(config.lookup("io.pipe-template").c_str(), qm_settings);
         // TODO print the templates to the output

      }
   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing PMC setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}





void InputCheck::check_mmmc (const libconfig::Config &config, Run_Periodic *run)
{
   try {
      run->solute_molecules = 1;
      config.lookupValue("simulation.solute-molecules", run->solute_molecules);

      // Setup
      config.lookupValue("setup.devices", run->num_devices);
      config.lookupValue("setup.chains" , run->num_chains );


      run->rosenbluth_steps = 0;
      run->rosenbluth_trials = 50;
      config.lookupValue("method.rosenbluth-steps", run->rosenbluth_steps);
      config.lookupValue("method.rosenbluth-trials", run->rosenbluth_trials);


      // if device is 0 (= dynamic) set it to 1, which is correct for the
      // CPU-only version
      if (run->num_devices == 0) run->num_devices = 1;

      // Method
      run->maxmoves        = config.lookup("method.steps");
      run->cutoff_coulomb  = config.lookup("method.coulomb-cutoff"); // periodic
      run->cutoff_vdw      = config.lookup("method.vdw-cutoff");     // periodic

      if (config.exists("io.parameters-type"))
      {
         const std::string type = config.lookup("io.parameters-type").c_str();
         if (type == "pmc") {
            read_params_periodic (config.lookup("method.parameters").c_str(), run);
         } else if (type == "tinker") {
            read_params_periodic_tinker (config.lookup("method.parameters").c_str(), run);
         }
      } else {
         read_params_periodic (config.lookup("method.parameters").c_str(), run);
      }

      // Simulation
      run->stepmax         = config.lookup("simulation.max-step");
      run->temperature     = config.lookup("simulation.temperature");
      run->theta_max       = config.lookup("simulation.max-theta");
      run->set_dimension_box(config.lookup("simulation.periodic.dimension")); // cutoffs have to be set before, periodic

      run->adjust_max_step  = 0; // no adjust by default
      run->adjust_max_theta = 0;
      config.lookupValue("simulation.adjust-max-step" , run->adjust_max_step );
      config.lookupValue("simulation.adjust-max-theta", run->adjust_max_theta);

      if (config.exists("simulation.acceptance-ratio")) {
         run->acceptance_ratio = config.lookup("simulation.acceptance-ratio");
         if (run->acceptance_ratio > 1.0 || run->acceptance_ratio < 0.0)
         {
            std::cerr << "Acceptance ratio has to be set between 0.0 and 1.0." << std::endl;
            std::exit(EXIT_FAILURE);
         }
      } else {
         run->acceptance_ratio = 0.5;
         config.lookup("simulation").add("acceptance-ratio", libconfig::Setting::TypeFloat) = 0.5;
      }
      if ((run->adjust_max_step != 0) && (run->adjust_max_theta != 0))
      {
         std::cerr << "Only one of simulation.adjust-max-step and simulation.adjust-max-theta can be set." << std::endl;
         std::exit(EXIT_FAILURE);
      }

      // restart or continue?
      if (not(config.lookupValue("simulation.restart" , run->restart) or 
              config.lookupValue("simulation.continue", run->restart))) {
         // adds default value to configuration
         libconfig::Setting &simulation = config.lookup("simulation");
         libconfig::Setting &setting =
            simulation.add("restart", libconfig::Setting::TypeBoolean);
         setting = false;
      }
      run->geometry = config.lookup("simulation.geometry").c_str();
      if (not std::ifstream(run->geometry).good()) {
         std::cerr << "Cannot read file: " << run->geometry << std::endl;
         std::exit (EXIT_FAILURE);
      }

      // IO
      if (config.exists("io")) {
         run->print_every = config.lookup("io.save-geometry");
         config.lookupValue("io.save-energy", run->print_energy);

         // reads or sets seed
         if (config.exists("io.seed")) {
            // unsigned int or string with path to seed files
            libconfig::Setting &setting = config.lookup("io.seed");
            if (setting.getType() == libconfig::Setting::TypeString) {
               run->seed_path = setting.c_str();
               run->has_seed_path = true;
            } else {
               run->seed = setting;
            }
         } else {
            std::random_device rd; // real random number if available
            std::uniform_int_distribution<int> dist(
                  std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
            run->seed = dist(rd);
            config.lookup("io").add("seed", libconfig::Setting::TypeInt) = run->seed;
         }
      }
   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing MM MC setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}





void InputCheck::check_fep (const libconfig::Config &config, Run_Periodic *run)
{
   check_pmc (config, run);

   try {
      // Setup
      config.lookupValue("setup.fep.devices", run->num_devices);
      config.lookupValue("setup.fep.chains" , run->num_chains );

      // Method
      config.lookupValue("method.fep.reverse", run->fep_reverse);

      // TODO multiple systems B should be an optional setting, although it always needs at least 2 frames?!
      if (config.exists("method.fep.qm-frames")) { // default is 1,2
         libconfig::Setting &setting = config.lookup("method.fep.qm-frames");
         run->fep_first_frame = setting[0];
         run->fep_last_frame  = setting[1];
         run->fep_num_frames  = run->fep_last_frame - run->fep_first_frame + 1;
      } else {
         // TODO find way to add to libconfig
         run->fep_first_frame = 1;
         run->fep_last_frame  = 2;
         run->fep_num_frames  = 1;
      }

      run->fep_qm_geometry = config.lookup("method.fep.qm-geometry").c_str();
      if (not std::ifstream(run->fep_qm_geometry).good()) {
         std::cerr << "Cannot read file: " << run->fep_qm_geometry << std::endl;
         std::exit (EXIT_FAILURE);
      }

      if (!config.exists("method.fep.translate")) {
         libconfig::Setting &setting = config.lookup("method.fep").
            add("translate", libconfig::Setting::TypeArray);
         setting.add(libconfig::Setting::TypeFloat) = 0.0;
         setting.add(libconfig::Setting::TypeFloat) = 0.0;
         setting.add(libconfig::Setting::TypeFloat) = 0.0;
      } else {
         libconfig::Setting &setting = config.lookup("method.fep.translate");
         run->fep_translate[0] = setting[0];
         run->fep_translate[1] = setting[1];
         run->fep_translate[2] = setting[2];
      }

      if (config.exists("method.fep.switch-params")) {
         libconfig::Setting &setting = config.lookup("method.fep.switch-params");
         if (setting.isArray()) { // single tuple
            const int first  = setting[0];
            const int second = setting[1];
            run->fep_switch_pairs.emplace_back(first, second);
         } else { // list of tuples
            for (int i = 0; i < setting.getLength(); ++i) {
               const int first  = setting[i][0];
               const int second = setting[i][1];
               run->fep_switch_pairs.emplace_back(first, second);
            }
         }
      }

      // XXX only method values can be overriden at the moment
      // XXX use above also method.fep as root
      libconfig::Setting &setting = config.lookup("method.fep");
      if (setting.exists("steps"         )) setting.lookupValue("steps"         , run->maxmoves      );
      if (setting.exists("wfu-update"    )) setting.lookupValue("wfu-update"    , run->wf_adjust     );
      if (setting.exists("coulomb-cutoff")) setting.lookupValue("coulomb-cutoff", run->cutoff_coulomb);
      if (setting.exists("vdw-cutoff"    )) setting.lookupValue("vdw-cutoff"    , run->cutoff_vdw    );


   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing FEP setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}




void InputCheck::check_rdf (const libconfig::Config &config, Run_Periodic *run)
{
   try {
      
      // Analysis
      run->trajectory = config.lookup("analysis.trajectory").c_str();

      // Setup XXX this is not used for anything?!
      config.lookupValue("setup.devices", run->num_devices);
      config.lookupValue("setup.chains" , run->num_chains );

      // Simulation
      run->set_dimension_box(config.lookup("simulation.periodic.dimension"));
      run->geometry = config.lookup("simulation.geometry").c_str();
      if (not std::ifstream(run->geometry).good()) {
         std::cerr << "Cannot read file: " << run->geometry << std::endl;
         std::exit (EXIT_FAILURE);
      }

      // RDF
      libconfig::Setting &setting = config.lookup("analysis.rdf.ids");
      run->rdf_first_id  = setting[0];
      run->rdf_second_id = setting[1];
      run->rdf_delta = 0.1; // defaults
      run->rdf_max_bin = 80;
      if (not config.lookupValue("analysis.rdf.delta"   , run->rdf_delta)) {
         config.lookup("analysis.rdf").add("delta", libconfig::Setting::TypeFloat) = run->rdf_delta;
      }
      if (not config.lookupValue("analysis.rdf.max-bin" , run->rdf_max_bin)) {
         config.lookup("analysis.rdf").add("max-bin", libconfig::Setting::TypeInt) = static_cast<int>(run->rdf_max_bin);
      }

      // determines defaults for frames
      run->first_frame = 1;
      if (not config.lookupValue("analysis.first-frame", run->first_frame)) {
         config.lookup("analysis").add("first-frame", libconfig::Setting::TypeInt) = static_cast<int>(run->first_frame);
      }
      if (config.exists("analysis.last-frame")) {
         run->last_frame = config.lookup("analysis.last-frame");
      } else {
         //TODO hardcoded _0 feelsbadman
         std::string file = run->trajectory + "_0";
         unsigned int lines = file_lines(file.c_str());
         unsigned int dim;
         std::ifstream in(file);
         std::string line;
         getline(in, line);
         std::istringstream(line) >> dim;
         run->last_frame = lines / dim;
         config.lookup("analysis").add("last-frame", libconfig::Setting::TypeInt) = static_cast<int>(run->last_frame);
      }

   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing RDF setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}




void InputCheck::check_rdfangle (const libconfig::Config &config, Run_Periodic *run)
{
   check_rdf (config, run);

   try {
      libconfig::Setting &base = config.lookup("analysis.rdf.angle");

      run->rdf_angle_delta = 5.0;
      if (not base.lookupValue("delta", run->rdf_angle_delta)) {
         base.add("delta", libconfig::Setting::TypeFloat) = run->rdf_angle_delta;
      }

      run->rdf_angle_max_bin = 36;
      if (not base.lookupValue("max-bin", run->rdf_angle_max_bin)) {
         base.add("max-bin", libconfig::Setting::TypeInt) = run->rdf_angle_max_bin;
      }

      run->rdf_angle_id = config.lookup("analysis.rdf.angle.id");

   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing RDF setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }

}




std::string InputCheck::add_output (
      const libconfig::Config &config,
      const char* default_output) 
{
   if (!config.exists("io")) {
      config.getRoot().add("io", libconfig::Setting::TypeGroup);
   }
   std::string output;
   // XXX not supported in older libconfig versions
   //if (!config.lookupValue("io.output", output)) {

   try {
      output = config.lookup("io.output").c_str();
   } catch (const libconfig::SettingNotFoundException &ex)
   {
      // if no setting is found, sets an automatic default below
      libconfig::Setting &io = config.lookup("io");
      libconfig::Setting &setting = io.add("output", libconfig::Setting::TypeString);
      std::string tmp = default_output; // remove file type if necessary
      int lastindex = tmp.find_last_of(".");
      output = tmp.substr(0, lastindex);
      setting = output.c_str();
   }
   return output;
}




void InputCheck::check_fes (const libconfig::Config &config, Run_Periodic *run)
{
   check_pmc (config, run);

   try {
      // Setup
      config.lookupValue("setup.fep.devices", run->num_devices);
      config.lookupValue("setup.fep.chains" , run->num_chains );

      // Method
      config.lookupValue("method.fep.reverse", run->fep_reverse);

      // XXX only method values can be overriden at the moment
      // XXX use above also method.fep as root
      libconfig::Setting &setting = config.lookup("method.fep");
      if (setting.exists("steps"         )) setting.lookupValue("steps"         , run->fes_maxmoves  );
      else run->fes_maxmoves = run->maxmoves;
      if (setting.exists("wfu-update"    )) setting.lookupValue("wfu-update"    , run->wf_adjust     );
      if (setting.exists("coulomb-cutoff")) setting.lookupValue("coulomb-cutoff", run->cutoff_coulomb);
      if (setting.exists("vdw-cutoff"    )) setting.lookupValue("vdw-cutoff"    , run->cutoff_vdw    );

      // switch parameters
      if (config.exists ("method.fes.switch-molecule")) {
         run->fes_switch_molecule = config.lookup ("method.fes.switch-molecule");
      } else if (config.exists("method.fes.switch-params")) {
         libconfig::Setting &setting = config.lookup("method.fes.switch-params");
         for (int i = 0; i < setting.getLength(); ++i) {
            run->fep_switch_pairs.emplace_back (setting[i], OPLS_Switcher::DUMMY_TYPE);
         }
      } else if (config.exists("method.fep.switch-params")) {
         libconfig::Setting &setting = config.lookup("method.fep.switch-params");
         if (setting.isArray()) { // single tuple
            const int first  = setting[0];
            const int second = setting[1];
            run->fep_switch_pairs.emplace_back(first, second);
         } else { // list of tuples
            for (int i = 0; i < setting.getLength(); ++i) {
               const int first  = setting[i][0];
               const int second = setting[i][1];
               run->fep_switch_pairs.emplace_back(first, second);
            }
         }
      }

      // TODO uses always the same protocoll for FES so far...
      if (config.exists ("method.fes.coulomb-steps")) {
         run->fes_coulomb_steps = config.lookup("method.fes.coulomb-steps");
         run->fes_vdw_steps = config.lookup("method.fes.vdw-steps");
      } else {
         // at least one of the factors has to be given as an error:
         // * if both are given then the dimensions have to match
         // * the missing array is filled with 1.0s
         // * if a single element is given it is used to fill the missing array
         //
         bool has_vdw            = false;
         bool has_coulomb        = false;
         bool has_vdw_single     = false;
         bool has_coulomb_single = false;
         double vdw_single     = 0.0;
         double coulomb_single = 0.0;
         int dim = 0;

         if (config.exists("method.fes.coulomb-factors")) 
         {
            has_coulomb = true;
            int tmp = config.lookup("method.fes.coulomb-factors").getLength();
            if (tmp == 0)
            {
               has_coulomb_single = true;
               coulomb_single = config.lookup("method.fes-coulomb-factors");
            } else {
               dim = tmp;
            }
         }
         if (config.exists("method.fes.vdw-factors"))
         {
            has_vdw = true;
            int tmp = config.lookup("method.fes.vdw-factors").getLength();
            if (tmp == 0)
            {
               has_vdw_single = true;
               vdw_single = config.lookup("method.fes-vdw-factors");
            } else {
               // in case that both arrays are given check if they have the same dimension
               if (dim != 0 and dim != tmp)
               {
                  std::cerr << "Dimensions of vdw-factors and coulomb-factors do not match." << std::endl;
                  std::exit (EXIT_FAILURE);
               }
               dim = tmp;
            }
         }
         if ((not (has_vdw or has_coulomb)) or (has_coulomb_single and has_vdw_single))
         {
            std::cerr << "Missing FES Setting. Needs at least one of coulomb-factors or vdw-factors defined as an array." << std::endl;
            std::exit (EXIT_FAILURE);
         }
         for (int i = 0; i < dim; ++i) {
            if (has_coulomb) run->fes_coulomb_factors.push_back (config.lookup("method.fes.coulomb-factors")[i]);
            else if (has_coulomb_single) run->fes_coulomb_factors.push_back (coulomb_single);
            else run->fes_coulomb_factors.push_back (1.0);
            if (has_vdw) run->fes_vdw_factors.push_back (config.lookup("method.fes.vdw-factors")[i]);
            else if (has_vdw_single) run->fes_vdw_factors.push_back (vdw_single);
            else run->fes_vdw_factors.push_back (1.0);
         }
      }

   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing FES setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}


void InputCheck::check_mmfep (const libconfig::Config &config, Run_Periodic *run)
{
   check_mmmc (config, run);

   try {
      // minimum of two is only what makes sense
      run->solute_molecules = 2;
      config.lookupValue("simulation.solute-molecules", run->solute_molecules);

      config.lookupValue("method.fep.reverse", run->fep_reverse);

      libconfig::Setting &setting = config.lookup("method.fep");
      setting.lookupValue ("steps", run->mm_fep_steps);
      setting.lookupValue ("lambda-steps", run->fep_lambda_steps);
      run->fep_reference_molecule = 0;
      if (setting.exists ("reference-molecule")) {
         setting.lookupValue ("reference-molecule", run->fep_reference_molecule);
      }
      run->fep_target_molecule = 1;
      if (setting.exists ("target-molecule")) {
         setting.lookupValue ("target-molecule", run->fep_target_molecule);
      }

   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing MM FEP setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}


void InputCheck::check_mmfes (const libconfig::Config &config, Run_Periodic *run)
{
   check_mmmc (config, run);

   try {

      // Method
      config.lookupValue("method.fep.reverse", run->fep_reverse);

      // XXX only method values can be overriden at the moment
      // XXX use above also method.fep as root
      libconfig::Setting &setting = config.lookup("method.fep");
      if (setting.exists("steps")) setting.lookupValue("steps", run->fes_maxmoves);
      else run->fes_maxmoves = run->maxmoves;
      if (setting.exists("coulomb-cutoff")) setting.lookupValue("coulomb-cutoff", run->cutoff_coulomb);
      if (setting.exists("vdw-cutoff")) setting.lookupValue("vdw-cutoff", run->cutoff_vdw);

      // switch parameters
      if (config.exists ("method.fes.switch-molecule")) {
         run->fes_switch_molecule = config.lookup ("method.fes.switch-molecule");
      } else if (config.exists ("method.fes.switch-params")) {
         libconfig::Setting &setting = config.lookup("method.fes.switch-params");
         for (int i = 0; i < setting.getLength(); ++i) {
            run->fep_switch_pairs.emplace_back (setting[i], OPLS_Switcher::DUMMY_TYPE);
         }
      } else if (config.exists("method.fep.switch-params")) {
         libconfig::Setting &setting = config.lookup("method.fep.switch-params");
         if (setting.isArray()) { // single tuple
            const int first  = setting[0];
            const int second = setting[1];
            run->fep_switch_pairs.emplace_back(first, second);
         } else { // list of tuples
            for (int i = 0; i < setting.getLength(); ++i) {
               const int first  = setting[i][0];
               const int second = setting[i][1];
               run->fep_switch_pairs.emplace_back(first, second);
            }
         }
      }

      // TODO uses always the same protocoll for FES so far...
      if (config.exists ("method.fes.coulomb-steps")) {
         run->fes_coulomb_steps = config.lookup("method.fes.coulomb-steps");
         // set steps or populate otherwise the factors vector
         // TODO the compatibility is taken care of in main for now, should be migrated here
         if (not config.lookupValue ("method.fes.vdw-steps", run->fes_vdw_steps) and
               config.exists ("method.fes.vdw-factors")) {
            const auto &c = config.lookup("method.fes.vdw-factors");
            const unsigned int dim = c.getLength();
            for (unsigned int i = 0; i < dim; ++i) {
               run->fes_vdw_factors.push_back (c[i]);
            }
         }
      } else {
         // at least one of the factors has to be given as an error:
         // * if both are given then the dimensions have to match
         // * the missing array is filled with 1.0s
         // * if a single element is given it is used to fill the missing array
         //
         bool has_vdw            = false;
         bool has_coulomb        = false;
         bool has_vdw_single     = false;
         bool has_coulomb_single = false;
         double vdw_single     = 0.0;
         double coulomb_single = 0.0;
         int dim = 0;

         if (config.exists("method.fes.coulomb-factors")) 
         {
            int tmp = config.lookup("method.fes.coulomb-factors").getLength();
            if (tmp == 0)
            {
               has_coulomb_single = true;
               coulomb_single = config.lookup("method.fes.coulomb-factors");
            } else {
               has_coulomb = true;
               dim = tmp;
            }
         }

         if (config.exists("method.fes.vdw-factors"))
         {
            int tmp = config.lookup("method.fes.vdw-factors").getLength();
            if (tmp == 0)
            {
               has_vdw_single = true;
               vdw_single = config.lookup("method.fes-vdw-factors");
            } else {
               // in case that both arrays are given check if they have the same dimension
               if (dim != 0 and dim != tmp)
               {
                  std::cerr << "Dimensions of vdw-factors and coulomb-factors do not match." << std::endl;
                  std::exit (EXIT_FAILURE);
               }
               has_vdw = true;
               dim = tmp;
            }
         }

         if ((not (has_vdw or has_coulomb)) or (has_coulomb_single and has_vdw_single))
         {
            std::cerr << "Missing FES Setting. Needs at least one of coulomb-factors or vdw-factors defined as an array." << std::endl;
            std::exit (EXIT_FAILURE);
         }

         // populates both arrays
         for (int i = 0; i < dim; ++i) {
            if (has_coulomb) run->fes_coulomb_factors.push_back (config.lookup("method.fes.coulomb-factors")[i]);
            else if (has_coulomb_single) run->fes_coulomb_factors.push_back (coulomb_single);
            else run->fes_coulomb_factors.push_back (1.0);

            if (has_vdw) run->fes_vdw_factors.push_back (config.lookup("method.fes.vdw-factors")[i]);
            else if (has_vdw_single) run->fes_vdw_factors.push_back (vdw_single);
            else run->fes_vdw_factors.push_back (1.0);
         }
      }

   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing MM FES setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}




void InputCheck::check_dd (const libconfig::Config &config, Run_Periodic *run)
{
   check_pmc (config, run);

   try {
      if (config.exists ("qm.state")) {
         run->target_state = config.lookup ("qm.state");
      } else {
         run->target_state = 2;
      }

      if (config.exists ("method.dd.emission")) {
         run->emission = config.lookup ("method.dd.emission");
      } else {
         run->emission = false;
      }

   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing Dual Density PMC setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}



void InputCheck::check_qmmc (const libconfig::Config &config, Run *run)
{
   try {

      // Setup
      config.lookupValue("setup.devices", run->num_devices);
      config.lookupValue("setup.chains" , run->num_chains );

      // Method
      run->maxmoves        = config.lookup("method.steps");

      // Simulation
      run->stepmax         = config.lookup("simulation.max-step");
      run->temperature     = config.lookup("simulation.temperature");
      run->adjust_max_step  = 0; // no adjust by default
      run->adjust_max_theta = 0;
      config.lookupValue("simulation.adjust-max-step" , run->adjust_max_step );

      if (config.exists("simulation.acceptance-ratio")) {
         run->acceptance_ratio = config.lookup("simulation.acceptance-ratio");
         if (run->acceptance_ratio > 1.0 || run->acceptance_ratio < 0.0)
         {
            std::cerr << "Acceptance ratio has to be set between 0.0 and 1.0." << std::endl;
            std::exit(EXIT_FAILURE);
         }
      } else {
         run->acceptance_ratio = 0.5;
         config.lookup("simulation").add("acceptance-ratio", libconfig::Setting::TypeFloat) = 0.5;
      }
      // restart or continue?
      if (not(config.lookupValue("simulation.restart" , run->restart) or 
              config.lookupValue("simulation.continue", run->restart))) {
         // adds default value to configuration
         libconfig::Setting &simulation = config.lookup("simulation");
         libconfig::Setting &setting =
            simulation.add("restart", libconfig::Setting::TypeBoolean);
         setting = false;
      }
      run->geometry = config.lookup("simulation.geometry").c_str();
      if (not std::ifstream(run->geometry).good()) {
         std::cerr << "Cannot read file: " << run->geometry << std::endl;
         std::exit (EXIT_FAILURE);
      }


      // QM
      std::unordered_map<std::string, std::string> qm_settings;
      libconfig::Setting &settings = config.lookup("qm");
      for (int i = 0; i < settings.getLength(); ++i) {
         libconfig::Setting &setting = settings[i];
         std::string tmp;
         switch (setting.getType()) {
            case libconfig::Setting::TypeInt:
               tmp = std::to_string(static_cast<int> (setting));
               break;
            case libconfig::Setting::TypeFloat:
               tmp = std::to_string(static_cast<double> (setting));
               break;
            case libconfig::Setting::TypeString:
               tmp = setting.c_str();
               break;
            default:
               // XXX not supported in some libconfig versions
               //std::cerr << "Unsupported type for " << setting.getPath() << std::endl;
               std::cerr << "Unsupported type for " << setting.c_str() << std::endl;
               std::cerr << "Currently supported types are:"
                            " Int, Float, String." << std::endl;
               std::exit(EXIT_FAILURE);
         }
         std::string key = std::string("%") + setting.getName() + "%";
         qm_settings[key] = tmp;
      }

      // IO
      run->molpro_exe = config.lookup("io.molpro-exe").c_str();
      config.lookupValue ("io.molpro-exe-cores", run->molpro_exe_cores);

      if (config.exists("io")) {
         run->print_every = config.lookup("io.save-geometry");
         config.lookupValue("io.save-energy", run->print_energy);

         // reads or sets seed
         if (config.exists("io.seed")) {
            // unsigned int or string with path to seed files
            libconfig::Setting &setting = config.lookup("io.seed");
            if (setting.getType() == libconfig::Setting::TypeString) {
               run->seed_path = setting.c_str();
               run->has_seed_path = true;
            } else {
               run->seed = setting;
            }
         } else {
            std::random_device rd; // real random number if available
            std::uniform_int_distribution<int> dist(
                  std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
            run->seed = dist(rd);
            config.lookup("io").add("seed", libconfig::Setting::TypeInt) = run->seed;
         }

         // TODO for now uses grid input
         // path to templates
         if (not config.exists("io.grid-template")) {
            config.lookup("io").add("grid-template", libconfig::Setting::TypeString) = "grid.template";
         }
         run->grid_input = read_template(config.lookup("io.grid-template").c_str(), qm_settings);
         // TODO print the templates to the output

      }
   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing QM MC setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}




void InputCheck::set_defaults (Run *run)
{
   // molpro binaries
   run->molpro_grid_exe = global_config.lookup ("io.molpro-grid").c_str();
   run->molpro_pipe_exe = global_config.lookup ("io.molpro-pipe").c_str();
   run->molpro_exe      = global_config.lookup ("io.molpro-exe").c_str();

   // number of cores for molpro
   run->molpro_grid_cores = global_config.lookup ("io.molpro-grid-cores");
   run->molpro_exe_cores  = global_config.lookup ("io.molpro-exe-cores");

   // skip first # OCL devices
   run->skip_first = global_config.lookup ("setup.skip-first");
   run->n_default_autonomous = global_config.lookup ("setup.n-default-autonomous");
   run->max_inner_steps = global_config.lookup ("setup.max-inner-steps");
   run->n_save_systems = global_config.lookup ("setup.n-save-systems");

   run->num_devices = 1;
   run->num_chains  = 1;
}





void InputCheck::check_displacement (const libconfig::Config &config, Run_Periodic *run)
{
   try {
      
      // Analysis
      run->trajectory = config.lookup("analysis.trajectory").c_str();

      // Setup XXX this is not used for anything?!
      config.lookupValue("setup.devices", run->num_devices);
      config.lookupValue("setup.chains" , run->num_chains );

      // Simulation
      run->set_dimension_box(config.lookup("simulation.periodic.dimension"));
      run->geometry = config.lookup("simulation.geometry").c_str();
      if (not std::ifstream(run->geometry).good()) {
         std::cerr << "Cannot read file: " << run->geometry << std::endl;
         std::exit (EXIT_FAILURE);
      }

      // determines defaults for frames
      run->first_frame = 1;
      if (not config.lookupValue("analysis.first-frame", run->first_frame)) {
         config.lookup("analysis").add("first-frame", libconfig::Setting::TypeInt) = (int)run->first_frame;
      }
      if (config.exists("analysis.last-frame")) {
         run->last_frame = config.lookup("analysis.last-frame");
      } else {
         std::string file = run->trajectory;
         unsigned int lines = file_lines(file.c_str());
         unsigned int dim;
         std::ifstream in(file);
         std::string line;
         getline(in, line);
         std::istringstream(line) >> dim;
         run->last_frame = lines / dim;
         config.lookup("analysis").add("last-frame", libconfig::Setting::TypeInt) = (int) run->last_frame;
      }

   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing Displacement Analysis setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}



void InputCheck::check_dipole (const libconfig::Config &config, Run_Periodic *run)
{
   InputCheck::check_displacement (config, run);

   try {

      run->cutoff_coulomb = 0.5;
      run->cutoff_vdw = 0.5;

      if (config.exists("io.parameters-type"))
      {
         std::string type = config.lookup("io.parameters-type").c_str();
         if (type == "pmc") {
            read_params_periodic(config.lookup("method.parameters").c_str(), run);
         } else if (type == "tinker") {
            read_params_periodic_tinker (config.lookup("method.parameters").c_str(), run);
         }
      } else {
         read_params_periodic(config.lookup("method.parameters").c_str(), run);
      }
      

   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing Dipole Analysis setting: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for: " << ex.getPath() << std::endl;
      std::exit(EXIT_FAILURE);
   }
}
