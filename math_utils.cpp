/**
 *
 * File: math_utils.cpp
 * Author: Jonas Feldt, João Oliveira
 *
 * Version: 2013-10-15
 *
 */

#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <fstream>

#include <time.h>
#include <iomanip>

#include "math_utils.h"

// Boltzmann constant is 8,3144621 J/K mol with this operation is in kJ/ K mol
#define BOLTZMAN_KB 8.3144621 / 1000.0


/**
 * Computes the Boltzmann factor for the MC simulation.
 *
 * @param temp_K temperature in Kelvin
 * @param delta_energy change of the energy
 * @return Boltzmann factor
 */
double boltzmann_factor (double temp_K, double delta_energy) {
   // Thermodynamic beta, were beta = 1 / Kb.T
   double beta = 1.0 / (BOLTZMAN_KB * temp_K);
   return exp (-delta_energy * beta);
}



/**
 * Computes the length of the given 3-dimensional vector. Doesn't check any
 * dimensions.
 *
 * @param v
 * @return length
 */
double length (double *v) {
   return sqrt (v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}
