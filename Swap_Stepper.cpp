
#include <cstdlib>
#include <iostream>

#include "Swap_Stepper.h"
#include "System.h"
#include "OPLS.h"
#include "typedefs.h"
#include "geom_utils.h"
#include "Run.h"
#include "options.h"


Swap_Stepper::Swap_Stepper (
      random_engine *rnd_engine,
      System *system,
      Run *run)
   : rnd_engine (rnd_engine),
     temperature (run->temperature)
{
   dist_molecule = new std::uniform_int_distribution<int> (1, system->nmol_total - 1);
} 



void Swap_Stepper::step (System *system, const OPLS *opls)
{
   random_mol1 = (*dist_molecule) (*rnd_engine);
   random_mol2 = (*dist_molecule) (*rnd_engine);

   auto m2a = system->molecule2atom;

   // selects a second random molecule which has a different atom type for the
   // first atom XXX can of course easily fail
   int type1 = system->atom_type[m2a[random_mol1].atom_id[0]];
   int type2 = system->atom_type[m2a[random_mol2].atom_id[0]];
   while (type1 == type2)
   {
      random_mol2 = (*dist_molecule) (*rnd_engine);
      type2 = system->atom_type[m2a[random_mol2].atom_id[0]];
   }

   // computes center of masses
   double comx1, comy1, comz1;
   center_mass (random_mol1, system, comx1, comy1, comz1);

   double comx2, comy2, comz2;
   center_mass (random_mol2, system, comx2, comy2, comz2);

   // computes vector from 1->2
   double rx = comx2 - comx1;
   double ry = comy2 - comy1;
   double rz = comz2 - comz1;

   // translates 1+r and 2-r, so that they switch the place
   for(int pos = 0 ; pos < m2a[random_mol1].natoms ; ++pos) {
      int atom = m2a[random_mol1].atom_id[pos];
      translation (1.0, rx, ry, rz, system->rx, system->ry, system->rz, atom);
   }

   for(int pos = 0 ; pos < m2a[random_mol2].natoms ; ++pos) {
      int atom = m2a[random_mol2].atom_id[pos];
      translation (1.0, -rx, -ry, -rz, system->rx, system->ry, system->rz, atom);
   }

   // updates distances
   system->UpdateDistances (random_mol1);
   system->UpdateDistances (random_mol2);
}



double Swap_Stepper::dE (
      System *system,
      System *system_old,
      const OPLS *opls)
{
   // Compute the new energy
   de = opls->energy (system->GetDistance,
         system_old->GetDistance, random_mol1);
   de += opls->energy (system->GetDistance,
         system_old->GetDistance, random_mol2);
   return de;
}



void Swap_Stepper::accept (System *system, System *system_old)
{
   copy_coordinates (system, system_old, random_mol1);
   copy_coordinates (system, system_old, random_mol2);
   // TODO maybe not very efficient?
   system_old->UpdateDistances (random_mol1);
   system_old->UpdateDistances (random_mol2);
}



void Swap_Stepper::reject (System *system, System *system_old)
{
   copy_coordinates (system_old, system, random_mol1);
   copy_coordinates (system_old, system, random_mol2);
   // TODO maybe not very efficient?
   system->UpdateDistances (random_mol1);
   system->UpdateDistances (random_mol2);
}


bool Swap_Stepper::is_accepted ()
{
   std::uniform_real_distribution<_FPOINT_S_> dist_boltzman (0.0, 1.0);
   if (de < 0 ||
         dist_boltzman (*rnd_engine) < boltzmann_factor (temperature, de))
   {
      return true;
   }
   return false;
}



