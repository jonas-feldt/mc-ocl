/*
 * File:   opls_coulomb_fes.h
 * Author: Jonas Feldt
 *
 * Version: 2017-11-02
 */

#ifndef OPLS_COULOMB_FES_H
#define	OPLS_COULOMB_FES_H

#include "OPLS_Periodic.h"

class System;
class Run_Periodic;

class OPLSCoulombFES : public OPLS_Periodic {
public:

   OPLSCoulombFES (
         const System *system,
         Run_Periodic *run,
         double fes_factor);


protected:

   double energy_coulomb (DistanceCall GetDistance) const override;
   double energy_coulomb (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const override;

   double energy_coulomb_qmmm (DistanceCall GetDistance) const;
   double energy_coulomb_qmmm (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const;

   const double fes_factor;

};

#endif	/* OPLS_COULOMB_FES_H */

