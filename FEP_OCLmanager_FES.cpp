/**
 *
 * File: FEP_OCLmanager_FES.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-10-30
 *
 */

#include <iostream>

#include "FEP_OCLmanager.h"
#include "FEP_OCLmanager_FES.h"
#include "FEP_OCLDevice_FES.h"
#include "options.h"


FEP_OCLmanager_FES::FEP_OCLmanager_FES (
                        int n_pre_steps,
                        int nkernel,
                        int ndevices,
                        int nchains,
                        const vvs &systemsA,
                        System_Periodic* systemB,
                        OPLS_Periodic* opls,
                        OPLS_Periodic* oplsB,
                        Run_Periodic* run,
                        vvg &gridsA,
                        vvg &gridsB,
                        int config_data_size,
                        int energy_data_size,
                        _FPOINT_S_ temperature,
                        double theta_max,
                        double stepmax,
                        vvd &energies_qmA,
                        vvd &energies_qmB,
                        _FPOINT_S_ energy_qm_gp,
                        vvd &energies_mmA,
                        vvd &energies_vdw_qmmmA,
                        vvd &energies_vdw_qmmmB,
                        vvd &energies_oldA,
                        vvd &energies_oldB,
                        Pipe_Host **hosts,
                        std::string basename)
: FEP_OCLmanager(n_pre_steps, nkernel, ndevices, nchains, systemsA, systemB,
      opls, oplsB, run, gridsA, gridsB, config_data_size, energy_data_size,
      temperature, theta_max, stepmax, energies_qmA, energies_qmB,
      energy_qm_gp, energies_mmA, energies_vdw_qmmmA, energies_vdw_qmmmB,
      energies_oldA, energies_oldB, hosts, basename)
{
   #ifdef SET_FPOINT_G_DOUBLE
      m_kernel_file_closure = KERNEL_FILE_CLOSURE_FEP_FES;
   #else
      m_kernel_file_closure = "not-existing-kernel";
   #endif
}


/**
 * Destructor
 */
FEP_OCLmanager_FES::~FEP_OCLmanager_FES ()
{
}



OCLDevice* FEP_OCLmanager_FES::create_device (const int i)
{
   return new FEP_OCLDevice_FES (
      m_captured_device[i],
      m_captured_context[i],
      &v_ignite_isReady[i],//Sync variables
      &v_ignite_mutex[i],  //Sync variables
      &v_ignite_slave[i],  //Sync variables
      &ignite_master,      //Sync variables
      m_n_pre_steps, 
      m_nkernel,
      m_nchains_per_device,     //Max chains per device 
      m_systems[0][0],
      m_opls, 
      m_oplsB,
      m_run, 
      m_temperature,
      m_ndevices,
      has_extra_energy_file);
}
