
#include "gtest/gtest.h"

#include "System_Periodic.h"
#include "io_utils.h"
#include "typedefs.h"

#define ERROR 1e-12

TEST (System_Periodic, general) {
   double box_dim = 10.0;
   System_Periodic *system = read_tinker_periodic_system("gtest_opls.xyz", box_dim);

   DistanceCall GetDistance = system->GetDistance;
   EXPECT_EQ (GetDistance (1, 0), 0.1);

   system->rz[0] = 1.0;
   system->UpdateDistances ();
   EXPECT_LT (GetDistance (1, 0) - 0.1, ERROR);

   system->rz[0] = -2.0;
   system->UpdateDistances (0);
   EXPECT_LT (GetDistance (1, 0) - 0.1, ERROR);

   delete system;

   box_dim = 20.0;
   system = read_tinker_periodic_system("gtest_opls.xyz", box_dim);
   GetDistance = system->GetDistance;

   EXPECT_EQ (GetDistance (1, 0), 0.05);
   delete system;
}
