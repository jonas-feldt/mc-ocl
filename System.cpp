/*
 * File:   System.cpp
 * Author: João Oliveira, Jonas Feldt
 *
 * Version: 2013-04-29
 */

#include <iostream>
#include <fstream>
#include <stdlib.h>

#include "System.h"
#include "options.h"
#include "geom_utils.h"
#include "atomic_weight.h"
#include "Atomic_Number.h"
#include "distance_interface.h"



/**
 * Constructor
 *
 * @param natom number of atoms
 * @param rx coordinates
 * @param ry coordinates
 * @param rz coordinates
 * @param atom_type force field atom types
 * @param atom_name name of elements
 * @param n_12 total number of connect atoms
 * @param connect gives the atom number of the connected atom
 */
System::System (unsigned int natom, double rx[], double ry[], double rz[],
        int atom_type[], std::string atom_name[], int n_12[], int connect[][4],
        DistanceInterface *distances)
:  natom (natom),
   rx (rx), ry (ry), rz (rz),
   atom_type (atom_type),
   atom_name (atom_name),
   n_12 (n_12),
   connect (connect),
   distances (distances)
{

   Atom_weight *atomic_mass = new Atom_weight();
   atom_mass = new double[natom];
   for (unsigned int i = 0; i < natom; ++i) {
      atom_mass[i] = atomic_mass->find_mass(atom_name[i]);
   }
   delete atomic_mass;

   Atomic_Number *atomic_number = new Atomic_Number ();
   atomic_numbers = new int[natom];
   for (unsigned int i = 0; i < natom; ++i) {
      atomic_numbers[i] = atomic_number->get_z (atom_name[i]);
   }
   delete atomic_number;

   molecule = new int[natom];
   create_molecules ();

   distances->SetContext (this);

   GetDistance = distances->GetDelegate ();

}



System::~System () {
   delete[] atom_name;
   delete[] rx;
   delete[] ry;
   delete[] rz;
   delete[] atom_type;
   delete[] n_12;
   delete[] connect;
   delete[] molecule;
   delete[] atom_mass;
   delete[] atomic_numbers;
   delete distances;
}

/**
 * Copy constructor.
 *
 * @param other
 */
System::System (System& other)
   : natom (other.natom)
{
   nmol_total = other.nmol_total;
   atom_type = new int[natom];
   n_12      = new int[natom];
   connect   = new int[natom][4];
   molecule  = new int[natom];
   atom_name = new std::string[natom];
   atom_mass = new double[natom];
   atomic_numbers = new int[natom];
   molecule2atom = other.molecule2atom;

   for (unsigned int i = 0; i < natom; ++i) {
      atom_type[i] = other.atom_type[i];
      n_12[i]      = other.n_12[i];
      molecule[i]  = other.molecule[i];
      atom_name[i] = other.atom_name[i];
      atom_mass[i] = other.atom_mass[i];
      atomic_numbers[i] = other.atomic_numbers[i];
      for (int j = 0; j < 4; ++j) {
         connect[i][j] = other.connect[i][j];
      }
   }


   // copies coordinates
   rx = new double[natom];
   ry = new double[natom];
   rz = new double[natom];
   for (unsigned int i = 0; i < natom; ++i) {
      rx[i] = other.rx[i];
   }
   for (unsigned int i = 0; i < natom; ++i) {
      ry[i] = other.ry[i];
   }
   for (unsigned int i = 0; i < natom; ++i) {
      rz[i] = other.rz[i];
   }

   distances = other.distances->clone ();
   distances->SetContext (this);

   GetDistance = distances->GetDelegate ();
}




/**
 * Copy constructor that copies only the molecule with the given ID.
 *
 * @param other
 * @param mol id of the molecule that should be copied
 */
System::System(System& other, int mol)
   : natom (other.molecule2atom[mol].natoms) // only atoms of this mol
{
   nmol_total = 1; // 1 mol
   atom_type = new int[natom];
   n_12      = new int[natom];
   connect   = new int[natom][4];
   molecule  = new int[natom];
   atom_name = new std::string[natom];
   atom_mass = new double[natom];
   atomic_numbers = new int[natom];

   int start = other.molecule2atom[mol].atom_id[0]; // first atom
   int end; // last atom + 1
   if (mol == other.nmol_total) { // exception for the last molecule
      end = other.natom;
   } else {
      end   = other.molecule2atom[mol+1].atom_id[0];
   }

   int cnt = 0;
   for (int i = start; i < end; ++i) {
      atom_type[cnt]      = other.atom_type[i];
      n_12[cnt]           = other.n_12[i];
      molecule[cnt]       = other.molecule[i];
      atom_name[cnt]      = other.atom_name[i];
      atom_mass[cnt]      = other.atom_mass[i];
      atomic_numbers[cnt] = other.atomic_numbers[i];
      for (int j = 0; j < 4; ++j) {
         connect[cnt][j] = other.connect[i][j];
      }
      cnt++;
   }


   // copies coordinates
   rx = new double[natom];
   ry = new double[natom];
   rz = new double[natom];
   cnt = 0;
   for (int i = start; i < end; ++i) {
      rx[cnt] = other.rx[i];
      cnt++;
   }
   cnt = 0;
   for (int i = start; i < end; ++i) {
      ry[cnt] = other.ry[i];
      cnt++;
   }
   cnt = 0;
   for (int i = start; i < end; ++i) {
      rz[cnt] = other.rz[i];
      cnt++;
   }

   // copies molecule to atom mapping
   molecule2atom.emplace_back(other.molecule2atom[mol]);

   distances = other.distances->clone ();
   distances->SetContext (this);

   GetDistance = distances->GetDelegate ();
}




/**
 * Updates distances and uses the current coordinates.
 */
void System::UpdateDistances () {
   distances->ComputeDistances ();
}



/**
 * Updates distances for the changed molecule
 *
 * @param id_molecule index of the changed molecule
 */
void System::UpdateDistances (int id_molecule) {
   distances->ComputeDistances (id_molecule);
}



/**
 * Appends the informations about the system in xyz file format to the given
 * file.
 *
 * @param file name of output file
 */
void System::save_xyz (std::string file) {

   std::ofstream out;
   out.open (file.c_str(), std::ios::app);
   out.precision (6);
   out.setf (std::ios::fixed, std::ios::floatfield);

   if (out.is_open ()) {
      out << "\t" << natom << std::endl;
      out << std::endl;
      for (unsigned int i = 0; i < natom; i++) {
         out << atom_name[i] << "\t";
         out << rx[i] << "\t" << ry[i] << "\t" << rz[i] << "\t";
         out << std::endl;
      }
      out.close ();
   } else {
      std::cout << " ERROR CAN'T OPEN OUTPUT FILE" << std::endl;
      exit (EXIT_FAILURE);
   }
}



/**
 * Appends the informations about the system in tinker file format to the given
 * file.
 *
 * @param file name of output file
 */
void System::save_all (std::string file) {

   std::ofstream out;
   out.open (file.c_str(), std::ios::app);
   out.precision (6);
   out.setf (std::ios::fixed, std::ios::floatfield);

   if (out.is_open ()) {
      out << "\t" << natom << std::endl;
      for (unsigned int i = 0; i < natom; i++) {
         out << i << "\t";
         out << atom_name[i] << "\t";
         out << rx[i] << "\t" << ry[i] << "\t" << rz[i] << "\t";
         out << atom_type[i] << "\t";
         for (int j = 0; j < n_12[i]; ++j) {
            out << (connect[i][j] + 1) << "\t";
         }
         out << std::endl;
      }
      out.close ();
   } else {
      std::cout << " ERROR CAN'T OPEN OUTPUT FILE" << std::endl;
      exit (EXIT_FAILURE);
   }
}




void System::save_all(std::string file, std::string title) {
   std::ofstream out;
   out.open (file.c_str(), std::ios::app);

   if (out.is_open ()) {
      out << std::endl << title << std::endl;
      out.close ();
   } else {
      std::cerr << " ERROR CAN'T OPEN OUTPUT FILE" << std::endl;
      exit (EXIT_FAILURE);
   }
   save_all(file);
}




/**
 * Prints all informations about this system.
 */
void System::print () {

   printf (" Total number Number of atoms %i \n", natom);
   for (unsigned int i = 0; i < natom; i++) {
      printf ("    %4i   %4s  %10.7f   %10.7f  %10.7f  %6i  Connections %i \n", i, atom_name[i].c_str(), rx[i], ry[i], rz[i], atom_type[i], n_12[i]);
   }
   for (unsigned int i = 0; i < natom; i++) {
      for (int j = 0; j < n_12[i]; j++) {
         std::cout << "Atom  " << i << ": Nr of atoms connected   " << n_12[i] << "  Connect to atom " << connect[i][j] << std::endl;
      }
   }
   printf ("TOTAL NUMBER OF MOLECULES %5i \n", nmol_total);
   for (unsigned int i = 0; i < natom; i++) {
      printf ("Molecule %5i  Atom %5i \n", molecule[i], i);
   }

   printf("Distances\n");
   printf("    ");
   for (unsigned int i = 0; i < natom; ++i) {
      printf("%10i", i);
   }
   std::cout << std::endl;
   for (unsigned int i = 0; i < natom; ++i) {
      printf("%4i", i);
      for (unsigned int j = 0; j < natom; ++j) {
         printf("%10.5f", GetDistance(i, j));
      }
      std::cout << std::endl;
   }

   printf("Atomic Weights\n");
   for (unsigned int i = 0; i < natom; ++i) {
     printf("Atom  %4s      A_Weight  %10.7f \n",atom_name[i].c_str(), atom_mass[i] );
   }

   printf("Atomic Number\n");
   for (unsigned int i = 0; i < natom; ++i) {
     printf("Atom  %4s      A_Weight  %10i \n",atom_name[i].c_str(), atomic_numbers[i] );
   }
}


/**
 * Creates molecules based on the connectivities.
 *
 * XXX Expects that all atoms of a molecule appear in a continuous block in the
 * input file. (So far not a problem with Geometries from Tinker.)
 */
void System::create_molecules () {

   int nmol = 0;
   for (unsigned int i = 0; i < natom; i++) // initialize molecule array
   {
      molecule[i] = -1;
   }

   for (unsigned int i = 0; i < natom; i++) {
      if (molecule[i] == -1) {
         molecule[i] = nmol;
         set_neighbors (i, nmol);
         nmol++;
      }
   }
   nmol_total = nmol;

   // initializes mapping from molecules to atoms
   int pos = 0;
   //molecule2atom = new Molecule_db[nmol];

   for (int i = 0 ; i < nmol ; i++) {
      molecule2atom.emplace_back();
      // counts number of atoms of this molecule
      molecule2atom[i].natoms = 0;
      for (unsigned int j = 0 ; j < natom ; j++) {
         if (molecule[j] == i) {
            molecule2atom[i].natoms++;
         }
      }

      // initializes array with IDs of atoms
      molecule2atom[i].atom_id = new int[molecule2atom[i].natoms];
      pos = 0;
      for (unsigned int j = 0 ; j < natom ; j++) {
         if (molecule[j] == i) {
            molecule2atom[i].atom_id[pos] = j;
            pos++;
         }
      }
   }
}



/**
 * Sets the correct molecule ID for the neighbors of atom and recursively for
 * all neighbors of the neighbors and so on...
 *
 * @param atom original atom
 * @param id of the molecule
 */
void System::set_neighbors (int atom, int id) {
   for (int i = 0; i < n_12[atom]; i++) {
      int neighbor = connect[atom][i];
      if (molecule[neighbor] == -1) {
         molecule[neighbor] = id;
         set_neighbors (neighbor, id);
      }
   }
}



std::vector<double*> System::get_normal_coords () {
   return std::vector<double*> {rx, ry, rz};
}



/*
 * Sets new coordinates. Expects the given coordinates to be in Angstrom.
 */
void System::set_coordinates (double *x, double *y, double *z)
{
   for (unsigned int i = 0; i < natom; ++i) {
      rx[i] = x[i];
   }
   for (unsigned int i = 0; i < natom; ++i) {
      ry[i] = y[i];
   }
   for (unsigned int i = 0; i < natom; ++i) {
      rz[i] = z[i];
   }
}






