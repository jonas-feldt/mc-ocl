/**
 *
 * File: qmmm_tasks_d.cl
 * Author: Sebastiao Miranda
 *
 * Version: 2014-03-18
 *
 */
 
/**
 * Equivalent pseudo-calls in Reference PMC CYCLE: 
 *
 * 1)Compute 'Coulomb qmmm (nuclei)'
 *
 * 2)Compute 'VDW qmmm' 
 * 
 */
 
#pragma OPENCL EXTENSION cl_khr_fp64: enable

#define KCAL_TO_KJ 4.183999734824774
#define E2_ANG_TO_KJMOL 1389.354867249

inline double compute_minimal_distance (double rxi, double ryi, double rzi, double rxj, double ryj, double rzj) {
   
   double diffx = rxi - rxj;
   double diffy = ryi - ryj;
   double diffz = rzi - rzj;

   //TODO: Checkout if this code is efficient.
   diffx -= convert_int(floor (diffx + 0.5));
   diffy -= convert_int(floor (diffy + 0.5));
   diffz -= convert_int(floor (diffz + 0.5));
   
   double dist = diffx * diffx + diffy * diffy + diffz * diffz;

   return sqrt(dist);
}

__kernel
void qmmm_tasks(
  
  // Memory buffer local to the work-group
  __local double *local_qmmm_c_nucl,
  __local double *local_qmmm_vdw,
  
  // Scalar arguments
  // TODO this is not used
  int step_number,
  
  // MM molecule old x,y,z,mass,atomic numbers
  __global double    *old_x,
  __global double    *old_y,
  __global double    *old_z,                 // 5. arg
  __global double    *charges,
  __global int       *atomic_numbers,
  
  // Changed MM molecule (Kernel will write here)
  __global int      *changed_molecule,
  __global int      *changed_molecule_size,
  __global double   *ch_x,                   // 10. arg
  __global double   *ch_y,
  __global double   *ch_z,   
  
  // Squared box dim for grid
  __global double   *sqrt_box_dim,
  
  // Output energy of this kernel
  __global double   *minors_energy, //old qmmm_vdw_energy
     
  // Consts about the system
  __constant int    *n_total_mol,            // 15. arg
  __constant int    *n_total_atom,
  __constant int    *mol2atom0,
  
  // Run Constants
  __constant double *cutoff_coulomb_rec_sq,
  __constant double *cutoff_coulomb_rec,
  __constant double *cutoff_coulomb,         // 20. arg
  __constant double *cutoff_vdw,
  
  __global double *epsilon,
  __global double *sigma
  
   ){
   
   // ids
   __private int     local_id          ;
   //__private int     global_id         ;
   __private int     my_group         ;
   __private int     local_size        ;

   
   __private int p_random_molecule        ;
   // Molecule atoms
   __private int ch_start_atom_i          ;
   __private int ch_end_atom              ;
   __private int qm_start_atom_i          ;
   __private int qm_end_atom              ;
   // Some private variables 
   // to reduce global memory access
   __private double p_cutoff_coulomb_rec_sq  ;
   __private double p_cutoff_coulomb_rec     ;
   __private double p_cutoff_coulomb         ;
   __private double p_cutoff_vdw             ;
   
   // Hold the energy
   __private double energy_c_nuclei_qmmm    = 0.0;
   __private double energy_vdw_qmmm         = 0.0;
   
   // Collect some values to private memory
   p_cutoff_coulomb_rec_sq    = *cutoff_coulomb_rec_sq;
   p_cutoff_coulomb_rec       = *cutoff_coulomb_rec   ;
   p_cutoff_coulomb           = *cutoff_coulomb       ;
   p_cutoff_vdw               = *cutoff_vdw           ;
   
   // Collect ids
   //global_id   = get_global_id(0);
   local_id    = get_local_id(0);
   my_group    = get_group_id(0);
   local_size  = get_local_size(0);


   // Get random molecule
   p_random_molecule = *changed_molecule;
    
   // Find Out atoms belonging to the molecule
   // end_atom no longer belongs to molecule
   // end_atom-start_atom_i gives molecule size
   if(p_random_molecule<(*n_total_mol)-1){
      ch_start_atom_i = mol2atom0[p_random_molecule];
      ch_end_atom     = mol2atom0[p_random_molecule+1];
   }else{//Last molecule
      ch_start_atom_i = mol2atom0[p_random_molecule];
      ch_end_atom     = *n_total_atom;
   }
   
   // Find Out atoms belonging to the QM molecule
   // end_atom no longer belongs to molecule
   // end_atom-start_atom_i gives molecule size
   
   qm_start_atom_i = mol2atom0[0];
   qm_end_atom     = mol2atom0[1];
      
   /*** Coulomb qmmm (nuclei) ***/
   /***                       ***/
   
   if(my_group == 0 ){
      // The QM molecule 'for' loops through the global old_x,y,z list whereas
      // the changed MM changed Molecule 'for' loops through the ch_x,y,z list.
      // To acess the old coordinates of the changed molecule, the value
      // ch_start_atom_i must be added to index j.
      
      int i = local_id + qm_start_atom_i;
      if(i < qm_end_atom){//QM molecule
      
      //Original serial for loop
      //for (int i = qm_start_atom_i; i < qm_end_atom ; i++){ 
      
         for (int j = 0; j < (ch_end_atom-ch_start_atom_i); j++) { //changed MM molecule
                          
            double dist    = compute_minimal_distance (
                             old_x[i], old_y[i], old_z[i],
                              ch_x[j],  ch_y[j],  ch_z[j]);
                          
            double dist_old = compute_minimal_distance (
                    old_x[i],                 old_y[i],                 old_z[i],
                    old_x[j+ch_start_atom_i], old_y[j+ch_start_atom_i], old_z[j+ch_start_atom_i]);
                    
            if (dist < p_cutoff_coulomb) {
                // minus because charges have "wrong" sign, reduced atomic number
               double qs = atomic_numbers[i] / *sqrt_box_dim * charges[j+ch_start_atom_i];
               if (dist_old < p_cutoff_coulomb) {     // dist < p_cutoff_coulomb && dist_old < p_cutoff_coulomb
                  energy_c_nuclei_qmmm += qs * (1.0 / dist - 1.0 / dist_old +
                          p_cutoff_coulomb_rec_sq * (dist - dist_old));
               } else {                     // dist < p_cutoff_coulomb && dist_old > p_cutoff_coulomb
                  energy_c_nuclei_qmmm += qs * (1.0 / dist - p_cutoff_coulomb_rec +
                          p_cutoff_coulomb_rec_sq * (dist - p_cutoff_coulomb));
               }
            } else if (dist_old < p_cutoff_coulomb) { // dist > p_cutoff_coulomb && dist_old < p_cutoff_coulomb
               // minus because charges have "wrong" sign
               double qs = atomic_numbers[i] / *sqrt_box_dim * charges[j+ch_start_atom_i];
               energy_c_nuclei_qmmm -= qs * (1.0 / dist_old - p_cutoff_coulomb_rec +
                       p_cutoff_coulomb_rec_sq * (dist_old - p_cutoff_coulomb));

               // dist > p_cutoff_coulomb && dist_old > p_cutoff_coulomb --> always 0
            }
         }         
      }//energy_c_nuclei_qmmm is 0 by default
      
      local_qmmm_c_nucl[local_id] = energy_c_nuclei_qmmm;
               
      barrier(CLK_LOCAL_MEM_FENCE);
      
      //Now all work-items of the same work group must accumulate their energy.
      for(int offset = local_size/2; offset > 0 ;offset >>= 1){
         if(local_id < offset){
            local_qmmm_c_nucl[local_id] = local_qmmm_c_nucl[local_id + offset] + local_qmmm_c_nucl[local_id];
         }
         barrier(CLK_LOCAL_MEM_FENCE);
      }

      if (local_id == 0) { 
         
         //Serial somation
         //for(int a = 1 ; a < local_size ; a++)
         //   local_qmmm_c_nucl[0]+=local_qmmm_c_nucl[a];
      
         minors_energy[2] = local_qmmm_c_nucl[0] * E2_ANG_TO_KJMOL;
      }  
   }
   
   /***    VDW qmmm    ***/
   /***                ***/
   
   else if(my_group == 1){
   
      int i = local_id + qm_start_atom_i;
      if(i < qm_end_atom){//QM molecule
      
      //Original serial for loop
      //for(int i = qm_start_atom_i ; i < qm_end_atom ; i++ ){ //QM molecule
         
         for (int j = 0; j < (ch_end_atom-ch_start_atom_i); j++) { //changed MM molecule
            
            double dist    = compute_minimal_distance (
                             old_x[i], old_y[i], old_z[i],
                              ch_x[j],  ch_y[j],  ch_z[j]);
                          
            double dist_old = compute_minimal_distance (
                    old_x[i],                 old_y[i],                 old_z[i],
                    old_x[j+ch_start_atom_i], old_y[j+ch_start_atom_i], old_z[j+ch_start_atom_i]);

            if (dist < p_cutoff_vdw) {

               double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j+ch_start_atom_i]);
               double sigmas = sigma[i] * sigma[j+ch_start_atom_i];
               double t = sigmas / (dist * dist);
               double V6 = t * t * t;
               double V12 = V6 * V6;

               if (dist_old < p_cutoff_vdw) {
                  double t_o = sigmas / (dist_old * dist_old);
                  double V6_o = t_o * t_o * t_o;
                  double V12_o = V6_o * V6_o;
                  energy_vdw_qmmm += sqrt_epsilon * (V12 - V12_o - V6 + V6_o);
               } else { // dist < p_cutoff_vdw && dist_old > p_cutoff_vdw
                  energy_vdw_qmmm += sqrt_epsilon * (V12 - V6);
               }
            } else if (dist_old < p_cutoff_vdw) { // dist > p_cutoff_vdw && dist_old < p_cutoff_vdw
               // subtracts the old value
               double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j+ch_start_atom_i]);
               double sigmas = sigma[i] * sigma[j+ch_start_atom_i];
               double t_o = sigmas / (dist_old * dist_old);
               double V6_o = t_o * t_o * t_o;
               double V12_o = V6_o * V6_o;
               energy_vdw_qmmm -= sqrt_epsilon * (V12_o - V6_o);

               // dist > p_cutoff_vdw && dist_old > p_cutoff_vdw --> always 0
            }
         }
         
      }//energy_vdw_qmmm is 0 by default
      
      local_qmmm_vdw[local_id] = energy_vdw_qmmm;
               
      barrier(CLK_LOCAL_MEM_FENCE);
      
      //Now all work-items of the same work group must accumulate their energy.
      for(int offset = local_size/2; offset > 0 ;offset >>= 1){
         if(local_id < offset){
            local_qmmm_vdw[local_id] = local_qmmm_vdw[local_id + offset] + local_qmmm_vdw[local_id];
         }
         barrier(CLK_LOCAL_MEM_FENCE);
      }

      if (local_id == 0) { 
         minors_energy[1] = local_qmmm_vdw[0] * 4.0 * KCAL_TO_KJ;
      }  
   }
}
