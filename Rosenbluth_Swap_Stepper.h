
/*
 * File:   Rosenbluth_Swap_Stepper.h
 * Author: Jonas Feldt
 *
 * Version: 2017-05-10
 */

#ifndef ROSENBLUTH_SWAP_STEPPER_H
#define	ROSENBLUTH_SWAP_STEPPER_H

#include "Abstract_Stepper.h"
#include "typedefs.h"

class System;
class OPLS;
class Run;

class Rosenbluth_Swap_Stepper : public Abstract_Stepper
{
   public:

      Rosenbluth_Swap_Stepper (
            random_engine *rnd_engine,
            System *system,
            int num_trial_configurations,
            Run *run);

      virtual ~Rosenbluth_Swap_Stepper();

      virtual void step (System *system, const OPLS *opls) override;
      virtual double dE (System *system, System *system_old, const OPLS *opls) override;

      virtual bool is_accepted () override;
      
      virtual void accept (System *system, System *system_old) override;
      virtual void reject (System *system, System *system_old) override;

   private:

      int select (double *w, double sum);

      random_engine *rnd_engine;
      std::uniform_int_distribution<int> *dist_molecule;
      std::uniform_real_distribution<double> dist_two;
      std::uniform_real_distribution<_FPOINT_S_> *dist_step;

      int random_mol1;
      int random_mol2;

      const int num_trials;
      double *w;
      double *vectorsx1, *vectorsy1, *vectorsz1, *steps1;
      double *vectorsx2, *vectorsy2, *vectorsz2, *steps2;
      Run *run;
      double prob_acc;
};

#endif	/* ROSENBLUTH_SWAP_STEPPER_H */

