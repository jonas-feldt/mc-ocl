/*
 * File: distance_matrix_periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2017-07-11
 *
 * Concrete implementation of the distances template which is used in Systems.
 */

#ifndef DISTANCE_MATRIX_PERIODIC_H
#define DISTANCE_MATRIX_PERIODIC_H

#include "distance_matrix.h"

class DistanceMatrixPeriodic : public DistanceMatrix
{

   public:

      void ComputeDistances () override;
      void ComputeDistances (unsigned int molecule) override;
      DistanceMatrixPeriodic *clone () const override;

};


#endif
