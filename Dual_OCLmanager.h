/**
 *
 * File: Dual_OCLmanager.h
 * Author: Jonas Feldt
 *
 * Version: 2017-02-13
 *
 */

#ifndef DUAL_OCLMANAGER_H_INCLUDED
#define DUAL_OCLMANAGER_H_INCLUDED

#include <string>

#include "OCLmanager.h"
#include "typedefs.h"
#include "options.h"

class System_Periodic;
class Run_Periodic;
class Pipe_Host;
class Charge_Grid;
class OCLDevice;

class Dual_OCLmanager : public OCLmanager {

public:

   Dual_OCLmanager(
         int n_pre_steps,
         int nkernel,
         int ndevices,
         int nchains,
         const vvs &systems,
         OPLS_Periodic* opls,
         Run_Periodic* run,
         vvg &gridsA,
         vvg &gridsB,
         int config_data_size,
         int energy_data_size,
         _FPOINT_S_ temperature,
         double theta_max,
         double stepmax,
         vvd &energies_qmA,
         vvd &energies_qmB,
         _FPOINT_S_ energy_qm_gp,
         vvd &energies_mm,
         vvd &energies_vdw_qmmm,
         vvd &energies_oldA,
         vvd &energies_oldB,
         Pipe_Host **hosts,
         std::string basename);

   virtual ~Dual_OCLmanager();

   virtual void init();

   vvg m_gridsB;
   vvd m_energies_qmB ;
   vvd m_energies_oldB;

protected:

   virtual void final_output();
   virtual OCLDevice *create_device (const int i) override;
};

#endif // DUAL_OCLMANAGER_H_INCLUDED
