/**
 *
 * File: Run.h
 * Author: Jonas Feldt
 *
 * Version: 2015-06-05
 *
 */

#ifndef RUN_H
#define RUN_H

#include <string>
#include <vector>
#include <array>

class OPLS_Periodic;
class OPLS_Switcher;


class Run {

public:

   // Method
   std::vector<int> atom_types;
   std::vector<double> epsilons;
   std::vector<double> sigmas;
   std::vector<double> charges;
   unsigned int solute_molecules = 1; // set default here for now

   // Method FEP
   bool fep_reverse;
   unsigned int fep_first_frame;
   unsigned int fep_last_frame;
   unsigned int fep_num_frames;
   std::string fep_qm_geometry;
   std::array<double, 3> fep_translate;
   std::vector<std::pair<int, int>> fep_switch_pairs;
   int fes_maxmoves;
   // specifies molecules for dual topology
   unsigned int fep_reference_molecule;
   unsigned int fep_target_molecule;
   unsigned int fep_lambda_steps;
   unsigned int mm_fep_steps;
   // FEP->FES
   double fes_factor_reference;
   double fes_factor_target;
   unsigned int fes_coulomb_steps;
   unsigned int fes_vdw_steps;
   std::vector<double> fes_coulomb_factors;
   std::vector<double> fes_vdw_factors;
   int fes_switch_molecule;


   // simulation settings
   unsigned int maxmoves;
   double stepmax;
   double temperature;
   double theta_max;
   unsigned int wf_adjust;
   double v_elast_constrain;
   double v_elast_radius_constrain;
   int adjust_max_step;
   int adjust_max_theta;
   bool restart;
   std::string geometry;
   double acceptance_ratio;

   // Rosenbluth
   int rosenbluth_steps;
   int rosenbluth_trials;

   // IO settings
   int print_every;
   int print_energy;
   std::string outfile;

   // QM input strings 
   std::string grid_input;
   std::string pipe_input;
   
   // seed
   int seed;
   bool has_seed_path;
   std::string seed_path;

   // setup
   unsigned int num_devices;
   unsigned int num_chains;
   unsigned int skip_first;
   unsigned int n_default_autonomous;
   unsigned int max_inner_steps;
   unsigned int n_save_systems;

   // Analysis
   std::string trajectory;
   unsigned int first_frame;
   unsigned int last_frame;

   // Analysis.RDF
   int rdf_first_id;
   int rdf_second_id;
   double rdf_delta;
   unsigned int rdf_max_bin;

   // Analysis.RDF_angle
   double rdf_angle_delta;
   int rdf_angle_max_bin;
   int rdf_angle_id;

   // set Molpro executables
   std::string molpro_grid_exe;
   std::string molpro_pipe_exe;
   // used by jobs which don't know about grid & pipe
   std::string molpro_exe;
   int molpro_exe_cores;
   int molpro_grid_cores;

   // default target state
   unsigned int target_state;
   bool emission;

   virtual void print();
   virtual void fprint(std::string file);

   Run(int maxmoves, double stepmax, double temperature, double theta_max,
       int adjust_max_step, int print_every, int wf_adjust,
       double v_elast_constrain, double v_elast_radius_constrain,
       int print_energy, std::string grid_input, std::string pipe_input, 
       int seed, bool has_seed_path, std::string seed_path);
   Run() = default;
   Run(const Run&) = default;

   virtual ~Run();

   void set_seed_path(const std::string path);
   
protected:
private:
};

#endif // RUN_H
