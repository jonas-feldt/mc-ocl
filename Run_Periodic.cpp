/**
 *
 * File: Run_Periodic.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-02-18
 *
 */

#include "Run_Periodic.h"
#include "Run.h"

#include <iostream>
#include <stdio.h>
#include <string>

/**
 *
 * Constructor for a job with a periodic system.
 *
 * @param maxmoves number of Monte Carlo steps
 * @param stepmax maximum displacement
 * @param temperature simulation temperature
 * @param cutoff_coulomb cutoff for coulomb interaction
 * @param cutoff_vdw cutoff for Van der Waals interaction
 * @param dimension_box dimension of the periodic box
 * @param print energy every # steps
 * @param basis set for QM
 * @param memory for QM
 * @param charge of QM part
 */
Run_Periodic::Run_Periodic (
      int maxmoves,
      double stepmax,
      double temperature,
      double theta_max,
      int wf_updates,
      int adjust_max_step,
      int print_confs,
      double cutoff_coulomb,
      double cutoff_vdw,
      double dimension_box,
      int print_energy,
      const std::string grid_input,
      const std::string pipe_input,
      int seed,
      bool has_seed_path,
      std::string seed_path)
:  Run (maxmoves, stepmax, temperature, theta_max, adjust_max_step, print_confs,
       wf_updates, v_elast_constrain, v_elast_radius_constrain, print_energy,
       grid_input, pipe_input , seed, has_seed_path, seed_path),
   dimension_box (dimension_box)
{
   this->stepmax /= dimension_box;
   this->cutoff_coulomb = cutoff_coulomb / dimension_box;
   this->cutoff_vdw     = cutoff_vdw     / dimension_box;
}




void Run_Periodic::set_dimension_box (const double dimension)
{
   dimension_box = dimension;
   stepmax /= dimension_box;
   cutoff_coulomb = cutoff_coulomb / dimension_box;
   cutoff_vdw     = cutoff_vdw     / dimension_box;
}



/**
 * Checks if the settings of this run are physically/chemically meaningfull.
 *
 * @param outfile main output file
 * @return true for sane settings, false otherwise
 */
bool Run_Periodic::is_sane(std::string file) {
   if (cutoff_coulomb > 0.5) {
      FILE *out = fopen (file.c_str (), "a");
      fprintf (out, "ERROR: The cutoff for the Coulomb interactions is larger than half of the box dimension.\n");
      fprintf (out, "       Decrease the cutoff or/and increase the box dimension.\n");
      fclose (out);
      return false;
   }
   if (cutoff_vdw > 0.5) {
      FILE *out = fopen (file.c_str (), "a");
      fprintf (out, "ERROR: The cutoff for the VdW interactions is larger than half of the box dimension.\n");
      fprintf (out, "       Decrease the cutoff or/and increase the box dimension.\n");
      fclose (out);
      return false;
   }
   return true;
}



void Run_Periodic::print () {
   stepmax *= dimension_box;
   Run::print ();
   stepmax /= dimension_box;
   printf (" Cutoff Coulomb       %6.2f Angstrom \n", cutoff_coulomb * dimension_box);
   printf (" Cutoff Van der Waals %6.2f Angstrom\n", cutoff_vdw * dimension_box);
   printf (" Dimension Box        %6.2f Angstrom\n", dimension_box);
}



void Run_Periodic::fprint (std::string file) {
   stepmax *= dimension_box;
   Run::fprint (file);
   stepmax /= dimension_box;
   FILE *out = fopen (file.c_str (), "a");
   fprintf (out, " Cutoff Coulomb       %6.2f Angstrom \n", cutoff_coulomb * dimension_box);
   fprintf (out, " Cutoff Van der Waals %6.2f Angstrom\n", cutoff_vdw * dimension_box);
   fprintf (out, " Dimension Box        %6.2f Angstrom\n", dimension_box);
   fclose (out);
}
