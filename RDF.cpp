/*
 * File:   RDF.cpp
 * Author: Jonas Feldt
 *
 * Version: 2014-02-18
 */

#include <iostream>

#include "RDF.h"
#include "io_utils.h"
#include "typedefs.h"


RDF::RDF(std::string trajectory_file,
      int first_ID, int second_ID, double delta, int max_bin, int first_frame,
      int last_frame, std::string output_file, const unsigned int num_chains)
   : TrajectoryAnalyzer(trajectory_file, first_frame, last_frame, output_file, num_chains),
   first (first_ID),
   second (second_ID),
   max_bin (max_bin),
   delta (delta)
{
   histogram = new long[max_bin];
   for (int i = 0; i < max_bin; ++i) {
      histogram[i] = 0.0;
   }
   normalized = new double[max_bin];
}



RDF::~RDF () {
   delete[] histogram;
   delete[] normalized;
}


void RDF::analyze () {

   int num = system->natom;
   int *atom_types = system->atom_type;
   int *molecules  = system->molecule;
   DistanceCall GetDistance = system->GetDistance;

   for (int i = 0; i < num; ++i) {
      if (atom_types[i] != first) continue;          // wrong id
      for (int j = 0; j < num; ++j) {
         if (i == j) continue;                       // same atom
         if (atom_types[j] != second) continue;      // wrong id
         if (molecules[i] == molecules[j]) continue; // same molecule
         int bin = static_cast<int> (GetDistance (i, j) / delta);
         if (bin >= max_bin) continue;               // distance too large
         histogram[bin] += 1;
      }
   }
}


/**
 * Creates normalized distribution function for non-periodic systems.
 */
void RDF::finalize () {
   // counts number of pairs
   int pairs = 0;
   int num = system->natom;
   int *atom_types = system->atom_type;
   int *molecules = system->molecule;
   DistanceCall GetDistance = system->GetDistance;

   for (int i = 0; i < num; ++i) {
      if (atom_types[i] != first)          continue; // wrong id
      for (int j = 0; j < num; ++j) {
         if (i == j) continue;                       // same atom
         if (atom_types[j] != second)      continue; // wrong id
         if (molecules[i] == molecules[j]) continue; // same molecule
         int bin = static_cast<int>(GetDistance (i, j) / delta);
         if (bin >= max_bin)               continue; // distance too large
         pairs += 1;
      }
   }

   double c = 1.0 / ((double) pairs * (double) steps);
   for (int i = 0; i < max_bin; ++i) {
      normalized[i] = (double) histogram[i] * c;
   }
}



void RDF::save_output () {
   if (file_exists (output_file)) { // TODO check this at the beginning
      std::cerr << "Output file already exists: " << output_file << std::endl;
      return;
   }

   FILE *out = fopen(output_file.c_str(), "w");
   fprintf(out, "%10s %10s %10s %10s\n", "bin", "distance", "histogram",
           "normalized");
   double d_half = delta / 2.0;
   for (int i = 0; i < max_bin; ++i) {         // loop over bins
      fprintf(out, "%10i %10.3f %10li %10.4f\n", i, (i * delta) + d_half,
              histogram[i], normalized[i]);
   }
   fclose (out);
}
