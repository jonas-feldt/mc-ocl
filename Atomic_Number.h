/*
 * File:   Atomic_Number.h
 * Author: Jonas Feldt
 *
 * Version: 2013-08-20
 */

#ifndef ATOMIC_NUMBER_H
#define	ATOMIC_NUMBER_H

#include <string>
#include <map>

/**
 * Atomic number for He to UUo.
 */
class Atomic_Number {
public:
   Atomic_Number();
   virtual ~Atomic_Number();
   int get_z(std::string element);
private:
   std::map<std::string,int> z;
};

#endif	/* ATOMIC_NUMBER_H */

