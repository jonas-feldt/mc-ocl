/*
 * File:   MMMC_Periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2015-11-30
 */

#ifndef MMMC_PERIODIC_H
#define	MMMC_PERIODIC_H

#include <string>

#include "typedefs.h"

class Run_Periodic;
class OPLS_Periodic;
class OPLS_Periodic_Softcore;
class System_Periodic;
class ChainResults;

/**
 * MM MC with periodic boundary conditions
 */
class MMMC_Periodic {
public:

   MMMC_Periodic(
         const std::string base_name,
         const Run_Periodic *run,
         const OPLS_Periodic *opls,
         const int num_devices,
         const int num_chains);
   virtual ~MMMC_Periodic();

   vvs run(System_Periodic *system);

   vvs run(vvs &systems);

private:

   const std::string m_base_name;
   Run_Periodic *m_run;
   const OPLS_Periodic *opls;
   const int num_devices;
   const int num_chains;

   vvs run (vvs &systems, const vvd &energies_old);

   std::tuple<ChainResults *, System_Periodic *> run_chain(
         System_Periodic *system_start,
         const int chain_id,
         const int seed,
         std::string basename,
         double energy_old);

};

#endif	/* MMMC_PERIODIC_H */

