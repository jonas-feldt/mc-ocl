

#include "gtest/gtest.h"
#include "System.h"
#include "System_Periodic.h"
#include "distance_matrix.h"
#include "io_utils.h"
#include "geom_utils.h"
#include "Rotate_Translate_Stepper.h"
#include "OPLS.h"
#include "typedefs.h"
#include "Run.h"


TEST (Stepper, accep_tstep) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   System *old = new System (*system);

   EXPECT_EQ (system->GetDistance (1, 0), 1.0);
   EXPECT_EQ (old->GetDistance (1, 0), 1.0);

   system->rz[0] = -1.0;
   system->UpdateDistances ();

   EXPECT_EQ (system->GetDistance (1, 0), 2.0);

   copy_coordinates (system, old, 0);
   old->UpdateDistances (0);

   EXPECT_EQ (old->GetDistance (1, 0), 2.0);

   delete old;
   delete system;
}


TEST (Stepper, reject_step) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   System *old = new System (*system);

   EXPECT_EQ (system->GetDistance (1, 0), 1.0);
   EXPECT_EQ (old->GetDistance (1, 0), 1.0);

   system->rz[0] = -1.0;
   system->UpdateDistances ();

   EXPECT_EQ (system->GetDistance (1, 0), 2.0);

   copy_coordinates (old, system, 0);
   system->UpdateDistances (0);

   EXPECT_EQ (system->GetDistance (1, 0), 1.0);

   delete old;
   delete system;
}




TEST (Stepper, accept_step_periodic) {
   const double box_dim = 10.0;
   System_Periodic *system = read_tinker_periodic_system("gtest_opls.xyz", box_dim);
   System_Periodic *old = new System_Periodic (*system);

   EXPECT_EQ (system->GetDistance (1, 0), 0.1);
   EXPECT_EQ (old->GetDistance (1, 0), 0.1);

   system->rz[0] = -1.0 / box_dim;
   system->UpdateDistances ();

   EXPECT_EQ (system->GetDistance (1, 0), 0.2);

   copy_coordinates (system, old, 0);
   old->UpdateDistances (0);

   EXPECT_EQ (old->GetDistance (1, 0), 0.2);

   delete old;
   delete system;
}



TEST (Stepper, reject_step_periodic) {
   const double box_dim = 10.0;
   System_Periodic *system = read_tinker_periodic_system("gtest_opls.xyz", box_dim);
   System_Periodic *old = new System_Periodic (*system);

   EXPECT_EQ (system->GetDistance (1, 0), 0.1);
   EXPECT_EQ (old->GetDistance (1, 0), 0.1);

   system->rz[0] = -1.0 / box_dim;
   system->UpdateDistances ();

   EXPECT_EQ (system->GetDistance (1, 0), 0.2);

   copy_coordinates (old, system, 0);
   system->UpdateDistances (0);

   EXPECT_EQ (system->GetDistance (1, 0), 0.1);

   delete old;
   delete system;
}



TEST (Stepper, rotate_translate_stepper) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   System *old = new System (*system);

   // sets up opls
   std::vector<int> types {1};
   std::vector<double> epsilons {0.0};
   std::vector<double> sigmas {0.0};
   std::vector<double> charges {0.0};
   OPLS opls {system, types, epsilons, sigmas, charges, 1};

   // sets up run for stepper
   //Run run {};
   //run.stepmax = 1.0;
   //run.theta_max = 10.0;
   //run.temperature = 298.0;
   //run.adjust_max_step = 1000;
   //run.adjust_max_theta = 0;
   //run.acceptance_ratio = 0.2;

   //// sets up stepper
   //std::random_device r;
   //random_engine e(r());
   //Rotate_Translate_Stepper stepper {&e, system, &run};

   //stepper.step (system, &opls);
   //EXPECT_EQ (stepper.dE (system, old, &opls), opls.energy (system, system_old, 1));


   delete old;
   delete system;
}
