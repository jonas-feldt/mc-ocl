/*
 * File:    OPLS_Switcher_factor.cpp
 * Author:  Jonas Feldt
 *
 * Version: 2015-11-04
 */


#include "OPLS_Switcher_factor.h"


OPLS_Switcher_factor::OPLS_Switcher_factor (
      const System *system, Run *run,
      std::vector<double> &factors)
   : OPLS_Switcher (system, run), factors (factors) 
{
}



OPLS_Switcher_factor::~OPLS_Switcher_factor ()
{
}



double OPLS_Switcher_factor::compute_factor (
      const unsigned int frame)
{
   return factors[frame];
}
