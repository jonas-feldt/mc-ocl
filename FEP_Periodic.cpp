/*
 * File:   FEP_Periodic.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-01-06
 */

#include <iostream>
#include <math.h>
#include <iomanip>
#include <string>
#include <sstream>
#include <stdio.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <signal.h>

#include "PMC_Periodic.h"
#include "FEP_Periodic.h"
#include "FEP_OCLmanager.h"
#include "jonas_io_utils.h"
#include "math_utils.h"
#include "geom_utils.h"
#include "System_Periodic.h"
#include "Run_Periodic.h"
#include "OPLS_Periodic.h"
#include "Charge_Grid.h"
#include "typedefs.h"

#include "options.h" // Header with all run options.

/**
 * Constructor
 */
FEP_Periodic::FEP_Periodic (Run_Periodic *run) 
   : m_run (run)
{
}



/**
 * Destructor
 */
FEP_Periodic::~FEP_Periodic () {
}




vvs FEP_Periodic::run (
      vvs &systemsA,
      System_Periodic *systemB,
      Run_Periodic *run_original,
      OPLS_Periodic *opls,
      OPLS_Periodic *oplsB,
      std::string base_name,
      int num_devices,
      int num_chains) {

   VERBOSE_LVL_INIT("[INIT] Making directory " << base_name << "..."<< std::endl);
   make_directory_jf (base_name);
   VERBOSE_LVL_INIT("[INIT] Writing lattice file..."<< std::endl);
   write_dummy_latt_file_jf (base_name);
   VERBOSE_LVL_INIT("[INIT] Gas phase update wf.."<< std::endl);
   // uses just the first system here
   m_energy_qm_gp = PMC_Periodic::qm_gas_phase_updates_wf (
         systemsA[0][0], base_name, m_run);

   return run (systemsA, systemB, run_original, opls, oplsB, base_name,
         num_devices, num_chains, m_energy_qm_gp);
}


/**
 * Runs the FEP PMC simulation.
 *
 * @param systemA
 * @param systemB
 * @param run
 * @param opls
 * @param base_name
 */
vvs FEP_Periodic::run (
      vvs &systemsA,
      System_Periodic *systemB,
      Run_Periodic *run_original,
      OPLS_Periodic *opls,
      OPLS_Periodic *oplsB,
      std::string base_name,
      int num_devices,
      int num_chains,
      double energy_qm_gp) {

   Run_Periodic *run = new Run_Periodic(*run_original);

   // all vectors (of vectors) are guaranteed to have the same size,
   // therefore they are iterated with the unchecked operator[] in the case
   // that simultaneous iterations are necessary
   vvd energies_mmA      (systemsA.size(), vd(systemsA.begin()->size())); // current MM energies
   vvd energies_oldA     (systemsA.size(), vd(systemsA.begin()->size())); // total energies of the last step
   vvd energies_oldB     (systemsA.size(), vd(systemsA.begin()->size()));
   vvd energies_vdw_qmmmA(systemsA.size(), vd(systemsA.begin()->size())); // current vdW and Coulomb MM energies
   vvd energies_vdw_qmmmB(systemsA.size(), vd(systemsA.begin()->size()));
   vvd energies_qmA      (systemsA.size(), vd(systemsA.begin()->size())); // current QM & Coulomb QMMM energy
   vvd energies_qmB      (systemsA.size(), vd(systemsA.begin()->size()));

   int update_lists_every;

   double stepmax       = run->stepmax;
   double temperature   = run->temperature;
   double theta_max     = run->theta_max;

   int old_adjust_stepmax=run->adjust_max_step;
   int old_maxmoves=run->maxmoves;
   //Adjust maxmoves according to number of chains
   run->maxmoves = (run->maxmoves)/(num_chains * num_devices);

   if(run->wf_adjust>run->maxmoves) run->wf_adjust=0;//No WFU.

   if(run->adjust_max_step==0){
      //update_lists_every must be the closest multiple to m_wf_adjust
      if (run->wf_adjust==0)
	 update_lists_every = run->n_default_autonomous;
      else if(run->n_default_autonomous > run->wf_adjust)
         update_lists_every =(run->n_default_autonomous / run->wf_adjust) * run->wf_adjust;
      else
         update_lists_every = run->wf_adjust;

   } else {
      // update_lists_every and ajdust_max_step
      // must be the closest multiple to m_wf_adjust
      if (run->wf_adjust!=0) {
         run->adjust_max_step =(run->adjust_max_step / run->wf_adjust) * run->wf_adjust;
      }
      update_lists_every = run->adjust_max_step;
   } 
     
   if(run->wf_adjust!=0){
      //max_moves should be a multiple of wf_adjust
      run->maxmoves =(ceil((double)run->maxmoves/(double)run->wf_adjust))*run->wf_adjust;
   }

   VERBOSE_LVL_WARN("[WARN] Set Update Lists Every : " << update_lists_every<<"(was "<<run->n_default_autonomous<<")"<< std::endl);
   VERBOSE_LVL_WARN("[WARN] Set Step Max : " << run->adjust_max_step <<"(was "<<old_adjust_stepmax<<")" << std::endl);
   VERBOSE_LVL_WARN("[WARN] Set Maxmoves : " << run->maxmoves <<"(was "<<old_maxmoves<<")" << std::endl);
   VERBOSE_LVL_INIT("[INIT] Making directory " << base_name << "..."<< std::endl);

   make_directory_jf (base_name);

   // init pipe hosts
   Pipe_Host *hosts[2 * num_devices * num_chains];
   int index = 0;
   int iname = 0;
   for (auto outer_it = systemsA.begin(); outer_it != systemsA.end(); ++outer_it) {
      for (auto it = outer_it->begin(); it != outer_it->end(); ++it) {

         // system A
         std::string tmp = "pipeA" + std::to_string (iname);
         hosts[index] = new Pipe_Host(base_name, run->molpro_pipe_exe);
         write_molecule_qm_jf (run->pipe_input, base_name, tmp, (*it));
         hosts[index]->launch_child_process (tmp + ".inp");
         index++;

         // system B
         tmp = "pipeB" + std::to_string (iname);
         hosts[index] = new Pipe_Host(base_name, run->molpro_pipe_exe);
         write_molecule_qm_jf (run->pipe_input, base_name, tmp, systemB);
         hosts[index]->launch_child_process (tmp + ".inp");
         index++;
         iname++;
      }
   }

   vvg gridsA;
   vvg gridsB;
   gridsA.reserve(systemsA.size());
   gridsB.reserve(systemsA.size());
   for (unsigned int i = 0; i < systemsA.size(); ++i) {
      vg tmpA;
      vg tmpB;
      tmpA.reserve(systemsA.begin()->size());
      tmpB.reserve(systemsA.begin()->size());
      for (unsigned int j = 0; j < systemsA.begin()->size(); ++j) {

         System_Periodic *systemA = systemsA[i][j];
         // QM with lattice to generate initial grid for A & B
         VERBOSE_LVL_INIT("[INIT] LatticeA update..."<< std::endl);
         energies_qmA[i][j] = PMC_Periodic::qm_lattice_updates_wf (
               i, j, systemA, opls, base_name, m_run);
         const std::string folder = std::to_string(i) + "-" + std::to_string(j);
         VERBOSE_LVL_INIT("\tInitializing GridA (and reading "<< base_name + "/" + folder + GRID_FILE << ")" << std::endl);
         Charge_Grid *gridA = new Charge_Grid ();
         gridA->read_density (base_name + "/" + folder + GRID_FILE);
         gridA->reduced_coords (run->dimension_box, run->cutoff_coulomb);
         if (!gridA->is_sane(base_name + ".out") && !IGNORE_ERRORS) exit(EXIT_FAILURE);
         VERBOSE_LVL_INIT("\tGrid Points " << gridA->dim << std::endl);
         tmpA.push_back(gridA);

         VERBOSE_LVL_INIT("[INIT] LatticeB update..."<< std::endl);
         energies_qmB[i][j] = qm_lattice_updates_wf (
               base_name, i, j, systemB, systemA, oplsB, m_run);
         VERBOSE_LVL_INIT("\tInitializing GridB (and reading "<< base_name + "/B-" + folder + GRID_FILE << ")" << std::endl);
         Charge_Grid *gridB = new Charge_Grid ();
         gridB->read_density (base_name + "/B-" + folder + GRID_FILE);
         gridB->reduced_coords (run->dimension_box, run->cutoff_coulomb);
         if (!gridB->is_sane(base_name + ".out") && !IGNORE_ERRORS) exit(EXIT_FAILURE);
         VERBOSE_LVL_INIT("\tGrid Points " << gridB->dim << std::endl);
         tmpB.push_back(gridB);

         VERBOSE_LVL_INIT("[INIT] System MM atoms=" << systemA->natom << std::endl);
         VERBOSE_LVL_INIT("[INIT] System MM atoms=" << systemB->natom << std::endl);
         VERBOSE_LVL_INIT("[INIT] Computing first MM..."<< std::endl);
         systemA->UpdateDistances ();
         energies_vdw_qmmmA[i][j] = opls->energy_vdw_qmmm (systemA->GetDistance);
         energies_mmA      [i][j] = opls->energy_mm       (systemA->GetDistance);
         // XXX switches of the parameters that effect energy_mm are neglected here
         energies_vdw_qmmmB[i][j] = oplsB->energy_vdw_qmmm (systemA, systemB);
         VERBOSE_LVL_INIT("\tMM energy A: " << energies_mmA[i][j] << std::endl);
         VERBOSE_LVL_INIT("\tvdw_qmmm energy A: " << energies_vdw_qmmmA[i][j] << std::endl);
         VERBOSE_LVL_INIT("\tvdw_qmmm energy B: " << energies_vdw_qmmmB[i][j] << std::endl);

         energies_oldA[i][j] = energies_qmA[i][j] + energies_vdw_qmmmA[i][j] + energies_mmA[i][j];
         energies_oldB[i][j] = energies_qmB[i][j] + energies_vdw_qmmmB[i][j] + energies_mmA[i][j]; // energyMM of A
      }
      gridsA.push_back(tmpA);
      gridsB.push_back(tmpB);
   }

   //
   // The main code is in ocl::run
   //
   VERBOSE_LVL_INIT("[INIT] Initializing OCL..." << std::endl);
   vvs final_systems;
   OCLmanager *ocl;
   try{
      int tmp = 0;
      if (run->print_energy != 0) {
         tmp = run->print_every / run->print_energy * run->n_save_systems * N_ENERGY_SAVE_PARAMS_FEP;
      }
      ocl = create_manager (
            update_lists_every,  // Max number of steps OCL can run between list refreshes
            10,                  // 10 max kernels
            num_devices,         // number of devices
            num_chains,          // number of chains
            systemsA,            // ptr to system
            systemB,
            opls,                // ptr to opls
            oplsB,
            run,                 // ptr to run
            gridsA,              // ptr to grid
            gridsB,
                                 // size of save-config data in opencl devices:
            systemsA[0][0]->natom*run->n_save_systems*3+ run->n_save_systems * N_ENERGY_SAVE_PARAMS_FEP,
            tmp,
                                 // Some run parameters:
            temperature,
            theta_max,
            stepmax,
            energies_qmA,
            energies_qmB,
            energy_qm_gp,
            energies_mmA,
            energies_vdw_qmmmA,
            energies_vdw_qmmmB,
            energies_oldA,
            energies_oldB,
            hosts,
            base_name
            );

      // Init Opencl, find platforms, contexts, devices
      VERBOSE_LVL_INIT("[INIT] FEP_Periodic Scope: Initing PMC" << std::endl);
      ocl->init();

      VERBOSE_LVL_INIT("[INIT] FEP_Periodic Scope: Starting PMC" << std::endl);
      ocl->run();

      ocl->final_profile();
      final_systems = ocl->get_final_systems();

   } catch(cl::Error &error){//openCL error catching
      std::cout<<"FEP_PERIODIC::Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      exit (EXIT_FAILURE);
   }

   VERBOSE_LVL_SYNC("[SYNC] Master thread will shutdown");
   for (auto outer_it = gridsA.begin(); outer_it != gridsA.end(); ++outer_it) {
      for (auto it = outer_it->begin(); it != outer_it->end(); ++it) {
         delete (*it);
      }
   }
   for (auto outer_it = gridsB.begin(); outer_it != gridsB.end(); ++outer_it) {
      for (auto it = outer_it->begin(); it != outer_it->end(); ++it) {
         delete (*it);
      }
   }
   delete ocl;
   delete run;
   for (int i = 0; i < 2 * num_devices * num_chains; i++){
      delete hosts[i];
   }

   return final_systems;
}




/**
 * QM calculation with full lattice that generates or updates the wavefunction.
 *
 * @param base_name base name of this job
 * @param counter counter for all steps
 * @param systemB only QM molecule
 * @param systemA MM lattice
 * @param opls
 * @return energy that Molpro shows as final result.
 */
double FEP_Periodic::qm_lattice_updates_wf (std::string base_name,
        int device, int chain, System_Periodic *systemB,
        System_Periodic *systemA, OPLS_Periodic *opls, const Run *run) {
   const std::string lattice_name = "lattice";
   const std::string input_name = "qm_lattice";
   std::string folder = "B-" + std::to_string(device) + "-" + std::to_string(chain);
   base_name += "/" + folder;
   make_directory_jf(base_name);

   write_molecule_qm_jf (run->grid_input, base_name, input_name, systemB);
   write_latt_file_name_jf (base_name, lattice_name, systemA, opls->get_normal_charges ());
   put_latt_to_input_name_jf (base_name, input_name, lattice_name);
   molpro_lattice (base_name, input_name, run->molpro_grid_exe, run->molpro_grid_cores);
   return read_energy_molpro_jf (base_name, input_name);
}



double FEP_Periodic::get_energy_qm_gp() {
   return m_energy_qm_gp;
}




FEP_OCLmanager* FEP_Periodic::create_manager (
               int n_pre_steps, 
               int nkernel, 
               int ndevices, 
               int nchains, 
               const vvs &systemsA,
               System_Periodic* systemB,
               OPLS_Periodic* opls, 
               OPLS_Periodic* oplsB,
               Run_Periodic* run, 
               vvg &gridsA,
               vvg &gridsB,
               int config_data_size, 
               int energy_data_size, 
               _FPOINT_S_ temperature,
               double theta_max,
               double stepmax,   
               vvd &energies_qmA,
               vvd &energies_qmB,
               _FPOINT_S_ energy_qm_gp,
               vvd &energies_mmA,
               vvd &energies_vdw_qmmmA,
               vvd &energies_vdw_qmmmB,
               vvd &energies_oldA,
               vvd &energies_oldB,
               Pipe_Host **hosts,
               std::string basename)
{
   return new FEP_OCLmanager(
         n_pre_steps, 
         nkernel, 
         ndevices, 
         nchains, 
         systemsA,
         systemB,
         opls, 
         oplsB,
         run, 
         gridsA,
         gridsB,
         config_data_size, 
         energy_data_size, 
         temperature,
         theta_max,
         stepmax,   
         energies_qmA,
         energies_qmB,
         energy_qm_gp,
         energies_mmA,
         energies_vdw_qmmmA,
         energies_vdw_qmmmB,
         energies_oldA,
         energies_oldB,
         hosts,
         basename);
}
