/*
 * File: distance_matrix.h
 * Author: Jonas Feldt
 *
 * Version: 2017-10-07
 *
 * Distances of the system stored in matrix form.
 */

#ifndef DISTANCE_MATRIX_H
#define DISTANCE_MATRIX_H

#include <vector>

#include "distance_interface.h"

class System;
struct Molecule_db;

class DistanceMatrix : public DistanceInterface
{

   public:

      ~DistanceMatrix () override;
      void SetContext (System *system) override;
      DistanceMatrix *clone () const override;

      void ComputeDistances () override;
      void ComputeDistances (unsigned int molecule) override;

      void accept (DistanceInterface *other, unsigned int molecule) override;

      DistanceCall GetDelegate() const override;
      double GetDistance (unsigned int i, unsigned int j) const;

   protected:

      unsigned int dim;
      double *x;
      double *y;
      double *z;
      std::vector<Molecule_db> *mol2atom;

      double **dists = nullptr;

};


#endif // DISTANCE_MATRIX_H
