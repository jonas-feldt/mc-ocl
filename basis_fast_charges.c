#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "basis_GPT.h"

#include "boys_data.dat"


#define NTAYLOR 6
#define XSTART 0.0
#define XSTOP  15.0
#define XSTEP  0.1
#define OOXSTEP 10.0 /*  1.0/xstep  */
#define MMAX 32
#define NXVAL 151

double oof[7] = {1.0, 1.0, 1.0/2.0, 1.0/6.0, 1.0/24.0, 1.0/120.0, 1.0/720.0};


void boys_function (long int nmax, double x, double f[], double fac) {
  long int i, n, m;
  double dx, xn, expx;

  if (x < XSTART || x > XSTOP) {
    printf("Not enough data (x) in boys_function.");
    exit(1);
  }
  if ((nmax + NTAYLOR) > MMAX) {
    printf("Not enough data (m) in boys_function.");
    exit(1);
  }

  i = (int) ((x - XSTART) * OOXSTEP + 0.5);
  dx = XSTART + (double) i * XSTEP - x;
  f[nmax] = fac * bfdata[nmax][i];
  xn = fac;
  for (n = 1; n < 7; ++n) {
    xn *= dx;
    f[nmax] += bfdata[nmax + n][i] * xn * oof[n];
  }
  expx = fac * exp(-x);
  for (m = nmax -1; m > -1; --m) {
    f[m] = (2.0 * x * f[m + 1] + expx) / (double) (2 * m + 1);
  }
}



#define BOYS_LONGRANGE 34.0
#define BOYS_UPWARD 10.0
#define BOYS_MONO 1e-16

void repulsion_f(long int nmax, double x, double f[], double fac) { /* iskip = 1*/
  long int n;
  double ex, oox, rootx;
  if (x > BOYS_LONGRANGE) {
    oox = 1.0 / x;
    f[0] = fac * 0.5 * sqrt (M_PI * oox);
    for (n = 1; n <= nmax; ++n) {
      f[n] = 0.5 * f[n - 1] * (2 * n - 1) * oox;
    }
  } else if (x > BOYS_UPWARD) {
    oox = 1.0 / x;
    rootx = sqrt (x);
    ex = fac * exp (-x) * 0.5 * oox;
    f[0] = fac * 0.5 * sqrt (M_PI * oox) * erf (rootx);
    for (n = 0; n <= nmax - 1; ++n) {
      f[n + 1] = ((n + 0.5) * oox) * f[n] - ex;
    }
  } else if (x > BOYS_MONO) {
    boys_function (nmax, x, f, fac);
  } else {
    for (n = 0; n <= nmax; ++n) {
      f[n] = fac / (double) (1 + 2 * n);
    }
  }
}




void basis_shell_1idx_charges (
  long int ltot, double zeta, double p[], long int nq, double qq[],
  double rq[], double ans[]) {

  double fm[9];
  double ooz = 1.0 / zeta;
  double ooz2 = ooz * 0.5;
  double fac = 2 * M_PI * ooz;
  double rpx, rpy, rpz, rpr2;
  double G0, G1, G2;
  double Gx1, Gy1, Gz1, Gx2, Gy2, Gz2;
  double Gxx2, Gxy2, Gxz2, Gyy2, Gyz2, Gzz2, Gxx3, Gxy3, Gxz3, Gyy3, Gyz3, Gzz3;
  double Gxxx3, Gxxy3, Gxxz3, Gxyy3, Gxyz3, Gxzz3, Gyyy3, Gyyz3, Gyzz3, Gzzz3;
  double Gxxxx4, Gxxxy4, Gxxxz4, Gxxyy4, Gxxyz4, Gxxzz4, Gxyyy4, Gxyyz4, Gxyzz4, Gxzzz4, Gyyyy4, Gyyyz4, Gyyzz4, Gyzzz4, Gzzzz4;
  double t1, t2, t3, t4, t7, t11, t14, t28, t30, t34, t35, t38, t39, t42, t45, t48, t56, t59;
  long int lambda;
  if (ltot > 4) {
    printf("ERROR: ltot > 4 in my_basis_shell_1idx_charges\n");
    return;
  }
  if (ltot == 0) {
    G0 = 0;
    for (lambda = 0; lambda < nq; ++lambda) {
      rpx = rq[3 * lambda    ] - p[0];
      rpy = rq[3 * lambda + 1] - p[1];
      rpz = rq[3 * lambda + 2] - p[2];
      rpr2 = rpx * rpx + rpy * rpy + rpz * rpz;
      repulsion_f (0, zeta * rpr2, fm, fac);
      G0  += qq[lambda] * fm[0];
    }
    ans[0] = G0;
  } else if (ltot == 1) {
    G0 = 0;
    Gx1 = 0;
    Gy1 = 0;
    Gz1 = 0;
    for (lambda = 0; lambda < nq; ++lambda) {
      rpx = rq[3 * lambda    ] - p[0];
      rpy = rq[3 * lambda + 1] - p[1];
      rpz = rq[3 * lambda + 2] - p[2];
      rpr2 = rpx * rpx + rpy * rpy + rpz * rpz;
      repulsion_f (1, zeta * rpr2, fm, fac);
      G0  += qq[lambda] * fm[0];
      Gx1 += qq[lambda] * rpx * fm[1];
      Gy1 += qq[lambda] * rpy * fm[1];
      Gz1 += qq[lambda] * rpz * fm[1];
    }
    ans[0] = G0;
    ans[1] = Gx1;
    ans[2] = Gy1;
    ans[3] = Gz1;
  } else if (ltot == 2) {
    G0 = 0;
    Gx1 = 0;
    Gy1 = 0;
    Gz1 = 0;
    Gxx2 = 0;
    G1 = 0;
    Gxy2 = 0;
    Gxz2 = 0;
    Gyy2 = 0;
    Gyz2 = 0;
    Gzz2 = 0;
    lambda = 0;
    for (lambda = 0; lambda < nq; ++lambda) {
      rpx = rq[3 * lambda    ] - p[0];
      rpy = rq[3 * lambda + 1] - p[1];
      rpz = rq[3 * lambda + 2] - p[2];
      rpr2 = rpx * rpx + rpy * rpy + rpz * rpz;
      repulsion_f (2, zeta * rpr2, fm, fac);
      G0 += qq[lambda] * fm[0];
      G1 += qq[lambda] * fm[1];
      Gx1 += qq[lambda] * rpx * fm[1];
      Gy1 += qq[lambda] * rpy * fm[1];
      Gz1 += qq[lambda] * rpz * fm[1];
      Gxx2 += qq[lambda] * rpx * rpx * fm[2];
      Gxy2 += qq[lambda] * rpy * rpx * fm[2];
      Gxz2 += qq[lambda] * rpz * rpx * fm[2];
      Gyy2 += qq[lambda] * rpy * rpy * fm[2];
      Gyz2 += qq[lambda] * rpy * rpz * fm[2];
      Gzz2 += qq[lambda] * rpz * rpz * fm[2];
    }
    t2 = (G0 - G1) * ooz2;
    ans[0] = G0;
    ans[1] = Gx1;
    ans[2] = Gy1;
    ans[3] = Gz1;
    ans[4] = Gxx2 + t2;
    ans[5] = Gxy2;
    ans[6] = Gxz2;
    ans[7] = Gyy2 + t2;
    ans[8] = Gyz2;
    ans[9] = Gzz2 + t2;
  } else if (ltot == 3) {
    G0 = 0;
    Gx1 = 0;
    Gy1 = 0;
    Gz1 = 0;
    Gxx2 = 0;
    G1 = 0;
    Gxy2 = 0;
    Gxz2 = 0;
    Gyy2 = 0;
    Gyz2 = 0;
    Gzz2 = 0;
    Gxxx3 = 0;
    Gx2 = 0;
    Gxxy3 = 0;
    Gy2 = 0;
    Gxxz3 = 0;
    Gz2 = 0;
    Gxyy3 = 0;
    Gxyz3 = 0;
    Gxzz3 = 0;
    Gyyy3 = 0;
    Gyyz3 = 0;
    Gyzz3 = 0;
    Gzzz3 = 0;
    lambda = 0;
    for (lambda = 0; lambda < nq; ++lambda) {
      rpx = rq[3 * lambda    ] - p[0];
      rpy = rq[3 * lambda + 1] - p[1];
      rpz = rq[3 * lambda + 2] - p[2];
      rpr2 = rpx * rpx + rpy * rpy + rpz * rpz;
      repulsion_f (3, zeta * rpr2, fm, fac);
      G0 += qq[lambda] * fm[0];
      G1 += qq[lambda] * fm[1];
      Gx1 += qq[lambda] * rpx * fm[1];
      Gy1 += qq[lambda] * rpy * fm[1];
      Gz1 += qq[lambda] * rpz * fm[1];
      Gx2 += qq[lambda] * rpx * fm[2];
      Gy2 += qq[lambda] * rpy * fm[2];
      Gz2 += qq[lambda] * rpz * fm[2];
      Gxx2 += qq[lambda] * rpx * rpx * fm[2];
      Gxy2 += qq[lambda] * rpy * rpx * fm[2];
      Gxz2 += qq[lambda] * rpz * rpx * fm[2];
      Gyy2 += qq[lambda] * rpy * rpy * fm[2];
      Gyz2 += qq[lambda] * rpy * rpz * fm[2];
      Gzz2 += qq[lambda] * rpz * rpz * fm[2];
      Gxxx3 += qq[lambda] * rpx * rpx * rpx * fm[3];
      Gxxy3 += qq[lambda] * rpx * rpx * rpy * fm[3];
      Gxxz3 += qq[lambda] * rpx * rpx * rpz * fm[3];
      Gxyy3 += qq[lambda] * rpx * rpy * rpy * fm[3];
      Gxyz3 += qq[lambda] * rpx * rpy * rpz * fm[3];
      Gxzz3 += qq[lambda] * rpx * rpz * rpz * fm[3];
      Gyyy3 += qq[lambda] * rpy * rpy * rpy * fm[3];
      Gyyz3 += qq[lambda] * rpy * rpy * rpz * fm[3];
      Gyzz3 += qq[lambda] * rpy * rpz * rpz * fm[3];
      Gzzz3 += qq[lambda] * rpz * rpz * rpz * fm[3];
    }
    t1 = (G0 - G1) * ooz2;
    t2 = (Gx1 - Gx2) * ooz2;
    t3 = (Gy1 - Gy2) * ooz2;
    t4 = (Gz1 - Gz2) * ooz2;
    ans[0] = G0;
    ans[1] = Gx1;
    ans[2] = Gy1;
    ans[3] = Gz1;
    ans[4] = Gxx2 + t1;
    ans[5] = Gxy2;
    ans[6] = Gxz2;
    ans[7] = Gyy2 + t1;
    ans[8] = Gyz2;
    ans[9] = Gzz2 + t1;
    ans[10] = Gxxx3 + 3 * t2;
    ans[11] = Gxxy3 + t3;
    ans[12] = Gxxz3 + t4;
    ans[13] = Gxyy3 + t2;
    ans[14] = Gxyz3;
    ans[15] = Gxzz3 + t2;
    ans[16] = Gyyy3 + 3 * t3;
    ans[17] = Gyyz3 + t4;
    ans[18] = Gyzz3 + t3;
    ans[19] = Gzzz3 + 3 * t4;
  } else if (ltot == 4) {
    G0 = 0;
    Gx1 = 0;
    Gy1 = 0;
    Gz1 = 0;
    Gxx2 = 0;
    G1 = 0;
    Gxy2 = 0;
    Gxz2 = 0;
    Gyy2 = 0;
    Gyz2 = 0;
    Gzz2 = 0;
    Gxxx3 = 0;
    Gx2 = 0;
    Gxxy3 = 0;
    Gy2 = 0;
    Gxxz3 = 0;
    Gz2 = 0;
    Gxyy3 = 0;
    Gxyz3 = 0;
    Gxzz3 = 0;
    Gyyy3 = 0;
    Gyyz3 = 0;
    Gyzz3 = 0;
    Gzzz3 = 0;
    Gxxxx4 = 0;
    Gxx3 = 0;
    G2 = 0;
    Gxxxy4 = 0;
    Gxy3 = 0;
    Gxxxz4 = 0;
    Gxz3 = 0;
    Gxxyy4 = 0;
    Gyy3 = 0;
    Gxxyz4 = 0;
    Gyz3 = 0;
    Gxxzz4 = 0;
    Gzz3 = 0;
    Gxyyy4 = 0;
    Gxyyz4 = 0;
    Gxyzz4 = 0;
    Gxzzz4 = 0;
    Gyyyy4 = 0;
    Gyyyz4 = 0;
    Gyyzz4 = 0;
    Gyzzz4 = 0;
    Gzzzz4 = 0;

    lambda = 0;
    for (lambda = 0; lambda < nq; ++lambda) {
      rpx = rq[3 * lambda    ] - p[0];
      rpy = rq[3 * lambda + 1] - p[1];
      rpz = rq[3 * lambda + 2] - p[2];
      rpr2 = rpx * rpx + rpy * rpy + rpz * rpz;
      repulsion_f (4, zeta * rpr2, fm, fac);
      G0 += qq[lambda] * fm[0];
      G1 += qq[lambda] * fm[1];
      G2 += qq[lambda] * fm[2];
      Gx1 += qq[lambda] * rpx * fm[1];
      Gy1 += qq[lambda] * rpy * fm[1];
      Gz1 += qq[lambda] * rpz * fm[1];
      Gx2 += qq[lambda] * rpx * fm[2];
      Gy2 += qq[lambda] * rpy * fm[2];
      Gz2 += qq[lambda] * rpz * fm[2];
      Gxx2 += qq[lambda] * rpx * rpx * fm[2];
      Gxy2 += qq[lambda] * rpy * rpx * fm[2];
      Gxz2 += qq[lambda] * rpz * rpx * fm[2];
      Gyy2 += qq[lambda] * rpy * rpy * fm[2];
      Gyz2 += qq[lambda] * rpy * rpz * fm[2];
      Gzz2 += qq[lambda] * rpz * rpz * fm[2];
      Gxx3 += qq[lambda] * rpx * rpx * fm[3];
      Gxy3 += qq[lambda] * rpy * rpx * fm[3];
      Gxz3 += qq[lambda] * rpz * rpx * fm[3];
      Gyy3 += qq[lambda] * rpy * rpy * fm[3];
      Gyz3 += qq[lambda] * rpy * rpz * fm[3];
      Gzz3 += qq[lambda] * rpz * rpz * fm[3];
      Gxxx3 += qq[lambda] * rpx * rpx * rpx * fm[3];
      Gxxy3 += qq[lambda] * rpx * rpx * rpy * fm[3];
      Gxxz3 += qq[lambda] * rpx * rpx * rpz * fm[3];
      Gxyy3 += qq[lambda] * rpx * rpy * rpy * fm[3];
      Gxyz3 += qq[lambda] * rpx * rpy * rpz * fm[3];
      Gxzz3 += qq[lambda] * rpx * rpz * rpz * fm[3];
      Gyyy3 += qq[lambda] * rpy * rpy * rpy * fm[3];
      Gyyz3 += qq[lambda] * rpy * rpy * rpz * fm[3];
      Gyzz3 += qq[lambda] * rpy * rpz * rpz * fm[3];
      Gzzz3 += qq[lambda] * rpz * rpz * rpz * fm[3];
      Gxxxx4 += qq[lambda] * rpx * rpx * rpx * rpx * fm[4];
      Gxxxy4 += qq[lambda] * rpx * rpx * rpx * rpy * fm[4];
      Gxxxz4 += qq[lambda] * rpx * rpx * rpx * rpz * fm[4];
      Gxxyy4 += qq[lambda] * rpx * rpx * rpy * rpy * fm[4];
      Gxxyz4 += qq[lambda] * rpx * rpx * rpy * rpz * fm[4];
      Gxxzz4 += qq[lambda] * rpx * rpx * rpz * rpz * fm[4];
      Gxyyy4 += qq[lambda] * rpx * rpy * rpy * rpy * fm[4];
      Gxyyz4 += qq[lambda] * rpx * rpy * rpy * rpz * fm[4];
      Gxyzz4 += qq[lambda] * rpx * rpy * rpz * rpz * fm[4];
      Gxzzz4 += qq[lambda] * rpx * rpz * rpz * rpz * fm[4];
      Gyyyy4 += qq[lambda] * rpy * rpy * rpy * rpy * fm[4];
      Gyyyz4 += qq[lambda] * rpy * rpy * rpy * rpz * fm[4];
      Gyyzz4 += qq[lambda] * rpy * rpy * rpz * rpz * fm[4];
      Gyzzz4 += qq[lambda] * rpy * rpz * rpz * rpz * fm[4];
      Gzzzz4 += qq[lambda] * rpz * rpz * rpz * rpz * fm[4];
    }
    t2 = (G0-G1)*ooz2;
    t7 = (Gx1-Gx2)*ooz2;
    t11 = (Gy1-Gy2)*ooz2;
    t14 = (Gz1-Gz2)*ooz2;
    t28 = (G1-G2)*ooz2;
    t30 = (Gxx2+t2-Gxx3-t28)*ooz2;
    t34 = (Gxy2-Gxy3)*ooz2;
    t35 = 3 * t34;
    t38 = (Gxz2-Gxz3)*ooz2;
    t39 = 3 * t38;
    t42 = (Gyy2-Gyy3)*ooz2;
    t45 = (Gyz2-Gyz3)*ooz2;
    t48 = (Gzz2-Gzz3)*ooz2;
    t56 = (Gyy2+t2-Gyy3-t28)*ooz2;
    t59 = 3*t45;
    ans[0] = G0;
    ans[1] = Gx1;
    ans[2] = Gy1;
    ans[3] = Gz1;
    ans[4] = Gxx2 + t2;
    ans[5] = Gxy2;
    ans[6] = Gxz2;
    ans[7] = Gyy2 + t2;
    ans[8] = Gyz2;
    ans[9] = Gzz2 + t2;
    ans[10] = Gxxx3 + 3 * t7;
    ans[11] = Gxxy3 + t11;
    ans[12] = Gxxz3 + t14;
    ans[13] = Gxyy3 + t7;
    ans[14] = Gxyz3;
    ans[15] = Gxzz3 + t7;
    ans[16] = Gyyy3 + 3 * t11;
    ans[17] = Gyyz3 + t14;
    ans[18] = Gyzz3 + t11;
    ans[19] = Gzzz3 + 3 * t14;
    ans[20] = Gxxxx4 + 3 * (Gxx2 - Gxx3) * ooz2 + 3 * t30;
    ans[21] = Gxxxy4 + t35;
    ans[22] = Gxxxz4 + t39;
    ans[23] = Gxxyy4 + t42 + t30;
    ans[24] = Gxxyz4 + t45;
    ans[25] = Gxxzz4 + t48 + t30;
    ans[26] = Gxyyy4 + t35;
    ans[27] = Gxyyz4 + t38;
    ans[28] = Gxyzz4 + t34;
    ans[29] = Gxzzz4 + t39;
    ans[30] = Gyyyy4 + 3 * t42 + 3 * t56;
    ans[31] = Gyyyz4 + t59;
    ans[32] = Gyyzz4 + t48 + t56;
    ans[33] = Gyzzz4 + t59;
    ans[34] = Gzzzz4 + 3 * t48 + 3 * (Gzz2 + t2 - Gzz3 - t28) * ooz2;
  }
}



void basis_shell_pt_charges (
  double xa, double A[], long int lmina, long int lmaxa, long int na,
  double xb, double B[], long int lminb, long int lmaxb, long int nb,
  double shell[], long int ioa, long int iob, long int isa, long int isb,
  long int nq, double qq[], double rq[]) {

  double ans[220];
  double P[3];
  double zeta = xa + xb;
  double ooz = 1.0 / zeta;
  long int ltot = lmaxa + lmaxb;
  long int np = (ltot + 1) * (ltot + 2) * (ltot + 3) / 6;
  double dx = A[0] - B[0];
  double dy = A[1] - B[1];
  double dz = A[2] - B[2];
  double rAB2 = dx * dx + dy * dy + dz * dz;
  double fac = exp (-xa * xb * ooz * rAB2);

  P[0] = (xa * A[0] + xb * B[0]) * ooz;
  P[1] = (xa * A[1] + xb * B[1]) * ooz;
  P[2] = (xa * A[2] + xb * B[2]) * ooz;
  basis_shell_1idx_charges (ltot, zeta, P, nq, qq, rq, ans);
  basis_GPT_contract_vec (shell, ioa, iob, isa, isb, ans,
    xa, A, lmina, lmaxa, na,
    xb, B, lminb, lmaxb, nb,
    zeta, P, np, fac);
}
