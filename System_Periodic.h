/*
 * File:   System_Periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2014-10-14
 */

#ifndef SYSTEM_PERIODIC_H
#define	SYSTEM_PERIODIC_H

#include <string.h>

#include "System.h"

class DistanceInterface;

/**
 * This class contains the box dimension and contains coordinates in
 * reduced units and therefore computes also distances in reduced
 * units. (distances/boxDimension)
 *
 */
class System_Periodic : public System {
public:

   double box_dimension;

   bool is_sane(double cutoff_coulomb, double cutoff_vdw);
   virtual void print();

   void translate(double x, double y, double z, int mol);

   void save_all(std::string file) override;
   void save_xyz(std::string file) override;

   std::vector<double*> get_normal_coords() override;
   void wrap (unsigned int molecule);


   System_Periodic(unsigned int natom, double rx[], double ry[], double rz[],
           int atom_type[], std::string atom_name[], int n_12[], int connect[][4],
           double box_dimension, DistanceInterface *distances);
   System_Periodic(System_Periodic &other);
   System_Periodic(System_Periodic &other, int mol);
   ~System_Periodic () override;

private:
   void normal_coordinate_helper (const Molecule_db &mol, double com,
         double *r, double *n);

   double* normal_x;
   double* normal_y;
   double* normal_z;
};

#endif	/* SYSTEM_PERIODIC_H */

