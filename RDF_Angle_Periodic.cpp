/*
 * File:   RDF_Angle_Periodic.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-05-12
 */

#include <iostream>
#include <math.h>
#include <vector>

#include "RDF_Periodic.h"
#include "RDF_Angle_Periodic.h"
#include "io_utils.h"
#include "options.h"
#include "geom_utils.h"
#include "Constants.h"
#include "typedefs.h"



RDF_Angle_Periodic::RDF_Angle_Periodic(std::string trajectory_file,
         int first_ID, int second_ID, double delta, int max_bin, int first_frame,
         int last_frame, std::string output_file, double box_dim,
         const unsigned int num_chains, const double delta_angle,
         const int max_bin_angle, const int id)
   : RDF_Periodic (trajectory_file, first_ID, second_ID, delta, max_bin,
         first_frame, last_frame, output_file, box_dim, num_chains),
     id (id),
     delta_angle (delta_angle),
     max_bin_angle (max_bin_angle)
{
   // allocates and inits with 0.0
   histogram = new long*[max_bin];
   for (int i = 0; i < max_bin; ++i) {
      histogram[i] = new long[max_bin_angle];
      for (int j = 0; j < max_bin_angle; ++j) {
         histogram[i][j] = 0.0;
      }
   }

   // allocates
   normalized = new double*[max_bin];
   for (int i = 0; i < max_bin; ++i) {
      normalized[i] = new double[max_bin_angle];
   }
}



RDF_Angle_Periodic::~RDF_Angle_Periodic () {
   for (int i = 0; i < max_bin; ++i) {
      delete[] histogram[i];
      delete[] normalized[i];
   }
   delete[] histogram;
   delete[] normalized;
}




void RDF_Angle_Periodic::analyze () {

   const int num = system->natom;
   const int *atom_types = system->atom_type;
   const int *molecules  = system->molecule;
   std::vector<Molecule_db> mol2atom = system->molecule2atom;
   DistanceCall GetDistance = system->GetDistance;
   const double *rx = system->rx;
   const double *ry = system->ry;
   const double *rz = system->rz;

   for (int i = 0; i < num; ++i) {
      if (atom_types[i] != first) continue;          // wrong id
      for (int j = 0; j < num; ++j) {
         if (i == j) continue;                       // same atom
         if (atom_types[j] != second) continue;      // wrong id
         if (molecules[i] == molecules[j]) continue; // same molecule
         const double dist = GetDistance (i, j);
         const int bin = static_cast<int> (dist / delta);
         if (bin >= max_bin) continue;               // distance too large

         // searchs for id in the molecule of j
         const Molecule_db &mol = mol2atom[molecules[j]];
         for (int pos = 0; pos < mol.natoms; ++pos) {
            int k = mol.atom_id[pos];
            if (j == k) continue;
            if (atom_types[k] != id) continue;

            // compute angle i-k-j
            const double angle = compute_minimal_angle (
                 rx[i], ry[i], rz[i], 
                 rx[k], ry[k], rz[k], 
                 rx[j], ry[j], rz[j]) * RAD_TO_DEGREE;
            const int bin_angle = static_cast<int> (angle / delta_angle);
            if (bin_angle >= max_bin_angle) continue;

            histogram[bin][bin_angle] += 1;
         }
      }
   }
}


/**
 * Creates the normalized distribution function for periodic systems.
 */
void RDF_Angle_Periodic::finalize () {
   // counts number of pairs
   int pairs = 0;
   int num = system->natom;
   int *atom_types = system->atom_type;
   int *molecules = system->molecule;
   for (int i = 0; i < num; ++i) {
      if (atom_types[i] != first)          continue; // wrong id
      for (int j = 0; j < num; ++j) {
         if (i == j) continue;                       // same atom
         if (atom_types[j] != second)      continue; // wrong id
         if (molecules[i] == molecules[j]) continue; // same molecule
         pairs += 1;
      }
   }

   double delta_norm = delta * box_dim;
   for (int i = 0; i < max_bin; ++i) {
      double vol = pow(delta_norm * (i + 1), 3.0) - pow(delta_norm * i, 3.0);
      double v = (4.0 / 3.0 * M_PI * vol) / pow (box_dim, 3.0);

      for (int j = 0; j < max_bin_angle; ++j) {
         double n = (double) histogram[i][j] / (double) pairs / (double) steps;
         normalized[i][j] = n / v;
      }
   }
}



void RDF_Angle_Periodic::save_output () {
   if (file_exists(output_file)) {
      std::cerr << "Output file already exists: " << output_file << std::endl;
      return;
   }

   FILE *out = fopen(output_file.c_str(), "w");
   double delta_norm = delta * box_dim;
   double d_half = delta_norm / 2.0;
   double d_angle_half = delta_angle / 2.0;

   // first row
   fprintf(out, "%10s", "Dummy");
   for (int i = 0; i < max_bin; ++i) {
      fprintf(out, " %10.3f", (i * delta_norm) + d_half);
   }
   fprintf(out, "\n");

   // following rows
   for (int i = 0; i < max_bin_angle; ++i) { // loop over rows/angle
      fprintf(out, "%10.3f", (i * delta_angle) + d_angle_half);

      for (int j = 0; j < max_bin; ++j) { // loop over columns/distance
         fprintf(out, " %10.4f", normalized[j][i]);
      }
      fprintf(out, "\n");
   }

   fclose(out);
}





