/**
 *
 * File: OCLmanager.h
 * Author: Sebastiao Miranda, Jonas Feldt
 *
 * Version: 2015-04-13
 *
 */
 
#ifndef OCLMANAGER_H_INCLUDED
#define OCLMANAGER_H_INCLUDED

#define __CL_ENABLE_EXCEPTIONS

#include<string>
#include<vector>
#include<iostream>
#include<fstream>

#include "options.h" 
#include "Run_Periodic.h" 
#include "OCLDevice.h" 
#include "typedefs.h"

#include "Pipe_Host.h"

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

class OCLmanager{

public:
         
   OCLmanager( int n_pre_steps, 
               int nkernel, 
               int ndevices, 
               int nchains, 
               const vvs &systems,
               const OPLS_Periodic* opls, 
               Run_Periodic* run, 
               vvg &grid, 
               int config_data_size, 
               int energy_data_size, 
               _FPOINT_S_ temperature,
               double theta_max,
               double stepmax,   
               const vvd &energies_qm, 
               _FPOINT_S_ energy_qm_gp,
               const vvd &energies_mm,
               const vvd &energies_vdw_qmmm,
               const vvd &energies_old,
               Pipe_Host **hosts,
               std::string   basename                      
               );
      
   virtual ~OCLmanager();
   
   //Init Opencl, Opencl Devices and Chains
   virtual void init();
   
   //Start PMCQMMM
   void run();
   
   // Returns all final systems
   vvs get_final_systems();

   //Execute final profiles, like csv or python output data
   void final_profile();
   
   //Pointers to MM data and Grid and other Stuff
   vvs              m_systems;
   const OPLS_Periodic   *m_opls   ;
   Run_Periodic    *m_run    ;
   vvg              m_grids  ;
   Pipe_Host      **m_hosts  ;
   
   //Vector of pointers to devices
   std::vector<OCLDevice*> m_devices;
         
protected:

   virtual OCLDevice *create_device (const int i);
   
   //subfunctions for init
   void device_query();
   void print_device_status();

   virtual void final_output();
   
      //Synchronization variables
   int                     *v_ignite_isReady;
   std::mutex              *v_ignite_mutex  ;
   std::condition_variable *v_ignite_slave  ;
   std::condition_variable ignite_master;
   
   #ifdef LEGACY_MULTI_DEVICE
      //Variables for barrier synchronizaton
      //, energy bcast TODO: add performance bcast
      _FPOINT_G_              *v_bcast_qmmm_c;
      _FPOINT_S_              *v_bcast_mm;
      _FPOINT_S_              *v_bcast_qmmm_vdw;
      _FPOINT_S_              *v_bcast_qmmm_c_nucl;
      
      std::mutex              *p_barrier_mutex;
      std::condition_variable *p_barrier_cond;
      int                     *p_barrier_count;
      int                      m_barrier_max;
      double                  *m_bal;
   #endif
   

      //Vector of pointers to chains
   std::vector<OCLChain*>  m_chains;
      //Vector of all the platforms
   std::vector<cl::Platform> m_platforms;
   
      //Variables for device query
   cl::Context       **m_captured_context ;
   cl::Device         *m_captured_device  ; 
   
   int m_ndevices;
   int m_nchains;
   int m_nchains_per_device;
   int m_config_data_size;
   int m_energy_data_size;
   int m_nkernel;
   int m_n_pre_steps;
   std::string m_basename;
   double m_stepmax;
   double m_temperature;

   _FPOINT_S_ m_energy_qm_gp     ;
   vvd m_energies_qm        ;
   vvd m_energies_mm        ;
   vvd m_energies_vdw_qmmm  ;
   vvd m_energies_old       ;

   bool has_extra_energy_file;

   std::string m_kernel_file_closure = KERNEL_FILE_CLOSURE;
   

};

#endif //OCLMANAGER_H_INCLUDED
