/**
 *
 * File: FEP_OCLmanager.cpp
 * Author: Jonas Feldt 
 *
 * Version: 2015-04-13
 *
 */
 
#include <math.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include <random>

#include "FEP_OCLmanager.h"
#include "FEP_OCLDevice.h"
#include "FEP_OCLChain.h"
#include "typedefs.h"
#include "options.h"

#define NUM_MEANS 3 // number of means

FEP_OCLmanager::FEP_OCLmanager(
                        int n_pre_steps,
                        int nkernel,
                        int ndevices,
                        int nchains,
                        const vvs &systemsA,
                        System_Periodic* systemB,
                        OPLS_Periodic* opls,
                        OPLS_Periodic* oplsB,
                        Run_Periodic* run, 
                        vvg &gridsA,
                        vvg &gridsB,
                        int config_data_size, 
                        int energy_data_size, 
                        _FPOINT_S_ temperature,
                        double theta_max,
                        double stepmax,   
                        vvd &energies_qmA, 
                        vvd &energies_qmB, 
                        _FPOINT_S_ energy_qm_gp,
                        vvd &energies_mmA,
                        vvd &energies_vdw_qmmmA,
                        vvd &energies_vdw_qmmmB,
                        vvd &energies_oldA,
                        vvd &energies_oldB,
                        Pipe_Host **hosts,
                        std::string basename
                        )
: OCLmanager(n_pre_steps, nkernel, ndevices, nchains, systemsA, opls, run,
      gridsA, config_data_size, energy_data_size, temperature, theta_max,
      stepmax, energies_qmA, energy_qm_gp, energies_mmA,
      energies_vdw_qmmmA, energies_oldA, hosts, basename),
   m_systemB (systemB),
   m_gridsB  (gridsB),
   m_oplsB   (oplsB)
{
   m_energies_qmB       = energies_qmB      ;
   m_energies_vdw_qmmmB = energies_vdw_qmmmB;
   m_energies_oldB      = energies_oldB     ;      
   #ifdef SET_FPOINT_G_DOUBLE
   m_kernel_file_closure = KERNEL_FILE_CLOSURE_FEP;
   #else
      m_kernel_file_closure = "not-existing-kernel";
   #endif
}


/**
 * Destructor
 */
FEP_OCLmanager::~FEP_OCLmanager(){
   // nothing additional
}




/**
 * Init Opencl platforms, devices, etc
 */
void FEP_OCLmanager::init() { 
 
   m_nchains = m_nchains_per_device * m_ndevices;

   try{
      
      // Query machine for available hardware
      device_query();
      
      // Print some info associated with the device
      print_device_status();

      random_engine engine;
      std::uniform_int_distribution<int> dist (
            std::numeric_limits<int>::min(),
            std::numeric_limits<int>::max());
      std::string seed_file;
      if (not m_run->has_seed_path) {
         seed_file = "";
         engine.seed(m_run->seed);
      } else {
         seed_file = m_run->seed_path;
      }
      
      // Create chain objects
      // (Correspond to one path of the state space exploration tree)
      int index = 0;
      int seed  = 0;
      for (unsigned int i = 0; i < m_systems.size(); ++i) {
         for (unsigned int j = 0; j < m_systems.begin()->size(); ++j) {

            // seed for this chain
            if (not m_run->has_seed_path) {
               seed = dist(engine);
            }

            m_chains.push_back(
               new FEP_OCLChain(
                  m_systems[i][j]           , // Chain makes a copy of *m_system
                  m_systemB                 , // Chain makes a copy of *m_system
                  m_grids[i][j]             ,
                  m_gridsB[i][j]            ,
                  m_hosts[2 * index]        , // Each chain is associated ...
                  m_hosts[2 * index + 1]    , // with two pipe hosts
                  m_energies_qm[i][j]       ,
                  m_energies_qmB[i][j]      ,
                  m_energy_qm_gp            ,
                  m_energies_mm[i][j]       ,
                  m_energies_vdw_qmmm[i][j] ,
                  m_energies_vdw_qmmmB[i][j],
                  m_energies_old[i][j]      ,
                  m_energies_oldB[i][j]     ,
                  m_n_pre_steps             ,
                  m_config_data_size        ,
                  m_energy_data_size        ,
                  seed                      ,
                  m_stepmax                 ,
                  m_basename                ,
                  NUM_MEANS                 ,
                  N_ENERGY_SAVE_PARAMS_FEP  ,
                  PRINT_S_FEP               ,
                  has_extra_energy_file     ,
                  m_run->has_seed_path      ,
                  seed_file + "/" + std::to_string(index) + ".random",
                  m_run->dimension_box
               )
            );
            index++;
         }
      }

      // Create opencl device managers and add chains to them
      for (int i = 0; i < m_ndevices; i++){
         
         m_devices.push_back(create_device (i));  
               
         // Add chains to device and make association
         for (int j = 0; j < m_nchains_per_device; j++){
            m_devices[i]->add_chain(m_chains[i * m_nchains_per_device + j]);            
            m_chains[i * m_nchains_per_device + j]->assoc_device(m_devices[i], j);
         }
         
         // Startup device
         m_devices[i]->prepare_randomness();
         m_devices[i]->setup_pmc(m_kernel_file_closure);
         m_devices[i]->first_writes();
      }   
 
      // Launch Chain Threads (for pipe comn)
      #ifdef WILL_WF_ADJUST
         for (int i = 0; i < m_nchains; i++)
            m_chains[i]->launch_thread();
      #endif
      
      // Launch Device Threads (for device comn)
      for (int i = 0; i < m_ndevices ; i++)
         m_devices[i]->launch_thread();
       
   } catch(cl::Error error){//openCL error catching
      std::cout<<"OCLmanager:: Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
}




/**
 * Gathers results from devices and prints them.
 */
void FEP_OCLmanager::final_output() {

   std::vector<FEP_ChainResults*> results;

   #ifdef LEGACY_MULTI_DEVICE
      // In this mode, only the first device needs to output
      std::vector<ChainResults*> tmp = m_devices[0]->get_results();
      results.reserve(tmp.size());
      std::transform (tmp.begin(), tmp.end(), std::back_inserter(results),
            [] (ChainResults* c) {return static_cast<FEP_ChainResults*>(c);});
   #else
      for (int i = 0; i < m_ndevices; i++){
         std::vector<ChainResults*> tmp = m_devices[i]->get_results();
         results.reserve(results.size() + tmp.size());
         std::transform (tmp.begin(), tmp.end(), std::back_inserter(results),
               [] (ChainResults* c) {return static_cast<FEP_ChainResults*>(c);});
      } 
   #endif

   FEP_ChainResults::print_results(m_basename + ".out", results);

   // clean up results
   for (auto it = results.begin(); it != results.end(); ++it) {
      delete *it;
   }
}




OCLDevice* FEP_OCLmanager::create_device (const int i)
{
   return new FEP_OCLDevice(
      m_captured_device[i],
      m_captured_context[i],
      &v_ignite_isReady[i],//Sync variables
      &v_ignite_mutex[i],  //Sync variables
      &v_ignite_slave[i],  //Sync variables
      &ignite_master,      //Sync variables
      m_n_pre_steps, 
      m_nkernel,
      m_nchains_per_device,     //Max chains per device 
      m_systems[0][0],
      m_opls, 
      m_oplsB,
      m_run, 
      m_temperature,
      m_ndevices,
      has_extra_energy_file);
}
