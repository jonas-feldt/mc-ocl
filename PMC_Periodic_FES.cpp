/*
 * File:   PMC_Periodic_FES.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-11-06
 */

#include "PMC_Periodic_FES.h"
#include "OCLmanager_FES.h"



PMC_Periodic_FES::~PMC_Periodic_FES ()
{
}




OCLmanager* PMC_Periodic_FES::create_manager (
      int n_pre_steps, 
      int nkernel, 
      int ndevices, 
      int nchains, 
      const vvs &systems,
      const OPLS_Periodic* opls, 
      Run_Periodic* run, 
      vvg &grid, 
      int config_data_size, 
      int energy_data_size, 
      _FPOINT_S_ temperature,
      double theta_max,
      double stepmax,   
      const vvd &energies_qm, 
      _FPOINT_S_ energy_qm_gp,
      const vvd &energies_mm,
      const vvd &energies_vdw_qmmm,
      const vvd &energies_old,
      Pipe_Host **hosts,
      std::string basename)
{
   return new OCLmanager_FES (
         n_pre_steps,  // Max number of steps OCL can run between list refreshes
         nkernel,                  // 10 max kernels
         ndevices,         // number of devices
         nchains,          // number of chains 
         systems,              // ptr to system
         opls,                // ptr to opls
         run,                 // ptr to m_run
         grid,                // ptr to grid
         config_data_size,
         energy_data_size, 
         temperature,         
         theta_max,
         stepmax,
         energies_qm, 
         energy_qm_gp,
         energies_mm,
         energies_vdw_qmmm,
         energies_old,
         hosts,
         basename);
}
