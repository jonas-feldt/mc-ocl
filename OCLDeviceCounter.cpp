/**
 *
 * File: OCLDeviceCounter.cpp
 * Author: Jonas Feldt
 *
 * Version: 2016-12-20
 *
 */


#define __CL_ENABLE_EXCEPTIONS
#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

#include <iostream>
#include <string>
#include <vector>

#include "OCLDeviceCounter.h"
#include "options.h"
#include "Run.h"


namespace OCLDeviceCounter {
   
   int count_devices(Run *run)
   {
      
      std::vector<cl::Platform> platforms;
   
      int capturedDevices=0; 
      try{
   
         int avoid = run->skip_first;
         //Query for platforms
         cl::Platform::get(&platforms);
         std::string retval;
         for(std::vector<cl::Platform>::iterator it = platforms.begin(); it != platforms.end(); ++it) {  
         
            std::vector<cl::Device> curr_devices;
            
            it->getDevices(CL_DEVICE_TYPE_ALL, &curr_devices);
            
            for(std::vector<cl::Device>::iterator it1 = curr_devices.begin(); it1 != curr_devices.end(); ++it1) {    
               
               it1->getInfo(CL_DEVICE_VENDOR,&retval);
   
               if(((retval.find(std::string(TARGET_VENDOR)) != std::string::npos) or
                        std::string(TARGET_VENDOR)==std::string("ANY")) and
                     (retval.find(std::string(TARGET_VENDOR_AVOID)) == std::string::npos)){
                  if(avoid>0){avoid--;continue;} 
                  capturedDevices++;
               }
            }
         }
         
      } catch (cl::Error error) {// openCL error catching
         std::cout<<"OCLDeviceCounter::count_devices: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
         throw error;
      }      
   
      return capturedDevices;
   }
   
}


