/*
 * File:    OPLS_Switcher_linear.cpp
 * Author:  Jonas Feldt
 *
 * Version: 2015-04-14
 */


#include "OPLS_Switcher_linear.h"


double OPLS_Switcher_linear::compute_factor (
      const unsigned int frame)
{
   return frame / (num_frames - 1);
}
