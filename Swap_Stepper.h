
/*
 * File:   Swap_Stepper.h
 * Author: Jonas Feldt
 *
 * Version: 2017-05-15
 */

#ifndef SWAP_STEPPER_H
#define	SWAP_STEPPER_H

#include "Abstract_Stepper.h"
#include "typedefs.h"

class System;
class OPLS;
class Run;

class Swap_Stepper : public Abstract_Stepper
{
   public:

      Swap_Stepper (random_engine *rnd_engine, System *system, Run *run);

      virtual void step (System *system, const OPLS *opls) override;
      virtual double dE (System *system, System *system_old, const OPLS *opls) override;
      virtual bool is_accepted() override;
      
      virtual void accept (System *system, System *system_old) override;
      virtual void reject (System *system, System *system_old) override;

   private:

      random_engine *rnd_engine;
      const double temperature;
      double de;
      std::uniform_int_distribution<int> *dist_molecule;

      int random_mol1;
      int random_mol2;
};

#endif	/* SWAP_STEPPER_H */

