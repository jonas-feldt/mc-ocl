/**
 *
 * File: FEP_OCLmanager_FES.h
 * Author: Jonas Feldt
 *
 * Version: 2015-10-30
 *
 */

#ifndef FEP_OCLMANAGER_FES_H_INCLUDED
#define FEP_OCLMANAGER_FES_H_INCLUDED

#include "FEP_OCLmanager.h"
#include "typedefs.h"
#include "options.h"

class Run_Periodic;
class Pipe_Host;
class Charge_Grid;
class OCLDevice;
class System_Periodic;

class FEP_OCLmanager_FES : public FEP_OCLmanager {

public:

   FEP_OCLmanager_FES (
               int n_pre_steps,
               int nkernel,
               int ndevices,
               int nchains,
               const vvs &systemsA,
               System_Periodic* systemB,
               OPLS_Periodic* opls,
               OPLS_Periodic* oplsB,
               Run_Periodic* run,
               vvg &gridsA,
               vvg &gridsB,
               int config_data_size,
               int energy_data_size,
               _FPOINT_S_ temperature,
               double theta_max,
               double stepmax,
               vvd &energies_qmA,
               vvd &energies_qmB,
               _FPOINT_S_ energy_qm_gp,
               vvd &energies_mmA,
               vvd &energies_vdw_qmmmA,
               vvd &energies_vdw_qmmmB,
               vvd &energies_oldA,
               vvd &energies_oldB,
               Pipe_Host **hosts,
               std::string basename);

   virtual ~FEP_OCLmanager_FES () override;

protected:

   virtual OCLDevice *create_device (const int i) override;
};

#endif //FEP_OCLMANAGER_FES_H_INCLUDED
