/*
 * File:   OPLS_Periodic_Softcore.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-12-07
 */

#include <math.h>
#include <cmath>
#include <algorithm>
#include <iostream>

#include "OPLS_Periodic.h"
#include "OPLS_Periodic_Softcore.h"
#include "Run_Periodic.h"
#include "System.h"
#include "options.h"
#include "geom_utils.h"
#include "Constants.h"
#include "typedefs.h"


/**
 * Constructor
 *
 * @param system properties of the class System
 * @param run 
 */
OPLS_Periodic_Softcore::OPLS_Periodic_Softcore (
      const System *system,
      Run_Periodic *run,
      const unsigned int n,
      const unsigned int m,
      const double alpha,
      const double lambda)
:  OPLS_Periodic (system, run),
   s1 (std::pow(lambda, n)),
   s2 (alpha * std::pow(1.0 - lambda, m))
{
}




double OPLS_Periodic_Softcore::energy_vdw (DistanceCall GetDistance) const
{
   double energy = 0.0;
   energy += energy_vdw_mm (GetDistance);
   energy += energy_vdw_qmmm (GetDistance);
   return energy;
}


double OPLS_Periodic_Softcore::energy_vdw (
      DistanceCall GetDistance,
      DistanceCall GetDistanceOld,
      int changed_molecule) const
{
   double energy = 0.0;
   energy += energy_vdw_mm (GetDistance, GetDistanceOld, changed_molecule);
   energy += energy_vdw_qmmm (GetDistance, GetDistanceOld, changed_molecule);
   return energy;
}




/**
 * Calculates the van der Waals interaction between the QM molecule and
 * the MM molecules.
 *
 * @param dists
 * @return vdW energy in kJ/mol
 */
double OPLS_Periodic_Softcore::energy_vdw_qmmm (DistanceCall GetDistance) const
{

   double energy = 0.0;
   int start = molecule2atom[0].natoms;
   for(int pos = 0; pos < start; pos++){
      int i = molecule2atom[0].atom_id[pos];
      for (int j = start; j < natom; j++) {
         
         double dist = GetDistance (i, j);
         if (dist < cutoff_vdw) {
            double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            double sigmas = sigma[i] * sigma[j];
            double t = dist * dist / sigmas;
            double V6 = t * t * t;
            double t1 = 1.0 / (s2 + V6);
            energy += s1 * sqrt_epsilon *
               (std::pow(t1, 2) - t1);
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}




/**
 * Calculates the van der Waals interaction between the
 * QM molecule of the QM system and the MM molecules of the MM system.
 *
 * @param mm_system takes the MM part of this system
 * @param qm_system uses only the QM molecule 
 * @return vdW energy in kJ/mol!
 */
double OPLS_Periodic_Softcore::energy_vdw_qmmm (System *mm_system, System *qm_system) {
   double energy = 0.0;
   for (int pos = 0; pos < qm_system->molecule2atom[0].natoms; pos++) { // loop over qm mol
      int i = qm_system->molecule2atom[0].atom_id[pos];
      for (unsigned int j = mm_system->molecule2atom[0].natoms; j < mm_system->natom; j++) { // loop over mm mol's
         double dist = compute_minimal_distance (
               qm_system->rx[i], qm_system->ry[i], qm_system->rz[i],
               mm_system->rx[j], mm_system->ry[j], mm_system->rz[j]);

         if (dist < cutoff_vdw) {
            double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            double sigmas = sigma[i] * sigma[j];
            double t = dist * dist / sigmas;
            double V6 = t * t * t;
            double t1 = 1.0 / (s2 + V6);
            energy += s1 * sqrt_epsilon *
               (std::pow(t1, 2) - t1);
         }
      }
   }
   energy *= 4 * KCAL_TO_KJ;
   return energy;
}




/**
 * Calculates the change of the van der Waals interaction between the
 * QM molecule and the changed MM molecule.
 *
 * @param dists_new
 * @param dists_old
 * @param changed_molecule
 * @return change of the vdW energy in kJ/mol
 */
double OPLS_Periodic_Softcore::energy_vdw_qmmm (
        DistanceCall GetDistance,
        DistanceCall GetDistance_old,
        int changed_molecule) const
{
   double energy = 0.0;
   for(int pos = 0 ; pos < molecule2atom[0].natoms; pos++) {
      int i = molecule2atom[0].atom_id[pos];
      for (int pos2 = 0; pos2 < molecule2atom[changed_molecule].natoms; pos2++) {
         int j = molecule2atom[changed_molecule].atom_id[pos2];

         double dist_new = GetDistance (i, j);
         double dist_old = GetDistance_old (i, j);
         if (dist_new < cutoff_vdw) {

            double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            double sigmas = sigma[i] * sigma[j];
            double t = dist_new * dist_new / sigmas;
            double V6 = t * t * t;
            double t1 = 1.0 / (s2 + V6);

            if (dist_old < cutoff_vdw) {
               double t_o = dist_old * dist_old / sigmas;
               double V6_o = t_o * t_o * t_o;
               double t1_o = 1.0 / (s2 + V6_o);
               energy += s1 * sqrt_epsilon *
                  (std::pow(t1, 2) - t1 -
                   std::pow(t1_o, 2) + t1_o);
            } else { // dist < cutoff && dist_old > cutoff
               energy += s1 * sqrt_epsilon *
                  (std::pow(t1, 2) - t1);
            }
         } else if (dist_old < cutoff_vdw) { // dist_new > cutoff && dist_old < cutoff
            // subtracts the old value
            double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            double sigmas = sigma[i] * sigma[j];
            double t_o = dist_old * dist_old / sigmas;
            double V6_o = t_o * t_o * t_o;
            double t1_o = 1.0 / (s2 + V6_o);
            energy -= s1 * sqrt_epsilon *
               (std::pow(t1_o, 2) - t1_o);

            // dist_new > cutoff && dist_old > cutoff --> always 0
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}




void OPLS_Periodic_Softcore::print (std::string outfile)
{
   FILE *fout;
   fout = fopen (outfile.c_str(), "a");
   fprintf (fout, "\nSoftcore Parameters\n\n");
   fprintf (fout, "s1 = %15.6f\n", s1);
   fprintf (fout, "s2 = %15.6f\n", s2);
   fclose (fout);

   // print base class informations
   OPLS_Periodic::print (outfile);
}
