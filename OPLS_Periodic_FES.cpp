/*
 * File:   OPLS_Periodic_FES.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-10-29
 */

#include "OPLS_Periodic.h"
#include "OPLS_Periodic_FES.h"


/**
 * Constructor
 *
 * @param system properties of the class System
 * @param run 
 */
OPLS_Periodic_FES::OPLS_Periodic_FES (
      const System *system,
      Run_Periodic *run,
      const double factor_fes)
:  OPLS_Periodic(system, run)
{
   scaled_charges.reserve(normal_charges.size());
   for (unsigned int i = 0; i < normal_charges.size(); i++) {
      scaled_charges[i] = factor_fes * normal_charges[i];
   }
}




const double* OPLS_Periodic_FES::get_normal_charges () const
{
   return scaled_charges.data();
}
