
/*
 * File:   Rotate_Translate_Stepper.h
 * Author: Jonas Feldt
 *
 * Version: 2017-05-16
 */

#ifndef ROTATE_TRANSLATE_STEPPER_H
#define	ROTATE_TRANSLATE_STEPPER_H

#include "Abstract_Stepper.h"
#include "typedefs.h"
#include "options.h"

class System;
class OPLS;
class Run;

class Rotate_Translate_Stepper : public Abstract_Stepper
{
   public:

      Rotate_Translate_Stepper (random_engine *rnd_engine, System *system, Run *run);

      virtual ~Rotate_Translate_Stepper ();

      void step (System *system, const OPLS *opls) override;
      double dE (System *system, System *system_old, const OPLS *opls) override;
      bool is_accepted() override;
      
      void accept (System *system, System *system_old) override;
      void reject (System *system, System *system_old) override;
      void update (int step) override;

      double stepmax;
      double thetamax;

   protected:

      random_engine *rnd_engine;
      std::uniform_int_distribution<int> *dist_molecule;
      std::uniform_real_distribution<double> *dist_theta;
      std::uniform_real_distribution<_FPOINT_S_> *dist_step;
      std::uniform_real_distribution<double> dist_two {-1.0, 1.0};
      std::uniform_real_distribution<_FPOINT_S_> dist_boltzman {0.0, 1.0};
      const double temperature;

      double de;
      int random_molecule;

   private:
      const int adjust_step;
      const int adjust_theta;
      const double acceptance_ratio;

      int adj_steps = 0;
      int adj_acc_steps = 0;

};

#endif	/* ROTATE_TRANSLATE_STEPPER_H */

