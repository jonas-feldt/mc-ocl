/*
 * File:    OPLS_Switcher_factor.h
 * Author:  Jonas Feldt
 *
 * Version: 2015-11-04
 */

#ifndef OPLS_SWITCHER_FACTOR_H
#define OPLS_SWITCHER_FACTOR_H

#include "OPLS_Switcher.h"

#include <vector>

class OPLS_Switcher_factor : public OPLS_Switcher {

   public:

      OPLS_Switcher_factor (
            const System *system, Run *run,
            std::vector<double> &factors);
      virtual ~OPLS_Switcher_factor() override;


   protected:

      virtual double compute_factor (const unsigned int frame) override;

      std::vector<double> factors;

};

#endif // OPLS_SWITCHER_LINEAR_H
