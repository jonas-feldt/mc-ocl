/*
 * File:   opls_periodic_dual_topology.h
 * Author: Jonas Feldt
 *
 * Version: 2017-07-19
 */


#pragma once

#include "OPLS_Periodic_Softcore.h"
#include "typedefs.h"

class OPLSPeriodicDualTopology : public OPLS_Periodic_Softcore
{
   public:

      OPLSPeriodicDualTopology (
         const System *system,
         Run_Periodic *run,
         const unsigned int n,
         const unsigned int m,
         const double alpha,
         const double lambda);

      double energy (DistanceCall GetDistance) const override;
      double energy (DistanceCall GetDistance, DistanceCall GetDistanceOld, int changed_molecule) const override;

   protected:

      double EnergyVDWQMMMTopology (DistanceCall GetDistance, unsigned int topology, const double s1, const double s2) const;
      double EnergyVDWQMMMTopology (
            DistanceCall GetDistance, unsigned int topology,
            DistanceCall GetDistanceOld, unsigned int changed_molecule,
            const double s1, const double s2) const;

      double EnergyCoulombQMMMTopology (DistanceCall GetDistance, unsigned int topology) const;
      double EnergyCoulombQMMMTopology (
            DistanceCall GetDistance, unsigned int topology,
            DistanceCall GetDistanceOld, unsigned int changed_molecule) const;

      const double lambda;
      const unsigned int reference;
      const unsigned int target;
      const double s1r;
      const double s2r;
};
