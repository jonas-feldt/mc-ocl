/*
 * File: opls_no_coulomb.h
 * Author: Jonas Feldt
 *
 * Version: 2017-07-18
 *
 * Template based decorator which returns always 0.0 for any coulomb
 * interaction in order to gain performance for uncharged systems.
 */

#pragma once

#include <iostream>

#include "typedefs.h"

template <class T>
class OPLSNoCoulomb : public T
{
   public:

      using T::T;
      
   protected:

      double energy_coulomb (DistanceCall GetDistance) const override
      {
         return 0.0;
      }

      double energy_coulomb (
            DistanceCall GetDistance,
            DistanceCall GetDistance_old,
            int changed_molecule) const override
      {
         return 0.0;
      }

};


