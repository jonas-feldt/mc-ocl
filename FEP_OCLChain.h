/**
 *
 * File: FEP_OCLChain.h
 * Author: Jonas Feldt 
 *
 * Version: 2015-03-11
 *
 */

#ifndef FEP_OCLCHAIN_H_INCLUDED
#define FEP_OCLCHAIN_H_INCLUDED

#define __CL_ENABLE_EXCEPTIONS
#include <string>
#include <iostream>
#include <fstream>
#include <thread>
#include <math.h>
#include <mutex>
#include <vector>
#include <condition_variable>

#include "options.h"
#include "System_Periodic.h"
#include "Charge_Grid.h"
#include "math_utils.h"
#include "Pipe_Host.h" 
#include "OCLChain.h"
#include "FEP_ChainResults.h"

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include <CL/cl.hpp>
#endif

class FEP_OCLChain : public OCLChain {

public:
   
   FEP_OCLChain(  System_Periodic *systemA, 
                  System_Periodic *systemB, 
                  Charge_Grid *gridA, 
                  Charge_Grid *gridB, 
                  Pipe_Host *hostA,
                  Pipe_Host *hostB,
                  _FPOINT_S_ energy_qmA, 
                  _FPOINT_S_ energy_qmB, 
                  _FPOINT_S_ energy_qm_gp,
                  _FPOINT_S_ energy_mmA,
                  _FPOINT_S_ energy_vdw_qmmmA,
                  _FPOINT_S_ energy_vdw_qmmmB,
                  _FPOINT_S_ energy_oldA,
                  _FPOINT_S_ energy_oldB, 
                  int n_pre_steps,
                  int n_config_data_size,
                  int n_energy_data_size,
                  int seed, 
                  double stepmax, 
                  std::string base_name,
                  int num_means,
                  int n_energy_save_params,
                  std::string print_s,
                  bool extra_energy_file,
                  bool has_seed_state,
                  std::string state_file,
                  const double dimension_box);
                     
   virtual ~FEP_OCLChain() override;
   
   //Setup buffers for chain
   virtual void setup_buffers() override;
   
   //Write grid into the device's chain bufers
   virtual void write_grid(int inner_step, bool reduce,
         double dimension_box, double cutoff_coulomb) override;
   
   //Write init energies
   virtual void write_init_energies() override;
   
   //Write system xyz to device's buffer
   virtual void write_system_lattice() override;
   
   //Update reference energy
   virtual void update_reference_energy(int inner_step) override;
   
   virtual ChainResults *get_results (const int steps) override;
   
   // Object for pipe communication 
   Pipe_Host *m_hostB;
  
   // Pointers to host side objects
   System_Periodic   *m_systemB;
   Charge_Grid       *m_gridB  ;

   // Buffers that refer only to one chain

   //Reference system data
   cl::Buffer m_old_xB;                                
   cl::Buffer m_old_yB;                
   cl::Buffer m_old_zB;   
    
   #ifndef SET_FPOINT_G_DOUBLE 
      cl::Buffer m_old_x_fB;                                
      cl::Buffer m_old_y_fB;                
      cl::Buffer m_old_z_fB;  
   #endif
   
   //Grid data
   cl::Buffer m_grid_rxB    ;
   cl::Buffer m_grid_ryB    ;
   cl::Buffer m_grid_rzB    ;
   cl::Buffer m_grid_chargeB;
   cl::Buffer m_grid_sizeB  ;

   //buffers for energies 
   cl::Buffer m_mm_vdwc_energy_colB;//reduction buffer
   cl::Buffer m_mm_vdwc_energy_vdwB;//reduction buffer
   cl::Buffer m_qmmm_c_energyB;  
   cl::Buffer m_energy_vdw_qmmmB;   
   
   cl::Buffer m_minors_energyB;
   
   //Host provided energies
   cl::Buffer m_qm_energyB; // Also, updated by OpenCL device
   
   //Persistent OpenCL Device data
   cl::Buffer m_energy_oldB;
   
protected:

   virtual void chain_loop() override;

   //vectors for holding float MM reference
   #ifndef SET_FPOINT_G_DOUBLE
      float* v_rx_fB;
      float* v_ry_fB;
      float* v_rz_fB;
   #endif

   // Host side energy variables respecting to this chain
   // (host side _FPOINT_S_'s)
   _FPOINT_S_ m_f_energy_qmB       ;
   _FPOINT_S_ m_f_energy_vdw_qmmmB ;
   _FPOINT_S_ m_f_energy_oldB      ;
  
   // 
   // Events
   //
   
   // Buffer Events
   cl::Event m_EVENT_old_xB;
   cl::Event m_EVENT_old_yB;
   cl::Event m_EVENT_old_zB;
   
   // Global Grid Buffers Write
   cl::Event m_EVENT_grid_rxB;
   cl::Event m_EVENT_grid_ryB;
   cl::Event m_EVENT_grid_rzB;
   cl::Event m_EVENT_grid_chargeB;
   cl::Event m_EVENT_grid_sizeB;  
   
   // Energy qm
   cl::Event m_EVENT_qm_energyB;  
   cl::Event m_EVENT_energy_vdw_qmmmB;  
   cl::Event m_EVENT_energy_oldB;  
   cl::Event m_EVENT_get_energy_vdw_qmmmB;  
};

#endif //FEP_OCLCHAIN_H_INCLUDED

