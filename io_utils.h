/**
 *
 * File: io_utils.h
 * Author: Jonas Feldt, João Oliveira
 *
 * Version: 2015-05-06
 *
 */

#ifndef IO_UTILS_H_INCLUDED
#define IO_UTILS_H_INCLUDED

#include <string>
#include <unordered_map>
#include <libconfig.h++>
#include <utility>

#include "typedefs.h"

class System;
class System_Periodic;
class Run;
class Run_Periodic;
class OPLS;
class OPLS_Periodic;
class OPLS_Switcher;

System* read_input_system (std::string input_file);
vvs read_periodic_systems (Run_Periodic *run);

System_Periodic* read_tinker_periodic_system (std::string input_file,
        double box_dimension);
System_Periodic* read_tinker_periodic_system (std::ifstream &in,
        double box_dimension);

System* read_tinker_system (std::string input_file);
System* read_system (std::string file);
System* read_xyz (std::string file);

void read_frame (std::ifstream &trajectory, System *system);
void read_periodic_frame (std::ifstream &trajectory, System_Periodic *system);

Run* read_input_run (std::string input_file);

Run_Periodic* get_run_periodic (libconfig::Config &cfg);
Run_Periodic* get_fep_periodic (libconfig::Setting &set, Run_Periodic *original_run);

std::string read_template (const std::string &filename, std::unordered_map<std::string, std::string> &qm_settings);

OPLS* read_params(System *system, std::string params_file, std::string outfile);
void read_params_periodic (std::string params_file, Run_Periodic *run);
void read_params_periodic_tinker (std::string params_file, Run_Periodic *run);

bool file_exists (std::string filename);
unsigned int file_lines(std::string filename);

#endif // IO_UTILS_H_INCLUDED
