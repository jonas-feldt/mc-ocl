
#include <string>
#include <vector>
#include <cmath>

#include "gtest/gtest.h"
#include "System.h"
#include "OPLS.h"
#include "io_utils.h"
#include "Constants.h"
#include "opls_no_coulomb.h"
#include "opls_no_coulomb_qmmm.h"

#define ERROR 0.01

//
// Tests if opls gets correctly initialized.
//
TEST (OPLSTest, opls) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   system->rz[1] = 3.0;
   system->UpdateDistances ();

   std::vector<int> types {1};
   std::vector<double> epsilons {1.0};
   std::vector<double> sigmas {2.0};
   std::vector<double> charges {-1.0};
   OPLS *opls = new OPLS (system, types, epsilons, sigmas, charges, 1);

   ASSERT_EQ (opls->epsilon[0], 1.0);
   ASSERT_EQ (opls->epsilon[1], 1.0);

   ASSERT_EQ (opls->sigma[0], 2.0);
   ASSERT_EQ (opls->sigma[1], 2.0);

   ASSERT_EQ (opls->charge[0], -1.0);
   ASSERT_EQ (opls->charge[1], -1.0);

   delete opls;
}

//
// Tests vdw term of OPLS.
//
TEST (OPLSTest, vdw) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   system->rz[1] = 3.0;
   system->UpdateDistances ();

   std::vector<int> types {1};
   std::vector<double> epsilons {0.0};
   std::vector<double> sigmas {1.0};
   std::vector<double> charges {0.0};
   OPLS *opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_EQ (opls->energy (system->GetDistance), 0.0);
   delete opls;

   epsilons[0] = 1.0;
   sigmas[0] = 0.0;
   opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_EQ (opls->energy (system->GetDistance), 0.0);
   delete opls;

   sigmas[0] = 3.0;
   opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_EQ (opls->energy (system->GetDistance), 0.0);
   delete opls;

   sigmas[0] = 1.5;
   epsilons[0] = 1000.0;
   opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_LT (std::abs(opls->energy (system->GetDistance) - (-61.5234 * KCAL_TO_KJ)), ERROR);

   double e1 = opls->energy (system->GetDistance);
   System *old = new System (*system);
   system->rz[1] = 4.0;
   system->UpdateDistances ();
   double e2 = opls->energy (system->GetDistance);
   EXPECT_EQ (e2 - e1, opls->energy (system->GetDistance, old->GetDistance, 1));
   delete old;

   delete system;
   delete opls;
}



//
// Tests vdw qmmm term of OPLS.
//
TEST (OPLSTest, vdw_qmmm) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   system->rz[1] = 3.0;
   system->UpdateDistances ();

   std::vector<int> types {1};
   std::vector<double> epsilons {1000.0};
   std::vector<double> sigmas {1.5};
   std::vector<double> charges {0.0};
   OPLS *opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_LT (std::abs(opls->energy_vdw_qmmm (system->GetDistance) - (-61.5234 * KCAL_TO_KJ)), ERROR);

   system->rz[1] = 6.0;
   system->UpdateDistances ();
   EXPECT_LT (std::abs(opls->energy_vdw_qmmm (system->GetDistance) - (-0.9763 * KCAL_TO_KJ)), ERROR);

   delete system;
   delete opls;
}



//
// Tests vdw mm term of OPLS.
//
TEST (OPLSTest, vdw_mm) {
   System *system = read_tinker_system ("gtest_opls2.xyz");

   std::vector<int> types {1};
   std::vector<double> epsilons {1000.0};
   std::vector<double> sigmas {1.5};
   std::vector<double> charges {0.0};
   OPLS *opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_LT (std::abs(opls->energy_mm (system->GetDistance) - (-61.5234 * KCAL_TO_KJ)), ERROR);

   system->rz[2] = 6.0;
   system->UpdateDistances ();
   EXPECT_LT (std::abs(opls->energy_mm (system->GetDistance) - (-0.9763 * KCAL_TO_KJ)), ERROR);

   delete system;
   delete opls;
}



//
// Tests Coulomb term of OPLS.
//
TEST (OPLSTest, coulomb) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   system->rz[1] = 3.0;
   system->UpdateDistances ();

   std::vector<int> types {1};
   std::vector<double> epsilons {0.0};
   std::vector<double> sigmas {0.0};
   std::vector<double> charges {0.0};
   OPLS *opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_EQ (opls->energy (system->GetDistance), 0.0);
   delete opls;

   charges[0] = 1.0;
   opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_LT (std::abs(opls->energy (system->GetDistance) - (110.68666666264 * KCAL_TO_KJ)), ERROR);

   system->rz[1] = 30.0;
   system->UpdateDistances ();
   EXPECT_LT (std::abs(opls->energy (system->GetDistance) - (11.068666666264 * KCAL_TO_KJ)), ERROR);

   double e1 = opls->energy (system->GetDistance);
   System *old = new System (*system);
   system->rz[1] = 4.0;
   system->UpdateDistances ();
   double e2 = opls->energy (system->GetDistance);
   EXPECT_LT (std::abs(e2 - e1 - opls->energy (system->GetDistance, old->GetDistance, 1)), ERROR);
   delete old;

   delete system;
   delete opls;

}



//
// Tests OPLSNoCoulomb decorator.
//
TEST (OPLSTest, OPLSNoCoulomb) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   system->rz[1] = 3.0;
   system->UpdateDistances ();

   std::vector<int> types {1};
   std::vector<double> epsilons {0.0};
   std::vector<double> sigmas {0.0};
   std::vector<double> charges {0.0};
   OPLS *opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_EQ (opls->energy (system->GetDistance), 0.0);
   delete opls;

   charges[0] = 1.0;
   opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_LT (std::abs(opls->energy (system->GetDistance) - (110.68666666264 * KCAL_TO_KJ)), ERROR);

   auto decorated = new OPLSNoCoulomb<OPLS> {system, types, epsilons, sigmas, charges, 1};
   EXPECT_EQ (decorated->energy (system->GetDistance), 0.0);

   sigmas[0] = 1.5;
   epsilons[0] = 1000.0;
   delete decorated;
   decorated = new OPLSNoCoulomb<OPLS> {system, types, epsilons, sigmas, charges, 1};
   EXPECT_LT (std::abs(decorated->energy (system->GetDistance) - (-61.5234 * KCAL_TO_KJ)), ERROR);

   delete system;
   delete opls;

}




//
// Tests OPLSNoCoulombQMMM
//
TEST (OPLSTest, OPLSNoCoulombQMMM) {
   System *system = read_tinker_system ("gtest_opls.xyz");
   system->rz[1] = 3.0;
   system->UpdateDistances ();

   std::vector<int> types {1};
   std::vector<double> epsilons {1000.0};
   std::vector<double> sigmas {1.5};
   std::vector<double> charges {0.0};

   OPLS *opls = new OPLSNoCoulombQMMM<OPLS> (system, types, epsilons, sigmas, charges, 1);
   EXPECT_LT (std::abs(opls->energy_vdw_qmmm (system->GetDistance) - (-61.5234 * KCAL_TO_KJ)), ERROR);

   epsilons[0] = 0.0;
   sigmas[0] = 0.0;
   charges[0] = 1.0;
   delete opls;
   opls = new OPLS (system, types, epsilons, sigmas, charges, 1);
   EXPECT_LT (std::abs(opls->energy (system->GetDistance) - (110.68666666264 * KCAL_TO_KJ)), ERROR);

   delete opls;
   opls = new OPLSNoCoulombQMMM<OPLS> (system, types, epsilons, sigmas, charges, 1);
   EXPECT_LT (std::abs(opls->energy (system->GetDistance)), ERROR);

   delete system;
   delete opls;
}
