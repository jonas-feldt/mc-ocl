#ifndef ATOMIC_WEIGHT_H_INCLUDED
#define ATOMIC_WEIGHT_H_INCLUDED

#include <string>
#include <map>

class Atom_weight {
public:

   Atom_weight();

   double find_mass(std::string atom_name);

   ~Atom_weight();

protected:

private:
   std::map<std::string,double> mass;
};


#endif // ATOMIC_WEIGHT_H_INCLUDED
