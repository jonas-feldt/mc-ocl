/*
 * File:   Charge_Grid.cpp
 * Author: Jonas Feldt
 *
 * Version: 2014-08-09
 */

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <math.h>
#include <cmath>

#include "Charge_Grid.h"
#include "math_utils.h"
#include "geom_utils.h"
#include "Constants.h"
#include "System.h"

using namespace std;



/**
 * Constructor
 */
Charge_Grid::Charge_Grid () {
   sqrt_box_dim = 0;
}


/**
 * Copy Constructor
 */
Charge_Grid::Charge_Grid (Charge_Grid &other) {
   
   dim      = other.dim;
   x        = new _FPOINT_G_[dim];
   y        = new _FPOINT_G_[dim];
   z        = new _FPOINT_G_[dim];
   charges  = new _FPOINT_G_[dim];
    
   cutoff               = other.cutoff               ;
   cutoff_coulomb_rec   = other.cutoff_coulomb_rec   ;
   cutoff_coulomb_rec_sq= other.cutoff_coulomb_rec_sq;
   sqrt_box_dim         = other.sqrt_box_dim         ;
   
   for (int i = 0; i < dim; ++i) {
      x[i] = other.x[i];
      y[i] = other.y[i];
      z[i] = other.z[i];
      charges[i] = other.charges[i];
   }
   
}

 
/**
 * Destructor
 */
Charge_Grid::~Charge_Grid () {
   delete[] charges;
   delete[] x;
   delete[] y;
   delete[] z;
}


/**
 * Reads the point charges that represent the electron density from the given
 * file.
 *
 * @param file with point charges in the format:
 *        dimension
 *        x y z point_charge
 *        ...
 */
void Charge_Grid::read_density (string file) {

   ifstream in (file.c_str ());
   string line;
   istringstream iss;

   getline (in, line);   // reads number of grid points
   iss.str (line);
   iss >> dim;
   iss.clear ();

   x = new _FPOINT_G_[dim];
   y = new _FPOINT_G_[dim];
   z = new _FPOINT_G_[dim];
   charges = new _FPOINT_G_[dim];

   for (int i = 0; i < dim; ++i) {
      getline (in, line);
      iss.str (line);
      iss >> x[i] >> y[i] >> z[i] >> charges[i];
      iss.clear ();
   }

   in.close ();

   for (int i = 0; i < dim; ++i) { // converts coordinates to Angstrom
      x[i] *= BOHR_TO_ANG;
      y[i] *= BOHR_TO_ANG;
      z[i] *= BOHR_TO_ANG;
   }
}




/**
 * Transforms normal coordinates to reduced units and changes charges
 * accordingly. Assumes that the density was read before. Sets the cutoff for
 * the energy calculation.
 *
 * @param box_dim
 */
void Charge_Grid::reduced_coords (double box_dim, double cutoff) {
   sqrt_box_dim = sqrt (box_dim);
   this->cutoff = cutoff;
   cutoff_coulomb_rec = 1.0 / cutoff;
   cutoff_coulomb_rec_sq = cutoff_coulomb_rec * cutoff_coulomb_rec;
   double rec = 1.0 / box_dim;
   for (int i = 0; i < dim; ++i) {
      x[i] *= rec;
   }
   for (int i = 0; i < dim; ++i) {
      y[i] *= rec;
   }
   for (int i = 0; i < dim; ++i) {
      z[i] *= rec;
   }
   double sq_rec = sqrt (rec);
   for (int i = 0; i < dim; ++i) {
      charges[i] *= sq_rec;
   }
}



/**
 * Checks the sanity of this grid in the context of the current system. Returns
 * for non-periodic systems at the moment always true.
 *
 * @return true if sane, false otherwise
 */
bool Charge_Grid::is_sane(std::string file) {
   // checks if the grid is larger than the simulation box 
   FILE *out = fopen (file.c_str (), "a");
   if (sqrt_box_dim != 0) {
      bool sane = true;
      for (int i = 0; i < dim; ++i) {
         if (charges[i] == 0) continue;
         if (x[i] > 0.5 || x[i] < -0.5) {
            fprintf (out, "x[%d] %f.\n", i, x[i]);
            sane = false;
            break;
         }
      }
      if (sane) {
         for (int i = 0; i < dim; ++i) {
            if (charges[i] == 0) continue;
            if (y[i] > 0.5 || y[i] < -0.5) {
               fprintf (out, "y[%d] %f.\n", i, x[i]);
               sane = false;
               break;
            }
         }
      }
      if (sane) {
         for (int i = 0; i < dim; ++i) {
            if (charges[i] == 0) continue;
            if (z[i] > 0.5 || z[i] < -0.5) {
               fprintf (out, "z[%d] %f.\n", i, x[i]);
               sane = false;
               break;
            }
         }
      }
      if (!sane) {
         fprintf (out, "ERROR The grid is larger then the box dimension.\n");
         fprintf (out, "      Increase the dimension of the box or the target accuracy for the grid to get a smaller grid.\n");
         fclose(out);
         return false;
      }
   }

   fclose(out);
   return true;
}
