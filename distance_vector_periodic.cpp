
#include "distance_vector_periodic.h"
#include "geom_utils.h"
#include "System.h" // needed for molecule_db


DistanceVectorPeriodic* DistanceVectorPeriodic::clone () const
{
   return new DistanceVectorPeriodic ();
}



void DistanceVectorPeriodic::ComputeDistances ()
{
   for (unsigned int i = 0; i < dim - 1; ++i) {
      for (unsigned int j = i + 1; j < dim; ++j) {
         double dist = compute_minimal_distance (
               x[i], y[i], z[i],
               x[j], y[j], z[j]);
         const int index = i + (j - 1) * j / 2;
         dists[index] = dist;
      }
   }
}



void DistanceVectorPeriodic::ComputeDistances (unsigned int molecule)
{
   for (int pos = 0 ; pos < (*mol2atom)[molecule].natoms; ++pos){
      unsigned int i = (*mol2atom)[molecule].atom_id[pos];
      for (unsigned int j = 0; j < dim; ++j) {
         if (i == j) continue;

         const double dist = compute_minimal_distance (
                  x[i], y[i], z[i],
                  x[j], y[j], z[j]);
         int index;
         if (i < j) {
            index = i + (j - 1) * j / 2;
         } else {
            index = j + (i - 1) * i / 2;
         }
         dists[index] = dist;
      }
   }
}



