#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "basis_fast_charges.h"

#define MAX(a,b) (((a)>(b))?(a):(b))
#define TOANG 0.529177209
#define TOBOHR 1.0/TOANG


void mxma (double *a, long int mcola, long int mrowa, double *b, long int mcolb,
        long int mrowb, double *r, long int mcolr, long int mrowr, long int ncol,
        long int nlink, long int nrow) {
   long int i, j, k;
   long int iaa = 0;
   long int irr = 0;
   for (i = 0; i < ncol; ++i) {
      long int ibb = 0;
      long int ir = irr;
      for (j = 0; j < nrow; ++j) {
         long int ib = ibb;
         long int ia = iaa;
         double s = 0;
         for (k = 0; k < nlink; ++k) {
            s += a[ia] * b[ib];
            ib += mcolb;
            ia += mrowa;
         }
         r[ir] = s;
         ir += mrowr;
         ibb += mrowb;
      }
      irr += mcolr;
      iaa += mcola;
   }
}

void basis_contract_vector_group(
        long int group, double p[], double c[], long int icolprim,
        long int irowprim, long int ncol, long int icolcontr,
        long int irowcontr, long int ncont[], long int inshell[],
        long int incart[], double ccs[], long int ccoffs[]) {

   long int iprim, icontr, icol;
   long int one = 1;

   iprim = 0;
   icontr = 0;
   if (MAX(ncont[group], inshell[group]) > 0 && incart[group] == 1) { /* contract only s */
      mxma(&ccs[ccoffs[group]], inshell[group], one, p, icolprim, irowprim, &c[icontr], icolcontr, irowcontr, ncont[group], inshell[group], ncol);
   } else if (MAX(ncont[group], inshell[group]) > 0) { /* contract only  */
      for (icol = 0; icol < ncol; ++icol) {
         mxma(&p[iprim], icolprim, icolprim * incart[group], &ccs[ccoffs[group]], one, inshell[group], &c[icontr],
                 icolcontr, icolcontr * incart[group], incart[group], inshell[group], ncont[group]);
         iprim += irowprim;
         icontr += irowcontr;
      }
   } else if (incart[group] == 1) { /* copy only s   */
      printf("---------> ERROR my_basis_contract_vector_group <---------");
      exit(1);
      /* XXX ever needed? */
   } else { /* copy only */
      printf("---------> ERROR my_basis_contract_vector_group <---------");
      exit(1);
      for (icol = 0; icol < ncol; ++icol) {
         /* XXX ever needed? */
         iprim += irowprim;
         icontr += irowcontr;
      }
   }
}

void basis_contract_rectangle_group(
        long int igroup, long int jgroup, double *p, double *c,
        long int *inshell, long int *incart, long int *infuns,
        long int *ncont, double *ccs, long int *ccoffs,
        long int lmin_i, long int lmax_i, long int lmin_j, long int lmax_j) {

   long int nPi = inshell[igroup] * (((1 + lmax_i - lmin_i) * (6 + lmax_i * lmax_i + lmin_i * (4 + lmin_i) + lmax_i * (5 + lmin_i))) / 6);
   double *tmp = (double *) malloc((infuns[jgroup] + 1) * nPi * sizeof (double));
   long int one = 1;
   basis_contract_vector_group(jgroup, p, tmp, nPi, one, nPi, nPi,
           one, ncont, inshell, incart, ccs, ccoffs);
   basis_contract_vector_group(igroup, tmp, c, one, nPi, infuns[jgroup],
           one, infuns[igroup], ncont, inshell, incart, ccs, ccoffs);
   free(tmp);
}


void fzero(double arr[], long int dim) {
   long int i = 0;
   for (i = 0; i < dim; ++i) {
      arr[i] = 0.0;
   }
}



void read_density(double *density, FILE *file, long int nbas, FILE *out) {
   char *line = NULL;
   size_t len = 0;
   int i, j;
   int cnt = 0;
   getline(&line, &len, file);
   getline(&line, &len, file);
   for (i = 0; i < nbas; ++i) {
      for (j = 0; j < nbas; ++j) {
         getdelim(&line, &len, ',', file);
         density[cnt] = atof(line);
         cnt++;
      }
   }
   if (line) free(line);
}



void read_data(FILE *fp, long int ngrp, long int npsh, long int *inshell, long int *infuns,
        long int *ioffs, double *ixpss, long int *igpoffs, double *icentres,
        long int *ilmins, long int *ilmaxs, long int *incarts, long int *ncont,
        double *ccs, long int *ccoffs) {
   char * line = NULL;
   size_t len = 0;

   long int i, j, k;
   long int cnt = 0;
   for (i = 0; i < ngrp; ++i) {
      ccoffs[i] = cnt;
      getline(&line, &len, fp);
      inshell[i] = atoi(line);
      getline(&line, &len, fp);
      ncont[i] = atoi(line);
      for (j = 0; j < inshell[i]; ++j) {
         for (k = 0; k < ncont[i]; ++k) {
            getline(&line, &len, fp);
            ccs[cnt] = atof(line);
            cnt++;
         }
      }
   }

   for (i = 0; i < ngrp; ++i) {
      getline(&line, &len, fp);
      inshell[i] = atoi(line);
   }
   for (i = 0; i < ngrp; ++i) {
      getline(&line, &len, fp);
      infuns[i] = atoi(line);
   }
   for (i = 0; i < ngrp; ++i) {
      getline(&line, &len, fp);
      ioffs[i] = atoi(line);
   }
   for (i = 0; i < npsh; ++i) {
      getline(&line, &len, fp);
      ixpss[i] = atof(line);
   }
   for (i = 0; i < ngrp; ++i) {
      getline(&line, &len, fp);
      igpoffs[i] = atoi(line);
   }
   for (i = 0; i < 3 * ngrp; ++i) {
      getline(&line, &len, fp);
      icentres[i] = atof(line);
   }
   for (i = 0; i < ngrp; ++i) {
      getline(&line, &len, fp);
      ilmins[i] = atoi(line);
   }
   for (i = 0; i < ngrp; ++i) {
      getline(&line, &len, fp);
      ilmaxs[i] = atoi(line);
   }
   for (i = 0; i < ngrp; ++i) {
      getline(&line, &len, fp);
      incarts[i] = atoi(line);
   }

   if (line) free(line);
}



void basis_pt_charges(long int nbas, long int ngrp, long int npsh, long int *inshell, long int *infuns,
        long int *ioffs, double *ixpss, long int *igpoffs, double *icentres,
        long int *ilmins, long int *ilmaxs, long int *incarts, long int nq,
        double *qq, double *rq, double *a, long int *ncont, double *ccs,
        long int *ccoffs, FILE *out) {
   long int lmin_i, lmax_i, ncart_i, nshel_i, nprim_i, ixps_i, icen_i, nfun_i, igrpo_i;
   long int lmin_j, lmax_j, ncart_j, nshel_j, nprim_j, ixps_j, icen_j, nfun_j, igrpo_j;
   double *igp, *icgp;
   long int igrp, jgrp, ish, jsh, ioff_i, ioff_j, one, i, j;
   double xa, xb;
   for (igrp = 0; igrp < ngrp; ++igrp) {
      lmin_i = ilmins[igrp];
      lmax_i = ilmaxs[igrp];
      ncart_i = incarts[igrp];
      nshel_i = inshell[igrp];
      nprim_i = nshel_i * ncart_i;
      ixps_i = igpoffs[igrp];
      icen_i = igrp * 3;
      nfun_i = infuns[igrp];
      igrpo_i = ioffs[igrp];

      for (jgrp = igrp; jgrp < ngrp; ++jgrp) {
         lmin_j = ilmins[jgrp];
         lmax_j = ilmaxs[jgrp];
         ncart_j = incarts[jgrp];
         nshel_j = inshell[jgrp];
         nprim_j = nshel_j * ncart_j;
         ixps_j = igpoffs[jgrp];
         icen_j = jgrp * 3;
         nfun_j = infuns[jgrp];
         igrpo_j = ioffs[jgrp];

         igp = (double *) malloc(nprim_i * nprim_j * sizeof (double));
         fzero(igp, nprim_i * nprim_j);
         icgp = (double *) malloc((nfun_i * nfun_j + 1) * sizeof (double));
         ioff_i = 0;

         for (ish = 0; ish < nshel_i; ++ish) {
            ioff_j = 0;
            xa = ixpss[ixps_i + ish];
            for (jsh = 0; jsh < nshel_j; ++jsh) {
               xb = ixpss[ixps_j + jsh];
               one = 1;
               basis_shell_pt_charges(
                       xa, &icentres[icen_i], lmin_i, lmax_i, ncart_i,
                       xb, &icentres[icen_j], lmin_j, lmax_j, ncart_j,
                       igp, ioff_i, ioff_j, nprim_j, one, nq, qq, rq);
               ioff_j += ncart_j;
            }
            ioff_i += ncart_i;
         }

         basis_contract_rectangle_group(jgrp, igrp, igp, icgp, inshell, incarts, infuns, ncont, ccs, ccoffs, lmin_j, lmax_j, lmin_i, lmax_i);
         for (i = 0; i < nfun_i; ++i) {
            for (j = 0; j < nfun_j; ++j) {
               a[(igrpo_i + i) * nbas + igrpo_j + j] = icgp[i * nfun_j + j];
            }
         }
         free(igp);
         free(icgp);
      }
   }

   for (i = 0; i < nbas; ++i) {
      for (j = 0; j < i; ++j) {
         a[i * nbas + j] = a[j * nbas + i];
      }
   }
}


