/*
 * File:   Pipe_Host.cpp
 * Author: Sebastião Miranda, Jonas Feldt
 *
 * Version: 2014-09-12
 */

#include <string>
#include <unistd.h>
#include <ios>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>

#include "Pipe_Host.h"
#include "OPLS.h"

#ifdef BASE_PROFILING
   #include <iomanip>
   #include "papi.h"
#endif

#define WRITING 1
#define READING 0
#define FD_FILE "fd.dat"
#define HARTREE_TO_KJMOL 2625.5
#define BOHR_TO_ANG 0.529177209 // Molpro 2012
#define ANG_TO_BOHR 1.0/BOHR_TO_ANG

Pipe_Host::Pipe_Host (std::string base_name, std::string molpro)
: base_name (base_name),
   molpro (molpro)
{

   // Profiling vars
   #ifdef BASE_PROFILING
   t_new=0;
   t_new_end=0;
   t_new_start=0;
   n_new=0;
   t_delete=0;
   t_delete_end=0;
   t_delete_start=0;
   n_delete=0;
   t_readgrid=0;
   t_readgrid_end=0;
   t_readgrid_start=0;
   n_readgrid=0;
   t_bohr=0;
   t_bohr_end=0;
   t_bohr_start=0;
   n_bohr=0;
   #endif

}






Pipe_Host::~Pipe_Host () {
   kill (pid, SIGTERM); // friendly kill
}


/**
 * Launchs child process.
 */
void Pipe_Host::launch_child_process (std::string input) {
   __launch_child_process (input, pipe_pmc_to_userf, pipe_userf_to_pmc);
}


/**
 * Reads a new grid from the pipe and replaced the old grid in place.
 *
 * @param grid
 */
double Pipe_Host::read_grid_pipe (Charge_Grid *grid) {
   
   PROFILE_FIRST(0,t_delete_start);
   delete[] grid->x;
   delete[] grid->y;
   delete[] grid->z;
   delete[] grid->charges;
   PROFILE (0,t_delete,t_delete_end,t_delete_start,n_delete);

   double energy = 0.0;

   int dim = __read_size_pipe (pipe_userf_to_pmc[READING]);
   
   PROFILE_FIRST(0,t_new_start);
   grid->x = new _FPOINT_G_[dim];
   grid->y = new _FPOINT_G_[dim];
   grid->z = new _FPOINT_G_[dim];
   grid->charges = new _FPOINT_G_[dim];
   grid->dim = dim;
   PROFILE (0,t_new,t_new_end,t_new_start,n_new);

   MICRO_PROBE(v_micro_probe);//Time stamp before readgrid
   
   PROFILE_FIRST(0,t_readgrid_start);
   __read_grid_pipe (pipe_userf_to_pmc[READING], grid->x, grid->y, grid->z,
           grid->charges, &energy, &grid->dim);
   PROFILE (0,t_readgrid,t_readgrid_end,t_readgrid_start,n_readgrid);

   MICRO_PROBE(v_micro_probe);//Time stamp before readgrid
   
   PROFILE_FIRST(0,t_bohr_start);
   for (int i = 0; i < dim; ++i) {
      grid->x[i] *= BOHR_TO_ANG;
      grid->y[i] *= BOHR_TO_ANG;
      grid->z[i] *= BOHR_TO_ANG;
   }
   PROFILE (0,t_bohr,t_bohr_end,t_bohr_start,n_bohr);

   return energy * HARTREE_TO_KJMOL;
}
void Pipe_Host::print_profiling(){

#ifdef BASE_PROFILING
   std::cout << "pipe_host times_ " << std::endl; 
   if(n_delete!=0){
         std::cout << "t_delete:            "  
                   << std::setprecision (15) 
                   << t_delete/(n_delete*1000)
                   << " ms"     
                   << "(" 
                   << n_delete 
                   <<" samples)"
                   << std::endl;
   } 
   if(n_new!=0){
         std::cout << "t_new:               "  
                   << std::setprecision (15) 
                   << t_new/(n_new*1000)
                   << " ms"     
                   << "(" 
                   << n_new
                   <<" samples)"
                   << std::endl;
   }
   if(n_readgrid!=0){
         std::cout << "t_readgrid:          "  
                   << std::setprecision (15) 
                   << t_readgrid/(n_readgrid*1000)
                   << " ms"     
                   << "(" 
                   << n_readgrid
                   <<" samples)"
                   << std::endl;
   }
   if(n_bohr!=0){
         std::cout << "t_bohr:              "  
                   << std::setprecision (15) 
                   << t_bohr/(n_bohr*1000)
                   << " ms"     
                   << "(" 
                   << n_bohr
                   <<" samples)"
                   << std::endl;
   }
#endif

}


/**
 * Writes the system to the pipe. The QM molecule is updated and the
 * MM molecules are used as lattice.
 *
 * @param system
 */
void Pipe_Host::write_lattice_pipe (System* system, const OPLS *opls) {
   const auto coords = system->get_normal_coords ();
   __write_lattice_pipe (pipe_pmc_to_userf[WRITING],
           system->molecule2atom[0].natoms,
           system->natom,
           coords[0],
           coords[1],
           coords[2],
           opls->get_normal_charges ());
}




/**
 * Writes the system to the pipe. The QM molecule is updated and the
 * MM molecules are used as lattice. Takes the QM molecule from the qm-system
 * and the lattice from system.
 *
 * @param system
 * @param qm-system
 * @param opls
 */
void Pipe_Host::write_lattice_pipe (System* system, System *qm_system, const OPLS *opls) {
   const auto coords = system->get_normal_coords ();
   const auto qm_coords = qm_system->get_normal_coords ();
   __write_lattice_pipe (
         pipe_pmc_to_userf[WRITING],
         system->molecule2atom[0].natoms,
         system->natom,
         coords[0],
         coords[1],
         coords[2],
         opls->get_normal_charges (),
         qm_coords[0],
         qm_coords[1],
         qm_coords[2]);
}




/**
 *
 * @param pipe_pmc_to_userf
 * @param pipe_userf_to_pmc
 * @return
 */
void Pipe_Host::__launch_child_process (std::string input, int *pipe_pmc_to_userf,
        int *pipe_userf_to_pmc) {

   if ( pipe(pipe_pmc_to_userf) == -1 ) { perror("Error creating pipe"); exit (EXIT_FAILURE);}
   if ( pipe(pipe_userf_to_pmc) == -1 ) { perror("Error creating pipe"); exit (EXIT_FAILURE);}

   // needs to write FDs to input before starting molpro
   write_fd (input, pipe_pmc_to_userf[READING], pipe_userf_to_pmc[WRITING]);

   if ( (pid=fork()) < 0 )   { fprintf(stderr,"Error forking process\n"); exit (EXIT_FAILURE);}

   #ifdef SETCORES
      int core = ((pipe_pmc_to_userf[READING])/4)%4+2;
   #else
      #ifdef SET_VERBOSE_LVL_INIT
         int core = -1;
      #endif
   #endif
   if ( pid == 0 ) { /* CHILD PROCESS */

      close(pipe_pmc_to_userf[WRITING]); // Close Writing side of pipe
      close(pipe_userf_to_pmc[READING]); // Close Reading side of pipe

      VERBOSE_LVL_INIT("[INIT] changing to directory " << base_name << std::endl);
      int e = chdir (base_name.c_str());
      if (e == -1) {
         printf("Error changing directory: errno = %d\n", errno);
         printf("Readable errno: %s\n", strerror(errno));
         fflush(stdout);
         exit(-1);
      }
      #ifdef SETCORES
         //std::string call_line("taskset -cp "+std::to_string(((long long int)core))+" "+molpro);
         //
         e = execlp("taskset","-cp",(std::to_string(((long long int)core))).c_str(),molpro,"--no-xml-output", input.c_str (), (char*) 0);
      #else
         e = execlp(molpro.c_str(), molpro.c_str(), "--no-xml-output", input.c_str (), (char*) 0);
      #endif
      if (e == -1) {
         printf("Error launching execl: errno = %d\n", errno);
         printf("Readable errno: %s\n", strerror(errno));
         fflush(stdout);
         exit(-1);
      }
   }

   /* FATHER PROCESS */

   VERBOSE_LVL_INIT("[INIT] Pipe Host Summoned to core " << core << " with pid " << pid << "; QMup-(R: "<< pipe_pmc_to_userf[READING] <<
                                                   ",W: "<< pipe_userf_to_pmc[WRITING] <<
                                              ") HOST-(R: "<< pipe_userf_to_pmc[READING] <<
                                                   ",W: "<< pipe_pmc_to_userf[WRITING] << ")"<<std::endl);
  
   //XXX: Should close all unused pipe sides at the end ?
   //close(pipe_pmc_to_userf[READING]); // Close Reading side of pipe
   //close(pipe_userf_to_pmc[WRITING]); // Close Writing side of pipe
}



void Pipe_Host::write_fd (std::string input, int fd_read, int fd_write) {

//   std::ofstream out;
//   std::string file = base_name + "/" + FD_FILE;
//   out.open (file, std::ios::app);
//
//   if (out.is_open ()) {
//      out << pid << " " << fd_read << " " << fd_write << std::endl;
//      out.close ();
//   } else {
//      std::cerr << " ERROR Cannot write file descriptors for pipe." << std::endl;
//      exit (EXIT_FAILURE);
//   }

   // names in template
   std::string read_template = "$read$";
   std::string write_template = "$write$";

   // strings which shall be added
   std::string read_s = std::to_string (fd_read);
   std::string write_s = std::to_string (fd_write);


   // reads file into string
   std::string in_name = base_name + "/" + input;
   std::ifstream in (in_name);
   std::stringstream sstr;
   sstr << in.rdbuf();
   std::string templ = sstr.str();

   size_t found;
   // adds read fd
   if ((found = templ.find (read_template)) != std::string::npos) {
      templ.replace (found, read_template.length(), read_s);
   } else {
      printf("Cannot find %s in the template to add the FD.\n", read_template.c_str());
      exit(EXIT_FAILURE);
   }

   // adds write fd
   if ((found = templ.find (write_template)) != std::string::npos) {
      templ.replace (found, write_template.length(), write_s);
   } else {
      printf("Cannot find %s in the template to add the FD.\n", write_template.c_str());
      exit(EXIT_FAILURE);
   }

   // opens stream for modified tmp file
   std::string tmp_name = base_name + "/tmp_" + input;
   std::ofstream file_out (tmp_name.c_str());
   file_out << templ;
   file_out.close();

   if (rename (tmp_name.c_str(), in_name.c_str()) != 0) {
      perror ("Error: Cannot rename input when adding FDs.");
      exit (EXIT_FAILURE);
   }
}



/**
 *
 *
 * @param pipe_writefd
 * @param n_qm_atom
 * @param n_total_atom
 * @param rx
 * @param ry
 * @param rz
 * @return
 */
int Pipe_Host::__write_lattice_pipe (int pipe_writefd, int n_qm_atom,
        int n_total_atom, _FPOINT_S_ * rx, _FPOINT_S_ * ry, _FPOINT_S_ * rz,
        const _FPOINT_S_ *charges) {

   int n;
   double eight_bytes;

   //Write number of total (QM) atoms
   n = write(pipe_writefd,&n_qm_atom,sizeof(int));
   if(n==-1){perror("pipe");return -1;}
   if(n!=sizeof(int)){
      printf("Error: Write qm atoms: badly formated data. Expected to write %lu bytes, wrote %i.\n", sizeof(int), n);
      return-1;
   }

   //Write number of total (QM+MM) atoms
   n = write(pipe_writefd,&n_total_atom,sizeof(int));
   if(n==-1){perror("pipe");return -1;}
   if(n!=sizeof(int)){
      printf("Error: Write total atoms: badly formated data. Expected to write %lu bytes, wrote %i.\n", sizeof(int), n);
      return-1;
   }

   //Write xyz total
   for (int atom = 0; atom < n_total_atom; ++atom) {

      for (int i = 0; i < 4; ++i) {
         if (i == 0) {eight_bytes=(double)rx[atom] * ANG_TO_BOHR;}
         if (i == 1) {eight_bytes=(double)ry[atom] * ANG_TO_BOHR;}
         if (i == 2) {eight_bytes=(double)rz[atom] * ANG_TO_BOHR;}
         if (i == 3) {eight_bytes=(double)charges[atom];}

         n = write(pipe_writefd,&eight_bytes,sizeof(double));
         if (n == -1) {perror("pipe");return -1;}
         if(n!=sizeof(double)){
            printf("Error (atom %d): badly formated data. Expected to write %lu bytes, wrote %i.\n",atom,sizeof(double), n);
            return-1;
         }
      }
   }
   return 0;
}




/**
 *
 *
 * @param pipe_writefd
 * @param n_qm_atom
 * @param n_total_atom
 * @param rx
 * @param ry
 * @param rz
 * @param qm-rx
 * @param qm-ry
 * @param qm-rz
 * @return
 */
int Pipe_Host::__write_lattice_pipe (int pipe_writefd, int n_qm_atom,
        int n_total_atom, _FPOINT_S_ * rx, _FPOINT_S_ * ry, _FPOINT_S_ * rz,
        const _FPOINT_S_ *charges, _FPOINT_S_ *qm_rx, _FPOINT_S_ *qm_ry,
        _FPOINT_S_ *qm_rz) {

   int n;
   double eight_bytes;

   //Write number of total (QM) atoms
   n = write(pipe_writefd,&n_qm_atom,sizeof(int));
   if(n==-1){perror("pipe");return -1;}
   if(n!=sizeof(int)){
      printf("Error: Write qm atoms: badly formated data. Expected to write %lu bytes, wrote %i.\n", sizeof(int), n);
      return-1;
   }

   //Write number of total (QM+MM) atoms
   n = write(pipe_writefd,&n_total_atom,sizeof(int));
   if(n==-1){perror("pipe");return -1;}
   if(n!=sizeof(int)){
      printf("Error: Write total atoms: badly formated data. Expected to write %lu bytes, wrote %i.\n", sizeof(int), n);
      return-1;
   }

   // Writes qm xyz 
   for (int atom = 0; atom < n_qm_atom; ++atom) {

      for (int i = 0; i < 4; ++i) {
         if (i == 0) {eight_bytes=(double)qm_rx[atom] * ANG_TO_BOHR;}
         if (i == 1) {eight_bytes=(double)qm_ry[atom] * ANG_TO_BOHR;}
         if (i == 2) {eight_bytes=(double)qm_rz[atom] * ANG_TO_BOHR;}
         if (i == 3) {eight_bytes=(double)charges[atom];}

         n = write(pipe_writefd,&eight_bytes,sizeof(double));
         if (n == -1) {perror("pipe");return -1;}
         if(n!=sizeof(double)){
            printf("Error (atom %d): badly formated data. Expected to write %lu bytes, wrote %i.\n",atom,sizeof(double), n);
            return-1;
         }
      }
   }

   // writes lattice xyz
   for (int atom = n_qm_atom; atom < n_total_atom; ++atom) {

      for (int i = 0; i < 4; ++i) {
         if (i == 0) {eight_bytes=(double)rx[atom] * ANG_TO_BOHR;}
         if (i == 1) {eight_bytes=(double)ry[atom] * ANG_TO_BOHR;}
         if (i == 2) {eight_bytes=(double)rz[atom] * ANG_TO_BOHR;}
         if (i == 3) {eight_bytes=(double)charges[atom];}

         n = write(pipe_writefd,&eight_bytes,sizeof(double));
         if (n == -1) {perror("pipe");return -1;}
         if(n!=sizeof(double)){
            printf("Error (atom %d): badly formated data. Expected to write %lu bytes, wrote %i.\n",atom,sizeof(double), n);
            return-1;
         }
      }
   }
   return 0;
}



int Pipe_Host::__read_size_pipe (int pipe_readfd) {

   int n;
   int ngrid;

   //Read number of grid points
   n = read(pipe_readfd,&ngrid,sizeof(int));
   if(n==-1){perror("pipe");return -1;}
   if(n!=sizeof(int)){
      printf("Error: Read grid size: badly formated data. Expected %lu bytes, got %i.\n", sizeof(int), n);
      return-1;
   }
   return ngrid;
}





/**
 *
 *
 * @param pipe_readfd
 * @param gx
 * @param gy
 * @param gz
 * @param gc
 * @param qm_energy
 * @return
 */
int Pipe_Host::__read_grid_pipe (int pipe_readfd, _FPOINT_G_ * gx, _FPOINT_G_ * gy,
        _FPOINT_G_ * gz, _FPOINT_G_ * gc, double* qm_energy, int *ngrid) {

   int n;
   double eight_bytes;

   //Read grid x,y,z
   for (int point = 0; point < *ngrid; ++point) {

      for (int i = 0; i < 3; ++i) {

         n=read(pipe_readfd,&eight_bytes,sizeof(double));
         if(n==-1){perror("pipe");return -1;}
         if(n!=sizeof(double)){
            printf("Error (grid point %d): badly formated data. Expected %lu bytes, got %i.\n",point,sizeof(double), n);
            return-1;
         }

         if (i == 0) {gx[point] = (_FPOINT_G_) eight_bytes;}
         if (i == 1) {gy[point] = (_FPOINT_G_) eight_bytes;}
         if (i == 2) {gz[point] = (_FPOINT_G_) eight_bytes;}
      }

   }

   //Read grid charge
   for (int point = 0; point < *ngrid; ++point) {

      n = read(pipe_readfd,&eight_bytes,sizeof(double));
      if (n == -1) {perror("pipe");return -1;}
      if (n != sizeof(double)){
         printf("Error (grid point %d): badly formated data. Expected %lu bytes, got %i.\n",point,sizeof(double), n);
         return-1;
      }
      gc[point] = (_FPOINT_G_) eight_bytes;
   }


   //read QM energy
   n = read(pipe_readfd,qm_energy,sizeof(double));
   if(n==-1){perror("pipe");return -1;}
   if(n!=8){
      printf("Error (qm energy): badly formated data. Expected %lu bytes, got %i.\n",sizeof(double), n);
      return-1;
   }
   return 0;
}
