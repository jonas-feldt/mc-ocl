

#include "distance_matrix.h"
#include "System.h"
#include "geom_utils.h"




DistanceMatrix::~DistanceMatrix ()
{
   if (dists)
   {
      for (unsigned int i = 0; i < dim; ++i) {
         delete[] dists[i];
      }
      delete[] dists;
   }
}


DistanceMatrix* DistanceMatrix::clone () const
{
   return new DistanceMatrix ();
}



void DistanceMatrix::SetContext (System *system)
{
   if (dists) // in case the context changes
   {
      for (unsigned int i = 0; i < dim; ++i) {
         delete[] dists[i];
      }
      delete[] dists;
   }

   dim = system->natom;
   x = system->rx;
   y = system->ry;
   z = system->rz;
   mol2atom = &system->molecule2atom;

   dists = new double*[dim]; // creates distances matrix
   for (unsigned int i = 0; i < dim; ++i) {
      dists[i] = new double[dim];
   }
   for (unsigned int i = 0; i < dim; ++i) { // initializes trace
      dists[i][i] = 0.0;
   }
   ComputeDistances ();
}



void DistanceMatrix::ComputeDistances ()
{
   for (unsigned int i = 0; i < dim - 1; ++i) {
      for (unsigned int j = i + 1; j < dim; ++j) {
         double dist = compute_distance (
               x[i], y[i], z[i],
               x[j], y[j], z[j]);
         dists[i][j] = dist;
      }
   }
}



void DistanceMatrix::ComputeDistances (unsigned int molecule)
{
   for (int pos = 0 ; pos < (*mol2atom)[molecule].natoms; ++pos){
      unsigned int i = (*mol2atom)[molecule].atom_id[pos];
      for (unsigned int j = 0; j < dim; ++j) {
         const double dist = compute_distance (
                  x[i], y[i], z[i],
                  x[j], y[j], z[j]);
         if (i < j) {
            dists[i][j] = dist;
         } else {
            dists[j][i] = dist;
         }
      }
   }
}


DistanceCall DistanceMatrix::GetDelegate() const
{
   return DistanceCall::create<DistanceMatrix, &DistanceMatrix::GetDistance>(this);
}


inline double DistanceMatrix::GetDistance (unsigned int i, unsigned int j) const
{
   if (i > j) std::swap (i, j);
   return dists[i][j];
}



void DistanceMatrix::accept (DistanceInterface *other, unsigned int molecule)
{
   DistanceCall GetDistanceOther = other->GetDelegate ();
   for (int pos = 0 ; pos < (*mol2atom)[molecule].natoms; ++pos){
      unsigned int i = (*mol2atom)[molecule].atom_id[pos];
      for (unsigned int j = 0; j < dim; ++j) {
         if (i < j) {
            dists[i][j] = GetDistanceOther (i, j);
         } else {
            dists[j][i] = GetDistanceOther (j, i);
         }
      }
   }
}





