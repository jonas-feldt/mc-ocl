/*
 * File:   PMC_Periodic.cpp
 * Author: Jonas Feldt
 * 
 * Version: 2015-01-06
 */

#include <iostream>
#include <math.h>
#include <iomanip>
#include <string.h>
#include <sstream>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <algorithm>
#include <iomanip>
#include <signal.h>
#include <functional>
#include <thread>
#include <future>

#include "PMC_Periodic.h"
#include "OCLmanager.h"
#include "jonas_io_utils.h"
#include "math_utils.h"
#include "geom_utils.h"
#include "System_Periodic.h"
#include "Run_Periodic.h"
#include "OPLS_Periodic.h"
#include "Charge_Grid.h"
#include "typedefs.h"


#include "Pipe_Host.h"

#include "options.h" // Header with all run options.

#ifdef BASE_PROFILING
   #include "papi.h"
#endif


/**
 * Constructor
 */
PMC_Periodic::PMC_Periodic (
      const std::string m_base_name,
      const Run_Periodic *run,
      const OPLS_Periodic *opls,
      const int num_devices,
      const int num_chains)
   : m_base_name (m_base_name), opls (opls),
   num_devices (num_devices), num_chains (num_chains)
{
   // works on a copy to not modify the original run object
   m_run = new Run_Periodic(*run);
}



/**
 * Destructor
 */
PMC_Periodic::~PMC_Periodic () {
   delete m_run;
}




/**
 * Runs the PMC simulation.
 *
 * @param systems
 * @param energy_qm_gp
 *
 * @return a vector of vectors of the final systems (=systems[n_device][n_chain])
 */
vvs PMC_Periodic::run (vvs &systems, const double energy_qm_gp) {

   m_energy_qm_gp = energy_qm_gp;

   vvd energies_mm       (systems.size(), vd(systems.begin()->size())); // current MM energies
   vvd energies_old      (systems.size(), vd(systems.begin()->size())); // total energies of the last step
   vvd energies_vdw_qmmm (systems.size(), vd(systems.begin()->size())); // current vdW and Coulomb MM energies
   vvd energies_qm       (systems.size(), vd(systems.begin()->size())); // current QM & Coulomb QMMM energy

   PROFILE_FIRST(PROF_EVERY,t_firstmol_start);

   vvg grids = qm_lattice(
         systems,
         energies_mm,
         energies_old,
         energies_vdw_qmmm,
         energies_qm);

   PROFILE(PROF_EVERY,t_firstmol,t_firstmol_end,t_firstmol_start, n_firstmol);

   return run (systems, m_energy_qm_gp, grids, energies_mm, energies_old,
         energies_vdw_qmmm, energies_qm);
}



/**
 * Runs the PMC simulation.
 *
 * @param system
 *
 * @return a vector of vectors of the final systems (=systems[n_device][n_chain])
 */
vvs PMC_Periodic::run (vvs &systems)
{
   vvd energies_mm       (systems.size(), vd(systems.begin()->size())); // current MM energies
   vvd energies_old      (systems.size(), vd(systems.begin()->size())); // total energies of the last step
   vvd energies_vdw_qmmm (systems.size(), vd(systems.begin()->size())); // current vdW and Coulomb MM energies
   vvd energies_qm       (systems.size(), vd(systems.begin()->size())); // current QM & Coulomb QMMM energy

   PROFILE_FIRST(PROF_EVERY,t_firstmol_start);

   m_energy_qm_gp = energy_gp(systems[0][0]);

   vvg grids = qm_lattice(
         systems,
         energies_mm,
         energies_old,
         energies_vdw_qmmm,
         energies_qm);

   PROFILE(PROF_EVERY,t_firstmol,t_firstmol_end,t_firstmol_start, n_firstmol);

   return run (systems, m_energy_qm_gp, grids, energies_mm, energies_old,
         energies_vdw_qmmm, energies_qm);
}




/**
 * Runs the PMC simulation.
 *
 * @param system
 *
 * @return a vector of vectors of the final systems (=systems[n_device][n_chain])
 */
vvs PMC_Periodic::run (System_Periodic *system)
{
   PROFILE_FIRST(PROF_EVERY,t_firstmol_start);

   m_energy_qm_gp = energy_gp(system);

   // QM in gas-phase to calculate later the interaction energy
   Charge_Grid *grid = new Charge_Grid ();
   #ifndef STANDALONE
      // QM with lattice to generate initial WFUs
      VERBOSE_LVL_INIT("[INIT] Lattice update:" << std::endl);
      double energy_qm = qm_lattice_updates_wf (0, 0, system, opls, m_base_name, m_run);
      VERBOSE_LVL_INIT("\tInitializing Grid (and reading " << m_base_name + GRID_FILE << ")" << std::endl);
      grid->read_density (m_base_name + GRID_FILE);
   #else
      m_energy_qm_gp   = -76.02598452 * 2625.5;
      double energy_qm = -77.13613237 * 2625.5;
      VERBOSE_LVL_INIT("\tInitializing Grid (and reading ." << GRID_FILE << ")" << std::endl);
      grid->read_density (std::string(".") + std::string(GRID_FILE));
   #endif

   // checks sanity of grid
   grid->reduced_coords (m_run->dimension_box, m_run->cutoff_coulomb);
   if (!grid->is_sane(m_base_name + ".out") && !IGNORE_ERRORS) exit(EXIT_FAILURE); 
   VERBOSE_LVL_INIT("\tGrid Points " << grid->dim << std::endl);

   // computes initial MM, QMMM energies
   VERBOSE_LVL_INIT("[INIT] System MM atoms=" << system->natom << std::endl);    
   VERBOSE_LVL_INIT("[INIT] Computing first MM..." << std::endl);
   double energy_vdw_qmmm = opls->energy_vdw_qmmm (system->GetDistance);
   double energy_mm       = opls->energy_mm       (system->GetDistance);
   VERBOSE_LVL_INIT("\tVDW_QMMM energy: " << energy_vdw_qmmm << std::endl);
   VERBOSE_LVL_INIT("\tMM energy: "       << energy_mm       << std::endl);
   double energy_old = energy_qm + energy_vdw_qmmm + energy_mm;

   // prepares data for multi-device, multi-chain jobs
   vvd energies_mm       (num_devices, vd(num_chains)); // current MM energies
   vvd energies_old      (num_devices, vd(num_chains)); // total energies of the last step
   vvd energies_vdw_qmmm (num_devices, vd(num_chains)); // current vdW and Coulomb MM energies
   vvd energies_qm       (num_devices, vd(num_chains)); // current QM & Coulomb QMMM energy
   vvg grids;
   vvs systems;
   grids  .reserve(num_devices);
   systems.reserve(num_devices);

   for (int i = 0; i < num_devices; ++i) {
      vs tmpS;
      vg tmpG;
      tmpS.reserve(num_chains);
      tmpG.reserve(num_chains);
      for (int j = 0; j < num_chains; ++j) {
         tmpG.push_back(new Charge_Grid(*grid)); // copies of grid
         tmpS.push_back(system);
         energies_mm       [i][j] = energy_mm;
         energies_old      [i][j] = energy_old;
         energies_vdw_qmmm [i][j] = energy_vdw_qmmm;
         energies_qm       [i][j] = energy_qm;
      }
      systems.push_back(tmpS);
      grids  .push_back(tmpG);
   }
   delete grid;

   PROFILE(PROF_EVERY,t_firstmol,t_firstmol_end,t_firstmol_start, n_firstmol);

   return run (systems, m_energy_qm_gp, grids, energies_mm, energies_old,
         energies_vdw_qmmm, energies_qm);
}




/**
 * Runs the PMC simulation.
 *
 * @param system
 * @param run
 * @param opls
 * @param traject_file
 * @param energy_qm_gp
 * @param grids
 * @param energies_mm
 * @param energies_old
 * @param energies_vdw_qmmm
 * @param energies_qm
 *
 * @return a vector of vectors of the final systems (=systems[n_device][n_chain])
 */
vvs PMC_Periodic::run (
      vvs &systems, 
      const double energy_qm_gp,
      vvg &grids,
      const vvd &energies_mm,
      const vvd &energies_old,
      const vvd &energies_vdw_qmmm,
      const vvd &energies_qm)
{
   int update_lists_every;

   // TODO are these used anywhere?
   double stepmax       = m_run->stepmax;
   double temperature   = m_run->temperature;
   double theta_max     = m_run->theta_max;

   int old_adjust_stepmax=m_run->adjust_max_step; 
   int old_maxmoves=m_run->maxmoves; 
   //Adjust maxmoves according to number of chains
   m_run->maxmoves = (m_run->maxmoves)/(num_chains * num_devices);

   if (m_run->wf_adjust > m_run->maxmoves) m_run->wf_adjust = 0;//No WFU.

   if (m_run->adjust_max_step == 0){
      //update_lists_every must be the closest multiple to m_wf_adjust
      if (m_run->wf_adjust == 0)
	 update_lists_every = m_run->n_default_autonomous;
      else if(m_run->n_default_autonomous > m_run->wf_adjust)
         update_lists_every = (m_run->n_default_autonomous / m_run->wf_adjust) * m_run->wf_adjust;
      else
         update_lists_every = m_run->wf_adjust;

   }else{

      //update_lists_every and ajdust_max_step
      //must be the closest multiple to m_wf_adjust

      if (m_run->wf_adjust!=0)
         m_run->adjust_max_step =((int)m_run->adjust_max_step / m_run->wf_adjust)*m_run->wf_adjust;
     
      update_lists_every = m_run->adjust_max_step;
   } 
     
   if(m_run->wf_adjust!=0){
      //max_moves should be a multiple of wf_adjust
      m_run->maxmoves =(ceil((double)m_run->maxmoves/(double)m_run->wf_adjust))*m_run->wf_adjust;
   }

   VERBOSE_LVL_WARN("[WARN] Set Update Lists Every : " << update_lists_every << "(was " << m_run->n_default_autonomous << ")" << std::endl);
   VERBOSE_LVL_WARN("[WARN] Set Step Max : " << m_run->adjust_max_step <<"(was "<<old_adjust_stepmax<<")" << std::endl);
   VERBOSE_LVL_WARN("[WARN] Set Maxmoves : " << m_run->maxmoves <<"(was "<<old_maxmoves<<")" << std::endl);
    
   #ifndef STANDALONE
      VERBOSE_LVL_INIT("[INIT] NON-STANDALONE RUN..." << std::endl);
      
      VERBOSE_LVL_INIT("[INIT] Making directory " << m_base_name << "..."<< std::endl);
      make_directory_jf (m_base_name);

      Pipe_Host *hosts[num_devices * num_chains];
     
      int index = 0;
      for (auto outer_it = systems.begin(); outer_it != systems.end(); ++outer_it) {
         for (auto it = outer_it->begin(); it != outer_it->end(); ++it) {
            hosts[index] = new Pipe_Host(m_base_name, m_run->molpro_pipe_exe);
            const std::string tmp = "pipe_" + std::to_string(index);
            write_molecule_qm_jf (m_run->pipe_input, m_base_name, tmp, (*it));
            hosts[index]->launch_child_process (tmp + ".inp");
            index++;
         }
      }

   #else
      VERBOSE_LVL_INIT("[INIT] FULL STANDALONE RUN..." << std::endl);

      // Have this vector just for compatibility reasons
      Pipe_Host *hosts[num_devices * num_chains];                     
   #endif

   //
   // The main code is in ocl::run
   //

   VERBOSE_LVL_INIT("[INIT] Initializing OCL..." << std::endl);
   vvs final_systems;
   OCLmanager *ocl; 
   try{      
      int tmp = 0;
      if (m_run->print_energy != 0) {
         tmp = m_run->print_every / m_run->print_energy * m_run->n_save_systems * N_ENERGY_SAVE_PARAMS;
      }
      ocl = create_manager (
            update_lists_every,  // Max number of steps OCL can run between list refreshes
            10,                  // 10 max kernels
            num_devices,         // number of devices
            num_chains,          // number of chains 
            systems,              // ptr to system
            opls,                // ptr to opls
            m_run,                 // ptr to m_run
            grids,                // ptr to grid
                                 // size of save-config data in opencl devices:
            systems[0][0]->natom*m_run->n_save_systems*3+m_run->n_save_systems*N_ENERGY_SAVE_PARAMS,
            tmp, 
                                 // Some m_run parameters:
            temperature,         
            theta_max,
            stepmax,
            energies_qm, 
            energy_qm_gp,
            energies_mm,
            energies_vdw_qmmm,
            energies_old,
            hosts,
            m_base_name);
         
      // Init Opencl, find platforms, contexts, devices
      VERBOSE_LVL_INIT("[INIT] PMC_Periodic Scope: Initing PMC" << std::endl);
      
      PROFILE_FIRST(PROF_EVERY,t_oclinit_start);   
      ocl->init();        
      PROFILE(PROF_EVERY,t_oclinit_acc,t_oclinit_end,t_oclinit_start, n_oclinit);

      VERBOSE_LVL_INIT("[INIT] PMC_Periodic Scope: Starting PMC" << std::endl);
      
      PROFILE_FIRST(PROF_EVERY,t_mc_start);   
      ocl->run();
      PROFILE(PROF_EVERY,t_mc_acc,t_mc_end,t_mc_start, n_mc);
      
      ocl->final_profile();
      final_systems = ocl->get_final_systems();
   
   }catch(cl::Error &error){//openCL error catching
      std::cout<<"PMC_PERIODIC::Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      exit(EXIT_FAILURE);
   }

   #ifdef BASE_PROFILING
     std::cout << "PMC_Periodic outer: First molpros time (&first file write):      "  
                << std::setprecision (15) 
                << t_firstmol/(n_firstmol*1000)
                << " ms"
                << std::endl;

      std::cout << "PMC_Periodic outer: OCL Init time (ocl->init()):      "  
                << std::setprecision (15) 
                << t_oclinit_acc/(n_oclinit*1000)
                << " ms"
                << std::endl;
   
      std::cout << "PMC_Periodic outer: MC time (ocl->run()):      "  
                << std::setprecision (15) 
                << t_mc_acc/(n_mc*1000)
                << " ms"
                << std::endl;
      std::cout << "(Counting the time for the complete execution, without opencl init and without"
                << "the first molpro call (grid 0) - or grid.dat read in standalone)"
                << std::endl;  
   #endif

   VERBOSE_LVL_SYNC("[SYNC] Master thread will shutdown");

   for (auto outer_it = grids.begin(); outer_it != grids.end(); ++outer_it) {
      for (auto it = outer_it->begin(); it != outer_it->end(); ++it) {
         delete (*it);
      }
   }
   delete ocl;
   
   #ifndef STANDALONE
      for (int i = 0; i < num_devices * num_chains; i++){
         delete hosts[i];
      }
   #endif

   return final_systems;
}




/**
 * QM calculation with full lattice that generates or updates the wavefunction.
 *
 * @param m_base_name base name of this job
 * @param counter counter for all steps
 * @param system
 * @param opls
 * @return energy that Molpro shows as final result.
 */
double PMC_Periodic::qm_lattice_updates_wf (
      const unsigned int device,
      const unsigned int chain,
      System_Periodic *system,
      const OPLS_Periodic *opls,
      std::string base_name,
      const Run *run)
{
   const std::string lattice_name = "lattice";
   const std::string input_name = "qm_lattice";

   const std::string folder = std::to_string(device) + "-" + std::to_string(chain);
   base_name += "/" + folder;
   make_directory_jf(base_name);
   // TODO update this procedure...
   write_molecule_qm_jf (run->grid_input, base_name, input_name, system);
   write_latt_file_name_jf (base_name, lattice_name, system, opls->get_normal_charges ());
   put_latt_to_input_name_jf (base_name, input_name, lattice_name);
   molpro_lattice (base_name, input_name, run->molpro_grid_exe, run->molpro_grid_cores);
   return read_energy_molpro_jf (base_name, input_name);
}




/**
 * QM calculation in gas-phase that generates or updates the wavefunction. Uses
 * at the moment always the same name and should therefore used only once at the
 * beginning.
 *
 * @param m_base_name base name of this job
 * @param system
 * @return energy that Molpro shows as final result.
 */
double PMC_Periodic::qm_gas_phase_updates_wf (
      System_Periodic *system,
      const std::string base_name,
      const Run *run)
{
   static const std::string file_name = "qm_gp";
   
   write_molecule_qm_jf (run->grid_input, base_name, file_name, system);
   put_dummy_latt_to_input_jf (base_name, file_name);
   molpro_lattice (base_name, file_name, run->molpro_exe, run->molpro_exe_cores);
   return read_energy_molpro_jf (base_name, file_name);
}



double PMC_Periodic::get_energy_qm_gp () {
   return m_energy_qm_gp;
}




/**
 * Computes the initial grids and energies.
 *
 * @param energies_mm
 * @param energies_old
 * @param energies_vdw_qmmm
 * @param energies_qm
 */
vvg PMC_Periodic::qm_lattice(
      vvs &systems,
      vvd &energies_mm,
      vvd &energies_old,
      vvd &energies_vdw_qmmm,
      vvd &energies_qm)
{
   // QM with lattice to generate initial WFUs
   vvg grids;
   grids.reserve(systems.size());
   #ifndef STANDALONE

      VERBOSE_LVL_INIT("[INIT] System MM atoms=" << systems[0][0]->natom << std::endl);    
      VERBOSE_LVL_INIT("[INIT] Computing first MM..."<< std::endl);

      std::vector<std::vector<std::future<double>>> futures;
      futures.reserve(systems.size());
      for (unsigned int i = 0; i < systems.size(); ++i) {
         std::vector<std::future<double>> tmp;
         tmp.reserve(systems.begin()->size());
         for (unsigned int j = 0; j < systems.begin()->size(); ++j) {
            VERBOSE_LVL_INIT("[INIT] Lattice update: " << i << "-" << j << std::endl);
            std::packaged_task<double ()> task(std::bind(
                   qm_lattice_updates_wf, i, j, systems[i][j], opls, m_base_name, m_run));
            tmp.push_back(task.get_future());
            std::thread(move(task)).detach();
         }
         futures.push_back(std::move(tmp));
      }

      for (unsigned int i = 0; i < systems.size(); ++i) {

         vg tmp;
         tmp.reserve(systems.begin()->size());
         for (unsigned int j = 0; j < systems.begin()->size(); ++j) {
            System_Periodic *system = systems[i][j];

            energies_qm[i][j] = futures[i][j].get(); 

            system->UpdateDistances ();
            energies_vdw_qmmm[i][j] = opls->energy_vdw_qmmm (system->GetDistance);
            energies_mm      [i][j] = opls->energy_mm       (system->GetDistance);
            VERBOSE_LVL_INIT("\tQM energy: " << i << "-" << j << " " << energies_qm[i][j] << std::endl);
            VERBOSE_LVL_INIT("\tVDW_QMMM energy: " << i << "-" << j << " " << energies_vdw_qmmm[i][j] << std::endl);
            VERBOSE_LVL_INIT("\tMM energy: "       << i << "-" << j << " " << energies_mm[i][j] << std::endl);

            energies_old[i][j] = energies_qm[i][j] + energies_vdw_qmmm[i][j] + energies_mm[i][j];

            const std::string folder = "/" + std::to_string(i) + "-" + std::to_string(j);
            VERBOSE_LVL_INIT("\tInitializing Grid (and reading " << m_base_name + folder + GRID_FILE << ")" << std::endl);
            Charge_Grid *grid = new Charge_Grid ();
            grid->read_density (m_base_name + folder + GRID_FILE);
            grid->reduced_coords (m_run->dimension_box, m_run->cutoff_coulomb);
            if (!grid->is_sane(m_base_name + ".out") && !IGNORE_ERRORS) exit(EXIT_FAILURE); 
            VERBOSE_LVL_INIT("\tGrid Points " << grid->dim << std::endl);
            tmp.push_back(grid);
         }
         grids.push_back(tmp);
      }

   #else
      for (unsigned int i = 0; i < systems.size(); ++i) {
         vg tmp;
         tmp.reserve(systems.begin()->size());
         for (unsigned int j = 0; j < systems.begin()->size(); ++j) {
            System_Periodic *system = systems[i][j];

            energies_qm[i][j] = -77.13613237 * 2625.5;
            energies_vdw_qmmm[i][j] = opls->energy_vdw_qmmm (system->GetDistance);
            energies_mm      [i][j] = opls->energy_mm       (system->GetDistance);
            energies_old[i][j] = energies_qm[i][j] + energies_vdw_qmmm[i][j] + energies_mm[i][j];
            VERBOSE_LVL_INIT("Initializing Grid (and reading ." << GRID_FILE << ")" << std::endl);
            Charge_Grid *grid = new Charge_Grid ();
            grid->read_density (std::string(".") + std::string(GRID_FILE));
            grid->reduced_coords (m_run->dimension_box, m_run->cutoff_coulomb);
            if (!grid->is_sane(m_base_name + ".out") && !IGNORE_ERRORS) exit(EXIT_FAILURE); 
            VERBOSE_LVL_INIT("\tGrid Points " << grid->dim << std::endl);
            tmp.push_back(grid);
         }
         grids.push_back(tmp);
      }
   #endif

   return grids;
}




/**
 * QM in gas-phase to calculate later the interaction energy
 */
double PMC_Periodic::energy_gp (System_Periodic *system)
{
   #ifndef STANDALONE
      VERBOSE_LVL_INIT("[INIT] Making directory " << m_base_name << "..."<< std::endl);
      make_directory_jf (m_base_name);
      VERBOSE_LVL_INIT("[INIT] Writing lattice file..." << std::endl);
      write_dummy_latt_file_jf (m_base_name);
      VERBOSE_LVL_INIT("[INIT] Gas phase update wf.." << std::endl);
      // uses just the first system here
      return qm_gas_phase_updates_wf (system, m_base_name, m_run);
   #else
      return (-76.02598452 * 2625.5);
   #endif
}




OCLmanager* PMC_Periodic::create_manager (
      int n_pre_steps, 
      int nkernel, 
      int ndevices, 
      int nchains, 
      const vvs &systems,
      const OPLS_Periodic* opls, 
      Run_Periodic* run, 
      vvg &grid, 
      int config_data_size, 
      int energy_data_size, 
      _FPOINT_S_ temperature,
      double theta_max,
      double stepmax,   
      const vvd &energies_qm, 
      _FPOINT_S_ energy_qm_gp,
      const vvd &energies_mm,
      const vvd &energies_vdw_qmmm,
      const vvd &energies_old,
      Pipe_Host **hosts,
      std::string basename)
{
   return new OCLmanager(
         n_pre_steps,  // Max number of steps OCL can run between list refreshes
         nkernel,                  // 10 max kernels
         ndevices,         // number of devices
         nchains,          // number of chains 
         systems,              // ptr to system
         opls,                // ptr to opls
         run,                 // ptr to m_run
         grid,                // ptr to grid
         config_data_size,
         energy_data_size, 
         temperature,         
         theta_max,
         stepmax,
         energies_qm, 
         energy_qm_gp,
         energies_mm,
         energies_vdw_qmmm,
         energies_old,
         hosts,
         basename);
}
