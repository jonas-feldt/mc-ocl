/**
 *
 * File: FEP_OCLDevice_FES.h
 * Author: Jonas Feldt
 *
 * Version: 2015-10-30
 *
 */

#ifndef FEP_OCLDEVICE_FES_H_INCLUDED
#define FEP_OCLDEVICE_FES_H_INCLUDED

#define __CL_ENABLE_EXCEPTIONS

#include <condition_variable>

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

#include "FEP_OCLDevice.h"

class System_Periodic;
class OPLS_Periodic;
class Run_Periodic;

class FEP_OCLDevice_FES : public FEP_OCLDevice {

public:

   FEP_OCLDevice_FES(
             cl::Device device,
             cl::Context *context,
             int *ignite_isReady,
             std::mutex *ignite_mutex,
             std::condition_variable *ignite_slave,
             std::condition_variable *ignite_master,
             int n_pre_steps,
             int nkernel,
             int max_chains,
             System_Periodic* systemA,
             const OPLS_Periodic* opls,
             const OPLS_Periodic* oplsB,
             Run_Periodic* run,
             _FPOINT_S_ temperature,
             int ndevices,
             bool extra_energy_file);

   virtual ~FEP_OCLDevice_FES() override;

   //Sets up Kernels and buffers for PMC Cycle
   virtual void setup_pmc(std::string kernel_file_closure) override;

protected:

   cl::Buffer buffer_fes_reference;   
   cl::Buffer buffer_fes_target;   

   double factor_fes_reference;
   double factor_fes_target;

};

#endif //FEP_OCLDEVICE_FES_H_INCLUDED
