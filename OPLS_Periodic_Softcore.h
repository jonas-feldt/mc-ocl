/*
 * File:   OPLS_Periodic_Softcore.h
 * Author: Jonas Feldt
 *
 * Version: 2017-07-14
 */

#ifndef OPLS_PERIODIC_SOFTCORE_H
#define	OPLS_PERIODIC_SOFTCORE_H

#include <string>

#include "OPLS_Periodic.h"
#include "typedefs.h"

class System_Periodic;
class Run_Periodic;

class OPLS_Periodic_Softcore : public OPLS_Periodic {
public:

   OPLS_Periodic_Softcore (
         const System *system,
         Run_Periodic *run,
         const unsigned int n,
         const unsigned int m,
         const double alpha,
         const double lambda);

   double energy_vdw_qmmm (DistanceCall GetDistance) const override;
   double energy_vdw_qmmm (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const override;
   double energy_vdw_qmmm (System *mm_system, System *qm_system) override;

   void print (std::string outfile) override;

protected:

   double energy_vdw (DistanceCall GetDistance) const override;
   double energy_vdw (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const override;

   const double s1;
   const double s2;

};

#endif	/* OPLS_PERIODIC_SOFTCORE_H */

