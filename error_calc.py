# takes two energy files, calcs error statistics between the printed systems
#
#
#
from decimal import *

class Result:
	def __init__(self,name):
		self.f = open(name)
		self.num = []
	

def getnums(res):
	for index,line in enumerate(res.f):
		if index==0:
			continue
		data = line.split(' ')
	        numbers = [[Decimal(number) for number in data if (number!='' and number!=' ')][i] for i in [2,4,5]]
	        res.num.append(numbers) 

def statops(l):
	return max(l),min(l),reduce(lambda x,y: x+y, l)/(len(l))

def stat(r0,r1):

 	l0 = [abs(num0[0]-num1[0]) for num0,num1 in zip(r0.num,r1.num)]
 	l1 = [abs(num0[1]-num1[1]) for num0,num1 in zip(r0.num,r1.num)]
 	l2 = [abs(num0[2]-num1[2]) for num0,num1 in zip(r0.num,r1.num)]
        lastqm = l1.pop()

        print "\ttotal pert energy (of the system): (max,min,mean)"
	print "\t",",".join([str(item) for item in statops(l0)])	
        
        print "\tqm energy (accum): (last)" # for the qm_energy, which is an accumulator, only makes sense looking at the last one	
	print "\t",lastqm

        print "\tqm charges (qmmm_c kernel): (max,min,mean)" 
	print "\t",",".join([str(item) for item in statops(l2)])	

res0 = Result("LAUNCH_double_p1_energy.out_0")
res1 = Result("LAUNCH_float_p1_energy.out_0")
res2 = Result("LAUNCH_fixed_p1_energy.out_0")
res3 = Result("LAUNCH_fastfixed_p1_energy.out_0")

getnums(res0)
getnums(res1)
getnums(res2)
getnums(res3)

print "-----Float vs Double"
stat(res0,res1)
print "-----Fixed(32) vs Double"
stat(res0,res2)
print "-----Fast-Fixed(32/16) vs Double"
stat(res0,res3)

#for num0,num1 in zip(res0.num,res1.num):
#	print num0[0],num1[0],num0[0]-num1[0],num0[1],num1[1],num0[1]-num1[1]


	
