
#include <cstdlib>
#include <iostream>

#include "Rosenbluth_Swap_Stepper.h"
#include "System_Periodic.h"
#include "System.h"
#include "OPLS.h"
#include "typedefs.h"
#include "geom_utils.h"
#include "options.h"
#include "Run.h"


Rosenbluth_Swap_Stepper::Rosenbluth_Swap_Stepper (
      random_engine *rnd_engine,
      System *system, int num_trial_configurations, Run *run)
 : rnd_engine (rnd_engine),
   dist_two (-1.0, 1.0),
   num_trials (num_trial_configurations),
   run (run)
{
   dist_molecule = new std::uniform_int_distribution<int> (1, system->nmol_total - 1);
   dist_step = new std::uniform_real_distribution<_FPOINT_S_> ( 0.0, run->stepmax);
   w = new double[num_trial_configurations];
   vectorsx1 = new double[num_trial_configurations];
   vectorsy1 = new double[num_trial_configurations];
   vectorsz1 = new double[num_trial_configurations];
   steps1    = new double[num_trial_configurations];
   vectorsx2 = new double[num_trial_configurations];
   vectorsy2 = new double[num_trial_configurations];
   vectorsz2 = new double[num_trial_configurations];
   steps2    = new double[num_trial_configurations];
} 


Rosenbluth_Swap_Stepper::~Rosenbluth_Swap_Stepper ()
{
   delete dist_molecule;
   delete dist_step;
   delete[] w;
   delete[] vectorsx1;
   delete[] vectorsy1;
   delete[] vectorsz1;
   delete[] steps1;
   delete[] vectorsx2;
   delete[] vectorsy2;
   delete[] vectorsz2;
   delete[] steps2;
}



void Rosenbluth_Swap_Stepper::step (System *system_orig, const OPLS *opls)
{
   System_Periodic *system = dynamic_cast<System_Periodic*> (system_orig);
   auto m2a = system->molecule2atom;
   System_Periodic *system_old = new System_Periodic (*system);

   //
   // 1. Computes Rosenbluth factor of old configuration
   //
   
   double sumwo = 0.0;
   for (int i = 0; i < num_trials; i++)
   {
      // Translates from the old configuration
      // 1. mol
      double rnd1, rnd2, s_rnd_12;
      do {
         rnd1 = dist_two (*rnd_engine);
         rnd2 = dist_two (*rnd_engine); 
         s_rnd_12 = rnd1 * rnd1 + rnd2 * rnd2;
      } while (s_rnd_12 >= 1.0);
      
      double trans_x1 = 2 * rnd1 * sqrt (1 - s_rnd_12);
      double trans_y1 = 2 * rnd2 * sqrt (1 - s_rnd_12);
      double trans_z1 = 1 - 2 * s_rnd_12;
      
      double random_step1 = (*dist_step) (*rnd_engine); 
      for(int pos = 0 ; pos < m2a[random_mol1].natoms ; ++pos){
         int atom = m2a[random_mol1].atom_id[pos];
         translation (random_step1, trans_x1, trans_y1, trans_z1, 
               system->rx[atom], system->ry[atom], system->rz[atom]);
      }

      // 2. mol
      do {
         rnd1 = dist_two (*rnd_engine);
         rnd2 = dist_two (*rnd_engine); 
         s_rnd_12 = rnd1 * rnd1 + rnd2 * rnd2;
      } while (s_rnd_12 >= 1.0);
      
      double trans_x2 = 2 * rnd1 * sqrt (1 - s_rnd_12);
      double trans_y2 = 2 * rnd2 * sqrt (1 - s_rnd_12);
      double trans_z2 = 1 - 2 * s_rnd_12;
      
      double random_step2 = (*dist_step) (*rnd_engine); 
      for(int pos = 0 ; pos < m2a[random_mol2].natoms ; ++pos){
         int atom = m2a[random_mol2].atom_id[pos];
         translation (random_step2, trans_x2, trans_y2, trans_z2, 
               system->rx[atom], system->ry[atom], system->rz[atom]);
      }

      // updates distances
      system->UpdateDistances (random_mol1);
      system->UpdateDistances (random_mol2);

      sumwo += boltzmann_factor (run->temperature,
            dE (system, system_old, opls));

      // reverts to original position
      // TODO better to "backup and restore" the positions?
      trans_x1 *= -1.0;
      trans_y1 *= -1.0;
      trans_z1 *= -1.0;
      for(int pos = 0 ; pos < m2a[random_mol1].natoms ; ++pos) {
         int atom = m2a[random_mol1].atom_id[pos];
         translation (random_step1, trans_x1, trans_y1, trans_z1, 
               system->rx[atom], system->ry[atom], system->rz[atom]);
      }

      trans_x2 *= -1.0;
      trans_y2 *= -1.0;
      trans_z2 *= -1.0;
      for(int pos = 0 ; pos < m2a[random_mol2].natoms ; ++pos) {
         int atom = m2a[random_mol2].atom_id[pos];
         translation (random_step2, trans_x2, trans_y2, trans_z2, 
               system->rx[atom], system->ry[atom], system->rz[atom]);
      }

      // no update of systems because there will be another trial
   }

   //
   // 2. Generates new configuration
   //

   random_mol1 = (*dist_molecule) (*rnd_engine);
   random_mol2 = (*dist_molecule) (*rnd_engine);

   // selects a second random molecule which has a different atom type for the
   // first atom XXX can of course easily fail
   int type1 = system->atom_type[m2a[random_mol1].atom_id[0]];
   int type2 = system->atom_type[m2a[random_mol2].atom_id[0]];
   while (type1 == type2)
   {
      random_mol2 = (*dist_molecule) (*rnd_engine);
      type2 = system->atom_type[m2a[random_mol2].atom_id[0]];
   }

   // computes center of masses
   double comx1, comy1, comz1;
   center_mass (random_mol1, system, comx1, comy1, comz1);

   double comx2, comy2, comz2;
   center_mass (random_mol2, system, comx2, comy2, comz2);

   // computes vector from 1->2
   double rx = comx2 - comx1;
   double ry = comy2 - comy1;
   double rz = comz2 - comz1;

   // translates 1+r and 2-r, so that they switch the place
   for(int pos = 0 ; pos < m2a[random_mol1].natoms ; ++pos) {
      int atom = m2a[random_mol1].atom_id[pos];
      translation (1.0, rx, ry, rz, system->rx, system->ry, system->rz, atom);
   }

   for(int pos = 0 ; pos < m2a[random_mol2].natoms ; ++pos) {
      int atom = m2a[random_mol2].atom_id[pos];
      translation (1.0, -rx, -ry, -rz, system->rx, system->ry, system->rz, atom);
   }

   // updates distances
   system->UpdateDistances (random_mol1);
   system->UpdateDistances (random_mol2);

   double depos = dE (system, system_old, opls);
   double wdepos = boltzmann_factor (run->temperature, depos);

   delete system_old;
   system_old = new System_Periodic (*system);

   //
   // 3. Generate k trials for the new configuration
   //

   double sumwn = 0;
   for (int i = 0; i < num_trials; ++i) {

      // Translates from the new configuration
      // TODO probably better always from new conf
      double rnd1, rnd2, s_rnd_12;
      do {
         rnd1 = dist_two (*rnd_engine);
         rnd2 = dist_two (*rnd_engine); 
         s_rnd_12 = rnd1 * rnd1 + rnd2 * rnd2;
      } while (s_rnd_12 >= 1.0);
      
      double trans_x1 = 2 * rnd1 * sqrt (1 - s_rnd_12);
      double trans_y1 = 2 * rnd2 * sqrt (1 - s_rnd_12);
      double trans_z1 = 1 - 2 * s_rnd_12;
      
      double random_step1 = (*dist_step) (*rnd_engine); 
      for(int pos = 0 ; pos < m2a[random_mol1].natoms ; ++pos){
         int atom = m2a[random_mol1].atom_id[pos];
         translation (random_step1, trans_x1, trans_y1, trans_z1, 
               system->rx[atom], system->ry[atom], system->rz[atom]);
      }

      do {
         rnd1 = dist_two (*rnd_engine);
         rnd2 = dist_two (*rnd_engine); 
         s_rnd_12 = rnd1 * rnd1 + rnd2 * rnd2;
      } while (s_rnd_12 >= 1.0);
      
      double trans_x2 = 2 * rnd1 * sqrt (1 - s_rnd_12);
      double trans_y2 = 2 * rnd2 * sqrt (1 - s_rnd_12);
      double trans_z2 = 1 - 2 * s_rnd_12;
      
      double random_step2 = (*dist_step) (*rnd_engine); 
      for(int pos = 0 ; pos < m2a[random_mol2].natoms ; ++pos){
         int atom = m2a[random_mol2].atom_id[pos];
         translation (random_step2, trans_x2, trans_y2, trans_z2, 
               system->rx[atom], system->ry[atom], system->rz[atom]);
      }

      // saves trans of this trial
      vectorsx1[i] = trans_x1;
      vectorsy1[i] = trans_y1;
      vectorsz1[i] = trans_z1;
      steps1[i] = random_step1;

      vectorsx2[i] = trans_x2;
      vectorsy2[i] = trans_y2;
      vectorsz2[i] = trans_z2;
      steps2[i] = random_step2;

      // updates distances
      system->UpdateDistances (random_mol1);
      system->UpdateDistances (random_mol2);

      double ent = dE (system, system_old, opls);
      w[i] = boltzmann_factor (run->temperature, ent);
      sumwn += w[i];

      // reverts to original position
      // TODO better to "backup and restore" the positions?
      trans_x1 *= -1.0;
      trans_y1 *= -1.0;
      trans_z1 *= -1.0;
      for(int pos = 0 ; pos < m2a[random_mol1].natoms ; ++pos){
         int atom = m2a[random_mol1].atom_id[pos];
         translation (random_step1, trans_x1, trans_y1, trans_z1, 
               system->rx[atom], system->ry[atom], system->rz[atom]);
      }

      trans_x2 *= -1.0;
      trans_y2 *= -1.0;
      trans_z2 *= -1.0;
      for(int pos = 0 ; pos < m2a[random_mol2].natoms ; ++pos){
         int atom = m2a[random_mol2].atom_id[pos];
         translation (random_step2, trans_x2, trans_y2, trans_z2, 
               system->rx[atom], system->ry[atom], system->rz[atom]);
      }

      // no update of systems because there will be another trial
   }

   delete system_old;

   // selects one of the trials
   int selected = select (w, sumwn);
   for(int pos = 0 ; pos < m2a[random_mol1].natoms ; ++pos){
      int atom = m2a[random_mol1].atom_id[pos];
      translation (
            steps1   [selected],
            vectorsx1[selected],
            vectorsy1[selected],
            vectorsz1[selected],
            system->rx[atom], system->ry[atom], system->rz[atom]);
   }

   for(int pos = 0 ; pos < m2a[random_mol2].natoms ; ++pos){
      int atom = m2a[random_mol2].atom_id[pos];
      translation (
            steps2   [selected],
            vectorsx2[selected],
            vectorsy2[selected],
            vectorsz2[selected],
            system->rx[atom], system->ry[atom], system->rz[atom]);
   }

   prob_acc = sumwn / sumwo * wdepos;
}



double Rosenbluth_Swap_Stepper::dE (
      System *system,
      System *system_old,
      const OPLS *opls)
{
   // Compute the new energy
   double de = opls->energy (system->GetDistance,
         system_old->GetDistance, random_mol1);
   de += opls->energy (system->GetDistance,
         system_old->GetDistance, random_mol2);
   return de;
}




bool Rosenbluth_Swap_Stepper::is_accepted ()
{
   std::uniform_real_distribution<_FPOINT_S_> dist_boltzman (0.0, 1.0);
   if (dist_boltzman (*rnd_engine) < prob_acc)
   {
      return true;
   }
   return false;
}




// XXX equivalent to swap stepper
void Rosenbluth_Swap_Stepper::accept (System *system, System *system_old)
{
   copy_coordinates (system, system_old, random_mol1);
   copy_coordinates (system, system_old, random_mol2);
   // TODO maybe not very efficient?
   system_old->UpdateDistances (random_mol1);
   system_old->UpdateDistances (random_mol2);
}



void Rosenbluth_Swap_Stepper::reject (System *system, System *system_old)
{
   copy_coordinates (system_old, system, random_mol1);
   copy_coordinates (system_old, system, random_mol2);
   // TODO maybe not very efficient?
   system->UpdateDistances (random_mol1);
   system->UpdateDistances (random_mol2);
}



/*
 * Selects randomly a configuration n with the probability w[n]/sum
 * 
 * Returns: n
 */
int Rosenbluth_Swap_Stepper::select (double *w, double sum)
{
   std::uniform_real_distribution<double> dist (0.0, sum);
   const double rand = dist (*rnd_engine);
   double cumw = w[0];
   int n = 0;
   while (cumw < rand)
   {
      n++;
      cumw += w[n];
   }
   return n;
}
