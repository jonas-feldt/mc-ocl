/**
 *
 * File: OCLKernel.cpp
 * Author: Sebastiao Miranda
 *
 * Version: 2014-04-06
 *
 */
#include "OCLKernel.h"
#include "OCLDevice.h"
#include <string>
#include <fstream>

OCLKernel::OCLKernel(std::string kernelName, std::string fileName, OCLDevice *device){
 
   m_kernelName = kernelName;
   m_fileName = fileName;
   m_device = device;
}
OCLKernel::~OCLKernel(){

}
//Set global and local ranges
void OCLKernel::setRanges(int global, int local){

   int new_global;
   // Make sure global is multiple of local:
   
   #ifdef SET_VERBOSE_LVL_SETUP_R
      std::cout << "[SETU] Setting ranges for Kernel "<<m_kernelName<<std::endl;
      std::cout << "\tRequested Global: " << global << std::endl;
      std::cout << "\tRequested Local : " << local << std::endl;
   #endif
   
   // Make sure global is multiple of local:
   if(global%local!=0){
      new_global = ((global/local) + 1) * local; 
   }else{
      new_global = global;
   }
   m_int_globalRange = new_global;
   m_int_localRange = local;
   m_globalRange = cl::NDRange(new_global);
   m_localRange = cl::NDRange(local);
   
   #ifdef SET_VERBOSE_LVL_SETUP_R
      std::cout << "\tNew Global: " << new_global << std::endl;
      std::cout << "\tNumber of Workgroups : " << new_global/local << std::endl;
   #endif
   
}



// Returns the preferred work group size for a kernel (Respecting to a device)
int OCLKernel::get_pref_wg_mult_1d(){
   cl::size_t<1> pref_wgmult_query;
   m_kernel.getWorkGroupInfo(m_device->m_device,CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,&pref_wgmult_query);
   return WG_HINT_MULT*pref_wgmult_query[0];
}

// Compiles the kernel (For a device)
int OCLKernel::make_kernel(std::string flags){

   try{
      //Read source for compute distance kernel
      std::ifstream srcFile(m_fileName.c_str());
      if(!srcFile.is_open()){
         std::cout << "Could not open src file: " << m_fileName << std::endl;  
         return -1;
      }
      
         //Read source
      std::string srcCode(std::istreambuf_iterator<char>(srcFile),(std::istreambuf_iterator<char>()));
      cl::Program::Sources srcProgram(1, std::make_pair(srcCode.c_str(),srcCode.length()+1));
      
      #ifdef SET_VERBOSE_LVL_SETUP
         std::cout << "[SETU] Building kernel from file " << m_fileName << std::endl;           
      #endif
      
         //Make program from source
      m_program = cl::Program(*(m_device->m_p_context),srcProgram);
      
         //Build programs for selected device
      std::vector<cl::Device> selected_device;
      selected_device.push_back(m_device->m_device);
      
      //Some Nvidia options: -cl-nv-verbose -cl-nv-maxrregcount=32
      //For AMD verbose:
      //m_program.build(selected_device, "-I ./ -save-temps=../out_AMD");
      //Please use the flags argument for this^^^^
      
      std::string options = std::string("-I ./ ") + flags;
      m_program.build(selected_device, options.c_str());
      
         //Make kernel
      m_kernel = cl::Kernel(m_program, m_kernelName.c_str());

         //Log
      std::string log; 
      m_program.getBuildInfo(m_device->m_device,CL_PROGRAM_BUILD_LOG,&log);
      
      #ifdef SET_VERBOSE_LVL_SETUP
         std::cout<< "[SETU] Build log for kernel " << m_kernelName <<": "<< log << std::endl;
      #endif
      
   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLmanager::register_kernel Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      std::string log; 
      m_program.getBuildInfo(m_device->m_device,CL_PROGRAM_BUILD_LOG,&log);
      std::cout<< "Build log for kernel " << m_kernelName <<": "<< log << std::endl;
      throw error;
   }
   return 0;
}
