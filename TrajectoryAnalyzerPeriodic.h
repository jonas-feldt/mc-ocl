/*
 * File:   TrajectoryAnalyzerPeriodic.h
 * Author: Jonas Feldt
 *
 * Version: 2014-02-17
 */

#pragma once

#include <string>

#include "TrajectoryAnalyzer.h"
#include "System_Periodic.h"


class TrajectoryAnalyzerPeriodic : public TrajectoryAnalyzer {
   public:
      TrajectoryAnalyzerPeriodic(
           std::string trajectory_file, int first_frame, int last_frame,
           std::string output_file, double box_dim, const unsigned int num_chains);
      ~TrajectoryAnalyzerPeriodic() override;
   
      void run() override;
      void read_system (std::string tinker_file) override;
   
   protected:
      virtual void analyze() = 0;
      virtual void finalize() = 0;
      virtual void save_output() = 0;
   
      System_Periodic *system;
      const double box_dim;

};


