/**
 *
 * File: OCLDevice_FES.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-10-05
 *
 */

#include <iostream>

#include "OCLDevice.h"
#include "OCLDevice_FES.h"
#include "options.h"


OCLDevice_FES::OCLDevice_FES(
                        cl::Device device,
                        cl::Context *context,
                        int *ignite_isReady,
                        std::mutex *ignite_mutex,
                        std::condition_variable *ignite_slave,
                        std::condition_variable *ignite_master,
                        int n_pre_steps,
                        int nkernel,
                        int max_chains,
                        System_Periodic* system,
                        const OPLS_Periodic* opls,
                        Run_Periodic* run,
                        _FPOINT_S_ temperature,
                        int num_devices,
                        bool extra_energy_file)
   : OCLDevice (
        device,
        context,
        ignite_isReady,
        ignite_mutex,
        ignite_slave,
        ignite_master,
        n_pre_steps,
        nkernel,
        max_chains,
        system,
        opls,
        run,
        temperature,
        num_devices,
        extra_energy_file),
   factor_fes (run->fes_factor_reference)
{
}



OCLDevice_FES::~OCLDevice_FES ()
{
}




/**
 * Creates kernels, compiles them, sets their ranges, creates chain-shared
 * buffer and sets chain-shared arguments for the kernels.
 */
void OCLDevice_FES::setup_pmc (std::string kernel_file_closure)
{
   // calls base class implementation
   OCLDevice::setup_pmc (kernel_file_closure);

   int ncbytes = 0;

   try {

      //
      // constant memory
      //

      //Cutoffs
      buffer_fes = cl::Buffer(*(m_p_context),
            CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
            sizeof (double), // Just one number
            &factor_fes);

      ncbytes += sizeof (double);

      // sets additional const argument for the closure kernel
      m_KERNEL_closure->m_kernel.setArg (27, buffer_fes);


   } catch (const cl::Error &error) {
      std::cout << "OCLDevice_FES::setup_pmc Error: " << error.what() << "(" << error.err() << ")" << std::endl;
      throw error;
   }

   VERBOSE_LVL_SETUP2 ("[SETU] Device " << m_device_index << " Constant Mem Requested (bytes):" << ncbytes << std::endl);
}
