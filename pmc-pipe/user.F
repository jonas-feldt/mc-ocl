c
c key=value:
c
c energy=i : uses energ(i) for the energy
c
c
      subroutine user
      USE molpro_options, ONLY: molpro_pwd, lstring
      use iso_c_binding, only: c_int, c_double, c_char, c_null_char
      implicit double precision (a-h,o-z)
      integer*4 :: n_qm_atom, n_total_atom, n_grid
      CHARACTER (lstring) :: pwdFileName
      character*32 :: cmd
      character*16 :: key, instring 
      integer :: ienergy
      real :: invalue
      include "common/tapes"
      include "common/big"
      include "common/cgeom"
      include "common/cbas"
      include "common/etig"
      include "common/molen"
      include "common/cinput"


      interface
        subroutine read_lattice_pipe (n_qm_atom, n_total_atom, lattice)
        use iso_c_binding, only: c_int, c_double
        integer(c_int) :: n_qm_atom
        integer(c_int) :: n_total_atom
        real(c_double) :: lattice
        end subroutine

        subroutine write_grid_pipe (n_grid, xyz, charges, qm_energy)
        use iso_c_binding, only: c_int, c_double
        integer(c_int) :: n_grid
        real(c_double) :: xyz
        real(c_double) :: charges
        real(c_double) :: qm_energy
        end subroutine

        subroutine read_dimensions_pipe (n_qm_atom, n_total_atom)
        use iso_c_binding, only: c_int
        integer(c_int) :: n_qm_atom, n_total_atom
        end subroutine

        subroutine read_fds (fdfile)
          use iso_c_binding, only: c_char
          character(kind=c_char) :: fdfile(*)
        end subroutine

      end interface

      call openlog(1)

      call backspace_inp
      call input(ncol)
      do icol=2,ncol
        call geta(icol,key,instring,invalue,1)
        if(key.eq.'ENERGY') then
          write(iout,*) 'ENERGY=',int(invalue)
          ienergy=int(invalue)
        endif
      enddo

      call copy_string(molpro_pwd, pwdFileName)
      call append_string(pwdFileName, "fd.dat")
      write (iout,*) "JF  Read file descriptors"
      call read_fds(pwdFileName//C_NULL_CHAR)

      icnt = 0
      n_qm_atom = 0
      n_total_atom = 0
      do 
        icnt = icnt + 1

        write (iout, *) "JF  Starting iteration", icnt

        ! sets new lattice
        write (iout, *) "JF  Reading new geometry from pipe."
        ! TODO set new QM geometry
        call read_dimensions_pipe (n_qm_atom, n_total_atom)
        nlat = n_total_atom - n_qm_atom
        write (iout, *) "JF  Reading lattice with", nlat, "points."
        igeom = icorr(n_total_atom * 4)
        ilat = igeom + n_qm_atom * 4;
        call read_lattice_pipe (n_qm_atom, n_total_atom, q(igeom))
        
        write (iout, *) "JF  Setting new lattice."
        call setvar('!SEW_LATTICE',3d0,' ',1,1,nv,0)
        call writem(DBLE(nlat),1,2,800,0,'LATTICE')
        call writem(q(ilat),4*nlat,2,800,1,'LATTICE')
        call writem(0d0,1,2,801,0,'LATGRAD')
        call setvar('!LATTICE',2d0,' ',1,1,nv,0)
        call setvar('!NEWLATTICE',1.d0,' ',1,1,nv,0)
        call corlsr(igeom)
        write (iout, *) "JF  Finished."

        ! calls the QM method
        write (iout, *) "JF  Calling the method." 
        cmd='LABELMETHOD'
        call run_procedure(' ',cmd,0)
        write (iout, *) "JF  Finished. Energy=", energ(ienergy)

        ! creates grid
        write (iout, *) "JF  Creating a new grid."
        call grid_get_accuracy (record,thr)
        write (iout, *) "grid_acc ", thr
        call grid_set_dirty(-record)
        call grid_open(record)
        call grid_get_size(0,npt)
        ipt=icorr(npt*3)
        iwt=icorr(npt)
        call grid_obtain(0,npt,q(ipt),q(iwt),-1,.true.)
        call grid_orbital_initialize(1d-12)
        idensity=icorr(npt)
        ivalue=icorr(npt)
        iorbval=icorr(npt*ntg)
        ihelp=icorr(npt*2)
        iorbmed=icorr(ntg)
        iden=icorr(ntdg)
        call get_den(q(iden),0,0,0,1)
        imo=icorr(ntqg)
        call get_orb(q(imo),2100,2,1,1)
        idum=icorr(0)
   
        call cube_evaluate (q(ipt),q(ivalue),q(idensity),
     >         q(idum),npt,q(iorbval),
     >         q(idum),q(idum),
     >         q(iden),q(ihelp),0,
     >         q(imo),1,.true.,.false.,.false.,0,
     >         1,1,q(iorbmed))
   
        do i=1,npt
          q(idensity+i-1)=q(idensity+i-1)*q(iwt+i-1)
        end do
        write (iout, '(a,3f14.5,3e14.5)') " JF  1. point:", q(ipt),
     >                              q(ipt+1),q(ipt+2), q(idensity)
  
        ! writes grid
        write (iout, *) "JF  Sending the grid."
        n_grid = npt
        call write_grid_pipe (n_grid, q(ipt), q(idensity),
     >                        energ(ienergy))

        call grid_orbital_term ! sets grid_orbital to not initialized
        call corlsr(ipt)
        write (iout, *) "JF  Finished."

      enddo 
      call openlog(-1)
 
      return
      end
