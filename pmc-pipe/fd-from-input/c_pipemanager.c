#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <ctype.h>
#include <string.h>

static int g_pipe_writefd;
static int g_pipe_readfd;




int __write_grid_pipe(int pipe_writefd, int *ngrid, double *xyz, double *charges, double *qm_energy){
   
   int n;
   int point=0;
   
   /* Write number of grid points */
   n = write(pipe_writefd,ngrid,sizeof(int));
   if (n==-1) {
      perror("write_grid_pipe pipe");
      exit (EXIT_FAILURE);
   }
   if(n!=sizeof(int)){    
      printf("Error: Write grid size: badly formated data. Expected to write %d bytes, wrote %d.\n", (int) sizeof(int), n);
      fflush(stdout);
      exit (EXIT_FAILURE);
   }
       
   /* Write grid - times 3 is x,y,z */
   while (point<(*ngrid*3)) {

      n = write(pipe_writefd,&xyz[point],sizeof(double));
      if(n==-1){
         perror("write_grid_pipe pipe");
         exit (EXIT_FAILURE);
      }
      if(n!=sizeof(double)){
         printf("Error (grid point %d): badly formated data. Expected to write %d bytes, wrote %d.\n",point,(int)sizeof(double), n);
         fflush(stdout);
         exit (EXIT_FAILURE);
      }
      point++;
   }

   point=0;
   /* Write grid - charges */
   while(point<(*ngrid)){

      n = write(pipe_writefd,&charges[point],sizeof(double));
      if (n==-1) {
         perror("write_grid_pipe pipe");
         exit (EXIT_FAILURE);
      }
      if(n!=sizeof(double)){
         printf("Error (charge %d): badly formated data. Expected to write %d bytes, wrote %d.\n",point,(int)sizeof(double), n);
         fflush(stdout);
         exit (EXIT_FAILURE);
      }
      point++;
   }
   
   
   /* Write QM energy */
   n = write(pipe_writefd,qm_energy,sizeof(double));
   if (n==-1) {
      perror("write_grid_pipe pipe");
      exit (EXIT_FAILURE);
   }
   if(n!=8){
      printf("Error (qm energy): badly formated data. Expected to write %d bytes, wrote %d.\n",(int)sizeof(double), n);
      fflush(stdout);
      exit (EXIT_FAILURE);
   }
   return 0;
}



int __read_dimensions_pipe (int pipe_readfd, int *n_qm_atom, int *n_total_atom) {
   int n;
   /* Read number of total (QM) atoms */
   n = read(pipe_readfd,n_qm_atom,sizeof(int));
   if(n==-1){
      perror("read_dimensions_pipe pipe");
      exit (EXIT_FAILURE);
   }
   if(n!=sizeof(int)){ 
      printf("Error: Read qm atoms: badly formated data. Expected to read %d bytes, read %d.\n", (int)sizeof(int), n);
      fflush(stdout);
      exit (EXIT_FAILURE);
   }

   /* Read number of total (QM+MM) atoms */
   n = read(pipe_readfd,n_total_atom,sizeof(int));
   if(n==-1){
      perror("read_dimensions_pipe pipe");
      exit (EXIT_FAILURE);
   }
   if(n!=sizeof(int)){ 
      printf("Error: Read total atoms: badly formated data. Expected to read %d bytes, read %d.\n", (int)sizeof(int), n);
      fflush(stdout);
      exit (EXIT_FAILURE);
   }

   return 0;
}



int __read_lattice_pipe(int pipe_readfd, int *n_qm_atom, int *n_total_atom, double *lattice){
   
   int n;
   int atom=0;
   /* Read xyz total - times 4 is x,y,z,charge */
   while(atom<(*n_total_atom)*4){
 
      n = read(pipe_readfd,&lattice[atom],sizeof(double));
      if(n==-1){
         perror("read_lattice_pipe pipe");
         exit (EXIT_FAILURE);
      }
      if(n!=sizeof(double)){
         printf("Error : badly formated data. Expected to read %d bytes, read %d.\n",(int)sizeof(double), n);
         fflush(stdout);
         exit (EXIT_FAILURE);
      }
      
      atom++;
   }
   
   return 0;
}



void read_fds_ (char *fdfile){

   char line[128];
   FILE * file;
   char *s = fdfile;
   char *t = fdfile + strlen(fdfile);

   /* trim */
   while(isspace(*s)) ++s;
   while(isspace(*--t));
   *++t = '\0';
   
   file = fopen(fdfile,"r");

   if (file == NULL) {
      printf("Error: Can't read the file descriptors from %s.\n", fdfile); 
      fflush(stdout);
      exit (EXIT_FAILURE);
   }
   
   while(fgets(line, 128, file) != NULL){
   
      int pid;
      sscanf(line, "%d %d %d", &pid, &g_pipe_readfd, &g_pipe_writefd);
      
      if (pid == getpid()){
         fclose(file);
         return;
      }
   }

   fclose (file);
   printf ("Error: Cannot find FDs for PID %d.\n", getpid());
   fflush (stdout);
   exit (EXIT_FAILURE);
}


void set_fds_ (int *fdread, int *fdwrite) {
   g_pipe_readfd = *fdread;
   g_pipe_writefd = *fdwrite;
}




void write_grid_pipe_ (int *ngrid, double *xyz, double *charges, double *qm_energy){
   __write_grid_pipe(g_pipe_writefd, ngrid, xyz, charges, qm_energy);
}


void read_lattice_pipe_ (int *n_qm_atom, int *n_total_atom, double *lattice){
   __read_lattice_pipe(g_pipe_readfd, n_qm_atom, n_total_atom, lattice);
}


void read_dimensions_pipe_ (int *n_qm_atom, int *n_total_atom) {
   __read_dimensions_pipe(g_pipe_readfd, n_qm_atom, n_total_atom);
}
