/**
 *
 * File: main.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-06-02
 *
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <libconfig.h++>
#include <utility>
#include <unistd.h>

#include "io_utils.h"
#include "jonas_io_utils.h"
#include "System.h"
#include "Run.h"
#include "Abstract_Switcher.h"
#include "OPLS_Switcher.h"
#include "OPLS_switcher_molecule.h"
#include "OPLS_Switcher_linear.h"
#include "OPLS_Switcher_factor.h"
#include "OPLS_Periodic.h"
#include "OPLS_Periodic_Softcore.h"
#include "OPLS_Periodic_FES.h"
#include "opls_no_coulomb_qmmm.h"
#include "opls_coulomb_fes.h"
#include "opls_periodic_dual_topology.h"
#include "Run_Periodic.h"
#include "System_Periodic.h"
#include "MMMC_Periodic.h"
#include "QMMC.h"
#include "mm_fep_periodic.h"
#include "PMC_Periodic.h"
#include "PMC_Periodic_FES.h"
#include "PMC_Periodic_Dual.h"
#include "FEP_Periodic.h"
#include "FEP_Periodic_FES.h"
#include "RDF_Periodic.h"
#include "RDF_Angle_Periodic.h"
#include "displacement_periodic.h"
#include "dipole_periodic.h"
#include "options.h"
#include "geom_utils.h"
#include "OCLChain.h"
#include "typedefs.h"
#include "OCLDevice.h"
#include "InputCheck.h"
#include "Global.h"
#ifdef BASE_PROFILING
   #include "papi.h"
#endif


libconfig::Config global_config;

/**
 * Messy main...
 */
int main (int argc, char* argv[]) {

   // inits global config file
   try {
      global_config.readFile("global.cfg");
   } catch (const libconfig::FileIOException &ex) {
      std::cerr << "I/O error: Cannot read global configuration file global.cfg" << std::endl;
      return EXIT_FAILURE;
   } catch (const libconfig::ParseException &ex) {
      std::cerr << "Parse error in " << ex.getFile(); 
      std::cerr << " at line " << ex.getLine() << " - " << ex.getError();
      std::cerr << std::endl;
      return EXIT_FAILURE;
   }

   // parses input file
   libconfig::Config cfg;
   try {
      cfg.readFile(argv[1]);
   } catch (const libconfig::FileIOException &ex) {
      std::cerr << "I/O error while reading file " << argv[1] << std::endl;
      return EXIT_FAILURE;
   } catch (const libconfig::ParseException &ex) {
      std::cerr << "Parse error in " << ex.getFile(); 
      std::cerr << " at line " << ex.getLine() << " - " << ex.getError();
      std::cerr << std::endl;
      return EXIT_FAILURE;
   }

   // checks if jobs are valid
   InputCheck check;
   // TODO assumes for now always that the system is periodic
   std::vector<Run_Periodic*> runs;
   Run_Periodic *run_fep = nullptr;
   Run *run_nonp = nullptr;
   try {
      libconfig::Setting &job = cfg.lookup("job");
      // TODO for now manual hack for non-periodic jobs which assumes single job
      if (std::string(job[0].c_str()) == "qmmc") {
         run_nonp = new Run();
         check.check(job[0].c_str(), cfg, run_nonp);
      } else {
         for (int i = 0; i < job.getLength(); ++i) {
            std::string name = job[i].c_str();
            Run_Periodic *run = new Run_Periodic();
            runs.push_back(run);
            check.check (name, cfg, runs.back());
            if (name == "fep") {
               run_fep = runs.back();
            }
         }
      }
   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "No job specified." << std::endl;
      return EXIT_FAILURE;
   }

   // sets default output
   const std::string output = InputCheck::add_output (cfg, argv[1]); 
   const std::string outfile = output + ".out";

   std::vector<System_Periodic*> systemsB;
   System_Periodic *tmpB = nullptr;
   vvs last_systems;
   System *system_nonp = nullptr;
   // TODO hack because of non-periodic jobs
   if (run_nonp) {
      system_nonp = read_xyz (run_nonp->geometry);
   } else {
      for (Run_Periodic *run : runs) {
         run->outfile = outfile;
         // checks runs for errors
         if (!run->is_sane(outfile) && !IGNORE_ERRORS) {
            return EXIT_FAILURE;
         }
      }

      // XXX leave reading of systems for now here
      // TODO uses the first run for now which means that some properties
      // have to be shared between different jobs (at least for now)
      last_systems = read_periodic_systems (runs[0]);
   }

   /*
    * FEP specific input
    */
   // XXX read systemsB & init switcher for now here
   Abstract_Switcher *switcher = nullptr; // inits switcher
   if (run_fep != nullptr) {
      tmpB = new System_Periodic(*last_systems.front().front(), 0); // inits tmpB with QM part of system
      std::ifstream inB(run_fep->fep_qm_geometry);

      // skips unused frames
      for (unsigned int i = 0; i < run_fep->fep_first_frame - 1; ++i) {
         read_periodic_frame(inB, tmpB);
      }

      // reads, translates and checks systemB frames
      for (unsigned int i = 0; i < run_fep->fep_num_frames; ++i) {
         read_periodic_frame(inB, tmpB);
         tmpB->translate(
               run_fep->fep_translate[0],
               run_fep->fep_translate[1],
               run_fep->fep_translate[2], 0);
         if (!tmpB->is_sane(run_fep->cutoff_coulomb, run_fep->cutoff_vdw) &&
            !IGNORE_ERRORS) return EXIT_FAILURE;
         systemsB.push_back(new System_Periodic(*tmpB)); // creates copies of tmpB
      }

      // inits switcher
      switcher = new OPLS_Switcher_linear (last_systems.front().front(), run_fep);
   }

   // saves the final configuration to the output
   cfg.writeFile(outfile.c_str());

   #ifdef BASE_PROFILING
      double tstart,tend;
      std::cout << "Initializing PAPI library..." << std::endl;
      if(PAPI_library_init(PAPI_VER_CURRENT) != PAPI_VER_CURRENT ){
        std::cerr << "PAPI library initialization error!" << std::endl;
        return EXIT_FAILURE;
      }
   #endif

   // TODO hack for non-periodic runs
   bool has_last_systems = false;
   if (not run_nonp) has_last_systems = runs[0]->restart;
   
   std::string scratch_folder = global_config.lookup ("io.scratch-directory").c_str();

   //
   // FEP
   // 
   if (run_fep != nullptr) {

      // loop over frames
      int frame = 0;
      System_Periodic *systemA = nullptr;
      System_Periodic *systemB = nullptr;
      bool isFirst = true;
      int iSysB = run_fep->fep_first_frame;
      OPLS_Periodic *opls = new OPLS_Periodic (last_systems.front().front(), run_fep);
      OPLS_Periodic *oplsB = new OPLS_Periodic (*opls);
      for (auto itSysB = systemsB.begin(); itSysB != systemsB.end(); itSysB++) {

         bool has_energy_qm_gp = false;
         double energy_qm_gp = 0.0;
   
         if (isFirst) { // assumes at least two frames TODO check this earlier
            systemA = *itSysB;
            itSysB++;
            isFirst = false;
         } else {
            systemA = systemB;
         }
         systemB = *itSysB;

         if (run_fep->fep_reverse) std::swap(systemA, systemB);

         FILE *fout; // title for the output
         fout = fopen(outfile.c_str(), "a");

         // switch parameters
         if (run_fep->fep_reverse) {
            switcher->switch_params(frame + 1, opls , fout);
            switcher->switch_params(frame    , oplsB, fout);
         } else {
            switcher->switch_params(frame    , opls , fout);
            switcher->switch_params(frame + 1, oplsB, fout);
         }

         // TODO correct output
         fprintf (fout, "\nStructure %d\n\n", iSysB);
         fclose(fout);

         // TODO write restart & continue input files for the jobs

         std::string out_it = output + "-" + std::to_string(iSysB); // output for this iteration
         vvs tmp;

         // loops over jobs 
         libconfig::Setting &job = cfg.lookup("job");
         for (int j = 0; j < job.getLength(); ++j) {
            Run_Periodic *run = runs[j];
            std::string name = job[j].c_str();
            std::string job_folder = scratch_folder + out_it + "-" + name;
            make_directory_jf(job_folder);
            OCLChain::lastChain = 0;   // set to 0 for every job
            OCLDevice::lastDevice = 0; // set to 0 for every job

            // copy QM part from systemA to the system(s)
            for (auto outer : last_systems) {
               for (auto system : outer) {
                  copy_coordinates(systemA, system, 0);
                  system->save_all(job_folder + "/systems.restart");
               }
            }

            if (name.compare("pmc") == 0) {

               // TODO move all systems solute (molecule 0) into the origin as required by PMC
               //for (auto outer : last_systems) {
               //   for (auto system : outer) {
               //      move_into_center_mass (0, system);
               //   }
               //}

               PMC_Periodic *pmc = new PMC_Periodic (
                     job_folder, run, opls, run->num_devices, run->num_chains);

               // TODO specialized settings for PMC
               
               #ifdef BASE_PROFILING
                  tstart = PAPI_get_real_usec();
               #endif
             
               // run PMC with energy_qm_gp the first time
               if (not has_energy_qm_gp) { 
                  if (has_last_systems) { // restart/continue
                     tmp = pmc->run (last_systems);
                  } else { // new simulation -> all last systems are identical
                     tmp = pmc->run (last_systems.front().front());
                     has_last_systems = true;
                  }
                  energy_qm_gp = pmc->get_energy_qm_gp();
                  has_energy_qm_gp = true;
               } else {
                  tmp = pmc->run (last_systems, energy_qm_gp);
               }

               #ifdef BASE_PROFILING
                  tend = PAPI_get_real_usec();
                  std::cout << "main() Scope time: (complete pmc->run): "  << std::setprecision (15) << (tend-tstart) / 1000<< " ms"<< std::endl;
               #endif
               
               delete pmc;

            } else if (name.compare("fep") == 0) {

               // TODO how to do this for FEP?!

               //// move all systems solute (molecule 0) into the origin as required by PMC
               //for (auto outer : last_systems) {
               //   for (auto system : outer) {
               //      move_into_center_mass (0, system);
               //   }
               //}
               //double cx, cy, cz;
               //center_mass (0, last_systems.front().front(), cx, cy, cz);
               //// translates to origin
               //for(int pos = 0 ; pos < dim; ++pos){
               //   int atom = ids[pos];
               //   translation (1.0, -comx, -comy, -comz, 
               //         system->rx, system->ry, system->rz, atom);
               //}

               // TODO consistent output
               systemB->save_all(outfile);

               // output systems to restart 
               for (auto outer : last_systems) {
                  for (auto system : outer) {
                     system->save_all(job_folder + "/systems.restart");
                  }
               }

               // creates run for FEP
               FEP_Periodic *fep = new FEP_Periodic (run);

               // run FEP (first time with energy_qm_gp)
               if (not has_energy_qm_gp) {
                  tmp = fep->run (last_systems, systemB, run, opls, oplsB,
                        job_folder, run->num_devices, run->num_chains);
                  energy_qm_gp = fep->get_energy_qm_gp();
                  has_energy_qm_gp = true;
               } else {
                  tmp = fep->run (last_systems, systemB, run, opls, oplsB,
                        job_folder, run->num_devices, run->num_chains,
                        energy_qm_gp);
               }

               delete fep;
            }

            // output systems to continue
            for (auto outer : tmp) {
               for (auto system : outer) {
                  system->save_all(job_folder + "/systems.continue");
               }
            }

            // move files back from scratch to the cwd
            char *cwd = get_current_dir_name();
            std::string cmd = "mv " + job_folder + "* " + cwd;
            VERBOSE_LVL_INIT("[INIT] " << cmd << std::endl);
            std::system(cmd.c_str());
            free(cwd);

            // delete the old systems
            for (auto outer : last_systems) {
               for (auto system : outer) {
                  delete system;
               }
            }

            // assign new systems for chaining
            last_systems = tmp;
         }

         if (run_fep->fep_reverse) std::swap(systemA, systemB); // swaps back
         iSysB++;
         frame++;
      }
      delete opls;
      delete oplsB;
   } else {

      // TODO rdf+rdfangle should work but it does only the first one right now
    
      // XXX doesn't support multiple PMC jobs right now
      libconfig::Setting &job = cfg.lookup("job");
      if (job.getLength() > 1) {
         bool found = false;
         for (int i = 0; i < job.getLength(); ++i) {
            std::string name = job[i].c_str();
            if (name == "pmc") {
               if (found == true)
               {
                  std::cerr << "Multiple PMC jobs are not supported." << std::endl;
                  exit(EXIT_FAILURE);
               }
               found = true;
            }
         }
      }

      // TODO write restart & continue input files for the jobs

      std::string name = job[0].c_str();

      if (name == "pmc") {

         // move all systems solute (molecule 0) into the origin as required by PMC
         //for (auto outer : last_systems) {
         //   for (auto system : outer) {
         //      move_into_center_mass (0, system);
         //   }
         //}

         const std::string job_folder = output + "-" + name;
         make_directory_jf(job_folder);

         OPLS_Periodic *opls = new OPLS_Periodic (last_systems.front().front(), runs[0]);
         PMC_Periodic *pmc = new PMC_Periodic (
               job_folder, runs[0], opls, runs[0]->num_devices, runs[0]->num_chains);

         for (auto outer_it : last_systems) {
            for (auto system : outer_it) {
               system->save_all(job_folder + "/systems.restart");
            }
         }

         // TODO specialized settings for PMC
         
         #ifdef BASE_PROFILING
            tstart = PAPI_get_real_usec();
         #endif
         
         vvs tmp = pmc->run (last_systems);

         #ifdef BASE_PROFILING
            tend = PAPI_get_real_usec();
            std::cout << "main() Scope time: (complete pmc->run): "  << std::setprecision (15) << (tend-tstart) / 1000<< " ms"<< std::endl;
         #endif

         if (not has_last_systems) has_last_systems = true;

         delete pmc;
         delete opls;

         // output systems to continue
         for (auto outer : tmp) {
            for (auto system : outer) {
               system->save_all(job_folder + "/systems.continue");
            }
         }

         // delete the old systems
         for (auto outer : last_systems) {
            for (auto system : outer) {
               delete system;
            }
         }

         last_systems = tmp;

      } else if (name == "dual_density" || name == "dd") {
         // TODO how to do this for DD
         //// move all systems solute (molecule 0) into the origin as required by PMC
         //for (auto outer : last_systems) {
         //   for (auto system : outer) {
         //      move_into_center_mass (0, system);
         //   }
         //}

         const std::string job_folder = output + "-" + name;
         make_directory_jf(job_folder);

         OPLS_Periodic *opls = new OPLS_Periodic (last_systems.front().front(), runs[0]);
         PMC_Periodic_Dual *dd = new PMC_Periodic_Dual (runs[0]);

         for (auto outer_it : last_systems) {
            for (auto system : outer_it) {
               system->save_all(job_folder + "/systems.restart");
            }
         }

         // TODO specialized settings for DD
         // TODO fix this job/input mess
         bool has_energy_qm_gp = false; // work around 
         double energy_qm_gp = 0.0;
         
         #ifdef BASE_PROFILING
            tstart = PAPI_get_real_usec();
         #endif
         
         vvs tmp;
         if (not has_energy_qm_gp) {
            tmp = dd->run (last_systems, runs[0], opls, job_folder,
                  runs[0]->num_devices, runs[0]->num_chains);
            energy_qm_gp = dd->get_energy_qm_gp();
            has_energy_qm_gp = true;
         } else {
            tmp = dd->run (last_systems, runs[0], opls, job_folder,
                  runs[0]->num_devices, runs[0]->num_chains, energy_qm_gp);
         }


         #ifdef BASE_PROFILING
            tend = PAPI_get_real_usec();
            std::cout << "main() Scope time: (complete dd-pmc->run): "  << std::setprecision (15) << (tend-tstart) / 1000<< " ms"<< std::endl;
         #endif

         if (not has_last_systems) has_last_systems = true;

         delete dd;
         delete opls;

         // output systems to continue
         for (auto outer : tmp) {
            for (auto system : outer) {
               system->save_all(job_folder + "/systems.continue");
            }
         }

         // delete the old systems
         for (auto outer : last_systems) {
            for (auto system : outer) {
               delete system;
            }
         }

         last_systems = tmp;

      } else if (name == "mmmc" || name == "m3c") {

         FILE *fout;
         fout = fopen(outfile.c_str(), "a");
         fprintf (fout, "\n!!! Molecule with ID 0 is frozen !!!\n");
         fprintf (fout, "\n!!! adjusted max_step is not used for following job !!!\n");
         fclose (fout);

         const std::string job_folder = output + "-" + name;
         make_directory_jf(job_folder);

         // TODO evil softcore hacks
         //OPLS_Periodic_Softcore *opls_soft = new OPLS_Periodic_Softcore (last_systems.front().front(), runs[0], 1, 1, 0.5, 1.0);

         OPLS_Periodic *opls = new OPLS_Periodic (last_systems.front().front(), runs[0]);
         opls->print (outfile);
         MMMC_Periodic *mmmc = new MMMC_Periodic (
               job_folder, runs[0], opls, runs[0]->num_devices, runs[0]->num_chains);

         for (auto outer_it : last_systems) {
            for (auto system : outer_it) {
               system->save_all(job_folder + "/systems.restart");
            }
         }

         // XXX specialized settings for MMMC
         
         vvs tmp = mmmc->run (last_systems);

         if (not has_last_systems) has_last_systems = true;

         delete mmmc;
         delete opls;

         // output systems to continue
         for (auto outer : tmp) {
            for (auto system : outer) {
               system->save_all(job_folder + "/systems.continue");
            }
         }

         // delete the old systems
         for (auto outer : last_systems) {
            for (auto system : outer) {
               delete system;
            }
         }

         last_systems = tmp;

      } else if (name == "mmfep") {

         Run_Periodic *run = runs[0];

         // loop over FEP steps
         for (unsigned int step = 0; step < run->fep_lambda_steps; ++step) {

            // Compute lambda for A and B
            double lambdaA = (double) step / (double) run->fep_lambda_steps;
            double lambdaB = (double) (step + 1) / (double) run->fep_lambda_steps;

            // switch for reverse
            if (run->fep_reverse) std::swap (lambdaA, lambdaB);

            // Creates FF for both states (correct for normal and reverse)
            auto oplsA = new OPLSNoCoulombQMMM<OPLSPeriodicDualTopology> (last_systems.front().front(), run, 1, 1, 0.5, lambdaA);
            auto oplsB = new OPLSNoCoulombQMMM<OPLSPeriodicDualTopology> (last_systems.front().front(), run, 1, 1, 0.5, lambdaB);

            // switch opls 
            FILE *fout = fopen(outfile.c_str(), "a");
            fprintf (fout, "\nFEP %f -> %f\n", lambdaA, lambdaB);
            fclose (fout);

            // print parameters
            oplsA->print(outfile);

            //
            // Running MM MC to equilibrate for A
            //

            std::string job_folder = scratch_folder + output + "-" + std::to_string (step) + "-m3c";
            make_directory_jf(job_folder);

            MMMC_Periodic *mmmc = new MMMC_Periodic (
                  job_folder, runs[0], oplsA, runs[0]->num_devices, runs[0]->num_chains);

            for (auto outer_it : last_systems) {
               for (auto system : outer_it) {
                  system->save_all(job_folder + "/systems.restart");
               }
            }

            // TODO specialized settings for PMC
            vvs tmp = mmmc->run (last_systems);

            //if (not has_last_systems) has_last_systems = true;

            delete mmmc;

            // output systems to continue
            for (auto outer : tmp) {
               for (auto system : outer) {
                  system->save_all(job_folder + "/systems.continue");
               }
            }

            // move files back from scratch to the cwd
            char *cwd = get_current_dir_name();
            std::string cmd = "mv " + job_folder + "* " + cwd;
            VERBOSE_LVL_INIT("[INIT] " << cmd << std::endl);
            std::system(cmd.c_str());

            // delete the old systems
            for (auto outer : last_systems) {
               for (auto system : outer) {
                  delete system;
               }
            }

            last_systems = tmp;

            //
            // Running FEP from A->B
            //

            job_folder = scratch_folder + output + "-" + std::to_string (step) + "-" + name;
            make_directory_jf(job_folder);

            for (auto outer_it : last_systems) {
               for (auto system : outer_it) {
                  system->save_all(job_folder + "/systems.restart");
               }
            }

            // XXX mmfes is a single job and therefore I cannot override the maxmoves
            // which makes this temporary hack necessary until I have some better idea
            Run_Periodic *tmp_run = new Run_Periodic(*runs[0]);
            tmp_run->maxmoves = tmp_run->mm_fep_steps;
            MMFEPPeriodic *fep = new MMFEPPeriodic (
                  job_folder, tmp_run, oplsA, oplsB,
                  runs[0]->num_devices, runs[0]->num_chains);
            tmp = fep->run (last_systems);
            delete tmp_run;

            delete fep;
            delete oplsA;
            delete oplsB;

            // output systems to continue
            for (auto outer : tmp) {
               for (auto system : outer) {
                  system->save_all(job_folder + "/systems.continue");
               }
            }

            // move files back from scratch to the cwd
            cwd = get_current_dir_name();
            cmd = "mv " + job_folder + "* " + cwd;
            VERBOSE_LVL_INIT("[INIT] " << cmd << std::endl);
            std::system(cmd.c_str());

            // delete the old systems
            for (auto outer : last_systems) {
               for (auto system : outer) {
                  delete system;
               }
            }

            last_systems = tmp;
         }

      } else if (name == "mmfes") {

         Run_Periodic *run = runs[0];

         // XXX I would like to have this in InputCheck to avoid any input parsing logic here
         std::vector<double> coulomb_factors;
         std::vector<double> vdw_factors;
         if (run->fes_coulomb_steps != 0) {
            for (unsigned int i = 0; i <= run->fes_coulomb_steps; ++i) {
               coulomb_factors.push_back(static_cast<double> (run->fes_coulomb_steps - i) / static_cast<double> (run->fes_coulomb_steps));
               vdw_factors.push_back(1.0);
            }
            if (not run->fes_vdw_factors.empty()) { // allows number of steps for Coulomb + explicite factors in vdw
               vdw_factors.insert(vdw_factors.end(), run->fes_vdw_factors.begin(), run->fes_vdw_factors.end());
               for (unsigned int i = 0; i < run->fes_vdw_factors.size(); ++i) { // appends same number of zeros at the end of coulomb
                  coulomb_factors.push_back (0.0);
               }
            } else if (run->fes_vdw_steps != 0) {
               for (unsigned int i = 0; i <= run->fes_vdw_steps; ++i) {
                  coulomb_factors.push_back(0.0);
                  vdw_factors.push_back(static_cast<double> (run->fes_vdw_steps - i) / static_cast<double> (run->fes_vdw_steps));
               }
            }
         } else {
            coulomb_factors = run->fes_coulomb_factors;
            vdw_factors = run->fes_vdw_factors;
         }

         // TODO right now manually switches between coulomb or vdw part, don't know if it can be at all both at the same time

         // IO for FES
         FILE *fout;
         fout = fopen(outfile.c_str(), "a");
         fprintf (fout, "\nFES Pathway (frame coulomb vdw)\n");
         for (unsigned int i = 0; i < coulomb_factors.size(); ++i) {
            fprintf (fout, "%8d %f %f\n", i, coulomb_factors[i], vdw_factors[i]);
         }
         fclose (fout);
         
         // loop over FES steps
         for (unsigned int step = 0; step < coulomb_factors.size() - 1; ++step) {

            // Compute factors
            run->fes_factor_reference = coulomb_factors[step]; 
            run->fes_factor_target    = coulomb_factors[step + 1]; 

            // switch for reverse
            if (run->fep_reverse) std::swap (run->fes_factor_reference, run->fes_factor_target);

            OPLS_Periodic *oplsA = nullptr;
            OPLS_Periodic *oplsB = nullptr;
            if (run->fes_factor_reference != 0.0 or
                run->fes_factor_target    != 0.0)
            {
               oplsA = new OPLSCoulombFES (last_systems.front().front(), run, run->fes_factor_reference);
               oplsB = new OPLSCoulombFES (last_systems.front().front(), run, run->fes_factor_target);
            } else {
               // Creates FF for both states (correct for normal and reverse)
               // uses NoCoulombQMMM decorator because right now this can/should be only used for uncharged solutes?
               oplsA = new OPLSNoCoulombQMMM<OPLS_Periodic_Softcore>(last_systems.front().front(), run, 1, 1, 0.5, vdw_factors[step]  );
               oplsB = new OPLSNoCoulombQMMM<OPLS_Periodic_Softcore>(last_systems.front().front(), run, 1, 1, 0.5, vdw_factors[step+1]);
               // switch opls 
               if (run->fep_reverse) std::swap (oplsA, oplsB);
            }

            fout = fopen(outfile.c_str(), "a");
            if (run->fep_reverse) {
               fprintf (fout, "\nFES %f -> %f %f -> %f\n\n",
                     run->fes_factor_reference, run->fes_factor_target,
                     vdw_factors[step + 1], vdw_factors[step]);
            } else {
               fprintf (fout, "\nFES %f -> %f %f -> %f\n\n",
                     run->fes_factor_reference, run->fes_factor_target,
                     vdw_factors[step], vdw_factors[step + 1]);
            }
            fclose (fout);

            // print parameters
            oplsA->print(outfile);
            oplsB->print(outfile);

            //
            // Running MM MC to equilibrate for A
            //

            std::string job_folder = scratch_folder + output + "-" + std::to_string (step) + "-m3c";
            make_directory_jf(job_folder);

            MMMC_Periodic *mmmc = new MMMC_Periodic (
                  job_folder, runs[0], oplsA, runs[0]->num_devices, runs[0]->num_chains);

            for (auto outer_it : last_systems) {
               for (auto system : outer_it) {
                  system->save_all(job_folder + "/systems.restart");
               }
            }

            // TODO specialized settings for PMC
            vvs tmp = mmmc->run (last_systems);

            //if (not has_last_systems) has_last_systems = true;

            delete mmmc;

            // output systems to continue
            for (auto outer : tmp) {
               for (auto system : outer) {
                  system->save_all(job_folder + "/systems.continue");
               }
            }

            // move files back from scratch to the cwd
            char *cwd = get_current_dir_name();
            std::string cmd = "mv " + job_folder + "* " + cwd;
            VERBOSE_LVL_INIT("[INIT] " << cmd << std::endl);
            std::system(cmd.c_str());

            // delete the old systems
            for (auto outer : last_systems) {
               for (auto system : outer) {
                  delete system;
               }
            }

            last_systems = tmp;

            //
            // Running FES from A->B
            //

            job_folder = scratch_folder + output + "-" + std::to_string (step) + "-" + name;
            make_directory_jf(job_folder);

            for (auto outer_it : last_systems) {
               for (auto system : outer_it) {
                  system->save_all(job_folder + "/systems.restart");
               }
            }

            // XXX mmfes is a single job and therefore I cannot override the maxmoves
            // which makes this temporary hack necessary until I have some better idea
            Run_Periodic *tmp_run = new Run_Periodic(*runs[0]);
            tmp_run->maxmoves = tmp_run->fes_maxmoves;
            MMFEPPeriodic *fes = new MMFEPPeriodic (
                  job_folder, tmp_run, oplsA, oplsB,
                  runs[0]->num_devices, runs[0]->num_chains);
            tmp = fes->run (last_systems);
            delete tmp_run;

            delete fes;
            delete oplsA;
            delete oplsB;

            // output systems to continue
            for (auto outer : tmp) {
               for (auto system : outer) {
                  system->save_all(job_folder + "/systems.continue");
               }
            }

            // move files back from scratch to the cwd
            cwd = get_current_dir_name();
            cmd = "mv " + job_folder + "* " + cwd;
            VERBOSE_LVL_INIT("[INIT] " << cmd << std::endl);
            std::system(cmd.c_str());

            // delete the old systems
            for (auto outer : last_systems) {
               for (auto system : outer) {
                  delete system;
               }
            }

            last_systems = tmp;
         }

      } else if (name == "fes") {

         // move all systems solute (molecule 0) into the origin as required by PMC
         // systemB is inited from these systems and is therefore correctly centered
         for (auto outer : last_systems) {
            for (auto system : outer) {
               move_into_center_mass (0, system);
            }
         }

         Run_Periodic *run = runs[0];
         System_Periodic *systemB = nullptr;
         systemB = new System_Periodic(*last_systems.front().front(), 0); // inits with QM part of system

         std::vector<double> coulomb_factors;
         std::vector<double> vdw_factors;
         if (run->fes_coulomb_steps != 0) {
            for (unsigned int i = 0; i < run->fes_coulomb_steps; ++i) {
               // creates coulomb factors in the normal FEP terminology (0=start,1=end)
               coulomb_factors.push_back(static_cast<double> (run->fes_coulomb_steps - i) / static_cast<double> (run->fes_coulomb_steps));
               vdw_factors.push_back(0.0);
            }
            for (unsigned int i = 0; i <= run->fes_vdw_steps; ++i) { // <=!
               // creates vdw factors in the normal FEP terminology (0=start,1=end)
               coulomb_factors.push_back(0.0);
               vdw_factors.push_back(static_cast<double> (run->fes_vdw_steps - i) / static_cast<double> (run->fes_vdw_steps));
            }
         } else {
            coulomb_factors = run->fes_coulomb_factors;
            vdw_factors = run->fes_vdw_factors;
         }

         Abstract_Switcher *switcher;
         if (run->fep_switch_pairs.size() != 0) {
            switcher = new OPLS_Switcher_factor (
                  last_systems.front().front(), run, vdw_factors);
         } else {
            switcher = new OPLS_switcher_molecule (
                  last_systems.front().front(), run, vdw_factors);
         }

         // IO for FES
         FILE *fout;
         fout = fopen(outfile.c_str(), "a");
         fprintf (fout, "\nFES Pathway (frame coulomb vdw)\n");
         for (unsigned int i = 0; i < coulomb_factors.size(); ++i) {
            fprintf (fout, "%8d %f %f\n", i, coulomb_factors[i], vdw_factors[i]);
         }
         fclose (fout);
         
         // loop over FES steps
         for (unsigned int step = 0; step < coulomb_factors.size() - 1; ++step) {

            OCLChain::lastChain = 0;   // set to 0 for every job
            OCLDevice::lastDevice = 0; // set to 0 for every job

            // Compute factors
            run->fes_factor_reference = coulomb_factors[step]; 
            run->fes_factor_target    = coulomb_factors[step + 1]; 

            // switch for reverse
            if (run->fep_reverse) std::swap (run->fes_factor_reference, run->fes_factor_target);

            // Creates FF for both states (correct for normal and reverse)
            OPLS_Periodic_FES *oplsA = new OPLS_Periodic_FES (
                  last_systems.front().front(),
                  run,
                  run->fes_factor_reference);
            OPLS_Periodic_FES *oplsB = new OPLS_Periodic_FES (
                  last_systems.front().front(),
                  run,
                  run->fes_factor_target);

            // switch parameters
            fout = fopen(outfile.c_str(), "a");
            if (run->fep_reverse) {
               fprintf (fout, "\nFES %f -> %f %f -> %f\n\n", run->fes_factor_reference, run->fes_factor_target, vdw_factors[step + 1], vdw_factors[step]);
               switcher->switch_params(step + 1, oplsA, fout);
               switcher->switch_params(step    , oplsB, fout);
            } else {
               fprintf (fout, "\nFES %f -> %f %f -> %f\n\n", run->fes_factor_reference, run->fes_factor_target, vdw_factors[step], vdw_factors[step + 1]);
               switcher->switch_params(step    , oplsA, fout);
               switcher->switch_params(step + 1, oplsB, fout);
            }
            fclose (fout);

            // print parameters
            oplsA->print(outfile);
            oplsB->print(outfile);

            //
            // Running PMC to equilibrate for A
            //

            std::string job_folder = scratch_folder + output + "-" + std::to_string (step) + "-pmc";
            make_directory_jf(job_folder);

            // TODO fix for now to get PMC settings, make later a multi-job of fes
            Run_Periodic *run_pmc = new Run_Periodic();
            // TODO this was missing in this run_pmc and I have no idea why I create a new
            // run here at all and how that ever works, but if I just use run it crashes.
            // Need to fix this mess
            run_pmc->fes_factor_reference = run->fes_factor_reference;
            run_pmc->fes_factor_target    = run->fes_factor_target   ;
            check.check("pmc", cfg, run_pmc);

            PMC_Periodic_FES *pmc = new PMC_Periodic_FES (
                  job_folder, run_pmc, oplsA, run_pmc->num_devices, run_pmc->num_chains);

            delete run_pmc;

            for (auto outer_it : last_systems) {
               for (auto system : outer_it) {
                  system->save_all(job_folder + "/systems.restart");
               }
            }

            // TODO specialized settings for PMC
            vvs tmp = pmc->run (last_systems);

            //if (not has_last_systems) has_last_systems = true;

            delete pmc;

            // output systems to continue
            for (auto outer : tmp) {
               for (auto system : outer) {
                  system->save_all(job_folder + "/systems.continue");
               }
            }

            // move files back from scratch to the cwd
            char *cwd = get_current_dir_name();
            std::string cmd = "mv " + job_folder + "* " + cwd;
            VERBOSE_LVL_INIT("[INIT] " << cmd << std::endl);
            std::system(cmd.c_str());

            // delete the old systems
            for (auto outer : last_systems) {
               for (auto system : outer) {
                  delete system;
               }
            }

            last_systems = tmp;

            //
            // Running FES from A->B
            //
            
            OCLChain::lastChain = 0;   // set to 0 for every job
            OCLDevice::lastDevice = 0; // set to 0 for every job

            job_folder = scratch_folder + output + "-" + std::to_string (step) + "-" + name;
            make_directory_jf(job_folder);

            for (auto outer_it : last_systems) {
               for (auto system : outer_it) {
                  system->save_all(job_folder + "/systems.restart");
               }
            }

            // XXX fes is a single job and therefore I cannot override the maxmoves
            // which makes this temporary hack necessary until I have some better idea
            Run_Periodic *tmp_run = new Run_Periodic(*runs[0]);
            tmp_run->maxmoves = tmp_run->fes_maxmoves;
            FEP_Periodic_FES *fep = new FEP_Periodic_FES (runs[0]);
            tmp = fep->run (last_systems, systemB, tmp_run, oplsA, oplsB,
                  job_folder, run->num_devices, run->num_chains);

            //if (not has_last_systems) has_last_systems = true;

            delete fep;
            delete oplsA;
            delete oplsB;

            // output systems to continue
            for (auto outer : tmp) {
               for (auto system : outer) {
                  system->save_all(job_folder + "/systems.continue");
               }
            }

            // move files back from scratch to the cwd
            cwd = get_current_dir_name();
            cmd = "mv " + job_folder + "* " + cwd;
            VERBOSE_LVL_INIT("[INIT] " << cmd << std::endl);
            std::system(cmd.c_str());

            // delete the old systems
            for (auto outer : last_systems) {
               for (auto system : outer) {
                  delete system;
               }
            }

            last_systems = tmp;
         }

         delete systemB;

      } else if (name == "rdf") {

         Run_Periodic *run = runs[0];
         RDF_Periodic *rdf = new RDF_Periodic(
               run->trajectory,
               run->rdf_first_id,
               run->rdf_second_id,
               run->rdf_delta,
               run->rdf_max_bin,
               run->first_frame,
               run->last_frame,
               output + "_rdf.dat",
               run->dimension_box,
               run->num_devices * run->num_chains);
         rdf->read_system(run->geometry);
         rdf->run();
         delete rdf;

      } else if (name == "rdfangle") {

         Run_Periodic *run = runs[0];
         RDF_Angle_Periodic *rdf = new RDF_Angle_Periodic(
               run->trajectory,
               run->rdf_first_id,
               run->rdf_second_id,
               run->rdf_delta,
               run->rdf_max_bin,
               run->first_frame,
               run->last_frame,
               output + "_rdf_angle.dat",
               run->dimension_box,
               run->num_devices * run->num_chains,
               run->rdf_angle_delta,
               run->rdf_angle_max_bin,
               run->rdf_angle_id);
         rdf->read_system(run->geometry);
         rdf->run();
         delete rdf;

      } else if (name == "displacement") {

         Run_Periodic *run = runs[0];
         DisplacementPeriodic analysis {
               run->trajectory,
               (int) run->first_frame,
               (int) run->last_frame,
               output + "_" + name + ".dat",
               run->dimension_box,
               run->num_devices * run->num_chains};
         analysis.read_system(run->geometry);
         analysis.run();

      } else if (name == "dipole") {

         Run_Periodic *run = runs[0];
         const OPLS_Periodic opls {last_systems.front().front(), run};
         DipolePeriodic analysis {
               run->trajectory,
               (int) run->first_frame,
               (int) run->last_frame,
               output + "_" + name + ".dat",
               run->dimension_box,
               run->num_devices * run->num_chains,
               opls.get_normal_charges ()};
         analysis.read_system(run->geometry);
         analysis.run();

      } else if (name == "qmmc") {

         const std::string job_folder = output + "-" + name;
         make_directory_jf(job_folder);

         QMMC *qmmc = new QMMC (
               job_folder, run_nonp, run_nonp->num_devices, run_nonp->num_chains);

         std::vector<std::vector<System*>> tmp = qmmc->run (system_nonp);

         delete qmmc;

         // output systems to continue
         for (auto outer : tmp) {
            for (auto system : outer) {
               system->save_all(job_folder + "/systems.continue");
            }
         }

         // XXX delete tmp for now
         for (auto outer : tmp) {
            for (auto system : outer) {
               delete system;
            }
         }
      }
   }

   if (run_nonp) {
      delete run_nonp;
   } else {
      for (Run_Periodic *run : runs) {
         delete run;
      }
      delete tmpB;
      delete switcher;

      for (auto outer : last_systems) {
         for (auto system : outer) {
            delete system;
         }
      }

      for (auto system : systemsB) {
         delete system;
      }
   }

   return EXIT_SUCCESS;
} 
