

#include "distance_vector.h"
#include "System.h"
#include "geom_utils.h"




DistanceVector::~DistanceVector ()
{
   delete[] dists;
}


DistanceVector* DistanceVector::clone () const
{
   return new DistanceVector ();
}



void DistanceVector::SetContext (System *system)
{
   if (dists) // in case the context changes
   {
      delete[] dists;
   }

   dim = system->natom;
   x = system->rx;
   y = system->ry;
   z = system->rz;
   mol2atom = &system->molecule2atom;

   dists = new double [(dim * dim - dim) / 2];
   ComputeDistances ();
}



void DistanceVector::ComputeDistances ()
{
   for (unsigned int i = 0; i < dim - 1; ++i) {
      for (unsigned int j = i + 1; j < dim; ++j) {
         double dist = compute_distance (
               x[i], y[i], z[i],
               x[j], y[j], z[j]);
         const int index = i + (j - 1) * j / 2;
         dists[index] = dist;
      }
   }
}



void DistanceVector::ComputeDistances (unsigned int molecule)
{
   for (int pos = 0 ; pos < (*mol2atom)[molecule].natoms; ++pos){
      unsigned int i = (*mol2atom)[molecule].atom_id[pos];
      for (unsigned int j = 0; j < dim; ++j) {
         if (i == j) continue;

         const double dist = compute_distance (
                  x[i], y[i], z[i],
                  x[j], y[j], z[j]);
         int index;
         if (i < j) {
            index = i + (j - 1) * j / 2;
         } else {
            index = j + (i - 1) * i / 2;
         }
         dists[index] = dist;
      }
   }
}


DistanceCall DistanceVector::GetDelegate() const
{
   return DistanceCall::create<DistanceVector, &DistanceVector::GetDistance>(this);
}


inline double DistanceVector::GetDistance (unsigned int i, unsigned int j) const
{
   if (i == j) return 0.0;
   if (i > j) std::swap (i, j);
   return dists[i + (j - 1) * j / 2];
}



void DistanceVector::accept (DistanceInterface *other, unsigned int molecule)
{
   DistanceCall GetDistanceOther = other->GetDelegate ();
   for (int pos = 0 ; pos < (*mol2atom)[molecule].natoms; ++pos){
      unsigned int i = (*mol2atom)[molecule].atom_id[pos];
      for (unsigned int j = 0; j < dim; ++j) {
         if (i == j) continue;
         if (i < j) {
            dists[i + (j - 1) * j / 2] = GetDistanceOther (i, j);
         } else {
            dists[j + (i - 1) * i / 2] = GetDistanceOther (j, i);
         }
      }
   }
}





