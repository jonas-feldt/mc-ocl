/*
 * File: distance_interface.h
 * Author: Jonas Feldt
 *
 * Version: 2017-10-08
 *
 * Interface for distance classes.
 */


#ifndef DISTANCE_INTERFACE_H
#define DISTANCE_INTERFACE_H

#include "typedefs.h"

class System;

class DistanceInterface
{

   public:

      virtual ~DistanceInterface () {};
      virtual void SetContext (System *system) = 0;
      virtual DistanceInterface *clone () const = 0;

      virtual void ComputeDistances () = 0;
      virtual void ComputeDistances (unsigned int molecule) = 0;

      virtual void accept (DistanceInterface *dists, unsigned int molecule) = 0;

      virtual DistanceCall GetDelegate() const = 0;
};


#endif // DISTANCE_INTERFACE_H
