/*
 * File:   Charge_Grid.h
 * Author: Jonas Feldt
 *
 * Version: 2013-11-05
 */

#ifndef CHARGE_GRID_H
#define	CHARGE_GRID_H

#include <string>

#include "System.h"
#include "System_Periodic.h"
#include "options.h"

class Charge_Grid {
public:
   Charge_Grid();
   Charge_Grid (Charge_Grid &other);
   
   virtual ~Charge_Grid();
   void read_density(std::string cube_file);
   void reduced_coords (double box_dim, double cutoff);
   bool is_sane(std::string file);

   _FPOINT_G_ *charges;
   _FPOINT_G_ *x;
   _FPOINT_G_ *y;
   _FPOINT_G_ *z;
   int dim;

   double cutoff;
   double cutoff_coulomb_rec;
   double cutoff_coulomb_rec_sq;
   double sqrt_box_dim;
   
private:
};

#endif	/* CHARGE_GRID_H */

