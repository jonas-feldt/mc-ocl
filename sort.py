#!/usr/bin/python

import sys
import random

if(len(sys.argv) != 2):
   print "usage: sorter <input-file>"
   sys.exit()

f = open(sys.argv[1], 'r')
f2 = open(str(sys.argv[1]+'_sorted'+'.inp'), 'w')

lines=[]
lines_index=[]

new_lines=[]

for line in f:
   lines.append(line)
   lines_index.append(int(line.split(' ')[0].split(':')[1]))
   
keydict = dict(zip(lines,lines_index))
lines.sort(key=keydict.get)

for line in lines:
   f2.write(line)