/*
 * File: InputCheck.h
 * Author: Jonas Feldt
 *
 * Version: 2017-08-15
 */

#ifndef INPUTCHECK_H
#define INPUTCHECK_H

#include <libconfig.h++>
#include <string>
#include <map>

#include "Global.h"

class Run_Periodic;
class Run;

class InputCheck
{

   public:

      InputCheck();
      ~InputCheck();
      void check (const std::string job, const libconfig::Config &config,
            Run_Periodic *run);
      void check (const std::string job, const libconfig::Config &config,
            Run *run);
      static std::string add_output (const libconfig::Config &config,
            const char* default_output);

   private:

      std::map<std::string, void (InputCheck::*)(const libconfig::Config&, Run_Periodic*)> job_map_periodic;
      std::map<std::string, void (InputCheck::*)(const libconfig::Config&, Run*)> job_map;

      // checks for periodic jobs
      void check_mmmc         (const libconfig::Config &config, Run_Periodic *run);
      void check_pmc          (const libconfig::Config &config, Run_Periodic *run);
      void check_dd           (const libconfig::Config &config, Run_Periodic *run);
      void check_fep          (const libconfig::Config &config, Run_Periodic *run);
      void check_fes          (const libconfig::Config &config, Run_Periodic *run);
      void check_mmfes        (const libconfig::Config &config, Run_Periodic *run);
      void check_mmfep        (const libconfig::Config &config, Run_Periodic *run);
      void check_rdf          (const libconfig::Config &config, Run_Periodic *run);
      void check_rdfangle     (const libconfig::Config &config, Run_Periodic *run);
      void check_displacement (const libconfig::Config &config, Run_Periodic *run);
      void check_dipole       (const libconfig::Config &config, Run_Periodic *run);

      // checks for non-periodic jobs
      void check_qmmc (const libconfig::Config &config, Run *run);

      // common helper functions
      void set_defaults (Run *run);
};



#endif /* INPUTCHECK_H */
