/*
 * File:   PMC_Periodic_Dual.cpp
 * Author: Jonas Feldt
 *
 * Version: 2017-02-15
 */

#include <iostream>
#include <math.h>
#include <iomanip>
#include <string>
#include <sstream>
#include <stdio.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <signal.h>

#include "PMC_Periodic.h"
#include "PMC_Periodic_Dual.h"
#include "Dual_OCLmanager.h"
#include "jonas_io_utils.h"
#include "math_utils.h"
#include "geom_utils.h"
#include "System_Periodic.h"
#include "Run_Periodic.h"
#include "OPLS_Periodic.h"
#include "Charge_Grid.h"
#include "typedefs.h"

#include "options.h" // Header with all run options.


/**
 * Constructor
 */
PMC_Periodic_Dual::PMC_Periodic_Dual (Run_Periodic *run)
   : m_run (run)
{
}



/**
 * Destructor
 */
PMC_Periodic_Dual::~PMC_Periodic_Dual () {
}




/**
 * Runs the dual density PMC simulation. Computes once the gas-phase energy
 * of the QM system.
 *
 * @param systems
 * @param run
 * @param opls
 * @param base_name
 * @param num_devices
 * @param num_chains
 */
vvs PMC_Periodic_Dual::run (
      vvs &systems,
      Run_Periodic *run_original,
      OPLS_Periodic *opls,
      std::string base_name,
      int num_devices,
      int num_chains) {

   make_directory_jf (base_name);
   write_dummy_latt_file_jf (base_name);
   // uses just the first system here
   m_energy_qm_gp = PMC_Periodic::qm_gas_phase_updates_wf (
         systems[0][0], base_name, m_run);

   return run (systems, run_original, opls, base_name,
         num_devices, num_chains, m_energy_qm_gp);
}



/**
 * Runs the dual density PMC simulation.
 *
 * @param systemA
 * @param run
 * @param opls
 * @param base_name
 * @param num_devices
 * @param num_chains
 * @param energy_qm_gp
 */
vvs PMC_Periodic_Dual::run (
      vvs &systems,
      Run_Periodic *run_original,
      OPLS_Periodic *opls,
      std::string base_name,
      int num_devices,
      int num_chains,
      double energy_qm_gp) {

   Run_Periodic *run = new Run_Periodic(*run_original);

   // all vectors (of vectors) are guaranteed to have the same size,
   // therefore they are iterated with the unchecked operator[] in the case
   // that simultaneous iterations are necessary
   vvd energies_mm       (systems.size(), vd(systems.begin()->size())); // current MM energies
   vvd energies_oldA     (systems.size(), vd(systems.begin()->size())); // total energies of the last step
   vvd energies_oldB     (systems.size(), vd(systems.begin()->size()));
   vvd energies_vdw_qmmm (systems.size(), vd(systems.begin()->size())); // current vdW and Coulomb MM energies
   vvd energies_qmA      (systems.size(), vd(systems.begin()->size())); // current QM & Coulomb QMMM energy
   vvd energies_qmB      (systems.size(), vd(systems.begin()->size()));

   int update_lists_every;

   double stepmax       = run->stepmax;
   double temperature   = run->temperature;
   double theta_max     = run->theta_max;

   int old_adjust_stepmax = run->adjust_max_step;
   int old_maxmoves = run->maxmoves;
   //Adjust maxmoves according to number of chains
   run->maxmoves /= num_chains * num_devices;

   if (run->wf_adjust > run->maxmoves) run->wf_adjust = 0; // No WFU

   if (run->adjust_max_step == 0) {
      // update_lists_every must be the closest multiple to m_wf_adjust
      if (run->wf_adjust == 0)
	 update_lists_every = run->n_default_autonomous;
      else if (run->n_default_autonomous > run->wf_adjust)
         update_lists_every = ((int) run->n_default_autonomous / run->wf_adjust) * run->wf_adjust;
      else
         update_lists_every = run->wf_adjust;

   } else {
      // update_lists_every and ajdust_max_step
      // must be the closest multiple to m_wf_adjust
      if (run->wf_adjust != 0) {
         run->adjust_max_step = ((int) run->adjust_max_step / run->wf_adjust) * run->wf_adjust;
      }
      update_lists_every = run->adjust_max_step;
   }

   if(run->wf_adjust != 0){
      //max_moves should be a multiple of wf_adjust
      run->maxmoves =(ceil((double)run->maxmoves / (double) run->wf_adjust)) * run->wf_adjust;
   }

   VERBOSE_LVL_WARN("[WARN] Set Update Lists Every : " << update_lists_every<<"(was "<<run->n_default_autonomous<<")"<< std::endl);
   VERBOSE_LVL_WARN("[WARN] Set Step Max : " << run->adjust_max_step <<"(was "<<old_adjust_stepmax<<")" << std::endl);
   VERBOSE_LVL_WARN("[WARN] Set Maxmoves : " << run->maxmoves <<"(was "<<old_maxmoves<<")" << std::endl);
   VERBOSE_LVL_INIT("[INIT] Making directory " << base_name << "..."<< std::endl);

   make_directory_jf (base_name);

   // init pipe hosts
   Pipe_Host *hosts[num_devices * num_chains];

   int index = 0;
   for (auto outer_it = systems.begin(); outer_it != systems.end(); ++outer_it) {
      for (auto it = outer_it->begin(); it != outer_it->end(); ++it) {
         hosts[index] = new Pipe_Host(base_name, run->molpro_pipe_exe);
         const std::string tmp = "pipe_" + std::to_string(index);
         write_molecule_qm_jf (m_run->pipe_input, base_name, tmp, (*it));
         hosts[index]->launch_child_process (tmp + ".inp");
         index++;
      }
   }

   // Create initial grids
   vvg gridsA;
   vvg gridsB;
   gridsA.reserve(systems.size());
   gridsB.reserve(systems.size());
   for (unsigned int i = 0; i < systems.size(); ++i) {
      vg tmpA;
      vg tmpB;
      tmpA.reserve(systems.begin()->size());
      tmpB.reserve(systems.begin()->size());
      for (unsigned int j = 0; j < systems.begin()->size(); ++j) {

         System_Periodic *system = systems[i][j];
         // QM with lattice to generate initial grid for A
         VERBOSE_LVL_INIT("[INIT] LatticeA update..."<< std::endl);
         energies_qmA[i][j] = PMC_Periodic::qm_lattice_updates_wf (
               i, j, system, opls, base_name, m_run);
         VERBOSE_LVL_INIT("\tRead energy_qm A " << energies_qmA[i][j] << std::endl);
         const std::string folder = std::to_string(i) + "-" + std::to_string(j);
         VERBOSE_LVL_INIT("\tInitializing Grid A (and reading "<< base_name + "/" + folder + GRID_FILE << ")" << std::endl);
         Charge_Grid *gridA = new Charge_Grid ();
         gridA->read_density (base_name + "/" + folder + GRID_FILE);
         gridA->reduced_coords (run->dimension_box, run->cutoff_coulomb);
         if (!gridA->is_sane(base_name + ".out") && !IGNORE_ERRORS) exit(EXIT_FAILURE);
         VERBOSE_LVL_INIT("\tGrid Points " << gridA->dim << std::endl);

         VERBOSE_LVL_INIT("\tInitializing Grid B (and reading "<< base_name + "/" + folder + GRID_FILE_TARGET << ")" << std::endl);
         energies_qmB[i][j] = read_energy_molpro_jf (base_name + "/" + folder, "qm_lattice", m_run->target_state);
         VERBOSE_LVL_INIT("\tRead energy_qm B " << energies_qmB[i][j] << std::endl);
         Charge_Grid *gridB = new Charge_Grid ();
         gridB->read_density (base_name + "/" + folder + GRID_FILE_TARGET);
         gridB->reduced_coords (run->dimension_box, run->cutoff_coulomb);
         if (!gridB->is_sane(base_name + ".out") && !IGNORE_ERRORS) exit(EXIT_FAILURE);
         VERBOSE_LVL_INIT("\tGrid Points " << gridB->dim << std::endl);

         if (run->emission) {
            tmpA.push_back(gridB);
            tmpB.push_back(gridA);
         } else {
            tmpA.push_back(gridA);
            tmpB.push_back(gridB);
         }

         VERBOSE_LVL_INIT("[INIT] System MM atoms=" << system->natom << std::endl);
         VERBOSE_LVL_INIT("[INIT] Computing first MM..."<< std::endl);
         system->UpdateDistances ();
         energies_vdw_qmmm[i][j] = opls->energy_vdw_qmmm (system->GetDistance);
         energies_mm      [i][j] = opls->energy_mm       (system->GetDistance);
         VERBOSE_LVL_INIT("\tMM energy A: " << energies_mm[i][j] << std::endl);
         VERBOSE_LVL_INIT("\tvdw_qmmm energy A: " << energies_vdw_qmmm[i][j] << std::endl);

         energies_oldA[i][j] = energies_qmA[i][j] + energies_vdw_qmmm[i][j] + energies_mm[i][j];
         energies_oldB[i][j] = energies_qmB[i][j] + energies_vdw_qmmm[i][j] + energies_mm[i][j];
         VERBOSE_LVL_INIT("\tE_old A: " << energies_oldA[i][j] << std::endl);
         VERBOSE_LVL_INIT("\tE_old B: " << energies_oldB[i][j] << std::endl);
      }
      gridsA.push_back(tmpA);
      gridsB.push_back(tmpB);
   }

   // swap energies for emission spectra
   if (run->emission) {
      std::swap (energies_qmA, energies_qmB);
      std::swap (energies_oldA, energies_oldB);
   }

   //
   // The main code is in ocl::run
   //
   VERBOSE_LVL_INIT("[INIT] Initializing OCL..." << std::endl);
   vvs final_systems;
   OCLmanager *ocl;
   try{
      int tmp = 0;
      if (run->print_energy != 0) {
         tmp = run->print_every / run->print_energy * run->n_save_systems * N_ENERGY_SAVE_PARAMS_FEP;
      }
      ocl = new Dual_OCLmanager (
            update_lists_every,  // Max number of steps OCL can run between list refreshes
            10,                  // 10 max kernels
            num_devices,         // number of devices
            num_chains,          // number of chains
            systems,             // ptr to system
            opls,                // ptr to opls
            run,                 // ptr to run
            gridsA,              // ptr to grid
            gridsB,
                                 // size of save-config data in opencl devices:
            systems[0][0]->natom*run->n_save_systems*3+ run->n_save_systems * N_ENERGY_SAVE_PARAMS_DUAL,
            tmp,
            temperature,
            theta_max,
            stepmax,
            energies_qmA,
            energies_qmB,
            energy_qm_gp,
            energies_mm,
            energies_vdw_qmmm,
            energies_oldA,
            energies_oldB,
            hosts,
            base_name);

      ocl->init();
      ocl->run();
      final_systems = ocl->get_final_systems();

   } catch (cl::Error &error) {
      std::cout<<"PMC_Periodic_Dual::Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      exit (EXIT_FAILURE);
   }

   //
   // deletes resources
   //
   for (auto outer_it = gridsA.begin(); outer_it != gridsA.end(); ++outer_it) {
      for (auto it = outer_it->begin(); it != outer_it->end(); ++it) {
         delete (*it);
      }
   }
   for (auto outer_it = gridsB.begin(); outer_it != gridsB.end(); ++outer_it) {
      for (auto it = outer_it->begin(); it != outer_it->end(); ++it) {
         delete (*it);
      }
   }
   delete ocl;
   delete run;
   for (int i = 0; i < num_devices * num_chains; i++){
      delete hosts[i];
   }

   return final_systems;
}


double PMC_Periodic_Dual::get_energy_qm_gp() {
   return m_energy_qm_gp;
}
