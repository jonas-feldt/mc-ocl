
#include <string>
#include <vector>
#include <cmath>
#include <iostream>

#include "gtest/gtest.h"
#include "System.h"
#include "System_Periodic.h"
#include "distance_matrix.h"
#include "OPLS.h"
#include "OPLS_Periodic.h"
#include "opls_periodic_dual_topology.h"
#include "opls_no_coulomb_qmmm.h"
#include "io_utils.h"
#include "Run_Periodic.h"

#define ERROR 0.001

//
// Tests if opls gets correctly initialized.
//
TEST (OPLSPeriodic, General) {
   double box_dim = 1000.0;
   System_Periodic *system = read_tinker_periodic_system ("gtest_opls.xyz", box_dim);
   double step_max = 1.0;
   double theta_max = 1.0;
   Run_Periodic run {1000, step_max, 298.0, theta_max, 100, 100, 100, box_dim /
      2.0, box_dim / 2.0, box_dim, 100, "foo", "bar", 1234, false, "baz"};

   ASSERT_EQ (run.stepmax, step_max / box_dim);
   ASSERT_EQ (run.cutoff_coulomb, box_dim / 2.0 / box_dim);
   ASSERT_EQ (run.cutoff_vdw, box_dim / 2.0 / box_dim);

   run.atom_types = {1};
   run.epsilons = {1.0};
   run.sigmas = {2.0};
   run.charges = {-1.0};
   OPLS_Periodic opls {system, &run};

   ASSERT_EQ (opls.epsilon[0], 1.0);
   ASSERT_EQ (opls.epsilon[1], 1.0);

   ASSERT_EQ (opls.sigma[0], 2.0 / box_dim);
   ASSERT_EQ (opls.sigma[1], 2.0 / box_dim);

   ASSERT_EQ (opls.charge[0], -1.0 / sqrt (box_dim));
   ASSERT_EQ (opls.charge[1], -1.0 / sqrt (box_dim));
}

//
// Tests vdw term and compare for large box_dim with opls
//
TEST (OPLSPeriodic, VDW) {

   // systems
   System *system_ref = read_tinker_system ("gtest_opls.xyz");
   double box_dim = 1000.0;
   System_Periodic *system = read_tinker_periodic_system ("gtest_opls.xyz", box_dim);

   // run_periodic
   double step_max = 1.0;
   double theta_max = 1.0;
   Run_Periodic run {1000, step_max, 298.0, theta_max, 100, 100, 100, box_dim /
      2.0, box_dim / 2.0, box_dim, 100, "foo", "bar", 1234, false, "baz"};

   // opls with only vdw
   run.atom_types = {1};
   run.epsilons = {1.0};
   run.sigmas = {1.5};
   run.charges = {0.0};
   OPLS_Periodic opls {system, &run};
   std::vector<int> types {1};
   std::vector<double> epsilons {1.0};
   std::vector<double> sigmas {1.5};
   std::vector<double> charges {0.0};
   OPLS opls_ref {system_ref, types, epsilons, sigmas, charges, 1};

   // check if setup was correct
   ASSERT_EQ (opls.epsilon[0], opls_ref.epsilon[0]);
   ASSERT_EQ (opls.epsilon[1], opls_ref.epsilon[1]);
   ASSERT_EQ (opls.sigma[0], opls_ref.sigma[0] / box_dim);
   ASSERT_EQ (opls.sigma[1], opls_ref.sigma[1] / box_dim);
   ASSERT_EQ (opls.charge[0], opls_ref.charge[0] / sqrt (box_dim));
   ASSERT_EQ (opls.charge[1], opls_ref.charge[1] / sqrt (box_dim));

   // compare energies
   EXPECT_LT (std::abs(opls.energy (system->GetDistance) -
         opls_ref.energy(system_ref->GetDistance)), ERROR);
}



//
// Tests coulomb term and compare for giant box_dim with opls
//
TEST (OPLSPeriodic, Coulomb) {

   // systems
   System *system_ref = read_tinker_system ("gtest_opls.xyz");
   double box_dim = 10000000.0;
   System_Periodic *system = read_tinker_periodic_system ("gtest_opls.xyz", box_dim);

   // run_periodic
   double step_max = 1.0;
   double theta_max = 1.0;
   Run_Periodic run {1000, step_max, 298.0, theta_max, 100, 100, 100, box_dim /
      2.0, box_dim / 2.0, box_dim, 100, "foo", "bar", 1234, false, "baz"};

   // opls with only vdw
   run.atom_types = {1};
   run.epsilons = {0.0};
   run.sigmas = {1.5};
   run.charges = {1.0};
   OPLS_Periodic opls {system, &run};
   std::vector<int> types {1};
   std::vector<double> epsilons {0.0};
   std::vector<double> sigmas {1.5};
   std::vector<double> charges {1.0};
   OPLS opls_ref {system_ref, types, epsilons, sigmas, charges, 1};

   // check if setup was correct
   ASSERT_EQ (opls.epsilon[0], opls_ref.epsilon[0]);
   ASSERT_EQ (opls.epsilon[1], opls_ref.epsilon[1]);
   ASSERT_EQ (opls.sigma[0], opls_ref.sigma[0] / box_dim);
   ASSERT_EQ (opls.sigma[1], opls_ref.sigma[1] / box_dim);
   ASSERT_EQ (opls.charge[0], opls_ref.charge[0] / sqrt (box_dim));
   ASSERT_EQ (opls.charge[1], opls_ref.charge[1] / sqrt (box_dim));

   // compare energies
   EXPECT_LT (std::abs(opls.energy (system->GetDistance) -
         opls_ref.energy(system_ref->GetDistance)), ERROR);
}



//
// Tests vdw_qmmm term and compare for large box_dim with opls
//
TEST (OPLSPeriodic, VDWQMMM) {

   // systems
   System *system_ref = read_tinker_system ("gtest_opls.xyz");
   double box_dim = 1000.0;
   System_Periodic *system = read_tinker_periodic_system ("gtest_opls.xyz", box_dim);

   // run_periodic
   double step_max = 1.0;
   double theta_max = 1.0;
   Run_Periodic run {1000, step_max, 298.0, theta_max, 100, 100, 100, box_dim /
      2.0, box_dim / 2.0, box_dim, 100, "foo", "bar", 1234, false, "baz"};

   // opls with only vdw
   run.atom_types = {1};
   run.epsilons = {1.0};
   run.sigmas = {1.5};
   run.charges = {0.0};
   OPLS_Periodic opls {system, &run};
   std::vector<int> types {1};
   std::vector<double> epsilons {1.0};
   std::vector<double> sigmas {1.5};
   std::vector<double> charges {0.0};
   OPLS opls_ref {system_ref, types, epsilons, sigmas, charges, 1};

   // check if setup was correct
   ASSERT_EQ (opls.epsilon[0], opls_ref.epsilon[0]);
   ASSERT_EQ (opls.epsilon[1], opls_ref.epsilon[1]);
   ASSERT_EQ (opls.sigma[0], opls_ref.sigma[0] / box_dim);
   ASSERT_EQ (opls.sigma[1], opls_ref.sigma[1] / box_dim);
   ASSERT_EQ (opls.charge[0], opls_ref.charge[0] / sqrt (box_dim));
   ASSERT_EQ (opls.charge[1], opls_ref.charge[1] / sqrt (box_dim));

   // compare energies
   EXPECT_LT (std::abs(opls.energy_vdw_qmmm (system->GetDistance) - opls_ref.energy_vdw_qmmm(system_ref->GetDistance)), ERROR);
   EXPECT_EQ (opls.energy (system->GetDistance), opls.energy_vdw_qmmm(system->GetDistance));
}




//
// Tests vdw_mm term and compare for large box_dim with opls
//
TEST (OPLSPeriodic, VDWMM) {

   // systems
   System *system_ref = read_tinker_system ("gtest_opls2.xyz");
   double box_dim = 1000.0;
   System_Periodic *system = read_tinker_periodic_system ("gtest_opls2.xyz", box_dim);

   // run_periodic
   double step_max = 1.0;
   double theta_max = 1.0;
   Run_Periodic run {1000, step_max, 298.0, theta_max, 100, 100, 100, box_dim /
      2.0, box_dim / 2.0, box_dim, 100, "foo", "bar", 1234, false, "baz"};

   // opls with only vdw
   run.atom_types = {1};
   run.epsilons = {1.0};
   run.sigmas = {1.5};
   run.charges = {0.0};
   OPLS_Periodic opls {system, &run};
   std::vector<int> types {1};
   std::vector<double> epsilons {1.0};
   std::vector<double> sigmas {1.5};
   std::vector<double> charges {0.0};
   OPLS opls_ref {system_ref, types, epsilons, sigmas, charges, 1};

   // check if setup was correct
   ASSERT_EQ (opls.epsilon[0], opls_ref.epsilon[0]);
   ASSERT_EQ (opls.epsilon[1], opls_ref.epsilon[1]);
   ASSERT_EQ (opls.sigma[0], opls_ref.sigma[0] / box_dim);
   ASSERT_EQ (opls.sigma[1], opls_ref.sigma[1] / box_dim);
   ASSERT_EQ (opls.charge[0], opls_ref.charge[0] / sqrt (box_dim));
   ASSERT_EQ (opls.charge[1], opls_ref.charge[1] / sqrt (box_dim));

   // compare energies
   EXPECT_LT (std::abs(opls.energy_mm (system->GetDistance) -
         opls_ref.energy_mm(system_ref->GetDistance)), ERROR);
}




//
// Tests coulomb_mm term and compare for large box_dim with opls
//
TEST (OPLSPeriodic, CoulombMM) {

   // systems
   System *system_ref = read_tinker_system ("gtest_opls2.xyz");
   double box_dim = 100000000.0;
   System_Periodic *system = read_tinker_periodic_system ("gtest_opls2.xyz", box_dim);

   // run_periodic
   double step_max = 1.0;
   double theta_max = 1.0;
   Run_Periodic run {1000, step_max, 298.0, theta_max, 100, 100, 100, box_dim /
      2.0, box_dim / 2.0, box_dim, 100, "foo", "bar", 1234, false, "baz"};

   // opls with only vdw
   run.atom_types = {1};
   run.epsilons = {0.0};
   run.sigmas = {1.5};
   run.charges = {1.0};
   OPLS_Periodic opls {system, &run};
   std::vector<int> types {1};
   std::vector<double> epsilons {0.0};
   std::vector<double> sigmas {1.5};
   std::vector<double> charges {1.0};
   OPLS opls_ref {system_ref, types, epsilons, sigmas, charges, 1};

   // check if setup was correct
   ASSERT_EQ (opls.epsilon[0], opls_ref.epsilon[0]);
   ASSERT_EQ (opls.epsilon[1], opls_ref.epsilon[1]);
   ASSERT_EQ (opls.sigma[0], opls_ref.sigma[0] / box_dim);
   ASSERT_EQ (opls.sigma[1], opls_ref.sigma[1] / box_dim);
   ASSERT_EQ (opls.charge[0], opls_ref.charge[0] / sqrt (box_dim));
   ASSERT_EQ (opls.charge[1], opls_ref.charge[1] / sqrt (box_dim));

   // compare energies
   EXPECT_LT (std::abs(opls.energy_vdw_qmmm (system->GetDistance) -
         opls_ref.energy_vdw_qmmm(system_ref->GetDistance)), ERROR);
}






//
// 
// Tests OPLSPeriodicDualTopology and compare with OPLS_Periodic.
//
TEST (OPLSPeriodic, OPLSPeriodicDualTopology) {

   // systems
   const double box_dim = 10.0;
   System_Periodic *system  = read_tinker_periodic_system ("gtest_opls2.xyz", box_dim);
   System_Periodic *systema = read_tinker_periodic_system ("gtest_oplsa.xyz", box_dim);
   System_Periodic *systemb = read_tinker_periodic_system ("gtest_oplsb.xyz", box_dim);

   // run_periodic
   double step_max = 1.0;
   double theta_max = 1.0;
   Run_Periodic run {1000, step_max, 298.0, theta_max, 100, 100, 100, box_dim /
      2.0, box_dim / 2.0, box_dim, 100, "foo", "bar", 1234, false, "baz"};

   run.fep_reference_molecule = 1;
   run.fep_target_molecule = 1;


   // opls with only vdw
   run.atom_types = {1};
   run.epsilons = {1.0};
   run.sigmas = {1.5};
   run.charges = {0.0};
   OPLS_Periodic oplsa {systema, &run};
   OPLS_Periodic oplsb {systemb, &run};

   run.fep_reference_molecule = 0;
   run.fep_target_molecule = 1;
   run.solute_molecules = 2;

   // compare energies
   auto *oplsdual = new OPLSNoCoulombQMMM<OPLSPeriodicDualTopology> (system , &run, 1, 1, 0.5, 0.0);
   EXPECT_EQ (oplsa.energy (systema->GetDistance), oplsdual->energy (system->GetDistance));
   delete oplsdual;
   oplsdual = new OPLSNoCoulombQMMM<OPLSPeriodicDualTopology> (system , &run, 1, 1, 0.5, 1.0);
   EXPECT_EQ (oplsb.energy (systemb->GetDistance), oplsdual->energy (system->GetDistance));
   delete oplsdual;
}




