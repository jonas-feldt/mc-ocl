
#include "distance_matrix_periodic.h"
#include "geom_utils.h"
#include "System.h" // needed for molecule_db


DistanceMatrixPeriodic* DistanceMatrixPeriodic::clone () const
{
   return new DistanceMatrixPeriodic ();
}



void DistanceMatrixPeriodic::ComputeDistances ()
{
   for (unsigned int i = 0; i < dim - 1; ++i) {
      for (unsigned int j = i + 1; j < dim; ++j) {
         double dist = compute_minimal_distance (
               x[i], y[i], z[i],
               x[j], y[j], z[j]);
         dists[i][j] = dist;
      }
   }
}



void DistanceMatrixPeriodic::ComputeDistances (unsigned int molecule)
{
   for (int pos = 0 ; pos < (*mol2atom)[molecule].natoms; ++pos){
      unsigned int i = (*mol2atom)[molecule].atom_id[pos];
      for (unsigned int j = 0; j < dim; ++j) {
         const double dist = compute_minimal_distance (
                  x[i], y[i], z[i],
                  x[j], y[j], z[j]);
         if (i < j) {
            dists[i][j] = dist;
         } else {
            dists[j][i] = dist;
         }
      }
   }
}



