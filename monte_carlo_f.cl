/**
 *
 * File: monte_carlo_d.cl
 * Author: Sebastiao Miranda
 *
 * Version: 
 *
 */
 
/**
 * Equivalent calls in Reference PMC CYCLE: 
 *
 * int random_molecule = random_gen_mol->uniform_int_a_b (1, nmol_total - 1);
 * rotate_random_molecule (theta_max, random_molecule, copy, random_gen_mol);
 * translate_random_molecule (stepmax, random_molecule, copy, random_gen_mol);
 */
#pragma OPENCL EXTENSION cl_khr_fp64: enable

#include "options.h"
__kernel
void monte_carlo(

   // Scalar arguments
   int step_number,

   // MM molecule old x,y,z,mass
   __global double   *old_x,
   __global double   *old_y,
   __global double   *old_z,


   // Changed MM molecule (Kernel will write 
   __global int      *changed_molecule,
   __global int      *changed_molecule_size,
   __global double   *ch_x,
   __global double   *ch_y,
   __global double   *ch_z,
    
   // Random Number Lists

      /*For mc molecule picking*/
   __global int *rand_mc_mol,

      /*For rotation*/
   __global double *rand_rotate_theta,
   __global double *rand_rotate_q0,
   __global double *rand_rotate_q1,
   __global double *rand_rotate_q2,
   __global double *rand_rotate_q3,
      
      /*For transladation*/
   __global double *random_trans_num,
   __global double *rand_trans_ex,
   __global double *rand_trans_ey,
   __global double *rand_trans_ez,

   // System Constants
   __global int     *n_total_mol,
   __global int     *n_total_atom,
   __global double  *mass,
   __global int     *mol2atom0,
   
   // Update random lists frequency
   __constant int     *update_lists_every,
   
   // in this version (f) , there is also a float output
   __global float   *ch_x_f,
   __global float   *ch_y_f,
   __global float   *ch_z_f

   ){
   
   // Random Number private holders
   __private int    p_random_molecule     ;
   __private double p_rand_rotate_theta   ;
   __private double q0                    ;
   __private double q1                    ;
   __private double q2                    ;
   __private double q3                    ;
   __private double trans_num             ;
   __private double ex                    ;
   __private double ey                    ;
   __private double ez                    ;
   
   // Variables for rotation
   __private double x_center         = 0.0;
   __private double y_center         = 0.0;
   __private double z_center         = 0.0;
   __private double molecular_mass   = 0.0;
   __private double rec                   ;
   __private double rx_new                ;
   __private double ry_new                ;
   __private double rz_new                ;
   __private double rx_old                ;
   __private double ry_old                ;
   __private double rz_old                ;
   
   // Molecule atoms
   __private int start_atom_i             ;
   __private int end_atom                 ;
   __private int p_list_index             ;
   
   p_list_index = step_number%(*update_lists_every);
      
   // Read Random numbers from lists
   p_random_molecule   = rand_mc_mol       [p_list_index];
   p_rand_rotate_theta = rand_rotate_theta [p_list_index];
   q0                  = rand_rotate_q0    [p_list_index];
   q1                  = rand_rotate_q1    [p_list_index];
   q2                  = rand_rotate_q2    [p_list_index];
   q3                  = rand_rotate_q3    [p_list_index];
   trans_num           = random_trans_num  [p_list_index];
   ex                  = rand_trans_ex     [p_list_index];
   ey                  = rand_trans_ey     [p_list_index];
   ez                  = rand_trans_ez     [p_list_index];
   
   // TODO: Copy data do private or local memory ?
   // 
   //
   
   // Find Out atoms belonging to the molecule
   // end_atom no longer belongs to molecule
   // end_atom-start_atom_i gives molecule size
   if(p_random_molecule<(*n_total_mol)-1){
      start_atom_i = mol2atom0[p_random_molecule];
      end_atom     = mol2atom0[p_random_molecule+1];
   }else{//Last molecule
      start_atom_i = mol2atom0[p_random_molecule];
      end_atom     = *n_total_atom;
   }
  
   // Copy Reference to changed molecule buffers
   *changed_molecule       =p_random_molecule;
   *changed_molecule_size  =(end_atom-start_atom_i);

   for(int i=0; i<(end_atom-start_atom_i); i++){
   
      ch_x[i] = old_x[start_atom_i+i];
      ch_y[i] = old_y[start_atom_i+i];
      ch_z[i] = old_z[start_atom_i+i];

   }

   // Center Of Mass + find molecular mass
   
   for (int i = 0; i < (end_atom-start_atom_i); i++) {
      

      x_center += ch_x[i] * mass[i+start_atom_i];
      y_center += ch_y[i] * mass[i+start_atom_i];
      z_center += ch_z[i] * mass[i+start_atom_i];
      molecular_mass += mass[i+start_atom_i];
   }

   rec = 1.0 / molecular_mass;
   x_center *= rec;
   y_center *= rec;
   z_center *= rec;
   
   //Apply rotation to molecule
   for (int i = 0; i < (end_atom-start_atom_i); i++) {

         // r_center is the center of mass and the center of the rotation
         ch_x[i] -= x_center;
         ch_y[i] -= y_center;
         ch_z[i] -= z_center;
         
         rx_old = ch_x[i];
         ry_old = ch_y[i];
         rz_old = ch_z[i];
  
         rx_new = rx_old * (1.0 - 2    * (q2 * q2) - 2.0 * (q3 * q3))   + ry_old 
                         * (2.0        * (q1 * q2) - 2.0 * (q0 * q3))   + rz_old 
                         * (2.0        * (q1 * q3) + 2.0 * (q0 * q2));
         
         
         ry_new = rx_old * (2.0       * (q2 * q1) + 2.0 * (q0 * q3))    + ry_old 
                         * (1.0 - 2.0 * (q3 * q3) - 2.0 * (q1 * q1))    + rz_old 
                         * (2.0       * (q2 * q3) - 2.0 * (q0 * q1));
         
         
         rz_new = rx_old * (2.0        * (q3 * q1) - 2.0 * (q0 * q2))   + ry_old 
                         * (2.0        * (q3 * q2) + 2.0 * (q0 * q1))   + rz_old 
                         * (1.0 - 2.0  * (q1 * q1) - 2.0 * (q2 * q2));

         ch_x[i] = rx_new + x_center;
         ch_y[i] = ry_new + y_center;
         ch_z[i] = rz_new + z_center;
         
   }
   
   //Apply translation to molecule
   for (int i = 0; i < (end_atom-start_atom_i); i++) {
      ch_x[i] += (trans_num * ex);
      ch_y[i] += (trans_num * ey);
      ch_z[i] += (trans_num * ez);

      //create float counterparts
      ch_x_f[i] = (float)ch_x[i];
      ch_y_f[i] = (float)ch_y[i];
      ch_z_f[i] = (float)ch_z[i];
   }
     
}
