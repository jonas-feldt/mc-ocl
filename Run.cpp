/**
 *
 * File: Run.cpp
 * Author: Jonas Feldt, João Oliveira
 *
 * Version: 2015-02-18
 *
 */

#include "Run.h"
#include <iostream>
#include <stdio.h>
#include <string>



/**
 * Constructor
 *
 * @param maxmoves number of Monte Carlo steps
 * @param stepmax maximum displacement
 * @param temperature simulation temperature
 * @param theta_max maximum rotation angle
 * @param adjust_max_step adjust stepmax every # steps 
 * @param print_every save the geometry/energy every # steps 
 * @param wf_adjust update the wfu every # steps
 * @param v_elast_constrain force constant for the flat bottom potential 
 * @param v_elast_radius_constrain radius for the flat bottom potential 
 */

Run::Run (int maxmoves,
          double stepmax,
          double temperature, 
          double theta_max,
          int adjust_max_step,
          int print_every,
          int wf_adjust,
          double v_elast_constrain,
          double v_elast_radius_constrain,
          int print_energy,
          const std::string grid_input,
          const std::string pipe_input,
          int seed,
          bool has_seed_path,
          std::string seed_path)
:  maxmoves (maxmoves),
   stepmax (stepmax),
   temperature (temperature),
   theta_max (theta_max),
   wf_adjust (wf_adjust),
   v_elast_constrain (v_elast_constrain),
   v_elast_radius_constrain (v_elast_radius_constrain),
   adjust_max_step (adjust_max_step),
   print_every (print_every),
   print_energy (print_energy),
   grid_input (grid_input),
   pipe_input (pipe_input),
   seed (seed),
   has_seed_path (has_seed_path),
   seed_path (seed_path)
{
}




void Run::print () {
   printf (" Maximum number of moves %i\n", maxmoves);
   printf (" Step maximum %6.5f\n", stepmax);
   printf (" Temperature %6.2f\n", temperature);
   printf (" Theta max %6.2f\n", theta_max);
   printf (" Wave function adjust %i steps\n", wf_adjust);
   printf (" Adjust maximum displacement every %i steps\n", adjust_max_step);
   printf (" Print configuration every  %i steps\n", print_every);
   printf (" Print energy every  %i steps\n", print_energy);
   printf (" Flat bottom pot. force constant %6.2f\n", v_elast_constrain);
   printf (" Flat bottom pot. radius %6.2f \n", v_elast_radius_constrain);
}



void Run::fprint (std::string file) {
   FILE *out = fopen (file.c_str (), "a");
   fprintf (out, " Maximum number of moves %i\n", maxmoves);
   fprintf (out, " Step maximum %6.5f\n", stepmax);
   fprintf (out, " Temperature %6.2f\n", temperature);
   fprintf (out, " Theta max %6.2f\n", theta_max);
   fprintf (out, " Wave function adjust %i steps\n", wf_adjust);
   fprintf (out, " Adjust maximum displacement every %i steps\n", adjust_max_step);
   fprintf (out, " Print configuration every  %i steps\n", print_every);
   fprintf (out, " Print energy every  %i steps\n", print_energy);
   fprintf (out, " Flat bottom pot. force constant %6.2f\n", v_elast_constrain);
   fprintf (out, " Flat bottom pot. radius %6.2f \n", v_elast_radius_constrain);


   fclose (out);
}



Run::~Run ()
{
}



void Run::set_seed_path(const std::string path)
{
   seed_path = path;
   has_seed_path = true;
}


