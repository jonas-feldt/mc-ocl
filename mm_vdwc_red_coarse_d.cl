/**
 *
 * File: qmmm_c_red_coarse_d.cl
 * Author: Sebastiao Miranda
 *
 * Version: 2014-02-27
 *
 */
 
 /**
 * This reduction kernel allows tunning
 * the level of coarseness
 * 
 */
#define COULOMB_CONST 332.063806190589 // in Angs.kcal/mol
#define KCAL_TO_KJ 4.183999734824774

#pragma OPENCL EXTENSION cl_khr_fp64: enable

__kernel
void mm_vdwc_red_coarse(

   // Memory buffer local to the work-group
   __local double *local_mem_col,
   __local double *local_mem_vdw,
   
   // True size of the vector we wish to reduce.
   // (The vector comes in padded to be multiple of local_size)
   int true_vect_size,
   
   // Number of values this work-item will reduce
   int m_en_values,
   
   // Variable for collecting work-group energy result
   __global double *mm_energy_vect_vdw,
   __global double *mm_energy_vect_col,
   __global double *mm_energy //Now is vector minors_energy
   
   ){
    
   __private int     local_id          ;
   __private int     global_id         ;
   __private int     local_size        ;
   __private double  p_energy_col =0.0 ;
   __private double  p_energy_vdw =0.0 ;
    
   //Get id's
   local_id    = get_local_id(0);
   global_id   = get_global_id(0);
   local_size  = get_local_size(0);
   
   if(global_id < true_vect_size){
      //Collect previous energy results to private memory
      //and accumulate them.
      for(int i=0; i<m_en_values; i++){
         if(global_id*m_en_values+i<true_vect_size){
         
            p_energy_col+=mm_energy_vect_col[global_id*m_en_values+i];
            p_energy_vdw+=mm_energy_vect_vdw[global_id*m_en_values+i];
            
         }
      }      
      
      //Write energy to local memory
      local_mem_col[local_id] = p_energy_col;
      local_mem_vdw[local_id] = p_energy_vdw;
      
   }else{
      local_mem_col[local_id] = 0;
      local_mem_vdw[local_id] = 0;  
   }
   
   
   barrier(CLK_LOCAL_MEM_FENCE);

   //Now all work-items of the same work group must accumulate their energy.
   for(int offset = local_size/2; offset > 0 ;offset >>= 1){
      if(local_id < offset){
         local_mem_col[local_id] = local_mem_col[local_id + offset] + local_mem_col[local_id];
         local_mem_vdw[local_id] = local_mem_vdw[local_id + offset] + local_mem_vdw[local_id];
      }
      barrier(CLK_LOCAL_MEM_FENCE);
   }

   //Work item with id 0 writes the final result of the work-group
   if (local_id == 0) {
      mm_energy[0] = (local_mem_col[0]*COULOMB_CONST+local_mem_vdw[0]*4)*KCAL_TO_KJ;
   } 
   
}