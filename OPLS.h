/**
 *
 * File: OPLS.h
 * Author: Jonas Feldt, João Oliveira
 *
 * Version: 2015-04-09
 *
 */


#ifndef OPLS_H
#define OPLS_H

#include <vector>
#include <string>
#include "typedefs.h"

class System;
struct Molecule_db;

class OPLS {
public:

   OPLS( const System *system,
         std::vector<int> &atyp_param,
         std::vector<double> &epsilon_param,
         std::vector<double> &sigma_param,
         std::vector<double> &charge_param,
         unsigned int solute_molecules);

   OPLS (const OPLS &orig);

   virtual ~OPLS();

   virtual double energy(DistanceCall GetDistance) const;
   virtual double energy(DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const;

   double energy_mm (DistanceCall GetDistance) const;
   double energy_mm (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const;

   virtual double energy_vdw_qmmm (DistanceCall GetDistance) const;
   virtual double energy_vdw_qmmm (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const;
   virtual double energy_vdw_qmmm (System *mm_system, System *qm_system);

   virtual const double* get_normal_charges() const;

   virtual void set_sigma (const unsigned int index, const double value);

   virtual void set_charge (const unsigned int index, const double value);

   virtual void print (std::string outfile);

   double *epsilon;
   double *sigma;
   std::vector<double> charge;
   
protected:

   virtual double energy_vdw (DistanceCall GetDistance) const;
   virtual double energy_vdw (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const;

   virtual double energy_coulomb (DistanceCall GetDistance) const;
   virtual double energy_coulomb (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const;

   virtual double energy_vdw_mm (DistanceCall GetDistance) const;
   virtual double energy_vdw_mm (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const;

   virtual double energy_coulomb_mm (DistanceCall GetDistance) const;
   virtual double energy_coulomb_mm (DistanceCall dists_new, DistanceCall dists_old, int changed_molecule) const;


   const int natom;
   const std::vector<int> molecule;
   const std::vector<Molecule_db> molecule2atom;
   const System *system;
   const int solute_molecules; // number of Solute molecules in the beginning
   const unsigned int start_solvent;
};

#endif // OPLS_H
