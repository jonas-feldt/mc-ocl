/**
 *
 * File: mm_vdwc_d.cl
 * Author: Sebastiao Miranda
 *
 * Version: 2014-02-26
 *
 */
#pragma OPENCL EXTENSION cl_khr_fp64: enable

#define COULOMB_CONST 332.063806190589 // in Angs.kcal/mol
#define KCAL_TO_KJ 4.183999734824774

inline double compute_minimal_distance (double rxi, double ryi, double rzi, double rxj, double ryj, double rzj) {
   
   double diffx = rxi - rxj;
   double diffy = ryi - ryj;
   double diffz = rzi - rzj;

   //TODO: Checkout if this code is efficient.
   diffx -= convert_int(floor (diffx + 0.5));
   diffy -= convert_int(floor (diffy + 0.5));
   diffz -= convert_int(floor (diffz + 0.5));
   
   double dist = diffx * diffx + diffy * diffy + diffz * diffz;

   return sqrt(dist);
}

__kernel
void mm_vdwc(

   // Local Memory
   __local double *local_mem_vdw,   
   __local double *local_mem_col,   
   
   // kernel scalar arguments
   // (number of atoms for this work-item)
   int m_atoms,
   
   // MM molecule old x,y,z
   __global double   *old_x,
   __global double   *old_y,
   __global double   *old_z,
   __global double   *charges,
   
   // Changed MM molecule
   __global int      *changed_molecule,
   __global int      *changed_molecule_size,
   __global double   *ch_x,
   __global double   *ch_y,
   __global double   *ch_z,
      
   // Global buffer to store result
   __global double *mm_energy_vect_vdw,
   __global double *mm_energy_vect_col,
   
   // System Constants
   // (XXX: Change som to scalar parameter?)
   __constant int *n_total_atoms,
   __constant int *mol2atom0,
   
   //Consts
   __constant double *cutoff_coulomb,
   __constant double *cutoff_coulomb_rec,
   __constant double *cutoff_coulomb_rec_sq,
   __constant double *cutoff_vdw,
   
   __global double *epsilon,
   __global double *sigma
   
)
{   
   __private double  energy_col = 0.0  ;
   __private double  energy_vdw = 0.0  ;
   __private double  dist_old          ; 
   __private double  dist_new          ;
   __private int     global_id         ;
   __private int     local_id          ;
   __private int     local_size        ; 
   __private int     ch_start_atom     ; 
   __private int     mm_start_atom     ; 
   __private int     mm_atom           ; 
   __private int     ch_atom           ; 
   
   local_id   = get_local_id(0);
   global_id  = get_global_id(0);
   local_size = get_local_size(0);
   
   // If this is a valid work-item
   if(global_id*m_atoms + mol2atom0[1] < *n_total_atoms){
   
      // Find Out atoms belonging to the molecule
      ch_start_atom = mol2atom0[*changed_molecule];
   
      // Find Out first atom for this work-item (from total solvent atoms)
      mm_start_atom = global_id*m_atoms + mol2atom0[1];
   
      //For each atom of this work-item
      for(int i=0;i<m_atoms;i++){
      
         mm_atom = mm_start_atom+i;
         
         //Avoid computing energy for atoms of the same molecule
         if (mm_atom<ch_start_atom || mm_atom>= ch_start_atom+*changed_molecule_size){

            // To acess the old coordinates of the changed molecule, the value
            // start_atom_i must be added to index j.
            // (Same for other structures which hold all atoms)
            for(int j=0;j<*changed_molecule_size;j++){        

               ch_atom = ch_start_atom+j;
                           
               dist_old   = compute_minimal_distance(old_x[mm_atom],old_y[mm_atom],old_z[mm_atom],
                                                     old_x[ch_atom],old_y[ch_atom],old_z[ch_atom]);
                            
                      //ch_x,y,z are indexed by j because they just hold data for the changed mol.
               dist_new   = compute_minimal_distance(old_x[mm_atom],old_y[mm_atom],old_z[mm_atom],
                                                      ch_x[j],       ch_y[j],       ch_z[j]  );             
                       
               /* ENERGY Coulomb MM */
               double qs;          
               if (dist_new < *cutoff_coulomb && dist_old < *cutoff_coulomb) {
                  
                  qs = charges[mm_atom] * charges[ch_atom];
                  energy_col += qs * (1.0 / dist_new - 1.0 / dist_old +
                                 *cutoff_coulomb_rec_sq * (dist_new - dist_old));
                                   
               }else if (dist_new < *cutoff_coulomb && dist_old >= *cutoff_coulomb) {
               
                  qs = charges[mm_atom] * charges[ch_atom];
                  energy_col += qs * (1.0 / dist_new - *cutoff_coulomb_rec +
                                 *cutoff_coulomb_rec_sq * (dist_new - *cutoff_coulomb));
                                 
               }else if(dist_old < *cutoff_coulomb){ // dist_new >= cutoff && dist_old < cutoff
                  
                  qs = charges[mm_atom] * charges[ch_atom];
                  energy_col -= qs * (1.0 / dist_old - *cutoff_coulomb_rec +
                                 *cutoff_coulomb_rec_sq * (dist_old - *cutoff_coulomb));
                                 
               }//dist_new > cutoff && dust_old > cutoff --> always 0
                                             
               /* ENERGY Van Der Waals MM*/
                     
               if (dist_new < *cutoff_vdw && dist_old < *cutoff_vdw) {
              
                  double sqrt_epsilon = sqrt (epsilon[mm_atom] * epsilon[ch_atom]);
                  double sigmas = sigma[mm_atom] * sigma[ch_atom];
                  double t = sigmas / (dist_new * dist_new);
                  double V6 = t * t * t;
                  double V12 = V6 * V6;
                  double t_o = sigmas / (dist_old * dist_old);
                  double V6_o = t_o * t_o * t_o;
                  double V12_o = V6_o * V6_o;
                  
                  energy_vdw += sqrt_epsilon * (V12 - V12_o - V6 + V6_o);
                  
               }else if (dist_new < *cutoff_vdw && dist_old >= *cutoff_vdw) {
               
                  double sqrt_epsilon = sqrt (epsilon[mm_atom] * epsilon[ch_atom]);
                  double sigmas = sigma[mm_atom] * sigma[ch_atom];
                  double t = sigmas / (dist_new * dist_new);
                  double V6 = t * t * t;
                  double V12 = V6 * V6;
                  
                  energy_vdw += sqrt_epsilon * (V12 - V6);
                 
               }else if (dist_old < *cutoff_vdw) { // dist_new > cutoff && dist_old < cutoff
                  // subtracts the old value
                  double sqrt_epsilon = sqrt (epsilon[mm_atom] * epsilon[ch_atom]);
                  double sigmas = sigma[mm_atom] * sigma[ch_atom];
                  double t_o = sigmas / (dist_old * dist_old);
                  double V6_o = t_o * t_o * t_o;
                  double V12_o = V6_o * V6_o;
                  energy_vdw -= sqrt_epsilon * (V12_o - V6_o);
               }
               // dist_new > cutoff && dust_old > cutoff --> always 0               
            }
         }
      }   
   }
        
   //Write private result to local memory
   local_mem_vdw[local_id] = energy_vdw;
   local_mem_col[local_id] = energy_col;
   
   barrier(CLK_LOCAL_MEM_FENCE);
   
   /* REDUCTION */
   
   //Now all work-items of the same work group must accumulate their energy.
   for(int offset = local_size/2; offset > 0 ;offset >>= 1){
      if(local_id < offset){
      
         local_mem_vdw[local_id] = local_mem_vdw[local_id + offset] + local_mem_vdw[local_id];
         local_mem_col[local_id] = local_mem_col[local_id + offset] + local_mem_col[local_id];
      
      }
      barrier(CLK_LOCAL_MEM_FENCE);
   }

   //Work item with id 0 writes the final result of the work-group
   if (local_id == 0) {
      mm_energy_vect_vdw[get_group_id(0)] = local_mem_vdw[0];
      mm_energy_vect_col[get_group_id(0)] = local_mem_col[0];
   } 
   
}