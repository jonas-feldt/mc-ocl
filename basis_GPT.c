#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "binom.h"

#define MAX(a,b) (((a)>(b))?(a):(b))
#define MIN(a,b) (!((a)>(b))?(a):(b))

#define THR 1e-10

void basis_GPT_3d(double c[], long int la, long int lb, double PA[], double PB[]) {
 long int n, m, k, i, nm, d1, d2, d3;
 double cx, cy, cz, f;

 long int lab = la + lb;
 d1 = la + 1;
 d2 = d1 * (lb + 1);
 d3 = d2 * (la + lb + 1);
 if (fabs (PA[0]) < THR) {
  if (fabs (PA[1]) < THR) {
   if (fabs (PA[2]) < THR) { /* cocentric */
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       c[n + m * d1 + k * d2         ] = 0.0;
       c[n + m * d1 + k * d2 +     d3] = 0.0;
       c[n + m * d1 + k * d2 + 2 * d3] = 0.0;
      }
      nm = n + m;
      c[n + m * d1 + nm * d2         ] = 1.0;
      c[n + m * d1 + nm * d2 +     d3] = 1.0;
      c[n + m * d1 + nm * d2 + 2 * d3] = 1.0;
     }
    }
   } else { /* coplanar (x,y) */
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       c[n + m * d1 + k * d2     ] = 0.0;
       c[n + m * d1 + k * d2 + d3] = 0.0;
      }
      nm = n + m;
      c[n + m * d1 + nm * d2     ] = 1.0;
      c[n + m * d1 + nm * d2 + d3] = 1.0;
     }
    }
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       cz = 0.0;
       for (i = MAX(0, k - m); i <= MIN(k, n); ++i) {
        f = (double) (get_binom(n, i) * get_binom(m, k - i));
        cz += f * pow (PA[2], n - i) * pow (PB[2], m - k + i);
       }
       c[n + m * d1 + k * d2 + 2 * d3] = cz;
      }
     }
    }
   }
  } else {
   if (fabs (PA[2]) < THR) { /* ! coplanar (x,z) */
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       c[n + m * d1 + k * d2         ] = 0.0;
       c[n + m * d1 + k * d2 + 2 * d3] = 0.0;
      }
      nm = n + m;
      c[n + m * d1 + nm * d2         ] = 1.0;
      c[n + m * d1 + nm * d2 + 2 * d3] = 1.0;
     }
    }
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       cy = 0.0;
       for (i = MAX(0, k - m); i <= MIN(k, n); ++i) {
        f = (double) (get_binom(n, i) * get_binom(m, k - i));
        cy += f * pow (PA[1], n - i) * pow (PB[1], m - k + i);
       }
       c[n + m * d1 + k * d2 + d3] = cy;
      }
     }
    }
   } else {  /* colinear(x) */
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       c[n + m * d1 + k * d2] = 0.0;
      }
      nm = n + m;
      c[n + m * d1 + nm * d2] = 1.0;
     }
    }
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       cy = 0.0;
       cz = 0.0;
       for (i = MAX(0, k - m); i <= MIN(k, n); ++i) {
        f = (double) (get_binom(n, i) * get_binom(m, k - i));
        cy += f * pow (PA[1], n - i) * pow (PB[1], m - k + i);
        cz += f * pow (PA[2], n - i) * pow (PB[2], m - k + i);
       }
       c[n + m * d1 + k * d2 +     d3] = cy;
       c[n + m * d1 + k * d2 + 2 * d3] = cz;
      }
     }
    }
   }
  }
 } else {   /* coplanar (y, z) */
  if (fabs (PA[1]) < THR) {
   if (fabs (PA[2]) < THR) {
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       c[n + m * d1 + k * d2 +     d3] = 0.0;
       c[n + m * d1 + k * d2 + 2 * d3] = 0.0;
      }
      nm = n + m;
      c[n + m * d1 + nm * d2 +     d3] = 1.0;
      c[n + m * d1 + nm * d2 + 2 * d3] = 1.0;
     }
    }
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       cx = 0.0;
       for (i = MAX(0, k - m); i <= MIN(k, n); ++i) {
        f = (double) (get_binom(n, i) * get_binom(m, k - i));
        cx += f * pow (PA[0], n - i) * pow (PB[0], m - k + i);
       }
       c[n + m * d1 + k * d2] = cx;
      }
     }
    }
   } else {     /* colinear (y) */
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       c[n + m * d1 + k * d2 + d3] = 0.0;
      }
      nm = n + m;
      c[n + m * d1 + nm * d2 + d3] = 1.0;
     }
    }
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       cx = 0.0;
       cz = 0.0;
       for (i = MAX(0, k - m); i <= MIN(k, n); ++i) {
        f = (double) (get_binom(n, i) * get_binom(m, k - i));
        cy += f * pow (PA[0], n - i) * pow (PB[0], m - k + i);
        cz += f * pow (PA[2], n - i) * pow (PB[2], m - k + i);
       }
       c[n + m * d1 + k * d2         ] = cx;
       c[n + m * d1 + k * d2 + 2 * d3] = cz;
      }
     }
    }
   }
  } else {    /* colinear (z) */
   if (fabs (PA[2]) < THR) {
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       c[n + m * d1 + k * d2 + 2 * d3] = 0.0;
      }
      nm = n + m;
      c[n + m * d1 + nm * d2 + 2 * d3] = 1.0;
     }
    }
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       cx = 0.0;
       cy = 0.0;
       for (i = MAX(0, k - m); i <= MIN(k, n); ++i) {
        f = (double) (get_binom(n, i) * get_binom(m, k - i));
        cx += f * pow (PA[0], n - i) * pow (PB[0], m - k + i);
        cz += f * pow (PA[1], n - i) * pow (PB[1], m - k + i);
       }
       c[n + m * d1 + k * d2     ] = cx;
       c[n + m * d1 + k * d2 + d3] = cy;
      }
     }
    }
   } else { /* not co-anything */
    for (n = 0; n <= la; ++n) {
     for (m = 0; m <= lb; ++m) {
      for (k = 0; k <= lab; ++k) {
       cx = 0.0;
       cy = 0.0;
       cz = 0.0;
       for (i = MAX(0, k - m); i <= MIN(k, n); ++i) {
        f = (double) (get_binom(n, i) * get_binom(m, k - i));
        cx += f * pow (PA[0], n - i) * pow (PB[0], m - k + i);
        cy += f * pow (PA[1], n - i) * pow (PB[1], m - k + i);
        cz += f * pow (PA[2], n - i) * pow (PB[2], m - k + i);
       }
       c[n + m * d1 + k * d2         ] = cx;
       c[n + m * d1 + k * d2 +     d3] = cy;
       c[n + m * d1 + k * d2 + 2 * d3] = cz;
      }
     }
    }
   }
  }
 }
}



void basis_GPT_contract_vec (
 double z[], long int ioa, long int iob, long int isa, long int isb,
 double v[], /* iop = 0, isp =1 */
 double xa, double ca[], long int lmina, long int lmaxa, long int na,
 double xb, double cb[], long int lminb, long int lmaxb, long int nb,
 double xp, double cp[], long int np, double factor) {

 double *icc;
 double PA[3], PB[3];
 long int d1, d2, d3, ia, la, mxa, mya, mza, ib, lb, mxb, myb, mzb, ip, lp, mxp, myp, mzp;
 int i;
 for (i = 0; i < 3; ++i) {
  PA[i] = cp[i] - ca[i];
  PB[i] = cp[i] - cb[i];
 }

 icc = malloc (3 * (lmaxa + 1) * (lmaxb + 1) * (lmaxa + lmaxb + 1) * sizeof(double));
 basis_GPT_3d (icc, lmaxa, lmaxb, PA, PB);
 d1 = lmaxa + 1;
 d2 = d1 * (lmaxb + 1);
 d3 = d2 * (lmaxa + lmaxb + 1);
 ia = ioa * isa;
 for (la = lmina; la <= lmaxa; ++la) {
  for (mxa = la; mxa > -1; --mxa) {
   for (mya = (la - mxa); mya > -1; --mya) {
    mza = la - mxa - mya;
    ib = iob * isb;

    for (lb = lminb; lb <= lmaxb; ++lb) {
     for (mxb = lb; mxb > -1; --mxb) {
      for (myb = (lb - mxb); myb > -1; --myb) {
       mzb = lb - mxb - myb;
       ip = 0; /*iop * isp = 0*/
       z[ia + ib] = 0.0;

       for (lp = 0; lp <= lmaxa + lmaxb; ++lp) {
        for (mxp = lp; mxp > -1; --mxp) {
         for (myp = (lp - mxp); myp > -1; --myp) {
          mzp = lp - mxp - myp;
          z[ia + ib] += icc[mxa + mxb * d1 + mxp * d2         ]
                      * icc[mya + myb * d1 + myp * d2 +     d3]
                      * icc[mza + mzb * d1 + mzp * d2 + 2 * d3]
                      * factor
                      * v[ip];
          ip++;
         }
        }
       }
       ib += isb;
      }
     }
    }
    ia += isa;
   }
  }
 }
 free (icc);
}


