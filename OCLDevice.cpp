/**
 *
 * File: OCLDevice.cpp
 * Author: Sebastiao Miranda, Jonas Feldt
 *
 * Version: 2015-01-06
 *
 */

#include "OCLDevice.h"
#include <math.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>

#ifdef BASE_PROFILING
   #include <iomanip>
   #include "papi.h"
#endif

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

#ifdef LEGACY_MULTI_DEVICE
#ifdef BALANCING
#include "OCLmanager.h"
#endif
#endif

int OCLDevice::lastDevice = 0;

OCLDevice::OCLDevice(
                        cl::Device device,
                        cl::Context *context,
                        int *ignite_isReady,
                        std::mutex *ignite_mutex,
                        std::condition_variable *ignite_slave,
                        std::condition_variable *ignite_master,
                        #ifdef LEGACY_MULTI_DEVICE
                        //Variables for barrier synchronizaton
                        //, energy bcast TODO: add performance bcast
                        _FPOINT_G_              *a_v_bcast_qmmm_c,
                        _FPOINT_S_              *a_v_bcast_mm,
                        _FPOINT_S_              *a_v_bcast_qmmm_vdw,
                        _FPOINT_S_              *a_v_bcast_qmmm_c_nucl,

                        std::mutex              *a_p_barrier_mutex,
                        std::condition_variable *a_p_barrier_cond,
                        int                     *a_p_barrier_count,
                        int                      a_m_barrier_max,
                        double                   *m_bal,
                        #ifdef BALANCING
                        OCLmanager *m_ocl,
                        #endif
                        #endif
                        int n_pre_steps,
                        int nkernel,
                        int max_chains,
                        System_Periodic* system,
                        const OPLS_Periodic* opls,
                        Run_Periodic* run,
                        _FPOINT_S_ temperature,
                        int num_devices,
                        bool extra_energy_file)
   : m_value_cutoff_coulomb (opls->cutoff_coulomb),
     m_value_cutoff_vdw     (opls->cutoff_vdw),
     has_extra_energy_file      (extra_energy_file)
{
   //Collect arguments to member variables
   m_device           = device;
   m_p_context        = context;
   m_const_system     = system;
   m_opls             = opls;
   m_run              = run;
   m_d_temperature    = temperature;
   m_maxmoves         = m_run->maxmoves;
   m_ndevices         = num_devices;

   m_device_index = lastDevice;
   lastDevice++;

   m_vect_mol2atom0 = new int[m_const_system->nmol_total];

   #ifndef SET_FPOINT_G_DOUBLE
      v_charge_f = new float[m_const_system->natom];
   #endif

   #ifdef LEGACY_MULTI_DEVICE
      //Variables for barrier synchronizaton
      //, energy bcast TODO: add performance bcast
      v_bcast_qmmm_c       =a_v_bcast_qmmm_c       ;
      v_bcast_mm           =a_v_bcast_mm           ;
      v_bcast_qmmm_vdw     =a_v_bcast_qmmm_vdw     ;
      v_bcast_qmmm_c_nucl  =a_v_bcast_qmmm_c_nucl  ;

      p_barrier_mutex =a_p_barrier_mutex;
      p_barrier_cond  =a_p_barrier_cond ;
      p_barrier_count =a_p_barrier_count;
      m_barrier_max   =a_m_barrier_max  ;
      m_barrier_index =  m_device_index ;

      this->m_bal     =  m_bal;

      VERBOSE_LVL_SETUP2("[SETU] Device is "<<m_barrier_index <<"/"<<m_barrier_max<< " in barrier ; m_bal= "<<this->m_bal[m_device_index]<<std::endl);
      #ifdef BASE_PROFILING
         t_mbarrier_acc=0;
         t_mbarrier_end=0;
         t_mbarrier_start=0;
         n_mbarrier=0;

         t_ulock_acc=0;
         t_ulock_end=0;
         t_ulock_start=0;
         n_ulock=0;
      #endif

      #ifdef BALANCING
         this->m_ocl = m_ocl;

         #ifdef BALANCING_PROBE

            m_tulock_probe = new double[m_maxmoves];
            m_tpmc_probe   = new double[m_maxmoves];
            m_prev_offset  = new double[m_maxmoves];

         #endif
      #endif

   #endif

   m_d_temperature   = m_run->temperature;
   m_theta_max       = m_run->theta_max;
   m_wf_adjust       = m_run->wf_adjust;
   m_n_pre_steps     = n_pre_steps;

   // For the case of a pure Standalone run
   // disable wfus
   #ifndef WILL_WF_ADJUST
      m_wf_adjust = 0;
      VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index <<": Pure standalone run. Disabled wfu, set wf_adjust to 0"<<std::endl);
   #endif

   if(m_wf_adjust!=0){
      m_outer_steps = m_maxmoves/m_wf_adjust;
      m_inner_steps = m_wf_adjust;
   }else{
      m_outer_steps = 1;
      m_inner_steps = m_maxmoves;
   }

   //Init Profiling vars
   #ifdef BASE_PROFILING
     t_launch_pmc_acc=0;
     t_launch_pmc_end=0;
     t_launch_pmc_start=0;
     n_launch_pmc=0;
     t_save_acc=0;
     t_save_end=0;
     t_save_start=0;
     n_save=0;
     t_lastsave_acc=0;
     t_lastsave_end=0;
     t_lastsave_start=0;
     n_lastsave=0;
     t_update_acc=0;
     t_update_end=0;
     t_update_start=0;
     n_update=0;
     t_get_ref=0;
     t_get_ref_end=0;
     t_get_ref_start=0;
     n_get_ref=0;
     t_wf_adjust=0;
     t_wf_adjust_end=0;
     t_wf_adjust_start=0;
     n_wf_adjust=0;
     t_stepmax=0;
     t_stepmax_end=0;
     t_stepmax_start=0;
     n_stepmax=0;
     t_outer=0;
     t_outer_end=0;
     t_outer_start=0;
     n_outer=0;

     t_pmc_iter_acc=0;
     t_pmc_iter_end=0;
     t_pmc_iter_start=0;
     n_pmc_iter=0;
     // Allocate profiling for PMC CYCLE profile.
     // (XXX: When changing the number of chains per device
     //  take care with the size of this vectors)
     t_pmc_chunk       = new double*[m_outer_steps];
     t_pmc_chunk_end   = new double*[m_outer_steps];
     t_pmc_chunk_start = new double*[m_outer_steps];
     n_pmc_chunk       = new int   *[m_outer_steps];
     for(int i=0; i<m_outer_steps;i++){
        t_pmc_chunk[i]       = new double[max_chains];
        t_pmc_chunk_end[i]   = new double[max_chains];
        t_pmc_chunk_start[i] = new double[max_chains];
        n_pmc_chunk[i]       = new int   [max_chains];
        for(int j=0; j<max_chains; j++){
           t_pmc_chunk[i][j]=0;
           t_pmc_chunk_end[i][j]=0;
           t_pmc_chunk_start[i][j]=0;
           n_pmc_chunk[i][j]=0;
        }
     }
   #endif

   //XXX: Check if still used
   m_nchains      = 0;
   m_n_ocl_prof   = 0;

   //Point to master-slave sync variables
   m_ignite_slave    = ignite_slave    ;
   m_ignite_master   = ignite_master   ;
   m_ignite_isReady  = ignite_isReady  ;
   m_ignite_mutex    = ignite_mutex    ;

   //Allocate chain sync variables
   v_wf_flag_launch   = new int[max_chains];
   v_wf_mutex_launch  = new std::mutex[max_chains];
   v_wf_slave_launch  = new std::condition_variable[max_chains];

   v_wf_flag_finish   = new int[max_chains];
   v_wf_mutex_finish  = new std::mutex[max_chains];
   v_wf_master_finish = new std::condition_variable[max_chains];

   for (int i=0; i<max_chains; i++){
      v_wf_flag_launch[i]=0;
      v_wf_flag_finish[i]=0;
   }

   //Allocate event vectors
   // allocate m_inner_steps or max_inner_steps arrays
   allocate_events = m_inner_steps;
   if (std::ceil (m_inner_steps / m_run->max_inner_steps) > 1) {
      allocate_events = m_run->max_inner_steps;
   }
   m_EVENT_monte_carlo        = new cl::Event[allocate_events];
   m_EVENT_qmmm_c             = new cl::Event[allocate_events];
   m_EVENT_qmmm_c_red_coarse  = new cl::Event[allocate_events];
   m_EVENT_mm_vdwc            = new cl::Event[allocate_events];
   m_EVENT_mm_vdwc_red_coarse = new cl::Event[allocate_events];
   m_EVENT_qmmm_tasks         = new cl::Event[allocate_events];
   m_EVENT_closure            = new cl::Event[allocate_events];

      // Init profiling
   m_PROFL_monte_carlo        = 0;
   m_PROFL_qmmm_c             = 0;
   m_PROFL_qmmm_c_red_coarse  = 0;
   m_PROFL_mm_vdwc            = 0;
   m_PROFL_mm_vdwc_red_coarse = 0;
   m_PROFL_qmmm_tasks         = 0;
   m_PROFL_closure            = 0;
   m_PROFL_pmc_cycle          = 0;

   #ifdef LEGACY_MULTI_DEVICE //barrier profiling vars
      m_PROFL_write_part_qmmm_c      =0;
      m_PROFL_read_part_qmmm_c       =0;
      m_PROFL_write_part_minors      =0;
      m_PROFL_read_part_minors       =0;
      m_PROFL_barrier_comn           =0;
      m_EVENT_write_part_qmmm_c = new cl::Event[allocate_events];
      m_EVENT_read_part_qmmm_c  = new cl::Event[allocate_events];
      m_EVENT_write_part_minors = new cl::Event[allocate_events];
      m_EVENT_read_part_minors  = new cl::Event[allocate_events];

   #endif

   createQueue();

}
OCLDevice::~OCLDevice(){

   // tell chains to finish, they are deleted in OCLmanager
   for(int chain = 0; chain < m_nchains; chain++) {
      std::unique_lock<std::mutex> ulock(v_wf_mutex_launch[chain]);
         v_wf_flag_launch[chain] = -1;
         v_wf_slave_launch[chain].notify_all();
      ulock.unlock();
      m_chains[chain]->join_thread();
   }

   delete   m_KERNEL_monte_carlo         ;
   delete   m_KERNEL_qmmm_c              ;
   delete   m_KERNEL_qmmm_c_red_coarse   ;
   delete   m_KERNEL_mm_vdwc             ;
   delete   m_KERNEL_mm_vdwc_red_coarse  ;
   delete   m_KERNEL_qmmm_tasks          ;
   delete   m_KERNEL_closure             ;
   delete[] m_EVENT_monte_carlo          ;
   delete[] m_EVENT_qmmm_c               ;
   delete[] m_EVENT_qmmm_c_red_coarse    ;
   delete[] m_EVENT_mm_vdwc              ;
   delete[] m_EVENT_mm_vdwc_red_coarse   ;
   delete[] m_EVENT_qmmm_tasks           ;
   delete[] m_EVENT_closure              ;

   #ifdef LEGACY_MULTI_DEVICE
      delete[] m_EVENT_write_part_qmmm_c;
      delete[] m_EVENT_read_part_qmmm_c ;
      delete[] m_EVENT_write_part_minors;
      delete[] m_EVENT_read_part_minors ;
   #endif

   delete[] m_vect_mol2atom0   ;
   delete[] v_wf_flag_launch   ;
   delete[] v_wf_mutex_launch  ;
   delete[] v_wf_slave_launch  ;
   delete[] v_wf_flag_finish   ;
   delete[] v_wf_mutex_finish  ;
   delete[] v_wf_master_finish ;
   delete m_queue;

   #ifdef BALANCING

      #ifdef BALANCING_PROBE

         delete m_tulock_probe;
         delete m_tpmc_probe  ;
         delete m_prev_offset ;

      #endif
   #endif

   #ifndef SET_FPOINT_G_DOUBLE
      delete[] v_charge_f;
   #endif

   #ifdef BASE_PROFILING
   for(int i = 0; i < m_outer_steps; i++){
      delete[] t_pmc_chunk       [i];
      delete[] t_pmc_chunk_end   [i];
      delete[] t_pmc_chunk_start [i];
      delete[] n_pmc_chunk       [i];
   }
   delete[] t_pmc_chunk      ;
   delete[] t_pmc_chunk_end  ;
   delete[] t_pmc_chunk_start;
   delete[] n_pmc_chunk      ;
   #endif

}

void OCLDevice::launch_thread(){
    m_thread = std::thread(&OCLDevice::pmc_loop, this);
}

void OCLDevice::join_thread(){
    m_thread.join();
}

void OCLDevice::pmc_loop()
{
   int last_memory_offset = 0;
   std::vector<int> unserviced; //unserviced chain tracking

   // First Ignition. Wait for master thread order
   //
   std::unique_lock<std::mutex> ulock(*m_ignite_mutex);
      while(*m_ignite_isReady == 0){
         VERBOSE_LVL_SYNC("[SYNC] Device "<<m_device_index<< "is waiting for OCLmanager ..." << std::endl);
         m_ignite_slave->wait(ulock);
      }
      *m_ignite_isReady = 0;
   ulock.unlock();

   // Device outer profiling
   PROFILE_FIRST(0,t_outer_start);

   // Outer Cycles
   //
   for (int outer_step = 0; outer_step < m_outer_steps ; outer_step++){

      // Launch 1 PMC_CYLCE (with <m_wf_adjust> iterations) for each chain
      //
      //for (int chain = 0; chain < m_nchains; chain++){

      unserviced.clear();
      for(int c=0;c<m_nchains;c++) unserviced.push_back(c);

      while(unserviced.empty()==false){

         //get next chain
         int chain = unserviced.front();
         unserviced.erase(unserviced.begin());

         // Wait for wf updates (wait for molpro)
         // (except in the first step)

         if(outer_step!=0){
            if(check_wf_update(chain)==true){
               // XXX how does this with last_memory_offset even work
               // ... shouldn't this be chain specific???
               update_device(chain, last_memory_offset); 
            }else{
               unserviced.push_back(chain);//put chain at end of list
               continue;
            }
         }

         PROFILE_FIRST(0,t_pmc_chunk_start[outer_step][chain]);

         MICRO_PROBE(v_micro_probe);

         // Set kernel arguments for this chain
         setChainArgs(chain);
         unsigned int memory_offset = 0;
         for (int inner_step = 0; inner_step < m_inner_steps; inner_step++){

            pmc_core(chain, inner_step, outer_step, memory_offset);
            #ifdef BALANCING_PROBE
               if (inner_step%BALANCING_PROBE == 0){
                  m_tpmc_probe[inner_step/BALANCING_PROBE] = t_pmc_iter_end-t_pmc_iter_start;
               }
            #endif

            // inner_steps are done in chunks if m_inner_steps is very large,
            // this computes the index with regard to the chunk, blocks when
            // one chunk is queued and waits until the last inner_step of that
            // chunk is done
            // XXX this might mess up the multi-chain on one device process but since
            // the pmc_adjustments is also blocking I might get away with it
            last_memory_offset = memory_offset;
            memory_offset++;
            if (memory_offset == m_run->max_inner_steps) {
               m_EVENT_closure[memory_offset - 1].wait();
               memory_offset = 0;
            }
         }

         MICRO_PROBE(v_micro_probe);

         // Ajust wf (ask for molpro) and random lists and stepmax
         // Currently, pmc_adjustments is blocking, so after this,
         // the pmc_core cycles have finished in the gpu for sure.
         pmc_adjustments(chain, last_memory_offset, outer_step);

         PROFILE (0,t_pmc_chunk[outer_step][chain],t_pmc_chunk_end[outer_step][chain],
                    t_pmc_chunk_start[outer_step][chain],n_pmc_chunk[outer_step][chain]);

      }
      VERBOSE_LVL_ALIVE(std::endl);fflush(stdout);

      // Update profiling
      // XXX: This is just using info from the last run chain.
      #ifdef GPU_PROFILING
         m_queue->finish();//XXX: non blocking gpus need this
         VERBOSE_LVL_SYNC("[SYNC] Device "<<m_device_index<<" will update gpu profiler"<<std::endl);
         update_profiler(m_inner_steps);
      #endif

      // Report profiling status to Device thread
      // TODO
      //
      // Recieve instructions from Device thread respecting chains
      // TODO
      //
   } // end loop outer step

   VERBOSE_LVL_ALIVE("Ended PMC CYCLE."<<std::endl);fflush(stdout);

   //Outer device profiling
   PROFILE (0,t_outer,t_outer_end,t_outer_start,n_outer);

   #ifdef BASE_PROFILING
      print_profiling();
   #endif

}
void OCLDevice::pmc_core(int chain, int inner_step, int outer_step, int memory_offset)
{

   int global_step = outer_step * m_inner_steps + inner_step;

   int n_save_energies = 0;
   if (m_run->print_energy != 0) n_save_energies = m_run->print_every / m_run->print_energy * m_run->n_save_systems;

   // Main PMC Cycle
   try{

      //  Launch PMC Cycle Step
      launch_pmc_cycle(inner_step, outer_step, memory_offset);

      //  Reads Accepted Systems back (Sometimes)
      bool reads_back = true;
      #ifdef LEGACY_MULTI_DEVICE
         if (m_device_index != 0) reads_back = false;
      #endif
      if (reads_back) {
         if (       global_step >= m_run->print_every
               and (global_step%m_run->print_every == 0)
               and (global_step / m_run->print_every + 1)%m_run->n_save_systems == 0)
         {
            m_chains[chain]->save_acc_systems(memory_offset, m_run->n_save_systems, n_save_energies);
         }
      }
   } catch (cl::Error error){//openCL error catching
      std::cout<<"OCLDevice::pmc_core:: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }

}

void OCLDevice::pmc_adjustments(int chain, int memory_offset, int outer_step){

   int global_step = (outer_step+1)*m_inner_steps;
   int n_save_energies = 0;
   if (m_run->print_energy != 0) n_save_energies = m_run->print_every / m_run->print_energy * ((global_step/m_run->print_every)%m_run->n_save_systems);

   try{

      // If this is the last outer_iteration, then read back all systems
      if(global_step==m_maxmoves){
         VERBOSE_LVL_TRANSFERS("[TRAN] Device "<<m_device_index<< " last step for chain "<<chain<<"(dev's)"<<" Remaing systems? (if !0):"
                              <<((global_step/m_run->print_every)+1)%m_run->n_save_systems<<std::endl);

         bool flush_output = true;
         #ifdef LEGACY_MULTI_DEVICE
            if(m_device_index!=0) flush_output = false;
         #endif
         if (flush_output){
            if(((global_step/m_run->print_every)+1)%m_run->n_save_systems !=0){
               PROFILE_FIRST(global_step,t_lastsave_start);
               m_chains[chain]->save_acc_systems(memory_offset,
                              ((global_step/m_run->print_every))%m_run->n_save_systems,
                              n_save_energies);
               PROFILE (global_step,t_lastsave_acc,t_lastsave_end,t_lastsave_start, n_lastsave);
            }
         }
      }else{// If next iteration is not the last iteration

         // Current reference system lattice x,y,z will be
         // retrieved from GPU and saved in m_chains[chain]->m_system;
         VERBOSE_LVL_TRANSFERS(std::endl<<"[TRAN] Device "<<m_device_index<<" will request wfu for chain "<<chain<<std::endl);
         PROFILE_FIRST(global_step,t_get_ref_start);
         m_chains[chain]->get_reference_system(memory_offset);
         PROFILE(global_step,t_get_ref,t_get_ref_end,t_get_ref_start,n_get_ref);

         // Ask for wf update
         // (this sends the request to the chain thread)
         #ifdef WILL_WF_ADJUST
            launch_wf_update(chain);
         #endif

         // Dev note: the bellow conditionals were happening at 'global_step+1'%...
         // changed to just 'global step'


         ////  Adjust Stepmax (Sometimes)
         //
         if (m_run->adjust_max_step != 0 &&
                 ((global_step) % m_run->adjust_max_step) == 0) {

            //m_queue->finish(); REMOVE LINE
            PROFILE_FIRST(global_step,t_stepmax_start);
            int number_accepted = m_chains[chain]->get_current_number_accepted(memory_offset);
            m_chains[chain]->m_stepmax = adjust_stepmax (global_step, number_accepted, m_chains[chain]->m_stepmax, m_run->acceptance_ratio);
            PROFILE (global_step,t_stepmax,t_stepmax_end,t_stepmax_start, n_stepmax);
            VERBOSE_LVL_TRANSFERS("[TRAN] STEPMAX: Device "<<m_device_index<<" stepmax ajusted for chain "<<chain<<"(dev's) to "<< m_chains[chain]->m_stepmax << std::endl);
         }

         ////  Update random lists (Sometimes)
         //
         if (global_step!=0 and ((global_step)%m_n_pre_steps)==0){
            VERBOSE_LVL_TRANSFERS("[RND_] (TRAN) Device "<<m_device_index<<" update rndlst for chain "<<chain<<"(dev's)  at step: "<< global_step << std::endl);
            PROFILE_FIRST(global_step,t_update_start);
            m_chains[chain]->update_lists(m_theta_max, memory_offset);
            PROFILE (global_step,t_update_acc,t_update_end,t_update_start,n_update);
         }
      }

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLDevice::pmc_core:: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
}
void OCLDevice::launch_wf_update(int chain){

   std::unique_lock<std::mutex> ulock(v_wf_mutex_launch[chain]);
      v_wf_flag_launch[chain] = 1;
      v_wf_slave_launch[chain].notify_all();
   ulock.unlock();
}

void OCLDevice::wait_wf_update(int chain){

   std::unique_lock<std::mutex> ulock(v_wf_mutex_finish[chain]);
      while(v_wf_flag_finish[chain] == 0){
         VERBOSE_LVL_SYNC("[SYNC] Device "<<m_device_index<<" waiting for wfu (device's chain " << chain <<")" << std::endl);
         v_wf_master_finish[chain].wait(ulock);
      }
      VERBOSE_LVL_SYNC("[SYNC] Device "<<m_device_index<<" released from wfu wait (device's chain " << chain <<")" << std::endl);
      v_wf_flag_finish[chain] = 0;
   ulock.unlock();

}

// unblocking check (alternative to wait)
bool OCLDevice::check_wf_update(int chain){

   std::unique_lock<std::mutex> ulock(v_wf_mutex_finish[chain]);
      if(v_wf_flag_finish[chain] == 0){
         ulock.unlock();
         return false;
      }
      VERBOSE_LVL_SYNC("[SYNC] Device "<<m_device_index<<" released from wfu wait (device's chain " << chain <<")" << std::endl);
      v_wf_flag_finish[chain] = 0;

   ulock.unlock();
   return true;
}
void OCLDevice::update_device(int chain, int memory_offset){

   // XXX I moved this into m_chains[chain]->write_grid(inner_step)
   // that allows to use this function also for FEP_OCLChain.
   //m_chains[chain]->m_grid->reduced_coords(m_run->dimension_box,
   //                                        m_run->cutoff_coulomb);

   // Update the energy of the current reference
   // This call Reads Ref energies,
   // updates old and re-sends to GPU udpated (XXX: Better way?)
   m_chains[chain]->update_reference_energy(memory_offset);

   // Write grid to GPU
   m_chains[chain]->write_grid(memory_offset, true, m_run->dimension_box,
         m_run->cutoff_coulomb);

}
void OCLDevice::add_chain(OCLChain *oclchain){
   m_nchains++;
   m_chains.push_back(oclchain);
}

void OCLDevice::prepare_randomness(){

   for (int i=0; i<m_nchains; ++i)
      m_chains[i]->prepare_randomness(m_theta_max);

   return;
}

// Builds structure that gives starting atom for each molecule
void OCLDevice::build_mol2atom0(){

   int curr_atom=0;

   // Start with QM molecule
   curr_atom = 0;
   m_vect_mol2atom0[0] = curr_atom; //First Atom of QM molecule is atom 0

   for(int mol = 1 ; mol < m_const_system->nmol_total ; mol++ ){

      curr_atom = curr_atom + m_const_system->molecule2atom[mol-1].natoms;

      m_vect_mol2atom0[mol] = curr_atom;
   }

}

void OCLDevice::setup_pmc(std::string kernel_file_closure){

   // Build the mol2atom0 structure
   build_mol2atom0();

   // Setup Kernels
   //
   // TODO:
   //       > Add counters to count Global/Const/Local memory usage

   try{

   m_KERNEL_monte_carlo         = new OCLKernel("monte_carlo",    //Kernel name (must match function in .cl)
                                                KERNEL_FILE_MC,   //File name
                                                this);            //ptr to OCLDevice

   m_KERNEL_qmmm_c              = new OCLKernel("qmmm_c",
                                                KERNEL_FILE_QMMM_C,
                                                this);

   m_KERNEL_qmmm_c_red_coarse   = new OCLKernel("qmmm_c_red_coarse",
                                                KERNEL_FILE_QMMM_C_RED_COARSE,
                                                this);

   m_KERNEL_mm_vdwc             = new OCLKernel("mm_vdwc",
                                                KERNEL_FILE_MM_VDWC,
                                                this);

   m_KERNEL_mm_vdwc_red_coarse  = new OCLKernel("mm_vdwc_red_coarse",
                                                KERNEL_FILE_MM_VDWC_RED_COARSE,
                                                this);

   m_KERNEL_qmmm_tasks          = new OCLKernel("qmmm_tasks",
                                                KERNEL_FILE_QMMM_TASKS,
                                                this);

   m_KERNEL_closure             = new OCLKernel("closure",
                                                kernel_file_closure,
                                                this);
   // Compile Kernels
   //
   //

   m_KERNEL_monte_carlo->make_kernel(std::string(""));//("-cl-nv-verbose"));

   #ifdef HALF_FIXED
      m_KERNEL_qmmm_c->make_kernel(
        std::string("-D N=")+std::to_string((long long int)Q_DISTS)
      + std::string(" -D A_N=")+std::to_string((long long int)A_N)
      + std::string(" -D N_05=")+std::to_string((long long int)N_05)
      + std::string(" -D N_1=")+std::to_string((long long int)N_1)
      );//("-cl-nv-verbose"));
   #else
      m_KERNEL_qmmm_c->make_kernel(std::string(""));//("-cl-nv-verbose"));
   #endif

   m_KERNEL_qmmm_c_red_coarse ->make_kernel(std::string(""));//("-cl-nv-verbose"));
   m_KERNEL_mm_vdwc           ->make_kernel(std::string(""));//("-cl-nv-verbose"));
   m_KERNEL_mm_vdwc_red_coarse->make_kernel(std::string(""));//("-cl-nv-verbose"));
   m_KERNEL_qmmm_tasks        ->make_kernel(std::string(""));//("-cl-nv-verbose"));

   // flags for closure
   std::string flags = "-D COLLECT_EVERY=" + std::to_string(m_run->print_every) + " -D N_SAVE_SYSTEMS=" + std::to_string(m_run->n_save_systems);
   #ifdef LEGACY_MULTI_DEVICE
      if(m_device_index>0){//Slave devices dont save steps
         flags += " -D AVOID_SAVING";
      }
   #endif
   if (has_extra_energy_file) {
      flags += " -D EXTRA_ENERGY -D N_SAVE_ENERGY=" +
         std::to_string(m_run->print_every / m_run->print_energy * m_run->n_save_systems) +
         " -D COLLECT_EVERY_ENERGY=" + std::to_string(m_run->print_energy);
   }

   m_KERNEL_closure           ->make_kernel(flags);

   // Setup Kernel Ranges
   //
   //
   // XXX: Some of the values here are substituted later, before the kernel calls...
   // XXX: m_chains[0] is being used because this is just the first setRanges call, doesn't matter
   m_KERNEL_monte_carlo->setRanges(1,1);
   m_KERNEL_mm_vdwc    ->setRanges(m_chains[0]->m_system->natom - m_chains[0]->m_system->molecule2atom[0].natoms, m_KERNEL_qmmm_c->get_pref_wg_mult_1d());
   m_KERNEL_qmmm_tasks ->setRanges(TASKS_N_THREADS,TASKS_N_THREADS/2);
   m_KERNEL_closure    ->setRanges(1,1);

   // Decide local for mem_alloc, but global is dummy
   m_KERNEL_qmmm_c_red_coarse->setRanges(0,WGSIZE_MAX_COARSE_RED_QMMM_C);
   m_KERNEL_mm_vdwc_red_coarse->setRanges(0,WGSIZE_MAX_COARSE_RED_MM_VDWC);


   // The Ranges for the following kernels are changed during execution:
   // m_KERNEL_qmmm_c
   // m_KERNEL_mm_vdwc_red_coarse
   // m_KERNEL_qmmm_c_red_coarse

   // Setup OpenCL Buffers and other parameters
   // For Each Chain
   //

   for(int i=0; i< m_nchains; ++i){
      m_chains[i]->m_qmmm_c_grain = QMMM_C_GRAIN; // first grain for qmmm_c
      #ifndef USE_HARD_WGSIZE_QMMM_C
         m_chains[i]->m_qmmm_c_localsize = m_KERNEL_qmmm_c->get_pref_wg_mult_1d(); // first local size
      #else
         m_chains[i]->m_qmmm_c_localsize = USE_HARD_WGSIZE_QMMM_C; // first local size
      #endif

      #ifdef LEGACY_MULTI_DEVICE
         if(i==0){

            // just to allow knowing localmemsize in local mem alloc
            m_KERNEL_qmmm_c->setRanges(
                               static_cast<int>((m_chains[0]->m_grid->dim/m_chains[0]->m_qmmm_c_grain)*m_bal[m_device_index])
                               , m_chains[0]->m_qmmm_c_localsize);
         }
      #else
         if(i==0){
            // just to allow knowing localmemsize in local mem alloc
            m_KERNEL_qmmm_c->setRanges((m_chains[0]->m_grid->dim/m_chains[0]->m_qmmm_c_grain)
                               , m_chains[0]->m_qmmm_c_localsize);
         }
      #endif

      m_chains[i]->setup_buffers();

   }

   // Setup OpenCL Buffers
   // For All Chains (Shared)
   //

   //atom charges
   m_mass =                   cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Charges are read-only
                                  (m_const_system->natom)*sizeof(_FPOINT_S_),
                                  m_const_system->atom_mass);
                                  //(CL_MEM_COPY_HOST_PTR will copy data to the GPU in this call)

   //ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   //atom charges
   m_atomic_numbers =         cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Charges are read-only
                                  (m_const_system->natom)*sizeof(int),
                                  m_const_system->atomic_numbers);
                                  //(CL_MEM_COPY_HOST_PTR will copy data to the GPU in this call)

   //ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   //atom charges
   m_charge =                 cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Charges are read-only
                                  (m_const_system->natom)*sizeof(_FPOINT_S_),
                                  const_cast<double*>(m_opls->charge.data()));
                                  //(CL_MEM_COPY_HOST_PTR will copy data to the GPU in this call)
   #ifndef SET_FPOINT_G_DOUBLE

   for(unsigned int a=0; a < m_const_system->natom; a++)
      v_charge_f[a] = (float) m_opls->charge[a];

   m_charge_f =                 cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Charges are read-only
                                  (m_const_system->natom)*sizeof(_FPOINT_G_),
                                  v_charge_f);
   #endif

   //ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   // epsilon and sigma vectors
   m_epsilon =                cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  (m_const_system->natom)*sizeof(_FPOINT_S_),
                                  m_opls->epsilon);

   //ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   m_sigma =                  cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  (m_const_system->natom)*sizeof(_FPOINT_S_),
                                  m_opls->sigma);

   //ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   /**** CONSTANT MEMORY ****/
   /*************************/
   /*************************/

   int ncbytes=0;
   //atom charges
   m_mol2atom0 =              cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Charges are read-only
                                  (m_const_system->nmol_total)*sizeof(int),
                                  m_vect_mol2atom0);
                                  //(CL_MEM_COPY_HOST_PTR will copy data to the GPU in this call)

   ncbytes+=(m_const_system->nmol_total)*sizeof(int);


   //atom to molecule vector
   m_molecule=                cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Molecule info is read-only
                                  (m_const_system->natom)*sizeof(int),
                                  m_const_system->molecule);
                                  //(CL_MEM_COPY_HOST_PTR will copy data to the GPU in this call)

   ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   //number of total atoms (TODO: Check if it would be faster to send as a scalar argument)
   m_n_total_atoms =          cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(unsigned int), // Just one number
                                  const_cast<unsigned int*>(&m_const_system->natom));

   ncbytes+=sizeof(int);

   //number of total atoms (TODO: Check if it would be faster to send as a scalar argument)
   m_n_total_mols =           cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(int), // Just one number
                                  &m_const_system->nmol_total);

   ncbytes+=sizeof(int);

   //Cutoffs

   m_cutoff_coulomb_rec =     cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_), // Just one number
                                  &m_chains[0]->m_grid->cutoff_coulomb_rec);
   ncbytes+=sizeof(_FPOINT_S_);

   m_cutoff_coulomb_rec_sq =  cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_), // Just one number
                                  &m_chains[0]->m_grid->cutoff_coulomb_rec_sq);
   ncbytes+=sizeof(_FPOINT_S_);


   m_cutoff_coulomb =         cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_), // Just one number
                                  &m_value_cutoff_coulomb);
   ncbytes+=sizeof(_FPOINT_S_);

   	//float versions for coulomb cutoffs
   #ifndef SET_FPOINT_G_DOUBLE
      #ifdef HALF_FIXED
         #ifdef LIGHT_FIXED
            // Just cuttoff sq in fixed
            cutoff_coulomb_rec_f    = (float) m_chains[0]->m_grid->cutoff_coulomb_rec;
            cutoff_coulomb_rec_sq_f = (float) m_chains[0]->m_grid->cutoff_coulomb_rec_sq;
            cutoff_coulomb_f        = (float) m_opls->cutoff_coulomb;
            cutoff_coulomb_sq_f     = (float) m_opls->cutoff_coulomb*m_opls->cutoff_coulomb;

            cutoff_coulomb_sq_q     = FLOAT2FIXED( cutoff_coulomb_sq_f     );

            m_cutoff_coulomb_rec_q =    cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FPOINT_G_), // Just one number
                                        &cutoff_coulomb_rec_f);
            ncbytes+=sizeof(_FPOINT_G_);

            m_cutoff_coulomb_rec_sq_q = cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FPOINT_G_), // Just one number
                                        &cutoff_coulomb_rec_sq_f);
            ncbytes+=sizeof(_FPOINT_G_);

            m_cutoff_coulomb_q =        cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FPOINT_G_), // Just one number
                                        &cutoff_coulomb_f);
            ncbytes+=sizeof(_FPOINT_G_);

            m_cutoff_coulomb_sq_q =     cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FIXED_), // Just one number
                                        &cutoff_coulomb_sq_q);
            ncbytes+=sizeof(_FIXED_);

         #else

            // Cutoffs in fixed-point ; Also, they are squared

            cutoff_coulomb_rec_f    = (float) m_chains[0]->m_grid->cutoff_coulomb_rec;
            cutoff_coulomb_rec_sq_f = (float) m_chains[0]->m_grid->cutoff_coulomb_rec_sq;
            cutoff_coulomb_f        = (float) m_opls->cutoff_coulomb;
            cutoff_coulomb_sq_f     = (float) m_opls->cutoff_coulomb*m_opls->cutoff_coulomb;

            cutoff_coulomb_rec_q    = FLOAT2FIXED( cutoff_coulomb_rec_f    );
            cutoff_coulomb_rec_sq_q = FLOAT2FIXED( cutoff_coulomb_rec_sq_f );
            cutoff_coulomb_q        = FLOAT2FIXED( cutoff_coulomb_f        );
            cutoff_coulomb_sq_q     = FLOAT2FIXED( cutoff_coulomb_sq_f     );

            m_cutoff_coulomb_rec_q =    cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FIXED_), // Just one number
                                        &cutoff_coulomb_rec_q);
            ncbytes+=sizeof(_FIXED_);

            m_cutoff_coulomb_rec_sq_q = cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FIXED_), // Just one number
                                        &cutoff_coulomb_rec_sq_q);
            ncbytes+=sizeof(_FIXED_);

            m_cutoff_coulomb_q =        cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FIXED_), // Just one number
                                        &cutoff_coulomb_q);
            ncbytes+=sizeof(_FIXED_);

            m_cutoff_coulomb_sq_q =     cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FIXED_), // Just one number
                                        &cutoff_coulomb_sq_q);
            ncbytes+=sizeof(_FIXED_);
         #endif

      #else
         cutoff_coulomb_rec_f    = (float) m_chains[0]->m_grid->cutoff_coulomb_rec;
         cutoff_coulomb_rec_sq_f = (float) m_chains[0]->m_grid->cutoff_coulomb_rec_sq;
         cutoff_coulomb_f        = (float) m_opls->cutoff_coulomb;

         m_cutoff_coulomb_rec_f =    cl::Buffer(*(m_p_context),
                                     CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                     sizeof(_FPOINT_G_), // Just one number
                                     &cutoff_coulomb_rec_f);
         ncbytes+=sizeof(_FPOINT_G_);

         m_cutoff_coulomb_rec_sq_f = cl::Buffer(*(m_p_context),
                                     CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                     sizeof(_FPOINT_G_), // Just one number
                                     &cutoff_coulomb_rec_sq_f);
         ncbytes+=sizeof(_FPOINT_G_);

         m_cutoff_coulomb_f =        cl::Buffer(*(m_p_context),
                                     CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                     sizeof(_FPOINT_G_), // Just one number
                                     &cutoff_coulomb_f);
         ncbytes+=sizeof(_FPOINT_G_);
      #endif
   #endif
      //

   m_cutoff_vdw =             cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_), // Just one number
                                  &m_value_cutoff_vdw);

   ncbytes+=sizeof(_FPOINT_S_);


   m_temperature=             cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_),
                                  &m_d_temperature);

   ncbytes+=sizeof(int);

   m_update_lists_every=      cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(int),
                                  &m_n_pre_steps);

   ncbytes+=sizeof(int);

   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<<" Constant Mem Requested (bytes):" << ncbytes << std::endl);

   // INFO: Local memory is allocated via the setArg Function of each kernel

   // Match Kernel Arguments to buffers.
   // (Arguments not shared between chains are set via ::setChainArgs)
   // Scalar arguments are sent
   // With the Kernel launch call

   // Monte Carlo Kernel - m_KERNEL_monte_carlo
   ////////////////////////////////////////////////////////////////////////
   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<< " is setting up Shared m_KERNEL_monte_carlo arguments..."<<std::endl);

   m_KERNEL_monte_carlo->m_kernel.setArg(19,m_n_total_mols);
   m_KERNEL_monte_carlo->m_kernel.setArg(20,m_n_total_atoms);
   m_KERNEL_monte_carlo->m_kernel.setArg(21,m_mass);
   m_KERNEL_monte_carlo->m_kernel.setArg(22,m_mol2atom0);
   m_KERNEL_monte_carlo->m_kernel.setArg(23,m_update_lists_every);

   // QMMM COULOMB Kernel - m_KERNEL_qmmm_c
   ////////////////////////////////////////////////////////////////////////

   int local_mem_bytes = m_KERNEL_qmmm_c->m_int_localRange*sizeof(_FPOINT_G_);
   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<<" QMMM Local mem(bytes): " << local_mem_bytes << std::endl);

   m_KERNEL_qmmm_c->m_kernel.setArg(0,cl::Local(local_mem_bytes));

   #ifdef SET_FPOINT_G_DOUBLE // Choose float/double cutoffs
      m_KERNEL_qmmm_c->m_kernel.setArg(5,m_charge);
      m_KERNEL_qmmm_c->m_kernel.setArg(17,m_cutoff_coulomb_rec_sq);
      m_KERNEL_qmmm_c->m_kernel.setArg(18,m_cutoff_coulomb_rec);
      m_KERNEL_qmmm_c->m_kernel.setArg(19,m_cutoff_coulomb);
   #else
      m_KERNEL_qmmm_c->m_kernel.setArg(5,m_charge_f);
      #ifdef HALF_FIXED
         m_KERNEL_qmmm_c->m_kernel.setArg(17,m_cutoff_coulomb_rec_sq_q);
         m_KERNEL_qmmm_c->m_kernel.setArg(18,m_cutoff_coulomb_rec_q);
         m_KERNEL_qmmm_c->m_kernel.setArg(19,m_cutoff_coulomb_q);
         m_KERNEL_qmmm_c->m_kernel.setArg(21,m_cutoff_coulomb_sq_q);//after mol2atom buffer
      #else
         m_KERNEL_qmmm_c->m_kernel.setArg(17,m_cutoff_coulomb_rec_sq_f);
         m_KERNEL_qmmm_c->m_kernel.setArg(18,m_cutoff_coulomb_rec_f);
         m_KERNEL_qmmm_c->m_kernel.setArg(19,m_cutoff_coulomb_f);
      #endif
   #endif

   m_KERNEL_qmmm_c->m_kernel.setArg(20,m_mol2atom0);

   #ifdef LEGACY_MULTI_DEVICE
      if(m_device_index==0){

         m_KERNEL_qmmm_c->m_kernel.setArg(21,0);
         m_KERNEL_qmmm_c->m_kernel.setArg(22,static_cast<int>((m_chains[0]->m_grid->dim)*m_bal[m_device_index]));

         VERBOSE_LVL_SETUP2("[BAL_] Device "<<m_device_index<<" s: "<<0<<" , e:"<<static_cast<int>((m_chains[0]->m_grid->dim)*m_bal[m_device_index])<<std::endl);
      }else{
         //Compute grid cursor position
         int gpos=0;
         for(int j=0;j<m_device_index;j++){
            gpos+=m_bal[j]*(m_chains[0]->m_grid->dim);
         }
         m_KERNEL_qmmm_c->m_kernel.setArg(21,gpos);
         m_KERNEL_qmmm_c->m_kernel.setArg(22,static_cast<int>(gpos+(m_chains[0]->m_grid->dim)*m_bal[m_device_index]));

         VERBOSE_LVL_SETUP2("[BAL_] Device "<<m_device_index<<" s: "<<gpos<<" , e:"<<static_cast<int>(gpos+(m_chains[0]->m_grid->dim)*m_bal[m_device_index])<<std::endl);
      }
   #endif

   // QMMM COULOMB Reduction Kernel - COARSE - m_KERNEL_qmmm_c_coarse_red
   ////////////////////////////////////////////////////////////////////////
   local_mem_bytes = m_KERNEL_qmmm_c_red_coarse->m_int_localRange*sizeof(_FPOINT_G_);
   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<<" QMMM_C_RED Local mem(bytes): " << local_mem_bytes << std::endl);

   m_KERNEL_qmmm_c_red_coarse->m_kernel.setArg(0,cl::Local(local_mem_bytes));
   // MM COULOMB + VDW Kernel - m_KERNEL_mm_vdwc
   ////////////////////////////////////////////////////////////////////////

   local_mem_bytes = m_KERNEL_mm_vdwc->m_int_localRange*sizeof(_FPOINT_S_);
   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<<" MM_VDWC Local mem(bytes): " << local_mem_bytes << std::endl);

   m_KERNEL_mm_vdwc->m_kernel.setArg(0,cl::Local(local_mem_bytes));
   m_KERNEL_mm_vdwc->m_kernel.setArg(1,cl::Local(local_mem_bytes));
   m_KERNEL_mm_vdwc->m_kernel.setArg(6,m_charge);
   m_KERNEL_mm_vdwc->m_kernel.setArg(14,m_n_total_atoms);
   m_KERNEL_mm_vdwc->m_kernel.setArg(15,m_mol2atom0);
   m_KERNEL_mm_vdwc->m_kernel.setArg(16,m_cutoff_coulomb);
   m_KERNEL_mm_vdwc->m_kernel.setArg(17,m_cutoff_coulomb_rec);
   m_KERNEL_mm_vdwc->m_kernel.setArg(18,m_cutoff_coulomb_rec_sq);
   m_KERNEL_mm_vdwc->m_kernel.setArg(19,m_cutoff_vdw);
   m_KERNEL_mm_vdwc->m_kernel.setArg(20,m_epsilon);
   m_KERNEL_mm_vdwc->m_kernel.setArg(21,m_sigma);

   // MM COULOMB+VDW Reduction Kernel-COARSE - m_KERNEL_mm_vdwc_red_coarse
   ////////////////////////////////////////////////////////////////////////

   local_mem_bytes = m_KERNEL_mm_vdwc_red_coarse->m_int_localRange*sizeof(_FPOINT_S_);
   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<<" VDWC_RED Local mem(bytes): " << local_mem_bytes << std::endl);

   m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(0,cl::Local(local_mem_bytes));
   m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(1,cl::Local(local_mem_bytes));


   //// QMMM TASKS (decision step, etc)- m_KERNEL_qmmm_tasks
   ////////////////////////////////////////////////////////////////////////

   local_mem_bytes = m_KERNEL_qmmm_tasks->m_int_localRange*sizeof(_FPOINT_S_);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(0,cl::Local(local_mem_bytes));
   m_KERNEL_qmmm_tasks->m_kernel.setArg(1,cl::Local(local_mem_bytes));
   // TODO this is not used, set dummy argument for now
   m_KERNEL_qmmm_tasks->m_kernel.setArg(2, 0);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(6,m_charge);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(7,m_atomic_numbers);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(15,m_n_total_mols);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(16,m_n_total_atoms);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(17,m_mol2atom0);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(18,m_cutoff_coulomb_rec_sq);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(19,m_cutoff_coulomb_rec);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(20,m_cutoff_coulomb);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(21,m_cutoff_vdw);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(22,m_epsilon);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(23,m_sigma);

   // CLOSURE (decision step, etc)- m_KERNEL_closure
   ////////////////////////////////////////////////////////////////////////

   m_KERNEL_closure->m_kernel.setArg(14,m_mol2atom0);
   m_KERNEL_closure->m_kernel.setArg(20,m_n_total_atoms);
   m_KERNEL_closure->m_kernel.setArg(21,m_n_total_mols);
   m_KERNEL_closure->m_kernel.setArg(23,m_temperature);
   m_KERNEL_closure->m_kernel.setArg(24,m_update_lists_every);


   // Set arguments for chain 0
   // XXX: Possibly not required here
   //setChainArgs(0);

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLDevice::setup_pmc Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }

}
void OCLDevice::setChainArgs(int chain){

   try{

      // Setup qmmm_c global size according to the chain's grid size
      // (and current chain parameters for grain, etc)
      // NOTE: The grain argument for qmmm_c is passed on, below, in this function

      #ifdef LEGACY_MULTI_DEVICE
         m_KERNEL_qmmm_c->setRanges(
                              static_cast<int>((m_chains[chain]->m_grid->dim/m_chains[chain]->m_qmmm_c_grain)*m_bal[m_device_index]),//XXX:HARD 1/m_ndevices BALANCING
                              m_chains[chain]->m_qmmm_c_localsize);
      #else
         m_KERNEL_qmmm_c->setRanges(
                              (m_chains[chain]->m_grid->dim/m_chains[chain]->m_qmmm_c_grain),
                              m_chains[chain]->m_qmmm_c_localsize);
      #endif
      // Setup kernel arguments to match the buffer of one chain
      // Scalar argument are sent with the Kernel launch call

      // Monte Carlo Kernel - m_KERNEL_monte_carlo
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_monte_carlo->m_kernel.setArg(1, m_chains[chain]->m_old_x);
      m_KERNEL_monte_carlo->m_kernel.setArg(2, m_chains[chain]->m_old_y);
      m_KERNEL_monte_carlo->m_kernel.setArg(3, m_chains[chain]->m_old_z);
      m_KERNEL_monte_carlo->m_kernel.setArg(4, m_chains[chain]->m_changed_molecule);
      m_KERNEL_monte_carlo->m_kernel.setArg(5, m_chains[chain]->m_changed_molecule_size);
      m_KERNEL_monte_carlo->m_kernel.setArg(6, m_chains[chain]->m_ch_x);
      m_KERNEL_monte_carlo->m_kernel.setArg(7, m_chains[chain]->m_ch_y);
      m_KERNEL_monte_carlo->m_kernel.setArg(8, m_chains[chain]->m_ch_z);
      m_KERNEL_monte_carlo->m_kernel.setArg(9, m_chains[chain]->m_random_molecule);
      m_KERNEL_monte_carlo->m_kernel.setArg(10,m_chains[chain]->m_rand_rotate_theta);
      m_KERNEL_monte_carlo->m_kernel.setArg(11,m_chains[chain]->m_q0);
      m_KERNEL_monte_carlo->m_kernel.setArg(12,m_chains[chain]->m_q1);
      m_KERNEL_monte_carlo->m_kernel.setArg(13,m_chains[chain]->m_q2);
      m_KERNEL_monte_carlo->m_kernel.setArg(14,m_chains[chain]->m_q3);
      m_KERNEL_monte_carlo->m_kernel.setArg(15,m_chains[chain]->m_random_trans_num);
      m_KERNEL_monte_carlo->m_kernel.setArg(16,m_chains[chain]->m_rand_trans_ex);
      m_KERNEL_monte_carlo->m_kernel.setArg(17,m_chains[chain]->m_rand_trans_ey);
      m_KERNEL_monte_carlo->m_kernel.setArg(18,m_chains[chain]->m_rand_trans_ez);

      //(float output for changed molecule)
      #ifndef SET_FPOINT_G_DOUBLE
         m_KERNEL_monte_carlo->m_kernel.setArg(24, m_chains[chain]->m_ch_x_f);
         m_KERNEL_monte_carlo->m_kernel.setArg(25, m_chains[chain]->m_ch_y_f);
         m_KERNEL_monte_carlo->m_kernel.setArg(26, m_chains[chain]->m_ch_z_f);
      #endif

      // QMMM COULOMB Kernel - m_KERNEL_qmmm_c
      ////////////////////////////////////////////////////////////////////////

      m_KERNEL_qmmm_c->m_kernel.setArg(1, m_chains[chain]->m_qmmm_c_grain);

      #ifndef SET_FPOINT_G_DOUBLE
         m_KERNEL_qmmm_c->m_kernel.setArg(2, m_chains[chain]->m_old_x_f);
         m_KERNEL_qmmm_c->m_kernel.setArg(3, m_chains[chain]->m_old_y_f);
         m_KERNEL_qmmm_c->m_kernel.setArg(4, m_chains[chain]->m_old_z_f);
      #else
         m_KERNEL_qmmm_c->m_kernel.setArg(2, m_chains[chain]->m_old_x);
         m_KERNEL_qmmm_c->m_kernel.setArg(3, m_chains[chain]->m_old_y);
         m_KERNEL_qmmm_c->m_kernel.setArg(4, m_chains[chain]->m_old_z);
      #endif
         //(arg5 is m_charges, setup in the main setup buffers function)
      m_KERNEL_qmmm_c->m_kernel.setArg(6, m_chains[chain]->m_changed_molecule);
      m_KERNEL_qmmm_c->m_kernel.setArg(7, m_chains[chain]->m_changed_molecule_size);

      // Choose which ch_mol buffers to use
      #ifndef SET_FPOINT_G_DOUBLE
         m_KERNEL_qmmm_c->m_kernel.setArg(8, m_chains[chain]->m_ch_x_f);
         m_KERNEL_qmmm_c->m_kernel.setArg(9, m_chains[chain]->m_ch_y_f);
         m_KERNEL_qmmm_c->m_kernel.setArg(10,m_chains[chain]->m_ch_z_f);
      #else
         m_KERNEL_qmmm_c->m_kernel.setArg(8, m_chains[chain]->m_ch_x);
         m_KERNEL_qmmm_c->m_kernel.setArg(9, m_chains[chain]->m_ch_y);
         m_KERNEL_qmmm_c->m_kernel.setArg(10,m_chains[chain]->m_ch_z);
      #endif

      m_KERNEL_qmmm_c->m_kernel.setArg(11,m_chains[chain]->m_grid_rx);
      m_KERNEL_qmmm_c->m_kernel.setArg(12,m_chains[chain]->m_grid_ry);
      m_KERNEL_qmmm_c->m_kernel.setArg(13,m_chains[chain]->m_grid_rz);
      m_KERNEL_qmmm_c->m_kernel.setArg(14,m_chains[chain]->m_grid_charge);
      m_KERNEL_qmmm_c->m_kernel.setArg(15,m_chains[chain]->m_grid_size);
      m_KERNEL_qmmm_c->m_kernel.setArg(16,m_chains[chain]->m_qmmm_c_energy);

      // QMMM COULOMB Reduction Kernel - COARSE - m_KERNEL_qmmm_c_coarse_red
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_qmmm_c_red_coarse->m_kernel.setArg(3,m_chains[chain]->m_qmmm_c_energy);

      // MM COULOMB + VDW Kernel - m_KERNEL_mm_vdwc
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_mm_vdwc->m_kernel.setArg(3, m_chains[chain]->m_old_x);
      m_KERNEL_mm_vdwc->m_kernel.setArg(4, m_chains[chain]->m_old_y);
      m_KERNEL_mm_vdwc->m_kernel.setArg(5, m_chains[chain]->m_old_z);
      m_KERNEL_mm_vdwc->m_kernel.setArg(7, m_chains[chain]->m_changed_molecule);
      m_KERNEL_mm_vdwc->m_kernel.setArg(8, m_chains[chain]->m_changed_molecule_size);
      m_KERNEL_mm_vdwc->m_kernel.setArg(9, m_chains[chain]->m_ch_x);
      m_KERNEL_mm_vdwc->m_kernel.setArg(10,m_chains[chain]->m_ch_y);
      m_KERNEL_mm_vdwc->m_kernel.setArg(11,m_chains[chain]->m_ch_z);
      m_KERNEL_mm_vdwc->m_kernel.setArg(12,m_chains[chain]->m_mm_vdwc_energy_vdw);
      m_KERNEL_mm_vdwc->m_kernel.setArg(13,m_chains[chain]->m_mm_vdwc_energy_col);

      // MM COULOMB+VDW Reduction Kernel-COARSE - m_KERNEL_mm_vdwc_red_coarse
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(4,m_chains[chain]->m_mm_vdwc_energy_vdw);
      m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(5,m_chains[chain]->m_mm_vdwc_energy_col);
      m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(6,m_chains[chain]->m_minors_energy);

      // QMMM TASKS (decision step, etc)- m_KERNEL_qmmm_tasks
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_qmmm_tasks->m_kernel.setArg(3,m_chains[chain]->m_old_x);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(4,m_chains[chain]->m_old_y);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(5,m_chains[chain]->m_old_z);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(8,m_chains[chain]->m_changed_molecule);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(9,m_chains[chain]->m_changed_molecule_size);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(10,m_chains[chain]->m_ch_x);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(11,m_chains[chain]->m_ch_y);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(12,m_chains[chain]->m_ch_z);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(13,m_chains[chain]->m_sqrt_box_dim);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(14,m_chains[chain]->m_minors_energy);

      // CLOSURE (decision step, etc)- m_KERNEL_closure
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_closure->m_kernel.setArg(1, m_chains[chain]->m_minors_energy);
      m_KERNEL_closure->m_kernel.setArg(2, m_chains[chain]->m_qmmm_c_energy);
      m_KERNEL_closure->m_kernel.setArg(3, m_chains[chain]->m_qm_energy);
      m_KERNEL_closure->m_kernel.setArg(4, m_chains[chain]->m_energy_qm_gp);
      m_KERNEL_closure->m_kernel.setArg(5, m_chains[chain]->m_energy_old);
      m_KERNEL_closure->m_kernel.setArg(6, m_chains[chain]->m_energy_vdw_qmmm);
      m_KERNEL_closure->m_kernel.setArg(7,m_chains[chain]->m_energy_mm);
      m_KERNEL_closure->m_kernel.setArg(8,m_chains[chain]->m_number_accepted);
      m_KERNEL_closure->m_kernel.setArg(9,m_chains[chain]->m_accepted_config_data);
      m_KERNEL_closure->m_kernel.setArg(10,m_chains[chain]->m_accepted_energy_data);
      m_KERNEL_closure->m_kernel.setArg(11,m_chains[chain]->m_old_x);
      m_KERNEL_closure->m_kernel.setArg(12,m_chains[chain]->m_old_y);
      m_KERNEL_closure->m_kernel.setArg(13,m_chains[chain]->m_old_z);
      m_KERNEL_closure->m_kernel.setArg(15,m_chains[chain]->m_changed_molecule);
      m_KERNEL_closure->m_kernel.setArg(16,m_chains[chain]->m_changed_molecule_size);
      m_KERNEL_closure->m_kernel.setArg(17,m_chains[chain]->m_ch_x);
      m_KERNEL_closure->m_kernel.setArg(18,m_chains[chain]->m_ch_y);
      m_KERNEL_closure->m_kernel.setArg(19,m_chains[chain]->m_ch_z);
      m_KERNEL_closure->m_kernel.setArg(22,m_chains[chain]->m_random_boltzman_acc);

      #ifndef SET_FPOINT_G_DOUBLE // second reference (float)
         m_KERNEL_closure->m_kernel.setArg(25,m_chains[chain]->m_old_x_f);
         m_KERNEL_closure->m_kernel.setArg(26,m_chains[chain]->m_old_y_f);
         m_KERNEL_closure->m_kernel.setArg(27,m_chains[chain]->m_old_z_f);
         m_KERNEL_closure->m_kernel.setArg(28,m_chains[chain]->m_old_means);
         m_KERNEL_closure->m_kernel.setArg(29,m_chains[chain]->m_old_variance);
      #else
	 m_KERNEL_closure->m_kernel.setArg(25,m_chains[chain]->m_old_means);
         m_KERNEL_closure->m_kernel.setArg(26,m_chains[chain]->m_old_variance);
      #endif

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLDevice::switchChainArgs Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
}

void OCLDevice::first_writes(){
   // Write init values of grids and lattices to gpu
   for(int i=0; i<m_nchains; i++){
      m_chains[i]->write_init_energies();
      m_chains[i]->write_grid(0, false, m_run->dimension_box,
            m_run->cutoff_coulomb); // it's already reduced
      m_chains[i]->write_system_lattice();
   }
}

void OCLDevice::launch_mc         (std::vector<cl::Event> *in_deps,
                                    cl::Event *out_dep,
                                    int step){
   m_KERNEL_monte_carlo->m_kernel.setArg(0,step);

   m_queue->enqueueNDRangeKernel( m_KERNEL_monte_carlo->m_kernel,     //Kernel to be launched
                                  cl::NullRange,                      //Offset set to 0
                                  m_KERNEL_monte_carlo->m_globalRange,//Number of work-items
                                  m_KERNEL_monte_carlo->m_localRange, //Size of work-group
                                  in_deps,                            //Event Dependencies
                                  out_dep);                           //This kernel event

}

void OCLDevice::launch_qmmm_c     (std::vector<cl::Event> *in_deps,
                                   OCLKernel *qmmm_c,
                                   OCLKernel *qmmm_c_reduction,
                                   cl::Event *out_dep,
                                   cl::Event *qmmm_c_out_dep)
{

   std::vector<cl::Event> qmmm_c_red_coarse_dependencies;

   VERBOSE_LVL_UDEBUG("\tEnqueueing MAIN QMMM C (global, local):");
   VERBOSE_LVL_UDEBUG(qmmm_c->m_int_globalRange);
   VERBOSE_LVL_UDEBUG(qmmm_c->m_int_localRange);

   // NOTE: the argument m_grid_points (grid points per work-item)
   // is setup in the setChainArgs function

   // Launch qmmm_c Kernel (includes first energy reduction)
   m_queue->enqueueNDRangeKernel(qmmm_c->m_kernel,
                                 cl::NullRange,
                                 qmmm_c->m_globalRange,
                                 qmmm_c->m_localRange,
                                 in_deps,
                                 qmmm_c_out_dep);

   qmmm_c_red_coarse_dependencies.push_back(*qmmm_c_out_dep);

   // Setup Coarse reduction:
   //
   int last_result_size =
      (qmmm_c->m_int_globalRange/
       qmmm_c->m_int_localRange);

   int grain_en_values = 1;//By default, one point per wi.

   if(last_result_size > WGSIZE_MAX_COARSE_RED_QMMM_C){

      grain_en_values = (int) ceil((float)last_result_size/
                                   (float)WGSIZE_MAX_COARSE_RED_QMMM_C);

      qmmm_c_reduction->m_int_globalRange = WGSIZE_MAX_COARSE_RED_QMMM_C;
      qmmm_c_reduction->m_int_localRange  = WGSIZE_MAX_COARSE_RED_QMMM_C;

   }else{

      // Must be power of two
      int powsize = pow(2,ceil(log2(last_result_size)));
      qmmm_c_reduction->m_int_globalRange = powsize;
      qmmm_c_reduction->m_int_localRange  = powsize;

   }

   qmmm_c_reduction->m_globalRange
                = cl::NDRange(qmmm_c_reduction->m_int_globalRange);

   qmmm_c_reduction->m_localRange
                = cl::NDRange(qmmm_c_reduction->m_int_localRange);

   qmmm_c_reduction->m_kernel.setArg(1,last_result_size);// true vect size
   qmmm_c_reduction->m_kernel.setArg(2,grain_en_values); // energy points per work-item

   VERBOSE_LVL_UDEBUG("\tEnqueueing coarse reduction kernel (grain,global,local,true)");
   VERBOSE_LVL_UDEBUG(grain_en_values);
   VERBOSE_LVL_UDEBUG(qmmm_c_reduction->m_int_globalRange);
   VERBOSE_LVL_UDEBUG(qmmm_c_reduction->m_int_localRange);
   VERBOSE_LVL_UDEBUG(last_result_size);

   m_queue->enqueueNDRangeKernel(qmmm_c_reduction->m_kernel,
                                 cl::NullRange,
                                 qmmm_c_reduction->m_globalRange,
                                 qmmm_c_reduction->m_localRange,
                                 &qmmm_c_red_coarse_dependencies,
                                 out_dep);

}
void OCLDevice::launch_mm_vdwc    (std::vector<cl::Event> *in_deps,
                                    cl::Event *out_dep,
                                    int memory_offset){

   std::vector<cl::Event> mm_vdwc_red_coarse_dependencies;

   VERBOSE_LVL_UDEBUG("\tEnqueueing MAIN MM VDWC (global, local):");
   VERBOSE_LVL_UDEBUG(m_KERNEL_mm_vdwc->m_int_globalRange);
   VERBOSE_LVL_UDEBUG(m_KERNEL_mm_vdwc->m_int_localRange);

   m_KERNEL_mm_vdwc->m_kernel.setArg(2,1);

   // Lanuch qmmm_c Kernel (includes first energy reduction)
   m_queue->enqueueNDRangeKernel(m_KERNEL_mm_vdwc->m_kernel,
                                                 cl::NullRange,
                                                 m_KERNEL_mm_vdwc->m_globalRange,
                                                 m_KERNEL_mm_vdwc->m_int_localRange,
                                                 in_deps,
                                                 &m_EVENT_mm_vdwc[memory_offset]);

   mm_vdwc_red_coarse_dependencies.push_back(m_EVENT_mm_vdwc[memory_offset]);

   int last_result_size =
      (m_KERNEL_mm_vdwc->m_int_globalRange/
      m_KERNEL_mm_vdwc->m_int_localRange);


   int grain_en_values = 1;//By default, one point per wi.

   if(last_result_size > WGSIZE_MAX_COARSE_RED_MM_VDWC){

      grain_en_values = (int) ceil((float)last_result_size/
                                   (float)WGSIZE_MAX_COARSE_RED_MM_VDWC);

      m_KERNEL_mm_vdwc_red_coarse->m_int_globalRange = WGSIZE_MAX_COARSE_RED_MM_VDWC;
      m_KERNEL_mm_vdwc_red_coarse->m_int_localRange  = WGSIZE_MAX_COARSE_RED_MM_VDWC;

   }else{

      // Must be power of two
      int powsize = pow(2,ceil(log2(last_result_size)));
      m_KERNEL_mm_vdwc_red_coarse->m_int_globalRange = powsize;
      m_KERNEL_mm_vdwc_red_coarse->m_int_localRange  = powsize;

   }

   m_KERNEL_mm_vdwc_red_coarse->m_globalRange
                = cl::NDRange(m_KERNEL_mm_vdwc_red_coarse->m_int_globalRange);

   m_KERNEL_mm_vdwc_red_coarse->m_localRange
                = cl::NDRange(m_KERNEL_mm_vdwc_red_coarse->m_int_localRange);

   m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(2,last_result_size);// true vect size
   m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(3,grain_en_values); // energy points per work-item

   VERBOSE_LVL_UDEBUG("\tEnqueueing coarse reduction kernel (grain,global,local,true)");
   VERBOSE_LVL_UDEBUG(grain_en_values);
   VERBOSE_LVL_UDEBUG(m_KERNEL_mm_vdwc_red_coarse->m_int_globalRange);
   VERBOSE_LVL_UDEBUG(m_KERNEL_mm_vdwc_red_coarse->m_int_localRange);
   VERBOSE_LVL_UDEBUG(last_result_size);

   m_queue->enqueueNDRangeKernel(m_KERNEL_mm_vdwc_red_coarse->m_kernel,
                                                 cl::NullRange,
                                                 m_KERNEL_mm_vdwc_red_coarse->m_globalRange,
                                                 m_KERNEL_mm_vdwc_red_coarse->m_localRange,
                                                 &mm_vdwc_red_coarse_dependencies,
                                                 out_dep);

}
void OCLDevice::launch_qmmm_tasks (std::vector<cl::Event> *in_deps,
                                    cl::Event *out_dep) {

   // TODO this is not used actually in the kernel
   //m_KERNEL_qmmm_tasks->m_kernel.setArg(2,step);

   // Launch qmmm_tasks Kernel (energy qmmm_vdw + qmmm_c_nuclei)
   m_queue->enqueueNDRangeKernel(m_KERNEL_qmmm_tasks->m_kernel,
                                                 cl::NullRange,
                                                 m_KERNEL_qmmm_tasks->m_globalRange,
                                                 m_KERNEL_qmmm_tasks->m_localRange,
                                                 in_deps,
                                                 out_dep);

}
void OCLDevice::launch_closure    (std::vector<cl::Event> *in_deps,
                                    cl::Event *out_dep,
                                    int step){

   #ifdef LEGACY_MULTI_DEVICE
      // Syncronize with the other devices, and rebalance (TODO: balancing & move this to a function)
      int inner_step = step;

      VERBOSE_LVL_UDEBUG3("FINISH 0 (dev "<<m_device_index<<")"<<std::endl);

      #ifdef SAFE_FINISH
         m_queue->finish();
      #endif

      VERBOSE_LVL_UDEBUG3("AFTER FINISH 0 (dev "<<m_device_index<<")"<<std::endl);
      PROFILE_FIRST(0,t_mbarrier_start);

      //std::vector<cl::Event> get_energy_part_dependencies;
      //get_energy_part_dependencies.push_back(*o_dep);

      bool will_read_qmmm_c  = true;
      bool will_write_qmmm_c = false;
      // For now, only allowing device 0 to not launching the qmmm_c kernel, if m_bal=0
      if(m_device_index == 0 and m_bal[m_device_index]==0)       will_read_qmmm_c  = false;
      if(m_ndevices != 2 or not(m_device_index==1 and m_bal[0]==0)) will_write_qmmm_c = true;


      // Read device's partial energies
      _FPOINT_G_ qmmm_c_part_energy     = 0;
      _FPOINT_S_ minors_part_energy[3]     ;
      _FPOINT_G_ qmmm_c_acc_energy      = 0;
      _FPOINT_S_ minors_acc_energy[3]      ;

      for(int a=0;a<3;a++) minors_acc_energy[a]=0;

      //Device 0 also does mm and qmmm tasks
      //
      if(m_device_index==0){ //Master device, has minors (vdwc, qmmm_c_nucl and qmmm_vdw)

         VERBOSE_LVL_UDEBUG3("READ MINORS (dev "<<m_device_index<<")"<<std::endl);

         m_queue->enqueueReadBuffer( m_chains[0]->m_minors_energy,
                                      CL_FALSE,
                                      0,
                                      3*sizeof(_FPOINT_S_),
                                      minors_part_energy,
                                      in_deps,//&get_energy_part_dependencies,
                                      &m_EVENT_read_part_minors[inner_step]);

      }

      if(will_read_qmmm_c){

         VERBOSE_LVL_UDEBUG3("READ QMMM C (dev "<<m_device_index<<")"<<std::endl);

         m_queue->enqueueReadBuffer( m_chains[0]->m_qmmm_c_energy,
                                      CL_TRUE,
                                      0,
                                      sizeof(_FPOINT_G_),
                                      &qmmm_c_part_energy,
                                      in_deps,//&get_energy_part_dependencies,
                                      &m_EVENT_read_part_qmmm_c[inner_step]);

      }else{
         qmmm_c_part_energy=0;
      }

      VERBOSE_LVL_UDEBUG3("FINISH 1 (dev "<<m_device_index<<")"<<std::endl);

      #ifdef SAFE_FINISH
         m_queue->finish();
      #endif

      // Broadcast energy
      v_bcast_qmmm_c[m_barrier_index]     = qmmm_c_part_energy;
      v_bcast_mm[m_barrier_index]         = minors_part_energy[0];
      v_bcast_qmmm_vdw[m_barrier_index]   = minors_part_energy[1];
      v_bcast_qmmm_c_nucl[m_barrier_index]= minors_part_energy[2];

      VERBOSE_LVL_UDEBUG3("BARRIER BEFORE (dev "<<m_device_index<<")"<<std::endl);


/////// BAL BEFORE
      #ifdef BALANCING
      double partsum = 0.0;
      //unsigned long t[m_ndevices];
      double t[m_ndevices];
      double m_bal_old[m_ndevices];

      if(inner_step!=0 and inner_step%BALANCING==0){

         /*if(m_device_index==0)
            std::cout << "CHANGING BALANCING" << std::endl;*/

         //Get mean times
         for(int i=0; i < m_ndevices; i++){

            if(inner_step/BALANCING==0){
               t[i] = (m_ocl->m_devices[i]->t_pmc_iter_acc - m_ocl->m_devices[i]->t_ulock_acc)/inner_step;
            }else{
               t[i] = (
                        (m_ocl->m_devices[i]->t_pmc_iter_acc - m_ocl->m_devices[i]->t_ulock_acc) -
                        (m_ocl->m_devices[i]->m_prev_offset[inner_step/BALANCING-1])

                      )/BALANCING;
            }

         }
         //Store prev accum to offset later XXX , loosely verified code
         m_prev_offset[inner_step/BALANCING] = t_pmc_iter_acc - t_ulock_acc;

         for(int i=0; i < m_ndevices; i++){
            partsum +=  m_bal[i] / t[i];

            m_bal_old[i] = m_bal[i];
         }
      }
      #endif
/////// BAL BEFORE

      PROFILE_FIRST(0,t_ulock_start);
      // Barrier operation with other devices
      ULOCK_BARRIER(p_barrier_mutex, p_barrier_cond, p_barrier_count, m_barrier_max*(inner_step+1),0,//switch from 0 to 1 for verbose
      "[BARR] Device "<<m_barrier_index<<"("<<m_device_index<<")"<<std::endl

      << " qmmm c    part en= "<<qmmm_c_part_energy<<std::endl
      << " mm        part en= "<<minors_part_energy[0]<<std::endl
      << " qmmm vdw  part en= "<<minors_part_energy[1]<<std::endl
      << " qmmm c nu part en= "<<minors_part_energy[2]<<std::endl);

      PROFILE(0,t_ulock_acc,t_ulock_end,t_ulock_start,n_ulock);
      #ifdef BALANCING_PROBE
         if(inner_step%BALANCING_PROBE==0){
            m_tulock_probe[inner_step/BALANCING_PROBE] = t_ulock_end-t_ulock_start;
         }
      #endif

/////// BAL AFTER
      #ifdef BALANCING

      if(inner_step!=0 and inner_step%BALANCING==0){

         /*if(m_device_index==0){
            std::cout <<"partsum= " <<partsum <<std::endl;

         }*/
         //Choose distribution
         for(int i=0; i < m_ndevices; i++){
            m_bal[i] = m_bal_old[i]/(t[i]*partsum);
            if(m_device_index==0){
               /*std::cout << "t["<<i<<"]: " << t[i] << std::endl;
               std::cout << "t["<<i<<"]*partsum =" << ((double)t[i])*partsum << std::endl;
               std::cout << "m_bal_old["<<i<<"]="<< m_bal_old[i] << std::endl;*/

               //ORIGINAL PRINT: std::cout << "bal["<<i<<"]: " << m_bal[i] <<" ; perf mean = " <<(m_ocl->m_devices[i]->t_pmc_iter_acc - m_ocl->m_devices[i]->t_ulock_acc)/inner_step<<" us"<<std::endl;

               //V2: std::cout << m_bal[i] <<","<<(m_ocl->m_devices[i]->t_pmc_iter_acc - m_ocl->m_devices[i]->t_ulock_acc)/inner_step<<",";

               std::cout << m_bal[i] <<","<<t[i]<<",";
            }
         }
         if(m_device_index==0)
             std::cout << std::endl;

         m_KERNEL_qmmm_c->setRanges(
                            static_cast<int>((m_chains[0]->m_grid->dim/m_chains[0]->m_qmmm_c_grain)*m_bal[m_device_index])
                            , m_chains[0]->m_qmmm_c_localsize);

         if(m_device_index==0){

            m_KERNEL_qmmm_c->m_kernel.setArg(21,0);
            m_KERNEL_qmmm_c->m_kernel.setArg(22,static_cast<int>((m_chains[0]->m_grid->dim)*m_bal[m_device_index]));

            //VERBOSE_LVL_SETUP2("[BAL_] Device "<<m_device_index<<" s: "<<0<<" , e:"
            //   <<static_cast<int>((m_chains[0]->m_grid->dim)*m_bal[m_device_index])<<endl);

         }else{

            //Compute grid cursor position
            int gpos=0;
            for(int j=0;j<m_device_index;j++){
               gpos+=m_bal[j]*(m_chains[0]->m_grid->dim);
            }
            m_KERNEL_qmmm_c->m_kernel.setArg(21,gpos);
            m_KERNEL_qmmm_c->m_kernel.setArg(22,static_cast<int>(gpos+(m_chains[0]->m_grid->dim)*m_bal[m_device_index]));

            //VERBOSE_LVL_SETUP2("[BAL_] Device "<<m_device_index<<" s: "<<gpos<<" , e:"<<static_cast<int>(gpos+(m_chains[0]->m_grid->dim)*m_bal[m_device_index])<<endl);
         }
      }
      #endif
/////// BAL AFTER


      VERBOSE_LVL_UDEBUG3("BEFORE AFTER (dev "<<m_device_index<<")"<<std::endl);

      // Add up results
      for(int i=0; i<m_barrier_max; i++){
         qmmm_c_acc_energy    += v_bcast_qmmm_c[i];
         minors_acc_energy[0] += v_bcast_mm[i];
         minors_acc_energy[1] += v_bcast_qmmm_vdw[i];
         minors_acc_energy[2] += v_bcast_qmmm_c_nucl[i];
      }

      //TODO: Bcast performance; Barrier ; Do balancing math and reschedule GRID

      // WRITE ENERGIES

      if(m_device_index!=0){// dev0 (master) does not need vdw update

         VERBOSE_LVL_UDEBUG3("WRITE MINORS (dev "<<m_device_index<<")"<<std::endl);

         // Write updated energy to each device
         m_queue->enqueueWriteBuffer(  m_chains[0]->m_minors_energy,
                                       CL_FALSE,
                                       0,
                                       3*sizeof(_FPOINT_S_),
                                       minors_acc_energy,
                                       NULL,
                                       &m_EVENT_write_part_minors[inner_step]);
      }

      if(will_write_qmmm_c){

         VERBOSE_LVL_UDEBUG3("WRITE QMMM C (dev "<<m_device_index<<")"<<std::endl);

         //For two devices, if m_bal[master=0]=0, the slave device doesn't need qmmm_c update!
         m_queue->enqueueWriteBuffer(  m_chains[0]->m_qmmm_c_energy,
                                       CL_TRUE,
                                       0,
                                       sizeof(_FPOINT_G_),
                                       &qmmm_c_acc_energy,
                                       NULL,
                                       &m_EVENT_write_part_qmmm_c[inner_step]);
      }

      VERBOSE_LVL_UDEBUG3("FINISH 2 (dev "<<m_device_index<<")"<<std::endl);

      #ifdef SAFE_FINISH
         m_queue->finish();
      #endif

      PROFILE(0,t_mbarrier_acc,t_mbarrier_end,t_mbarrier_start,n_mbarrier);

   #endif

   VERBOSE_LVL_UDEBUG3("LAUNCH CLOSURE (dev "<<m_device_index<<")"<<std::endl);


   m_KERNEL_closure->m_kernel.setArg(0,step);

   m_queue->enqueueNDRangeKernel(m_KERNEL_closure->m_kernel,
                                                 cl::NullRange,
                                                 m_KERNEL_closure->m_globalRange,
                                                 m_KERNEL_closure->m_localRange,
                                                 in_deps,
                                                 out_dep);

   VERBOSE_LVL_UDEBUG3("DONE CLOSURE (dev "<<m_device_index<<")"<<std::endl);

}

//
// Step must start at 0 for the first step
void OCLDevice::launch_pmc_cycle(int inner_step, int outer_step, int memory_offset)
{

   std::vector<cl::Event> qmmm_c_dependencies;
   std::vector<cl::Event> mm_vdwc_dependencies;
   std::vector<cl::Event> qmmm_tasks_dependencies;
   std::vector<cl::Event> closure_dependencies;

   int global_step = outer_step*m_inner_steps + inner_step;

   // Except for the first inner iteration of each
   // PMC Cycle, wait for previous PMC Cycle step
   if(inner_step != 0 && memory_offset != 0){
      VERBOSE_LVL_UDEBUG("Chaining previous pmc_cycle");
      m_mc_dependencies.push_back(m_EVENT_closure[memory_offset-1]);
   }

   try{

      // MONTE CARLO STEP
      // ****************
      VERBOSE_LVL_UDEBUG("Launching MC");

      launch_mc (&m_mc_dependencies,         //In  dependecies
                 &m_EVENT_monte_carlo[memory_offset], //Out dependency
                 global_step);


      VERBOSE_LVL_UDEBUG("Chaining next kernels to MC");

      // Kernels that depend directly on monte carlo
      qmmm_c_dependencies.push_back(m_EVENT_monte_carlo[memory_offset]);
      mm_vdwc_dependencies.push_back(m_EVENT_monte_carlo[memory_offset]);
      qmmm_tasks_dependencies.push_back(m_EVENT_monte_carlo[memory_offset]);

      bool will_launch_qmmm_c = true;
      #ifdef LEGACY_MULTI_DEVICE
         // For now, only allowing device 0 to not launching the qmmm_c kernel
         if(m_device_index == 0 and m_bal[m_device_index]==0) will_launch_qmmm_c = false;
      #endif

      if(will_launch_qmmm_c){

         // QMMM COULOMB
         // ************
         VERBOSE_LVL_UDEBUG("Launching QMMM C");

         launch_qmmm_c (&qmmm_c_dependencies,                   // In  dependecies
                        m_KERNEL_qmmm_c,                        // kernel
                        m_KERNEL_qmmm_c_red_coarse,             // reduction kernel
                        &m_EVENT_qmmm_c_red_coarse[memory_offset], // Out dependencies
                        &m_EVENT_qmmm_c[memory_offset]);
      }

      bool will_launch_minors = true;
      #ifdef LEGACY_MULTI_DEVICE
         if(m_device_index != 0) will_launch_minors = false;
      #endif

      if(will_launch_minors){

         // MM VAN DER WAALS + COULOMB
         // **************************
         VERBOSE_LVL_UDEBUG("Launching MM VDWC");

         launch_mm_vdwc (&mm_vdwc_dependencies,              //In  dependecies
                         &m_EVENT_mm_vdwc_red_coarse[memory_offset],  //Out dependencies
                         memory_offset);
         // Other QMMM Tasks
         // ****************
         VERBOSE_LVL_UDEBUG("Launching QMMM Tasks");

         launch_qmmm_tasks(&qmmm_tasks_dependencies,  //In  dependecies
                           &m_EVENT_qmmm_tasks[memory_offset]); //Out dependencies
      }

      // Closure Kernel, decision step
      // *****************************
      VERBOSE_LVL_UDEBUG("Launching Closure");

      if(will_launch_qmmm_c)
         closure_dependencies.push_back(m_EVENT_qmmm_c_red_coarse[memory_offset]);

      if(will_launch_minors){
         closure_dependencies.push_back(m_EVENT_mm_vdwc_red_coarse[memory_offset]);
         closure_dependencies.push_back(m_EVENT_qmmm_tasks[memory_offset]);
      }

      launch_closure (&closure_dependencies, &m_EVENT_closure[memory_offset],global_step);

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLDevice::launch_pmc_cycle Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }

   m_mc_dependencies.clear();
}


void OCLDevice::createQueue(){

    try{

      #ifdef SET_VERBOSE_LVL_SETUP
         std::cout << "Creating queue for device " << std::endl;
      #endif

      #ifdef OCL_OUT_OF_ORDER

         #ifdef GPU_PROFILING
            m_queue = new cl::CommandQueue(*m_p_context,
                                            m_device,
                                            CL_QUEUE_PROFILING_ENABLE|CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE);
         #endif
         #ifndef GPU_PROFILING
            m_queue = new cl::CommandQueue(*m_p_context,
                                            m_device,
                                            CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE);
         #endif

      #else

         #ifdef GPU_PROFILING
            m_queue = new cl::CommandQueue(*m_p_context,
                                            m_device,
                                            CL_QUEUE_PROFILING_ENABLE);

         #endif
         #ifndef GPU_PROFILING
            m_queue = new cl::CommandQueue(*m_p_context,
                                            m_device);
         #endif

      #endif
   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLDevice::Constructor createQueue Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
}
void OCLDevice::print_profiling(){

 #ifdef BASE_PROFILING
      std::cout <<std::endl<< "Outer Profiling For Device "<< m_device_index <<std::endl;
      std::cout << "Profiling Info (PROF_EVERY "<<PROF_EVERY<<" )"<< std::endl;
      std::cout << "Host (Mean times):"              << std::endl;

      std::cout << "t_launch_pmc_acc:        "
                << std::setprecision (15)
                << t_launch_pmc_acc/(n_launch_pmc*1000)
                << " ms"
                << "("
                << n_launch_pmc
                <<" samples)"
                << std::endl;

      if(n_save!=0){

         std::cout << "t_save_acc:              "
                   << std::setprecision (15)
                   << t_save_acc/(n_save*1000)
                   << " ms"
                   << "("
                   << n_save
                   <<" samples)"
                   << std::endl;
      }

      if(n_lastsave!=0){

         std::cout << "t_lastsave_acc:          "
                   << std::setprecision (15)
                   << t_lastsave_acc/(n_lastsave*1000)
                   << " ms"
                   << "("
                   << n_lastsave
                   <<" samples)"
                   << std::endl;
      }
      if(n_pmc_iter!=0){

         std::cout << "t_iter_acc(host-measure):"
                   << std::setprecision (15)
                   << t_pmc_iter_acc/(n_pmc_iter*1000)
                   << " ms"
                   << "("
                   << n_pmc_iter
                   <<" samples)"
                   << std::endl;
      }
      if(n_update!=0){
         std::cout << "t_update_acc:            "
                   << std::setprecision (15)
                   << t_update_acc/(n_update*1000)
                   << " ms"
                   << "("
                   << n_update
                   <<" samples)"
                   << std::endl;
      }
      if(n_wf_adjust!=0){
         std::cout << "t_wf_adjust:             "
                   << std::setprecision (15)
                   << t_wf_adjust/(n_wf_adjust*1000)
                   << " ms"
                   << "("
                   << n_wf_adjust
                   <<" samples)"
                   << std::endl;
      }
      if(n_stepmax!=0){
         std::cout << "t_stepmax:               "
                   << std::setprecision (15)
                   << t_stepmax/(n_stepmax*1000)
                   << " ms"
                   << "("
                   << n_stepmax
                   <<" samples)"
                   << std::endl;
      }
      if(n_get_ref!=0){
         std::cout << "t_get_ref:               "
                   << std::setprecision (15)
                   << t_get_ref/(n_get_ref*1000)
                   << " ms"
                   << "("
                   << n_get_ref
                   <<" samples)"
                   << std::endl;
      }

      std::cout << std::endl;
      std::cout << "PMC CYCLE Footprint" << std::endl;
      for(int i=0; i<m_outer_steps;i++){
         std::cout << "--- outer_iteration " << i << std::endl;
         for(int j=0; j<m_nchains; j++){

            if(n_pmc_chunk[i][j]!=0){

               std::cout << "chain (dev's) " << j <<" :pmc_chunk =>";
               std::cout
                      << std::setprecision (15)
                      << t_pmc_chunk[i][j]/(n_pmc_chunk[i][j]*1000)
                      << " ms"
                      << "("
                      << n_pmc_chunk[i][j]
                      <<" samples)"
                      << std::endl;
            }
         }
      }




      double t_acc_all = t_launch_pmc_acc + t_save_acc   + t_lastsave_acc + t_update_acc +
                         t_get_ref        + t_stepmax    + t_wf_adjust;

      std::cout << std::endl;
      std::cout << "Device outer MC Checksum:  "
                << std::setprecision (15)
                << t_acc_all/(1000)
                << " ms"
                << std::endl;

      std::cout << "Device outer Real Measure:  "
                << std::setprecision (15)
                << t_outer/(1000)
                << " ms"
                << std::endl;

      for(int i=0; i<m_nchains; i++){
         m_chains[i]->print_profiling();
      }

   #endif

   #ifdef GPU_PROFILING
      print_gpu_profiling();
   #endif
}

void OCLDevice::print_gpu_profiling(){

   bool will_profile_minors = true;
   #ifdef LEGACY_MULTI_DEVICE
      if(m_device_index!=0) will_profile_minors = false;
       // For now, only allowing device 0 to not launching the qmmm_c kernel, if m_bal=0
      bool will_read_qmmm_c=true;
      bool will_write_qmmm_c=false;
      if(m_device_index == 0 and m_bal[m_device_index]==0)       will_read_qmmm_c = false;
      if(m_ndevices != 2 or not(m_device_index==1 and m_bal[0]==0)) will_write_qmmm_c = true;
   #endif

   std::cout << "GPU PROFILING (Mean times) For Device "<< m_device_index <<std::endl;

   std::cout << "Number of Samples =  "<< m_n_ocl_prof <<std::endl;
   printf("m_PROFL_monte_carlo        %ld us\n", m_PROFL_monte_carlo       /(1000*m_n_ocl_prof));
   printf("m_PROFL_qmmm_c             %ld us\n", m_PROFL_qmmm_c            /(1000*m_n_ocl_prof));
   printf("m_PROFL_qmmm_c_red_coarse  %ld us\n", m_PROFL_qmmm_c_red_coarse /(1000*m_n_ocl_prof));
   if(will_profile_minors){
      printf("m_PROFL_mm_vdwc            %ld us\n", m_PROFL_mm_vdwc           /(1000*m_n_ocl_prof));
      printf("m_PROFL_mm_vdwc_red_coarse %ld us\n", m_PROFL_mm_vdwc_red_coarse/(1000*m_n_ocl_prof));
      printf("m_PROFL_qmmm_tasks         %ld us\n", m_PROFL_qmmm_tasks        /(1000*m_n_ocl_prof));
   }
   printf("m_PROFL_closure            %ld us\n", m_PROFL_closure           /(1000*m_n_ocl_prof));
   printf("\t(NOTE: Closure kernel is slower when saving steps.\n\tThis might have not been captured here)\n");

   #ifdef LEGACY_MULTI_DEVICE
      if(will_profile_minors){
         printf("--- barrier (master-device)\n");
         printf("m_PROFL_read_part_minors              %ld ns\n", m_PROFL_read_part_minors   /(m_n_ocl_prof));
      }else{
         printf("--- barrier (slave-device)\n");
      }

      if(will_read_qmmm_c)
         printf("m_PROFL_read_part_qmmm_c              %ld ns\n", m_PROFL_read_part_qmmm_c   /(m_n_ocl_prof));

      if(!will_profile_minors)
         printf("m_PROFL_write_part_minors          %ld ns\n", m_PROFL_write_part_minors     /(m_n_ocl_prof));

      if(will_write_qmmm_c)
         printf("m_PROFL_write_part_qmmm_c             %ld ns\n", m_PROFL_write_part_qmmm_c  /(m_n_ocl_prof));

      printf("\n");
      printf("Around barrier_comn (via events)       %ld us\n", m_PROFL_barrier_comn          /(1000*m_n_ocl_prof));

      if(n_mbarrier!=0){

         std::cout << "Around barrier with PAPI (incl compute.)   "
                   << std::setprecision (15)
                   << t_mbarrier_acc/(n_mbarrier)
                   << " us"
                   << "("
                   << n_mbarrier
                   <<" samples)"
                   << std::endl;
      }
      if(n_ulock!=0){

         std::cout << "Around ULOCK Barrier (just the barrier)    "
                   << std::setprecision (15)
                   << t_ulock_acc/(n_ulock)
                   << " us"
                   << "("
                   << n_ulock
                   <<" samples)"
                   << std::endl;
      }
   #endif

   printf("Pmc_cycle (via events)                %ld us\n", m_PROFL_pmc_cycle         /(1000*m_n_ocl_prof));
}
//
// Update Profiling info
void OCLDevice::update_profiler(int nevents){
   int nsamples=0;

   //
   // TODO
   // TODO
   // The profiling buffers for OpenCL device profiling are overflowing
   // in longer runs. Fixthis.


   bool will_launch_qmmm_c = true;
   bool will_profile_minors = true; //also used for barrier distinguish
   #ifdef LEGACY_MULTI_DEVICE
      if(m_device_index!=0) will_profile_minors = false;
      // For now, only allowing device 0 to not launching the qmmm_c kernel
      if(m_device_index == 0 and m_bal[m_device_index]==0) will_launch_qmmm_c = false;
   #endif

   try{

      #if PROF_DIV_GPU_SAMPLES == 0
         nsamples = nevents;
      #else
         nsamples = nevents/PROF_DIV_GPU_SAMPLES;
      #endif

      VERBOSE_LVL_SYNC("[SYNC] Device "<<m_device_index<< " will profile, nsamples= "<<nsamples<<std::endl);

      for(int k=0; k<nsamples; k++)
      {
         unsigned long end_time[7], start_time[7], all_start;

         int event_index = k*(nevents/nsamples);

         // START & END times
         m_EVENT_monte_carlo[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time[0]);
         m_EVENT_monte_carlo[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time[0]);

         #ifdef SAFE_BENCHMARK
            if(end_time[0] < start_time[0] or end_time[0]==0 or start_time[0]==0)
               continue; //Need to continue to avoid incrementing m_n_ocl_prof
         #else
            m_PROFL_monte_carlo += (unsigned long)(end_time[0]-start_time[0]);
         #endif

         // Debug...
         //if(m_n_ocl_prof!=0)
         //std::cout << "RM! dev "<<m_device_index<<" mc = "<< (unsigned long)(end_time[0]-start_time[0]) <<"; mc_mean = " <<m_PROFL_monte_carlo/m_n_ocl_prof<<std::endl;

         all_start=start_time[0];

         if(will_launch_qmmm_c){
            m_EVENT_qmmm_c[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time[1]);
            m_EVENT_qmmm_c[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time[1]);

            #ifdef SAFE_BENCHMARK
               if(end_time[1] < start_time[1] or end_time[1]==0 or start_time[1]==0)
                  continue; //Need to continue to avoid incrementing m_n_ocl_prof
            #else
               m_PROFL_qmmm_c += (unsigned long)(end_time[1]-start_time[1]);
            #endif

            m_EVENT_qmmm_c_red_coarse[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time[2]);
            m_EVENT_qmmm_c_red_coarse[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time[2]);

            #ifdef SAFE_BENCHMARK
               if(end_time[2] < start_time[2] or end_time[2]==0 or start_time[2]==0)
                  continue; //Need to continue to avoid incrementing m_n_ocl_prof
            #else
               m_PROFL_qmmm_c_red_coarse += (unsigned long)(end_time[2]-start_time[2]);
            #endif
         }

         if(will_profile_minors){
            m_EVENT_mm_vdwc[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time[3]);
            m_EVENT_mm_vdwc[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time[3]);


            #ifdef SAFE_BENCHMARK
               if(end_time[3] < start_time[3] or end_time[3]==0 or start_time[3]==0)
                  continue; //Need to continue to avoid incrementing m_n_ocl_prof
            #else
               m_PROFL_mm_vdwc += (unsigned long)(end_time[3]-start_time[3]);
            #endif

            m_EVENT_mm_vdwc_red_coarse[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time[4]);
            m_EVENT_mm_vdwc_red_coarse[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time[4]);

            #ifdef SAFE_BENCHMARK
               if(end_time[4] < start_time[4] or end_time[4]==0 or start_time[4]==0)
                  continue; //Need to continue to avoid incrementing m_n_ocl_prof
            #else
               m_PROFL_mm_vdwc_red_coarse += (unsigned long)(end_time[4]-start_time[4]);
            #endif

            m_EVENT_qmmm_tasks[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time[5]);
            m_EVENT_qmmm_tasks[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time[5]);

            #ifdef SAFE_BENCHMARK
               if(end_time[5] < start_time[5] or end_time[5]==0 or start_time[5]==0)
                  continue; //Need to continue to avoid incrementing m_n_ocl_prof
            #else
               m_PROFL_qmmm_tasks += (unsigned long)(end_time[5]-start_time[5]);
            #endif

         }

         m_EVENT_closure[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time[6]);
         m_EVENT_closure[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time[6]);

         #ifdef SAFE_BENCHMARK
            if(end_time[6] < start_time[6] or end_time[6]==0 or start_time[6]==0)
               continue; //Need to continue to avoid incrementing m_n_ocl_prof
         #else
            m_PROFL_closure += (unsigned long)(end_time[6]-start_time[6]);
         #endif

         m_PROFL_pmc_cycle += (unsigned long) end_time[6]-all_start;

         #ifdef SAFE_BENCHMARK//In this mode, counter acc is done in the end, when no errors were caught
            m_PROFL_monte_carlo        += (unsigned long)(end_time[0]-start_time[0]);


            if(will_launch_qmmm_c){
               m_PROFL_qmmm_c             += (unsigned long)(end_time[1]-start_time[1]);
               m_PROFL_qmmm_c_red_coarse  += (unsigned long)(end_time[2]-start_time[2]);
            }
            if(will_profile_minors){
               m_PROFL_mm_vdwc            += (unsigned long)(end_time[3]-start_time[3]);
               m_PROFL_mm_vdwc_red_coarse += (unsigned long)(end_time[4]-start_time[4]);
               m_PROFL_qmmm_tasks         += (unsigned long)(end_time[5]-start_time[5]);
            }

            m_PROFL_closure            += (unsigned long)(end_time[6]-start_time[6]);
         #endif

         #ifdef LEGACY_MULTI_DEVICE
            //Profile Barrier Events TODO: not supporting SAFE_BENCHMARK

            bool will_read_qmmm_c = true;
            bool will_write_qmmm_c = false;

            // For now, only allowing device 0 to not launching the qmmm_c kernel, if m_bal=0

            if(m_device_index == 0 and m_bal[m_device_index]==0)       will_read_qmmm_c = false;
            if(m_ndevices != 2 or not(m_device_index==1 and m_bal[0]==0)) will_write_qmmm_c = true;

            unsigned long end_time_B, start_time_B, all_start_B;

            if(will_profile_minors){
               m_EVENT_read_part_minors[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time_B);
               m_EVENT_read_part_minors[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time_B);
               m_PROFL_read_part_minors += (unsigned long)(end_time_B-start_time_B);

               all_start_B = start_time_B;
            }


            if(will_read_qmmm_c){
               m_EVENT_read_part_qmmm_c[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time_B);
               m_EVENT_read_part_qmmm_c[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time_B);
               m_PROFL_read_part_qmmm_c += (unsigned long)(end_time_B-start_time_B);
            }

            if(!will_profile_minors){
               all_start_B = start_time_B;//for this devices(!=0), first event is read_qmmm_c

               m_EVENT_write_part_minors[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time_B);
               m_EVENT_write_part_minors[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time_B);
               m_PROFL_write_part_minors += (unsigned long)(end_time_B-start_time_B);

            }

            if(will_write_qmmm_c){
               m_EVENT_write_part_qmmm_c[event_index].getProfilingInfo(CL_PROFILING_COMMAND_END,&end_time_B);
               m_EVENT_write_part_qmmm_c[event_index].getProfilingInfo(CL_PROFILING_COMMAND_START,&start_time_B);
               m_PROFL_write_part_qmmm_c += (unsigned long)(end_time_B-start_time_B);
            }

            m_PROFL_barrier_comn += (unsigned long)(end_time_B-all_start_B);

         #endif

         m_n_ocl_prof++;
      }
   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLDevice::update_profiler Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
}


/**
 * Returns the final systems of all chains in a vector.
 */
vs OCLDevice::get_final_systems() {
   m_queue->finish(); // XXX this shouldn't be necessary?
   vs systems;
   try {
      for (auto it = m_chains.begin(); it != m_chains.end(); ++it) {
         (*it)->get_reference_system(0); // 0 is just dummy, doesn't matter here
         systems.push_back((*it)->m_system);
      }
      m_queue->finish(); // wait until copying is done
   } catch (const cl::Error &error){
      std::cerr << "OCLDevice::get_final_systems():: " << error.what() << "(" << error.err() << ")" << std::endl;
      throw error;
   }
   return systems;
}



/**
 * Returns the final chain results.
 */
std::vector<ChainResults*> OCLDevice::get_results() {

   std::vector<ChainResults*> results;

   #ifdef LEGACY_MULTI_DEVICE
      // In this mode, only the first chain of the first device needs to output
      results.push_back(m_chains[0]->get_results(m_run->maxmoves));
   #else
      results.reserve(m_chains.size());
      for (auto it = m_chains.begin(); it != m_chains.end(); ++it) {
         results.push_back((*it)->get_results(m_run->maxmoves));
      }
   #endif

   return results;
}
