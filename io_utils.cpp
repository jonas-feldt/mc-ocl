/**
 *
 * File: io_utils.cpp
 * Author: Jonas Feldt, João Oliveira
 *
 * Version: 2015-05-06
 *
 */

#include <iostream>
#include <stdio.h>
#include <string>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <time.h>
#include <iomanip>
#include <sys/stat.h>
#include <sstream>
#include <sys/types.h>
#include <unistd.h>
#include <libconfig.h++>
#include <unordered_map>
#include <algorithm>
#include <random>
#include <utility>
#include <cstdlib>

#include "System.h"
#include "Run.h"
#include "OPLS.h"
#include "io_utils.h"
#include "Run_Periodic.h"
#include "System_Periodic.h"
#include "OPLS_Periodic.h"
#include "options.h"
#include "OPLS_Switcher_linear.h"
#include "OPLS_Switcher.h"
#include "typedefs.h"
#include "distance_vector.h"
#include "distance_vector_periodic.h"

using namespace std;



/*
 * Reads the system(s) and creates the vvs.
 */
vvs read_periodic_systems (Run_Periodic *run)
{
   vvs systems;
   if (not run->restart) {
      // reads a single system
      System_Periodic *system = read_tinker_periodic_system (run->geometry, run->dimension_box);
      if (!system->is_sane(run->cutoff_coulomb, run->cutoff_vdw) &&
            !IGNORE_ERRORS) {
         exit(EXIT_FAILURE);
      }

      for (unsigned int i = 0; i < run->num_devices; ++i) {
         vs tmp;
         for (unsigned int j = 0; j < run->num_chains; ++j) {
            tmp.push_back(new System_Periodic(*system));
         }
         systems.push_back(tmp);
      }

   } else {
      // reads num_devices * num_chains systems
      std::ifstream in (run->geometry.c_str());
      for (unsigned int i = 0; i < run->num_devices; ++i) {
         vs tmp;
         for (unsigned int j = 0; j < run->num_chains; ++j) {
            tmp.push_back(read_tinker_periodic_system(in, run->dimension_box));
         }
         systems.push_back(tmp);
      }
   }

   return systems;
}



/**
 * Creates the run object for a periodic system. 
 *
 * @return cfg libconfig Configuration object 
 */
Run_Periodic* get_run_periodic (libconfig::Config &cfg) {

   int maxmoves, wf_updates, adjust_max_step;
   unordered_map<string, string> qm_settings;
   int print_confs;
   int print_energy = 0; // default for no extra energy file
   double stepmax, temperature, theta_max;
   double cutoff_coulomb, cutoff_vdw, dimension_box;
   int seed = 0;
   string seed_path;
   string grid_input;
   string pipe_input;
   bool has_seed_path = false;

   try {

      //
      // Method
      //

      maxmoves       = cfg.lookup("method.steps");
      wf_updates     = cfg.lookup("method.wfu-update");
      cutoff_coulomb = cfg.lookup("method.coulomb-cutoff");
      cutoff_vdw     = cfg.lookup("method.vdw-cutoff");

      //
      // Simulation
      //

      stepmax        = cfg.lookup("simulation.max-step");
      temperature    = cfg.lookup("simulation.temperature");
      theta_max      = cfg.lookup("simulation.max-theta");
      dimension_box  = cfg.lookup("simulation.periodic.dimension");
      // bool or double
      libconfig::Setting &sett_adjust = cfg.lookup("simulation.adjust-max-step");
      if (sett_adjust.getType() == libconfig::Setting::TypeBoolean &&
            (bool) sett_adjust == false) {
         adjust_max_step = 0;
      } else {
         adjust_max_step = sett_adjust;
      }

      //
      // QM
      //

      libconfig::Setting &qm_setting = cfg.lookup("qm");
      for (int i = 0; i < qm_setting.getLength(); ++i) {
         libconfig::Setting &setting = qm_setting[i];
         string tmp;
         switch (setting.getType()) {
            case libconfig::Setting::TypeInt:
               tmp = to_string(static_cast<int> (setting));
               break;
            case libconfig::Setting::TypeFloat:
               tmp = to_string(static_cast<double> (setting));
               break;
            case libconfig::Setting::TypeString:
               tmp = setting.c_str();
               break;
            default:
               //cerr << "Unsupported type for " << setting.getPath() << endl; XXX not supported in some libconfig versions
               cerr << "Unsupported type for " << setting.c_str() << endl;
               cerr << "Possible types are Int, Float, String." << endl;
               exit(EXIT_FAILURE);
         }
         string percent = "%";
         string key = percent + setting.getName() + percent;
         qm_settings[key] = tmp;
      }

      //
      // IO
      // 
      // TODO check if io is present at all in the config

      print_confs    = cfg.lookup("io.save-geometry");
      cfg.lookupValue("io.save-energy", print_energy);

      // reads or sets seed
      if (cfg.exists("io.seed")) {
         // unsigned int or string with path to seed files
         libconfig::Setting &sett_seed = cfg.lookup("io.seed");
         if (sett_seed.getType() == libconfig::Setting::TypeString) {
            seed_path = sett_seed.c_str();
            has_seed_path = true;
         } else {
            seed = sett_seed;
         }
      } else {
         random_device rd; // real random number if available
         uniform_int_distribution<int> dist(numeric_limits<int>::min(), numeric_limits<int>::max());
         seed = dist(rd);
         cfg.lookup("io").add("seed", libconfig::Setting::TypeInt) = seed; // add to cfg
      }

      // path to templates
      if (not cfg.exists("io.pipe-template")) {
         cfg.lookup("io").add("pipe-template", libconfig::Setting::TypeString) = "pipe.template";
      }
      if (not cfg.exists("io.grid-template")) {
         cfg.lookup("io").add("grid-template", libconfig::Setting::TypeString) = "grid.template";
      }
      grid_input = read_template(cfg.lookup("io.grid-template").c_str(), qm_settings);
      pipe_input = read_template(cfg.lookup("io.pipe-template").c_str(), qm_settings);

      // TODO print the templates to the output


   } catch (const libconfig::SettingNotFoundException &ex) {
      std::cerr << "Missing setting " << ex.getPath() << std::endl;
      exit(EXIT_FAILURE);
   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for " << ex.getPath() << std::endl;
      exit(EXIT_FAILURE);
   }

   Run_Periodic *run = new Run_Periodic (maxmoves, stepmax, temperature,
           theta_max, wf_updates, adjust_max_step, print_confs, cutoff_coulomb,
           cutoff_vdw, dimension_box, print_energy, grid_input, pipe_input,
           seed, has_seed_path, seed_path);
   return run;
}




/**
 * Creates a new specialized run object for FEP for periodic systems. Uses the
 * default values of the run as long as they are not overriden in the FEP
 * settings.
 *
 * @param set libconfig Setting object for FEP
 * @param default run object
 * @return run object with FEP settings
 */
Run_Periodic* get_fep_periodic (libconfig::Setting &set, Run_Periodic *original_run) {

   Run_Periodic *run = new Run_Periodic (*original_run); // init with default values
   try {
      // XXX only method values can be overriden at the moment

      // these can throw only TypeExceptions
      if (set.exists("steps"           )) set.lookupValue("steps"          , run->maxmoves         );
      if (set.exists("wfu-update"      )) set.lookupValue("wfu-update"     , run->wf_adjust        );
      if (set.exists("coulomb-cutoff"  )) set.lookupValue("coulomb-cutoff" , run->cutoff_coulomb   );
      if (set.exists("vdw-cutoff"      )) set.lookupValue("vdw-cutoff"     , run->cutoff_vdw       );

   } catch (const libconfig::SettingTypeException &ex) {
      std::cerr << "Wrong type for " << ex.getPath() << std::endl;
      exit(EXIT_FAILURE);
   }

   return run;
}



/**
 * Read the template and replace the keywords with the given values.
 *
 * @return string with replaced keywords
 */
string read_template (
      const string &filename,
      unordered_map<string, string> &qm_settings)
{
   // reads file into string
   ifstream in (filename);
   stringstream sstr;
   sstr << in.rdbuf();
   string templ = sstr.str();

   // replaces all keywords, allows nested keywords
   size_t found;
   while ((found = templ.find ("%")) != string::npos) {

      // tries to find a second % after the first one
      found++;
      if (templ.find ("%", found) == string::npos) {
         std::cerr << "Cannot find matching % in the template:" << std::endl;
         std::cerr << "--------------" << std::endl;
         std::cerr << templ;
         std::cerr << "--------------" << std::endl;
         exit(EXIT_FAILURE);
      }

      bool replaced = false;
      for (auto const &pair : qm_settings) {
         if ((found = templ.find (pair.first)) != string::npos) {
            templ.replace (found, pair.first.length(), pair.second);
            replaced = true;
         }
      }
      if (not replaced) {
         std::cerr << "Missing key in input, cannot replace all settings in template:" << std::endl;
         std::cerr << "--------------" << std::endl;
         std::cerr << templ;
         std::cerr << "--------------" << std::endl;
         exit(EXIT_FAILURE);
      }
   }

   return templ;
}



/**
 * Reads all informations of your system.
 *
 * @return mysystem object
 */
System* read_input_system (string input_file) {

   ifstream instream (input_file.c_str());
   istringstream iss;
   string line;

   for (int i = 0; i < 10; ++i) { // ignores these lines
      getline(instream, line);
   }
   getline(instream, line);
   int natom;
   iss.str (line);
   iss >> natom;
   iss.clear ();

   getline(instream, line); // ignores empty line

   int *n_12 = new int[natom];
   int (*connect)[4] = new int[natom][4];
   string *atom_name = new string[natom];
   double *rx = new double[natom];
   double *ry = new double[natom];
   double *rz = new double[natom];
   int *atom_type = new int[natom];

   for (int i = 0; i < natom; i++) {
      n_12[i] = 0;
   }

   for (int i = 0; i < natom; i++) {
      getline (instream, line);
      iss.str (line);
      iss >> atom_name[i] >> rx[i] >> ry[i] >> rz[i] >> atom_type[i];
      while (iss.peek () != EOF) {
         string tmp;
         iss >> tmp;
         tmp.erase(tmp.find_last_not_of(" \t\n\r\f\v") + 1); // XXX hack for trailing whitespace
         if (tmp.size () != 0) {
            connect[i][n_12[i]] = std::stoi (tmp) - 1;
            n_12[i] += 1;
         } else break;
      }
      iss.clear ();
   }
   instream.close ();

   return new System (natom, rx, ry, rz,atom_type, atom_name, n_12,
           connect, new DistanceVector ());
}




System_Periodic* read_tinker_periodic_system (
      string input_file,
      double box_dimension)
{
   ifstream in_stream (input_file.c_str());
   return read_tinker_periodic_system (in_stream, box_dimension);
}



System_Periodic* read_tinker_periodic_system (
      ifstream &in,
      double box_dimension)
{
   istringstream iss;
   string line;

   getline (in, line);
   int natom;
   iss.str (line);
   iss >> natom;
   iss.clear ();

   int *n_12      = new int[natom];
   int *atom_type = new int[natom];
   int (*connect)[4] = new int[natom][4];
   string *atom_name = new string[natom];

   double *rx = new double[natom];
   double *ry = new double[natom];
   double *rz = new double[natom];

   for (int i = 0; i < natom; i++) {
      n_12[i] = 0;
   }

   int dummy;
   for (int i = 0; i < natom; i++) {
      getline (in, line);
      iss.str (line);
      iss >> dummy >> atom_name[i] >> rx[i] >> ry[i] >> rz[i] >> atom_type[i];
      while (iss.peek () != EOF) {
         string tmp;
         iss >> tmp;
         tmp.erase(tmp.find_last_not_of(" \t\n\r\f\v") + 1); // XXX hack for trailing whitespace
         if (tmp.size () != 0) {
            connect[i][n_12[i]] = std::stoi (tmp) - 1;
            n_12[i] += 1;
         } else break;
      }
      iss.clear ();
   }

   auto *sys = new System_Periodic (natom, rx, ry, rz, atom_type,
           atom_name, n_12, connect, box_dimension,
           new DistanceVectorPeriodic ());
   sys->wrap(0);
   return sys;
}



/**
 * TODO Not tested...
 *
 * @param input_file
 * @return
 */
System* read_tinker_system (string input_file) {

   ifstream delimfile (input_file.c_str());
   istringstream iss;
   string line;

   getline (delimfile, line);
   int natom;
   iss.str (line);
   iss >> natom;
   iss.clear ();

   int *n_12      = new int[natom];
   int *atom_type = new int[natom];
   int (*connect)[4] = new int[natom][4];
   string *atom_name = new string[natom];

   double *rx = new double[natom];
   double *ry = new double[natom];
   double *rz = new double[natom];

   for (int i = 0; i < natom; i++) {
      n_12[i] = 0;
   }

   int dummy;
   for (int i = 0; i < natom; i++) {
      getline (delimfile, line);
      iss.str (line);
      iss >> dummy >> atom_name[i] >> rx[i] >> ry[i] >> rz[i] >> atom_type[i];
      while (iss.peek () != EOF) {
         string tmp;
         iss >> tmp;
         tmp.erase(tmp.find_last_not_of(" \t\n\r\f\v") + 1); // XXX hack for trailing whitespace
         if (tmp.size () != 0) {
            connect[i][n_12[i]] = std::stoi (tmp) - 1;
            n_12[i] += 1;
         } else break;
      }
      iss.clear ();
   }
   delimfile.close ();

   return new System (natom, rx, ry, rz, atom_type,atom_name, n_12,
           connect, new DistanceVector ());
}






void read_params_periodic (
      string params_file,
      Run_Periodic *run)
{
   // counts number of lines to initialize arrays
   int dim = 0;
   string line;
   ifstream tmp_file (params_file.c_str());
   while (getline (tmp_file, line)) {
      ++dim;
   }
   tmp_file.close ();

   // reads parameter file
   ifstream params(params_file.c_str());

   run->atom_types  .reserve(dim);
   run->epsilons    .reserve(dim);
   run->sigmas      .reserve(dim);
   run->charges     .reserve(dim);

   istringstream iss;
   for (int i = 0; i < dim; i++) {
      getline (params, line);
      iss.str(line);
      int type;
      double sigma, epsilon, charge;
      iss >> type >> sigma >> epsilon >> charge;
      run->atom_types.push_back(type);
      run->sigmas    .push_back(sigma);
      run->epsilons  .push_back(epsilon);
      run->charges   .push_back(charge);
      iss.clear ();
   }
   params.close ();
}




void read_params_periodic_tinker (
      string params_file,
      Run_Periodic *run)
{
   string line;
   ifstream params (params_file.c_str());

   if (params.fail()) {
      std::cerr << "Cannot read the parameter file " << params_file << std::endl;
      exit(EXIT_FAILURE);
   }

   // counts number of lines to initialize arrays
   int dim = 0;
   bool found_atom_part = false;
   while (getline (params, line)) {
      if (not found_atom_part &&
            line.find("Atom Type Definitions") != std::string::npos)
      {
         found_atom_part = true;
         // skip four lines
         for (unsigned int i = 0; i < 4; ++i) getline (params, line);
      } else if (found_atom_part) {
         if (line.find("atom") != std::string::npos) {
            ++dim;
         } else {
            break; // counted all atoms
         }
      }
   }

   run->atom_types  .reserve(dim);
   run->epsilons    .reserve(dim);
   run->sigmas      .reserve(dim);
   run->charges     .reserve(dim);

   istringstream iss;
   while (getline (params, line)) {
      iss.str(line);
      int type;
      double sigma, epsilon, charge;
      std::string dummy;

      if (line.find("vdw") == 0)
      {
         iss >> dummy >> type >> sigma >> epsilon;
         run->atom_types.push_back (type);
         run->sigmas    .push_back (sigma);
         run->epsilons  .push_back (epsilon);
      } else if (line.find("charge") == 0)
      {
         iss >> dummy >> type >> charge;
         run->charges.push_back (charge);
      }
      iss.clear();
   }
   params.close ();
}




/**
 * XXX Seems to read tinker files
 *
 * @param input_file
 * @return
 */
System* read_system (string input_file) {

   ifstream instream (input_file.c_str());
   istringstream iss;
   string line;

   int natom;

   getline(instream, line); iss.str(line);
   iss >> natom;

   int *n_12         = new int [natom];
   int *atom_type    = new int [natom];
   string *atom_name   = new string[natom];
   int (*connect)[4] = new int [natom][4];
   double *rx = new double[natom];
   double *ry = new double[natom];
   double *rz = new double[natom];
   for (int i = 0; i < natom; i++) { // inits n_12
      n_12[i] = 0;
   }

   for (int i = 0; i < natom; i++) {
      getline (instream, line);
      iss.str (line);
      iss >> atom_name[i] >> rx[i] >> ry[i] >> rz[i] >> atom_type[i];
      while (iss.peek () != EOF) {
         string tmp;
         iss >> tmp;
         tmp.erase(tmp.find_last_not_of(" \t\n\r\f\v") + 1); // XXX hack for trailing whitespace
         if (tmp.size () != 0) {
            connect[i][n_12[i]] = std::stoi (tmp) - 1;
            n_12[i] += 1;
         } else break;
      }
      iss.clear ();
   }
   instream.close ();

   return new System (natom, rx, ry, rz, atom_type, atom_name, n_12,
              connect, new DistanceVector ());
}




/**
 * Reads one frame of a trajectory file in xyz file format. Modifies the
 * coordinates of the given system. Does not check if the number of
 * atoms in the file is equal to the number of atoms of the system.
 * Updates the distances.
 *
 * @param trajectory
 * @param system
 */
void read_frame (ifstream &trajectory, System *system) {

   istringstream iss;
   string line;

   double *rx = system->rx;
   double *ry = system->ry;
   double *rz = system->rz;
   int dim = 0;
   getline(trajectory, line); // reads number of atoms 
   iss.str(line);
   iss >> dim;
   iss.clear();
   getline(trajectory, line); // ignores empty line

   string dummy;
   for (int i = 0; i < dim; i++) {
      getline(trajectory, line);
      iss.str(line);
      iss >> dummy >> rx[i] >> ry[i] >> rz[i];
      iss.clear();
   }
   system->UpdateDistances ();
}



/**
 * Reads the file in xyz file format and returns the new system.
 *
 * @param xyzfile
 */
System *read_xyz (std::string xyzfile) {

   ifstream instream (xyzfile.c_str());
   istringstream iss;
   string line;

   int natom = 0;
   getline(instream, line); // reads number of atoms 
   iss.str(line);
   iss >> natom;
   iss.clear();
   getline(instream, line); // ignores empty line

   string *atom_name   = new string[natom];
   double *rx = new double[natom];
   double *ry = new double[natom];
   double *rz = new double[natom];
   int *n_12 = new int[natom];
   int *atom_type    = new int [natom];
   int (*connect)[4] = new int [natom][4];
   for (int i = 0; i < natom; ++i) {
      n_12[i] = 0;
      atom_type[i] = 0;
   }

   for (int i = 0; i < natom; i++) {
      getline(instream, line);
      iss.str(line);
      iss >> atom_name[i] >> rx[i] >> ry[i] >> rz[i];
      iss.clear();
   }
   return new System (natom, rx, ry, rz, atom_type, atom_name, n_12, connect,
         new DistanceVector ());
}



/**
 * Read one frame of a trajectory file in xyz file format. Modifies the
 * coordinates of the given system. Does not check if the number of atoms
 * in the xyz file and the system are equal. Updates the distances.
 *
 * @param trajectory
 * @param system
 */
void read_periodic_frame (ifstream &trajectory, System_Periodic *system) {

   double *rx = system->rx;
   double *ry = system->ry;
   double *rz = system->rz;
   int dim = 0;
   string line;
   istringstream iss;
   getline (trajectory, line); // reads number of atoms
   iss.str(line);
   iss >> dim;
   iss.clear();
   getline (trajectory, line); // ignores comment lines

   string dummy;
   double rec = 1.0 / system->box_dimension;
   for (int i = 0; i < dim; i++) {
      getline (trajectory, line);
      iss.str (line);
      iss >> dummy >> rx[i] >> ry[i] >> rz[i];
      iss.clear ();
      rx[i] *= rec;
      ry[i] *= rec;
      rz[i] *= rec;
   }
   system->UpdateDistances ();
}




/**
 * Returns true if the file exists.
 *
 * @param filename
 * @return true or false
 */
bool file_exists(string filename)
{
   ifstream ifile(filename.c_str());
   return static_cast<bool> (ifile);
}



unsigned int file_lines(string filename)
{
   ifstream ifs(filename);
   int n = 0;
   string s;
   while (getline(ifs, s)) {
      n++;
   }
   return n;
}
