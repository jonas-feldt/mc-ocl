/*
 * File:   Integrals.h
 * Author: Jonas Feldt
 *
 * Version: 2013-10-10
 */

#ifndef INTEGRALS_H
#define	INTERFACE_MOLPRO_H
#define LATT_DAT "data_lat.dat"
#define DENS_DAT "density.out"
#include <string>

class Integrals {
public:
   Integrals(std::string density);
   virtual ~Integrals();
   double compute_contribution(System *new_system, System *old_system,
           double *charges, int molecule_id);
private:
   long int nbas;
   long int ngrp;
   long int npsh;

   double *ccs;
   double *iv1;
   double *icentres;
   double *density;
   double *ixpss;

   long int *ccoffs;
   long int *inshell;
   long int *infuns;
   long int *ioffs;
   long int *igpoffs;
   long int *ilmins;
   long int *ilmaxs;
   long int *incarts;
   long int *ncont;
};

#endif	/* INTEGRALS_H */

