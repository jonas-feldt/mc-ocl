/**
 *
 * File: Dual_OCLDevice.cpp
 * Author: Jonas Feldt
 *
 * Version: 2017-02-13
 *
 */

#include <math.h>
#include <fstream>
#include <iostream>
#include <string>

#include "options.h"
#include "Dual_OCLDevice.h"
#include "Dual_OCLChain.h"
#include "OCLDevice.h"


/*
 * Constructor
 */
Dual_OCLDevice::Dual_OCLDevice(
                        cl::Device device,
                        cl::Context *context,
                        int *ignite_isReady,
                        std::mutex *ignite_mutex,
                        std::condition_variable *ignite_slave,
                        std::condition_variable *ignite_master,
                        int n_pre_steps,
                        int nkernel,
                        int max_chains,
                        System_Periodic* system,
                        const OPLS_Periodic* opls,
                        Run_Periodic* run,
                        _FPOINT_S_ temperature,
                        int ndevices,
                        bool extra_energy_file)
   : OCLDevice(device, context, ignite_isReady, ignite_mutex, ignite_slave,
         ignite_master, n_pre_steps, nkernel, max_chains, system, opls, run,
         temperature, ndevices, extra_energy_file)
{
   m_EVENT_qmmm_cB            = new cl::Event[allocate_events];
   m_EVENT_qmmm_c_red_coarseB = new cl::Event[allocate_events];
}




/*
 * Destructor
 */
Dual_OCLDevice::~Dual_OCLDevice(){
   delete m_KERNEL_qmmm_cB;
   delete m_KERNEL_qmmm_c_red_coarseB;
   delete[] m_EVENT_qmmm_cB;
   delete[] m_EVENT_qmmm_c_red_coarseB;
}




/**
 * Creates kernels, compiles them, sets their ranges, creates chain-shared
 * buffer and sets chain-shared arguments for the kernels.
 */
void Dual_OCLDevice::setup_pmc (std::string kernel_file_closure){

   build_mol2atom0();

   // Setup Kernels
   //
   // TODO:
   //       > Add counters to count Global/Const/Local memory usage

   try{

   m_KERNEL_monte_carlo         = new OCLKernel("monte_carlo",    //Kernel name (must match function in .cl)
                                                KERNEL_FILE_MC,   //File name
                                                this);            //ptr to OCLDevice

   m_KERNEL_qmmm_c              = new OCLKernel("qmmm_c",
                                                KERNEL_FILE_QMMM_C,
                                                this);

   m_KERNEL_qmmm_cB             = new OCLKernel("qmmm_c",
                                                KERNEL_FILE_QMMM_C,
                                                this);

   m_KERNEL_qmmm_c_red_coarse   = new OCLKernel("qmmm_c_red_coarse",
                                                KERNEL_FILE_QMMM_C_RED_COARSE,
                                                this);

   m_KERNEL_qmmm_c_red_coarseB  = new OCLKernel("qmmm_c_red_coarse",
                                                KERNEL_FILE_QMMM_C_RED_COARSE,
                                                this);

   m_KERNEL_mm_vdwc             = new OCLKernel("mm_vdwc",
                                                KERNEL_FILE_MM_VDWC,
                                                this);

   m_KERNEL_mm_vdwc_red_coarse  = new OCLKernel("mm_vdwc_red_coarse",
                                                KERNEL_FILE_MM_VDWC_RED_COARSE,
                                                this);

   m_KERNEL_qmmm_tasks          = new OCLKernel("qmmm_tasks",
                                                KERNEL_FILE_QMMM_TASKS,
                                                this);

   m_KERNEL_closure             = new OCLKernel("closure",
                                                kernel_file_closure,
                                                this);
   // Compile Kernels
   //
   //

   m_KERNEL_monte_carlo->make_kernel(std::string(""));//("-cl-nv-verbose"));

   #ifdef HALF_FIXED
      m_KERNEL_qmmm_c->make_kernel(
        std::string("-D N=")+std::to_string((long long int)Q_DISTS)
      + std::string(" -D A_N=")+std::to_string((long long int)A_N)
      + std::string(" -D N_05=")+std::to_string((long long int)N_05)
      + std::string(" -D N_1=")+std::to_string((long long int)N_1)
      );//("-cl-nv-verbose"));
      m_KERNEL_qmmm_cB->make_kernel(
        std::string("-D N=")+std::to_string((long long int)Q_DISTS)
      + std::string(" -D A_N=")+std::to_string((long long int)A_N)
      + std::string(" -D N_05=")+std::to_string((long long int)N_05)
      + std::string(" -D N_1=")+std::to_string((long long int)N_1)
      );//("-cl-nv-verbose"));
   #else
      m_KERNEL_qmmm_c->make_kernel(std::string(""));//("-cl-nv-verbose"));
      m_KERNEL_qmmm_cB->make_kernel(std::string(""));//("-cl-nv-verbose"));
   #endif

   m_KERNEL_qmmm_c_red_coarse ->make_kernel(std::string(""));//("-cl-nv-verbose"));
   m_KERNEL_qmmm_c_red_coarseB->make_kernel(std::string(""));//("-cl-nv-verbose"));
   m_KERNEL_mm_vdwc           ->make_kernel(std::string(""));//("-cl-nv-verbose"));
   m_KERNEL_mm_vdwc_red_coarse->make_kernel(std::string(""));//("-cl-nv-verbose"));
   m_KERNEL_qmmm_tasks        ->make_kernel(std::string(""));//("-cl-nv-verbose"));

   std::string flags = "-D COLLECT_EVERY=" + std::to_string(m_run->print_every) + " -D N_SAVE_SYSTEMS=" + std::to_string(m_run->n_save_systems);
   if (has_extra_energy_file) {
      flags += " -D EXTRA_ENERGY -D N_SAVE_ENERGY=" +
         std::to_string(m_run->print_every / m_run->print_energy * m_run->n_save_systems) +
         " -D COLLECT_EVERY_ENERGY=" + std::to_string(m_run->print_energy);
   }
   m_KERNEL_closure           ->make_kernel(flags);

   // Setup Kernel Ranges
   //
   //
   // XXX: Some of the values here are substituted later, before the kernel calls...
   // XXX: m_chains[0] is being used because this is just the first setRanges call, doesn't matter
   m_KERNEL_monte_carlo->setRanges(1,1);
   m_KERNEL_mm_vdwc    ->setRanges(m_chains[0]->m_system->natom - m_chains[0]->m_system->molecule2atom[0].natoms, m_KERNEL_qmmm_c->get_pref_wg_mult_1d());

   // TODO items per group has to be bigger than the number of atoms in the QM molecule, ensure that
   m_KERNEL_qmmm_tasks ->setRanges(TASKS_N_THREADS,TASKS_N_THREADS/4); // work items, items per group -> 4 work groups
   m_KERNEL_closure    ->setRanges(1,1);

   // Decide local for mem_alloc, but global is dummy
   m_KERNEL_qmmm_c_red_coarse ->setRanges(0,WGSIZE_MAX_COARSE_RED_QMMM_C);
   m_KERNEL_qmmm_c_red_coarseB->setRanges(0,WGSIZE_MAX_COARSE_RED_QMMM_C);
   m_KERNEL_mm_vdwc_red_coarse->setRanges(0,WGSIZE_MAX_COARSE_RED_MM_VDWC);


   // The Ranges for the following kernels are changed during execution:
   // m_KERNEL_qmmm_c
   // m_KERNEL_mm_vdwc_red_coarse
   // m_KERNEL_qmmm_c_red_coarse

   // Setup OpenCL Buffers and other parameters
   // For Each Chain
   //

   for(int i=0; i< m_nchains; ++i){
      m_chains[i]->m_qmmm_c_grain = QMMM_C_GRAIN; // first grain for qmmm_c
      #ifndef USE_HARD_WGSIZE_QMMM_C
         m_chains[i]->m_qmmm_c_localsize = m_KERNEL_qmmm_c->get_pref_wg_mult_1d(); // first local size
      #else
         m_chains[i]->m_qmmm_c_localsize = USE_HARD_WGSIZE_QMMM_C; // first local size
      #endif

      if(i==0){
         // just to allow knowing localmemsize in local mem alloc
         m_KERNEL_qmmm_c ->setRanges((m_chains[0]->m_grid->dim/m_chains[0]->m_qmmm_c_grain)
                            , m_chains[0]->m_qmmm_c_localsize);
         Dual_OCLChain *fep_chain = (Dual_OCLChain *) m_chains[0];
         m_KERNEL_qmmm_cB->setRanges((fep_chain->m_gridB->dim/m_chains[0]->m_qmmm_c_grain)
                            , m_chains[0]->m_qmmm_c_localsize);
      }

      m_chains[i]->setup_buffers();

   }

   // Setup OpenCL Buffers
   // For All Chains (Shared)
   //

   //atom charges
   m_mass =                   cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Charges are read-only
                                  (m_const_system->natom)*sizeof(_FPOINT_S_),
                                  m_const_system->atom_mass);
                                  //(CL_MEM_COPY_HOST_PTR will copy data to the GPU in this call)

   //ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   //atom charges
   m_atomic_numbers =         cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Charges are read-only
                                  (m_const_system->natom)*sizeof(int),
                                  m_const_system->atomic_numbers);
                                  //(CL_MEM_COPY_HOST_PTR will copy data to the GPU in this call)

   //ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   //atom charges
   m_charge =                 cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Charges are read-only
                                  (m_const_system->natom)*sizeof(_FPOINT_S_),
                                  const_cast<double*>(m_opls->charge.data()));
                                  //(CL_MEM_COPY_HOST_PTR will copy data to the GPU in this call)
   #ifndef SET_FPOINT_G_DOUBLE

   v_charge_f = new float[m_const_system->natom];
   for(unsigned int a=0; a < m_const_system->natom; a++)
      v_charge_f[a] = (float) m_opls->charge[a];

   m_charge_f =                 cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Charges are read-only
                                  (m_const_system->natom)*sizeof(_FPOINT_G_),
                                  v_charge_f);
   // TODO delete[] ???
   #endif

   //ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   // epsilon and sigma vectors
   m_epsilon  =               cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  (m_const_system->natom)*sizeof(_FPOINT_S_),
                                  m_opls->epsilon);

   //ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   m_sigma  =                 cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  (m_const_system->natom)*sizeof(_FPOINT_S_),
                                  m_opls->sigma);

   //ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   /**** CONSTANT MEMORY ****/
   /*************************/
   /*************************/

   int ncbytes=0;
   //atom charges
   m_mol2atom0 =              cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Charges are read-only
                                  (m_const_system->nmol_total)*sizeof(int),
                                  m_vect_mol2atom0);
                                  //(CL_MEM_COPY_HOST_PTR will copy data to the GPU in this call)

   ncbytes+=(m_const_system->nmol_total)*sizeof(int);


   //atom to molecule vector
   m_molecule=                cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, //Molecule info is read-only
                                  (m_const_system->natom)*sizeof(_FPOINT_S_),
                                  m_const_system->molecule);
                                  //(CL_MEM_COPY_HOST_PTR will copy data to the GPU in this call)

   ncbytes+=(m_const_system->natom)*sizeof(_FPOINT_S_);

   //number of total atoms (TODO: Check if it would be faster to send as a scalar argument)
   m_n_total_atoms =          cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(unsigned int), // Just one number
                                  const_cast<unsigned int*>(&m_const_system->natom));

   ncbytes+=sizeof(int);

   //number of total atoms (TODO: Check if it would be faster to send as a scalar argument)
   m_n_total_mols =           cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(int), // Just one number
                                  &m_const_system->nmol_total);

   ncbytes+=sizeof(int);

   //Cutoffs

   m_cutoff_coulomb_rec =     cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_), // Just one number
                                  &m_chains[0]->m_grid->cutoff_coulomb_rec);
   ncbytes+=sizeof(_FPOINT_S_);

   m_cutoff_coulomb_rec_sq =  cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_), // Just one number
                                  &m_chains[0]->m_grid->cutoff_coulomb_rec_sq);
   ncbytes+=sizeof(_FPOINT_S_);


   m_cutoff_coulomb =         cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_), // Just one number
                                  &m_value_cutoff_coulomb);
   ncbytes+=sizeof(_FPOINT_S_);

   	//float versions for coulomb cutoffs
   #ifndef SET_FPOINT_G_DOUBLE
      #ifdef HALF_FIXED
         #ifdef LIGHT_FIXED
            // Just cuttoff sq in fixed
            cutoff_coulomb_rec_f    = (float) m_chains[0]->m_grid->cutoff_coulomb_rec;
            cutoff_coulomb_rec_sq_f = (float) m_chains[0]->m_grid->cutoff_coulomb_rec_sq;
            cutoff_coulomb_f        = (float) m_opls->cutoff_coulomb;
            cutoff_coulomb_sq_f     = (float) m_opls->cutoff_coulomb*m_opls->cutoff_coulomb;

            cutoff_coulomb_sq_q     = FLOAT2FIXED( cutoff_coulomb_sq_f     );

            m_cutoff_coulomb_rec_q =    cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FPOINT_G_), // Just one number
                                        &cutoff_coulomb_rec_f);
            ncbytes+=sizeof(_FPOINT_G_);

            m_cutoff_coulomb_rec_sq_q = cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FPOINT_G_), // Just one number
                                        &cutoff_coulomb_rec_sq_f);
            ncbytes+=sizeof(_FPOINT_G_);

            m_cutoff_coulomb_q =        cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FPOINT_G_), // Just one number
                                        &cutoff_coulomb_f);
            ncbytes+=sizeof(_FPOINT_G_);

            m_cutoff_coulomb_sq_q =     cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FIXED_), // Just one number
                                        &cutoff_coulomb_sq_q);
            ncbytes+=sizeof(_FIXED_);

         #else

            // Cutoffs in fixed-point ; Also, they are squared

            cutoff_coulomb_rec_f    = (float) m_chains[0]->m_grid->cutoff_coulomb_rec;
            cutoff_coulomb_rec_sq_f = (float) m_chains[0]->m_grid->cutoff_coulomb_rec_sq;
            cutoff_coulomb_f        = (float) m_opls->cutoff_coulomb;
            cutoff_coulomb_sq_f     = (float) m_opls->cutoff_coulomb*m_opls->cutoff_coulomb;

            cutoff_coulomb_rec_q    = FLOAT2FIXED( cutoff_coulomb_rec_f    );
            cutoff_coulomb_rec_sq_q = FLOAT2FIXED( cutoff_coulomb_rec_sq_f );
            cutoff_coulomb_q        = FLOAT2FIXED( cutoff_coulomb_f        );
            cutoff_coulomb_sq_q     = FLOAT2FIXED( cutoff_coulomb_sq_f     );

            m_cutoff_coulomb_rec_q =    cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FIXED_), // Just one number
                                        &cutoff_coulomb_rec_q);
            ncbytes+=sizeof(_FIXED_);

            m_cutoff_coulomb_rec_sq_q = cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FIXED_), // Just one number
                                        &cutoff_coulomb_rec_sq_q);
            ncbytes+=sizeof(_FIXED_);

            m_cutoff_coulomb_q =        cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FIXED_), // Just one number
                                        &cutoff_coulomb_q);
            ncbytes+=sizeof(_FIXED_);

            m_cutoff_coulomb_sq_q =     cl::Buffer(*(m_p_context),
                                        CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                        sizeof(_FIXED_), // Just one number
                                        &cutoff_coulomb_sq_q);
            ncbytes+=sizeof(_FIXED_);
         #endif

      #else
         cutoff_coulomb_rec_f    = (float) m_chains[0]->m_grid->cutoff_coulomb_rec;
         cutoff_coulomb_rec_sq_f = (float) m_chains[0]->m_grid->cutoff_coulomb_rec_sq;
         cutoff_coulomb_f        = (float) m_opls->cutoff_coulomb;

         m_cutoff_coulomb_rec_f =    cl::Buffer(*(m_p_context),
                                     CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                     sizeof(_FPOINT_G_), // Just one number
                                     &cutoff_coulomb_rec_f);
         ncbytes+=sizeof(_FPOINT_G_);

         m_cutoff_coulomb_rec_sq_f = cl::Buffer(*(m_p_context),
                                     CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                     sizeof(_FPOINT_G_), // Just one number
                                     &cutoff_coulomb_rec_sq_f);
         ncbytes+=sizeof(_FPOINT_G_);

         m_cutoff_coulomb_f =        cl::Buffer(*(m_p_context),
                                     CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                     sizeof(_FPOINT_G_), // Just one number
                                     &cutoff_coulomb_f);
         ncbytes+=sizeof(_FPOINT_G_);
      #endif
   #endif
      //

   m_cutoff_vdw =             cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_), // Just one number
                                  &m_value_cutoff_vdw);

   ncbytes+=sizeof(_FPOINT_S_);


   m_temperature=             cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_),
                                  &m_d_temperature);

   ncbytes+=sizeof(int);

   m_update_lists_every=      cl::Buffer(*(m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  sizeof(int),
                                  &m_n_pre_steps);

   ncbytes+=sizeof(int);

   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<<" Constant Mem Requested (bytes):" << ncbytes << std::endl);

   // INFO: Local memory is allocated via the setArg Function of each kernel

   // Match Kernel Arguments to buffers.
   // (Arguments not shared between chains are set via ::setChainArgs)
   // Scalar arguments are sent
   // With the Kernel launch call

   // Monte Carlo Kernel - m_KERNEL_monte_carlo
   ////////////////////////////////////////////////////////////////////////
   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<< " is setting up Shared m_KERNEL_monte_carlo arguments..."<<std::endl);

   m_KERNEL_monte_carlo->m_kernel.setArg(19,m_n_total_mols);
   m_KERNEL_monte_carlo->m_kernel.setArg(20,m_n_total_atoms);
   m_KERNEL_monte_carlo->m_kernel.setArg(21,m_mass);
   m_KERNEL_monte_carlo->m_kernel.setArg(22,m_mol2atom0);
   m_KERNEL_monte_carlo->m_kernel.setArg(23,m_update_lists_every);

   // QMMM COULOMB Kernel - m_KERNEL_qmmm_c
   ////////////////////////////////////////////////////////////////////////

   int local_mem_bytes = m_KERNEL_qmmm_c->m_int_localRange*sizeof(_FPOINT_G_);
   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<<" QMMM Local mem(bytes): " << local_mem_bytes << std::endl);

   m_KERNEL_qmmm_c ->m_kernel.setArg(0,cl::Local(local_mem_bytes));
   m_KERNEL_qmmm_cB->m_kernel.setArg(0,cl::Local(local_mem_bytes));

   #ifdef SET_FPOINT_G_DOUBLE // Choose float/double cutoffs
      m_KERNEL_qmmm_c ->m_kernel.setArg(5,m_charge);
      m_KERNEL_qmmm_c ->m_kernel.setArg(17,m_cutoff_coulomb_rec_sq);
      m_KERNEL_qmmm_c ->m_kernel.setArg(18,m_cutoff_coulomb_rec);
      m_KERNEL_qmmm_c ->m_kernel.setArg(19,m_cutoff_coulomb);

      m_KERNEL_qmmm_cB->m_kernel.setArg(5,m_charge);
      m_KERNEL_qmmm_cB->m_kernel.setArg(17,m_cutoff_coulomb_rec_sq);
      m_KERNEL_qmmm_cB->m_kernel.setArg(18,m_cutoff_coulomb_rec);
      m_KERNEL_qmmm_cB->m_kernel.setArg(19,m_cutoff_coulomb);
   #else
      m_KERNEL_qmmm_c ->m_kernel.setArg(5,m_charge_f);
      m_KERNEL_qmmm_cB->m_kernel.setArg(5,m_charge_f);
      #ifdef HALF_FIXED
         m_KERNEL_qmmm_c ->m_kernel.setArg(17,m_cutoff_coulomb_rec_sq_q);
         m_KERNEL_qmmm_c ->m_kernel.setArg(18,m_cutoff_coulomb_rec_q);
         m_KERNEL_qmmm_c ->m_kernel.setArg(19,m_cutoff_coulomb_q);
         m_KERNEL_qmmm_c ->m_kernel.setArg(21,m_cutoff_coulomb_sq_q);//after mol2atom buffer

         m_KERNEL_qmmm_cB->m_kernel.setArg(17,m_cutoff_coulomb_rec_sq_q);
         m_KERNEL_qmmm_cB->m_kernel.setArg(18,m_cutoff_coulomb_rec_q);
         m_KERNEL_qmmm_cB->m_kernel.setArg(19,m_cutoff_coulomb_q);
         m_KERNEL_qmmm_cB->m_kernel.setArg(21,m_cutoff_coulomb_sq_q);//after mol2atom buffer
      #else
         m_KERNEL_qmmm_c ->m_kernel.setArg(17,m_cutoff_coulomb_rec_sq_f);
         m_KERNEL_qmmm_c ->m_kernel.setArg(18,m_cutoff_coulomb_rec_f);
         m_KERNEL_qmmm_c ->m_kernel.setArg(19,m_cutoff_coulomb_f);

         m_KERNEL_qmmm_cB->m_kernel.setArg(17,m_cutoff_coulomb_rec_sq_f);
         m_KERNEL_qmmm_cB->m_kernel.setArg(18,m_cutoff_coulomb_rec_f);
         m_KERNEL_qmmm_cB->m_kernel.setArg(19,m_cutoff_coulomb_f);
      #endif
   #endif

   m_KERNEL_qmmm_c ->m_kernel.setArg(20,m_mol2atom0);
   m_KERNEL_qmmm_cB->m_kernel.setArg(20,m_mol2atom0);


   // QMMM COULOMB Reduction Kernel - COARSE - m_KERNEL_qmmm_c_coarse_red
   ////////////////////////////////////////////////////////////////////////
   local_mem_bytes = m_KERNEL_qmmm_c_red_coarse->m_int_localRange*sizeof(_FPOINT_G_);
   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<<" QMMM_C_RED Local mem(bytes): " << local_mem_bytes << std::endl);

   m_KERNEL_qmmm_c_red_coarse ->m_kernel.setArg(0,cl::Local(local_mem_bytes));
   m_KERNEL_qmmm_c_red_coarseB->m_kernel.setArg(0,cl::Local(local_mem_bytes));

   // MM COULOMB + VDW Kernel - m_KERNEL_mm_vdwc
   ////////////////////////////////////////////////////////////////////////

   local_mem_bytes = m_KERNEL_mm_vdwc->m_int_localRange*sizeof(_FPOINT_S_);
   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<<" MM_VDWC Local mem(bytes): " << local_mem_bytes << std::endl);

   m_KERNEL_mm_vdwc->m_kernel.setArg(0,cl::Local(local_mem_bytes));
   m_KERNEL_mm_vdwc->m_kernel.setArg(1,cl::Local(local_mem_bytes));
   m_KERNEL_mm_vdwc->m_kernel.setArg(6,m_charge);
   m_KERNEL_mm_vdwc->m_kernel.setArg(14,m_n_total_atoms);
   m_KERNEL_mm_vdwc->m_kernel.setArg(15,m_mol2atom0);
   m_KERNEL_mm_vdwc->m_kernel.setArg(16,m_cutoff_coulomb);
   m_KERNEL_mm_vdwc->m_kernel.setArg(17,m_cutoff_coulomb_rec);
   m_KERNEL_mm_vdwc->m_kernel.setArg(18,m_cutoff_coulomb_rec_sq);
   m_KERNEL_mm_vdwc->m_kernel.setArg(19,m_cutoff_vdw);
   m_KERNEL_mm_vdwc->m_kernel.setArg(20,m_epsilon);
   m_KERNEL_mm_vdwc->m_kernel.setArg(21,m_sigma);

   // MM COULOMB+VDW Reduction Kernel-COARSE - m_KERNEL_mm_vdwc_red_coarse
   ////////////////////////////////////////////////////////////////////////

   local_mem_bytes = m_KERNEL_mm_vdwc_red_coarse->m_int_localRange*sizeof(_FPOINT_S_);
   VERBOSE_LVL_SETUP2("[SETU] Device "<<m_device_index<<" VDWC_RED Local mem(bytes): " << local_mem_bytes << std::endl);

   m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(0,cl::Local(local_mem_bytes));
   m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(1,cl::Local(local_mem_bytes));


   //// QMMM TASKS - m_KERNEL_qmmm_tasks
   ////////////////////////////////////////////////////////////////////////

   local_mem_bytes = m_KERNEL_qmmm_tasks->m_int_localRange*sizeof(_FPOINT_S_);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(0,cl::Local(local_mem_bytes));
   m_KERNEL_qmmm_tasks->m_kernel.setArg(1,cl::Local(local_mem_bytes));
   // TODO this is not used, set dummy argument for now
   m_KERNEL_qmmm_tasks->m_kernel.setArg(2, 0);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(6,m_charge);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(7,m_atomic_numbers);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(15,m_n_total_mols);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(16,m_n_total_atoms);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(17,m_mol2atom0);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(18,m_cutoff_coulomb_rec_sq);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(19,m_cutoff_coulomb_rec);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(20,m_cutoff_coulomb);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(21,m_cutoff_vdw);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(22,m_epsilon);
   m_KERNEL_qmmm_tasks->m_kernel.setArg(23,m_sigma);

   // CLOSURE (decision step, etc)- m_KERNEL_closure
   ////////////////////////////////////////////////////////////////////////

   m_KERNEL_closure->m_kernel.setArg(17,m_mol2atom0);
   m_KERNEL_closure->m_kernel.setArg(23,m_n_total_atoms);
   m_KERNEL_closure->m_kernel.setArg(24,m_n_total_mols);
   m_KERNEL_closure->m_kernel.setArg(26,m_temperature);
   m_KERNEL_closure->m_kernel.setArg(27,m_update_lists_every);

   } catch (cl::Error &error){//openCL error catching
      std::cout<<"Dual_OCLDevice::setup_pmc Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
}




/*
 * Sets chain specific kernel arguments.
 */
void Dual_OCLDevice::setChainArgs(int chainId){

   try{

      Dual_OCLChain *chain = (Dual_OCLChain *) OCLDevice::m_chains[chainId];

      // Setup qmmm_c global size according to the chain's grid size
      // (and current chain parameters for grain, etc)
      // NOTE: The grain argument for qmmm_c is passed on, below, in this function

      m_KERNEL_qmmm_c->setRanges(
                           (chain->m_grid->dim/chain->m_qmmm_c_grain),
                            chain->m_qmmm_c_localsize);
      // B
      m_KERNEL_qmmm_cB->setRanges(
                           (chain->m_gridB->dim/chain->m_qmmm_c_grain),
                            chain->m_qmmm_c_localsize);

      // Setup kernel arguments to match the buffer of one chain
      // Scalar argument are sent with the Kernel launch call

      // Monte Carlo Kernel - m_KERNEL_monte_carlo
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_monte_carlo->m_kernel.setArg(1, chain->m_old_x);
      m_KERNEL_monte_carlo->m_kernel.setArg(2, chain->m_old_y);
      m_KERNEL_monte_carlo->m_kernel.setArg(3, chain->m_old_z);
      m_KERNEL_monte_carlo->m_kernel.setArg(4, chain->m_changed_molecule);
      m_KERNEL_monte_carlo->m_kernel.setArg(5, chain->m_changed_molecule_size);
      m_KERNEL_monte_carlo->m_kernel.setArg(6, chain->m_ch_x);
      m_KERNEL_monte_carlo->m_kernel.setArg(7, chain->m_ch_y);
      m_KERNEL_monte_carlo->m_kernel.setArg(8, chain->m_ch_z);
      m_KERNEL_monte_carlo->m_kernel.setArg(9, chain->m_random_molecule);
      m_KERNEL_monte_carlo->m_kernel.setArg(10,chain->m_rand_rotate_theta);
      m_KERNEL_monte_carlo->m_kernel.setArg(11,chain->m_q0);
      m_KERNEL_monte_carlo->m_kernel.setArg(12,chain->m_q1);
      m_KERNEL_monte_carlo->m_kernel.setArg(13,chain->m_q2);
      m_KERNEL_monte_carlo->m_kernel.setArg(14,chain->m_q3);
      m_KERNEL_monte_carlo->m_kernel.setArg(15,chain->m_random_trans_num);
      m_KERNEL_monte_carlo->m_kernel.setArg(16,chain->m_rand_trans_ex);
      m_KERNEL_monte_carlo->m_kernel.setArg(17,chain->m_rand_trans_ey);
      m_KERNEL_monte_carlo->m_kernel.setArg(18,chain->m_rand_trans_ez);

      //(float output for changed molecule)
      #ifndef SET_FPOINT_G_DOUBLE
         m_KERNEL_monte_carlo->m_kernel.setArg(24, chain->m_ch_x_f);
         m_KERNEL_monte_carlo->m_kernel.setArg(25, chain->m_ch_y_f);
         m_KERNEL_monte_carlo->m_kernel.setArg(26, chain->m_ch_z_f);
      #endif

      // QMMM COULOMB Kernel - m_KERNEL_qmmm_c
      ////////////////////////////////////////////////////////////////////////

      m_KERNEL_qmmm_c ->m_kernel.setArg(1, chain->m_qmmm_c_grain);
      m_KERNEL_qmmm_cB->m_kernel.setArg(1, chain->m_qmmm_c_grain); // B

      #ifndef SET_FPOINT_G_DOUBLE
         m_KERNEL_qmmm_c ->m_kernel.setArg(2, chain->m_old_x_f);
         m_KERNEL_qmmm_c ->m_kernel.setArg(3, chain->m_old_y_f);
         m_KERNEL_qmmm_c ->m_kernel.setArg(4, chain->m_old_z_f);
         m_KERNEL_qmmm_cB->m_kernel.setArg(2, chain->m_old_x_f); // B, uses MM from A
         m_KERNEL_qmmm_cB->m_kernel.setArg(3, chain->m_old_y_f);
         m_KERNEL_qmmm_cB->m_kernel.setArg(4, chain->m_old_z_f);
      #else
         m_KERNEL_qmmm_c ->m_kernel.setArg(2, chain->m_old_x);
         m_KERNEL_qmmm_c ->m_kernel.setArg(3, chain->m_old_y);
         m_KERNEL_qmmm_c ->m_kernel.setArg(4, chain->m_old_z);
         m_KERNEL_qmmm_cB->m_kernel.setArg(2, chain->m_old_x); // B, uses MM from A
         m_KERNEL_qmmm_cB->m_kernel.setArg(3, chain->m_old_y);
         m_KERNEL_qmmm_cB->m_kernel.setArg(4, chain->m_old_z);
      #endif
         //(arg5 is m_charges, setup in the main setup buffers function)
      m_KERNEL_qmmm_c ->m_kernel.setArg(6, chain->m_changed_molecule);
      m_KERNEL_qmmm_c ->m_kernel.setArg(7, chain->m_changed_molecule_size);
      m_KERNEL_qmmm_cB->m_kernel.setArg(6, chain->m_changed_molecule); // B
      m_KERNEL_qmmm_cB->m_kernel.setArg(7, chain->m_changed_molecule_size);

      // Choose which ch_mol buffers to use
      #ifndef SET_FPOINT_G_DOUBLE
         m_KERNEL_qmmm_c ->m_kernel.setArg(8, chain->m_ch_x_f);
         m_KERNEL_qmmm_c ->m_kernel.setArg(9, chain->m_ch_y_f);
         m_KERNEL_qmmm_c ->m_kernel.setArg(10,chain->m_ch_z_f);
         m_KERNEL_qmmm_cB->m_kernel.setArg(8, chain->m_ch_x_f); // B, uses same as A
         m_KERNEL_qmmm_cB->m_kernel.setArg(9, chain->m_ch_y_f);
         m_KERNEL_qmmm_cB->m_kernel.setArg(10,chain->m_ch_z_f);
      #else
         m_KERNEL_qmmm_c ->m_kernel.setArg(8, chain->m_ch_x);
         m_KERNEL_qmmm_c ->m_kernel.setArg(9, chain->m_ch_y);
         m_KERNEL_qmmm_c ->m_kernel.setArg(10,chain->m_ch_z);
         m_KERNEL_qmmm_cB->m_kernel.setArg(8, chain->m_ch_x); // B, uses same as A
         m_KERNEL_qmmm_cB->m_kernel.setArg(9, chain->m_ch_y);
         m_KERNEL_qmmm_cB->m_kernel.setArg(10,chain->m_ch_z);
      #endif

      m_KERNEL_qmmm_c ->m_kernel.setArg(11, chain->m_grid_rx);
      m_KERNEL_qmmm_c ->m_kernel.setArg(12, chain->m_grid_ry);
      m_KERNEL_qmmm_c ->m_kernel.setArg(13, chain->m_grid_rz);
      m_KERNEL_qmmm_c ->m_kernel.setArg(14, chain->m_grid_charge);
      m_KERNEL_qmmm_c ->m_kernel.setArg(15, chain->m_grid_size);
      m_KERNEL_qmmm_c ->m_kernel.setArg(16, chain->m_qmmm_c_energy);
      m_KERNEL_qmmm_cB->m_kernel.setArg(11, chain->m_grid_rxB); // B, separate grid
      m_KERNEL_qmmm_cB->m_kernel.setArg(12, chain->m_grid_ryB);
      m_KERNEL_qmmm_cB->m_kernel.setArg(13, chain->m_grid_rzB);
      m_KERNEL_qmmm_cB->m_kernel.setArg(14, chain->m_grid_chargeB);
      m_KERNEL_qmmm_cB->m_kernel.setArg(15, chain->m_grid_sizeB);
      m_KERNEL_qmmm_cB->m_kernel.setArg(16, chain->m_qmmm_c_energyB);

      // QMMM COULOMB Reduction Kernel - COARSE - m_KERNEL_qmmm_c_coarse_red
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_qmmm_c_red_coarse ->m_kernel.setArg(3, chain->m_qmmm_c_energy);
      m_KERNEL_qmmm_c_red_coarseB->m_kernel.setArg(3, chain->m_qmmm_c_energyB); // B, separate energy

      // MM COULOMB + VDW Kernel - m_KERNEL_mm_vdwc
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_mm_vdwc->m_kernel.setArg( 3, chain->m_old_x);
      m_KERNEL_mm_vdwc->m_kernel.setArg( 4, chain->m_old_y);
      m_KERNEL_mm_vdwc->m_kernel.setArg( 5, chain->m_old_z);
      m_KERNEL_mm_vdwc->m_kernel.setArg( 7, chain->m_changed_molecule);
      m_KERNEL_mm_vdwc->m_kernel.setArg( 8, chain->m_changed_molecule_size);
      m_KERNEL_mm_vdwc->m_kernel.setArg( 9, chain->m_ch_x);
      m_KERNEL_mm_vdwc->m_kernel.setArg(10, chain->m_ch_y);
      m_KERNEL_mm_vdwc->m_kernel.setArg(11, chain->m_ch_z);
      m_KERNEL_mm_vdwc->m_kernel.setArg(12, chain->m_mm_vdwc_energy_vdw);
      m_KERNEL_mm_vdwc->m_kernel.setArg(13, chain->m_mm_vdwc_energy_col);

      // MM COULOMB+VDW Reduction Kernel-COARSE - m_KERNEL_mm_vdwc_red_coarse
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(4, chain->m_mm_vdwc_energy_vdw);
      m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(5, chain->m_mm_vdwc_energy_col);
      m_KERNEL_mm_vdwc_red_coarse->m_kernel.setArg(6, chain->m_minors_energy);

      // QMMM TASKS (decision step, etc)- m_KERNEL_qmmm_tasks
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_qmmm_tasks->m_kernel.setArg( 3, chain->m_old_x);
      m_KERNEL_qmmm_tasks->m_kernel.setArg( 4, chain->m_old_y);
      m_KERNEL_qmmm_tasks->m_kernel.setArg( 5, chain->m_old_z);
      m_KERNEL_qmmm_tasks->m_kernel.setArg( 8, chain->m_changed_molecule);
      m_KERNEL_qmmm_tasks->m_kernel.setArg( 9, chain->m_changed_molecule_size);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(10, chain->m_ch_x);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(11, chain->m_ch_y);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(12, chain->m_ch_z);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(13, chain->m_sqrt_box_dim);
      m_KERNEL_qmmm_tasks->m_kernel.setArg(14, chain->m_minors_energy);

      // CLOSURE (decision step, etc)- m_KERNEL_closure
      ////////////////////////////////////////////////////////////////////////
      m_KERNEL_closure->m_kernel.setArg( 1, chain->m_minors_energy);
      m_KERNEL_closure->m_kernel.setArg( 2, chain->m_qmmm_c_energy);
      m_KERNEL_closure->m_kernel.setArg( 3, chain->m_qmmm_c_energyB);// B, separate energy
      m_KERNEL_closure->m_kernel.setArg( 4, chain->m_qm_energy);
      m_KERNEL_closure->m_kernel.setArg( 5, chain->m_qm_energyB);// B, separate energy
      m_KERNEL_closure->m_kernel.setArg( 6, chain->m_energy_qm_gp);
      m_KERNEL_closure->m_kernel.setArg( 7, chain->m_energy_old);
      m_KERNEL_closure->m_kernel.setArg( 8, chain->m_energy_oldB);// B, separate energy
      m_KERNEL_closure->m_kernel.setArg( 9, chain->m_energy_vdw_qmmm);
      m_KERNEL_closure->m_kernel.setArg(10, chain->m_energy_mm);
      m_KERNEL_closure->m_kernel.setArg(11, chain->m_number_accepted);
      m_KERNEL_closure->m_kernel.setArg(12, chain->m_accepted_config_data);
      m_KERNEL_closure->m_kernel.setArg(13, chain->m_accepted_energy_data);
      m_KERNEL_closure->m_kernel.setArg(14, chain->m_old_x);
      m_KERNEL_closure->m_kernel.setArg(15, chain->m_old_y);
      m_KERNEL_closure->m_kernel.setArg(16, chain->m_old_z);
      m_KERNEL_closure->m_kernel.setArg(18, chain->m_changed_molecule);
      m_KERNEL_closure->m_kernel.setArg(19, chain->m_changed_molecule_size);
      m_KERNEL_closure->m_kernel.setArg(20, chain->m_ch_x);
      m_KERNEL_closure->m_kernel.setArg(21, chain->m_ch_y);
      m_KERNEL_closure->m_kernel.setArg(22, chain->m_ch_z);
      m_KERNEL_closure->m_kernel.setArg(25, chain->m_random_boltzman_acc);

      #ifndef SET_FPOINT_G_DOUBLE // second reference (float)
         // TODO don't have a Dual Density float kernel yet, still wrong setArgs !!!
         m_KERNEL_closure->m_kernel.setArg(27, chain->m_old_x_f);
         m_KERNEL_closure->m_kernel.setArg(28, chain->m_old_y_f);
         m_KERNEL_closure->m_kernel.setArg(29, chain->m_old_z_f);
         m_KERNEL_closure->m_kernel.setArg(30, chain->m_old_means);
         m_KERNEL_closure->m_kernel.setArg(31, chain->m_old_variance);
      #else
	 m_KERNEL_closure->m_kernel.setArg(28, chain->m_old_means);
         m_KERNEL_closure->m_kernel.setArg(29, chain->m_old_variance);
      #endif

   } catch (cl::Error &error) {//openCL error catching
      std::cout<<"Dual_OCLDevice::switchChainArgs Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
}





/*
 * Launches all kernels of one iteration of the PMC cycle.
 *
 * Step must start at 0 for the first step. Compared to the base class method
 * it enqueues the additional qmmm_c Kernel.
 */
void Dual_OCLDevice::launch_pmc_cycle(int inner_step, int outer_step, int memory_offset){

   std::vector<cl::Event> qmmm_c_dependencies;
   std::vector<cl::Event> mm_vdwc_dependencies;
   std::vector<cl::Event> qmmm_tasks_dependencies;
   std::vector<cl::Event> closure_dependencies;

   int global_step = outer_step*m_inner_steps + inner_step;

   // Except for the first inner iteration of each
   // PMC Cycle, wait for previous PMC Cycle step
   if(inner_step != 0 && memory_offset != 0){
      VERBOSE_LVL_UDEBUG("Chaining previous pmc_cycle");
      m_mc_dependencies.push_back(m_EVENT_closure[memory_offset-1]);
   }

   try{

      // MONTE CARLO STEP
      // ****************
      VERBOSE_LVL_UDEBUG("Launching MC");

      launch_mc (&m_mc_dependencies,         //In  dependecies
                 &m_EVENT_monte_carlo[memory_offset], //Out dependency
                 global_step);


      VERBOSE_LVL_UDEBUG("Chaining next kernels to MC");

      // Kernels that depend directly on monte carlo
      qmmm_c_dependencies    .push_back(m_EVENT_monte_carlo[memory_offset]);
      mm_vdwc_dependencies   .push_back(m_EVENT_monte_carlo[memory_offset]);
      qmmm_tasks_dependencies.push_back(m_EVENT_monte_carlo[memory_offset]);

      // QMMM COULOMB
      // ************
      VERBOSE_LVL_UDEBUG("Launching QMMM C");

      // A
      launch_qmmm_c (&qmmm_c_dependencies,                   // In dependecies
                     m_KERNEL_qmmm_c,                        // kernel
                     m_KERNEL_qmmm_c_red_coarse,             // reduction kernel
                     &m_EVENT_qmmm_c_red_coarse[memory_offset], // Out dependencies
                     &m_EVENT_qmmm_c[memory_offset]);           // extra buffer for internal dependencies

      // B
      launch_qmmm_c (&qmmm_c_dependencies,                   // same In dependecies
                     m_KERNEL_qmmm_cB,                       // extra kernel
                     m_KERNEL_qmmm_c_red_coarseB,            // extra reduction kernel
                     &m_EVENT_qmmm_c_red_coarseB[memory_offset],// extra Out dependencies
                     &m_EVENT_qmmm_cB[memory_offset]);          // extra buffer for internal dependencies

      // MM VAN DER WAALS + COULOMB
      // **************************
      VERBOSE_LVL_UDEBUG("Launching MM VDWC");

      launch_mm_vdwc (&mm_vdwc_dependencies,              //In  dependecies
                      &m_EVENT_mm_vdwc_red_coarse[memory_offset],  //Out dependencies
                      memory_offset);
      // Other QMMM Tasks
      // ****************
      VERBOSE_LVL_UDEBUG("Launching QMMM Tasks");

      launch_qmmm_tasks(&qmmm_tasks_dependencies,  //In  dependecies
                        &m_EVENT_qmmm_tasks[memory_offset]);//Out dependencies

      // Closure Kernel, decision step
      // *****************************
      VERBOSE_LVL_UDEBUG("Launching Closure");

      closure_dependencies.push_back(m_EVENT_qmmm_c_red_coarse [memory_offset]); // A
      closure_dependencies.push_back(m_EVENT_qmmm_c_red_coarseB[memory_offset]); // B

      closure_dependencies.push_back(m_EVENT_mm_vdwc_red_coarse[memory_offset]);
      closure_dependencies.push_back(m_EVENT_qmmm_tasks[memory_offset]);

      launch_closure (&closure_dependencies, &m_EVENT_closure[memory_offset],global_step);

   } catch (cl::Error &error) {//openCL error catching
      std::cout<<"Dual_OCLDevice::launch_pmc_cycle Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }

   m_mc_dependencies.clear();
}



