/**
 *
 * File: options.h
 * Author: Jonas Feldt, Sebastiao Miranda
 *
 * Version: 2015-01-06
 *
 */
 
#ifndef OPTIONS_H_INCLUDED
#define OPTIONS_H_INCLUDED


/******************************************/
// Run-Mode Options
/******************************************/

   // Currently, there are three types of run: ( XXX: update with newer run types! )
   //
   // 1) Normal run. Will run PMC cycles and Molpro for grid update.
   //    Will attempt to call several molpros, according to chain options
   //    and use the value of wf_adjust to control call frequency.
  //    : COMMENT STANDALONE & DUMMY_PIPES_STANDALONE
   //
   // 2) Standalone Dummy-pipes run. Will run PMC cycles and use dummy code to
   //    simulate the molpro processes. Same as above but the grid updates
   //    are just a pipe echo of the grid, and some fake computation to add
   //    some time. A grid.dat file must be suplied in the run folder.
   //    : UNCOMMENT STANDALONE & DUMMY_PIPES_STANDALONE
   //
   // 3) Completely standalone run. Will just run 1 outer iteration, without
   //    any grid updates. A grid.dat file must be suplied in the run folder.
   //    : UNCOMMENT STANDALONE ; COMMENT DUMMY_PIPES_STANDALONE

   //#define STANDALONE
  

   // Use Multi devices for 1 chain only TODO: update above descripton for this mode
   //#define LEGACY_MULTI_DEVICE //Incompatible with LEGACY_HETERO_DEVICE ; XXX: incompatible with multi chain

   //#define BALANCING 2000 //load balancing (every N iterations)
   
   //#define TIME_PROBE // Probes global time measurements with PAPI (requires BASE_PROFILING enabled!)
                      // TIME_PROBE outputs a matplotlib-compatible python script to plot data
   #ifdef TIME_PROBE
      #define MICRO_PROBE(probe) probe.push_back(PAPI_get_real_usec());
   #else
      #define MICRO_PROBE(probe)
   #endif
  
   //#define SETCORES // use taskset to set core for molpro slaves 
   //#define SAFE_FINISH//!!!required for ocl-cpu runs AND Hawaii runs.
   #define SAFE_BENCHMARK//required for devices that may return bad profilling data, TODO: not implemented for barrier profiling
   //#define WHIP_FINISH 100//Experiment. Enhances time in some machines.
   

   //#define BALANCING_PROBE 30000// costly, yet fine probe instructions to benchmark the balancing algorithm; XXX: possibly depracated OR/ too costly requires BALANCING enabled ; Also, BALANCING_PROBE outputs a csv file with balancing details!
                              // Probe every N iters

   //XXX XXX ALERT
   #define FIX_THIS_MULT 1 //Please fix this... it's a multiplier to allow having a bigger reduction vector size than
                            //the one allocated with seed balancing. This is a terrible solution. Putting to 10 gives a 0.1 to 0.9 balancing seed margin
 
   #define GRID_FILE "/grid.dat" // used in non-standalone runs 
   #define GRID_FILE_TARGET "/grid-target.dat" // target grid for dual density PMC
   
   // [AUTOMATIC, DONT EDIT] 
   // Decide if wf adjust will be used or not
   #ifndef STANDALONE
      #ifndef WILL_WF_ADJUST
         #define WILL_WF_ADJUST
      #endif
   #endif

   //XXX: temporary debug/avoidbug options
   #define MAX_GRID_SIZE_MULT 2 
       //  ^^^ Maximum number of points per grid is this number * initial grid size
       // This is currently hardcoded, it might cease to be if reallocating
       // in gpu is not a considerable overhead. Investigate.

/******************************************/
// Legacy Options
/******************************************/
   
   #define IGNORE_ERRORS false 
   #define PRINT_S      "%10i %10i %20.3f %20.3f %10.5f %10.5f %10.5f %10.5f\n"
   #define PRINT_S_FEP  "%10i %10i %20.3f %20.3f %10.5f %10.5f %10.5f %10.5f %20.3f %20.3f %10.5f %10.5f %10.5f\n"
   #define PRINT_S_DUAL "%10i %10i %20.3f %20.3f %10.3f %10.3f %20.3f %20.3f %10.3f %20.3f %10.3f\n"

/***************************/
// Acceleration Parameters
/***************************/
   
//*** OpenCL Parameter Configuration
   // (If dynamic adjustment is enabled, these will be just the
   //  init values. TODO: Dynamic ajudstment is not yet implemented)

   // KERNEL MC (monte carlo)
      
      // WORK-ITEMS (GLOBAL): 1
      // WGSIZE     (LOCAL ): 1

   // KERNEL QMMM_C (qmmm_c grid) 
      
      // WORK-ITEMS (GLOBAL): grid_size(grid.dat) / QMMM_C_GRAIN
      #define QMMM_C_GRAIN 2 // number of gridpoints per wi

      // WGSIZE (LOCAL): Either preferred wgsize query * WG_HINT_MULT
      //                 or USE_HARD_WGSIZE_QMMM_C (if defined)
      #define WG_HINT_MULT 4
      //#define USE_HARD_WGSIZE_QMMM_C 128
         
   // KERNEL QMMM_C_REDUCTION (qmmm_c reduction) 
      
      // WORK-ITEMS (GLOBAL): QMMM_C global / QMMM_C local      
      //                      So it depends on previous kernel, but
      //                      will never surpass this value:     
      #define WGSIZE_MAX_COARSE_RED_QMMM_C 128

      // WGSIZE (LOCAL): Same as Global, i.e, there is only 1 
      //                 Work group for this kernel

   // KERNEL VDWC (mm_vdw & mm_c) 
      
      // WORK-ITEMS (GLOBAL): mm_size(*.inp file)
      // WGSIZE (LOCAL): wgsize query * WG_HINT_MULT 
   
   // KERNEL VDWC_REDUCTION (mm_vdw & mm_c reduction) 
      
      // WORK-ITEMS (GLOBAL): VDWC global / VDWC local      
      //                      So it depends on previous kernel, but
      //                      will never surpass this value:     
      #define WGSIZE_MAX_COARSE_RED_MM_VDWC 128

      // WGSIZE (LOCAL): Same as Global, i.e, there is only 1 
      //                 Work group for this kernel

   // KERNEL TASKS (qmmm_c nuclei & qmmm_vdwc)
      
      // WORK-ITEMS (GLOBAL): TASKS_N_THREADS
      // WGSIZE (LOCAL): TASKS_N_THREADS/2 
      //                 (i.e, 2 work-groups always) 
      //
      #define TASKS_N_THREADS 512 // Unless really sure of what 
			          // your doing, do not change.

   // KERNEL Closure (decision and save)
      // TODO: This kernel needs to be vectorized.
      //       Please use workitems for step saving.       
      // WORK-ITEMS (GLOBAL): 1
      // WGSIZE     (LOCAL ): 1

   // (Legacy parameters, possibly deprecated)        
   #define N_MAX_FINE_RED_QMMM_C 1   
   #define N_MAX_FINE_RED_MM_VDWC 1
   
//** Device and Chain Options
 
   // Hints for device selection

   #define TARGET_VENDOR "NVIDIA Corporation"
   //#define TARGET_VENDOR "GenuineIntel"
   //#define TARGET_VENDOR "Intel(R) Corporation"
   //#define TARGET_VENDOR "Advanced Micro Devices, Inc."
   //#define TARGET_VENDOR "ANY"

   //#define TARGET_VENDOR_AVOID "NVIDIA Corporation"
   //#define TARGET_VENDOR_AVOID "GenuineIntel"
   #define TARGET_VENDOR_AVOID "Intel(R) Corporation"
   //#define TARGET_VENDOR_AVOID "Advanced Micro Devices, Inc."
   //#define TARGET_VENDOR_AVOID "NONE"
   
   //XXX: Make a better device grabber...
   //#define ALT_DEV0 //use second device as device 0 
   //#define FORCE_GRID_CL_TRUE //Enable for forcing blocking grid write
   //#define FORCE_LATTICE_CL_TRUE //Enable for forcing blocking system lattice.

/***************************/
// OpenCL Managing Options 
/***************************/

   // Use portable cl 1.2 wrapper provided in project folder
   //#define PORTABLE_CL_WRAPPER
   // XXX: README:
   //
   //  To allow compiling for "mariana" (or other machines with dubious ocl1.2 support):
   //
   //
   // USE #define CL_USE_DEPRECATED_OPENCL_1_1_APIS in the begining cl12cpp.h
   // USE #undef CL_VERSION_1_2 in cl12cpp.h , after the opencl header includes 
   // MAY need to change library linking in makefile. For mariana it works with:
   //     INC := -L/opt/intel/opencl/lib64/ -I/opt/intel/opencl/include/CL/

   // The number of energy parameters one wishes to save 
   // and print per PRINT_EVERY(now run->print_every) step
   #define N_ENERGY_SAVE_PARAMS 8
   #define N_ENERGY_SAVE_PARAMS_FEP 13
   #define N_ENERGY_SAVE_PARAMS_DUAL 11

   // Set/Unset Debug levels
   // (Verbosity)
   #define SET_VERBOSE_LVL_WARN // Most relevant Warning messages XXX: some prints should be ported to this one!
   #define SET_VERBOSE_LVL_INIT
   //#define SET_VERBOSE_LVL_ALIVE     //every step alive print
   #define SET_VERBOSE_LVL_SETUP       //stable setup prints
   #define SET_VERBOSE_LVL_SETUP2      //stable setup prints (costum endl)
   //#define SET_VERBOSE_LVL_SETUP_R      //For Kernel set ranges verbosity
   //#define SET_VERBOSE_LVL_UDEBUG      //experimental debug
   //#define SET_VERBOSE_LVL_UDEBUG2   //experimental debug2
   //#define SET_VERBOSE_LVL_UDEBUG3   //experimental debug2
   //#define SET_VERBOSE_LVL_SYNC        //thread Sync Prints
   //#define SET_VERBOSE_LVL_THREADS   //thread management Prints
   //#define SET_VERBOSE_LVL_TRANSFERS   //data transfers debug
   
   //Profiling Options
   //

 //  #define BASE_PROFILING // Profile using PAPI
 //  #define PROF_EVERY 1   // Profiling Frequency
 //  #define GPU_PROFILING  // Profile openCL device using openCL events
   
   // Fraction of number of samples for GPU profiling
   // (i.e n_samples = 1/PROF_DIV_GPU_SAMPLES * n_wf_adjust)
   // If 0 then it will sample every gpu step
   #define PROF_DIV_GPU_SAMPLES 0

   // Out of order OCL
   #define OCL_OUT_OF_ORDER
   
   // If you need to do localized debug profiling, use this:
   //
   //double t_start=0,t_end=0,t_acc=0;int nacc=0;PROFILE_FIRST(PROF_EVERY,t_start);        
   // ....code....
   //PROFILE(PROF_EVERY,t_acc,t_end,t_start,nacc);std::cout << "\nPROBE: "  << std::setprecision (15) << t_acc/(1000)<< " ms" << std::endl; 
   //


   // Select data type for QMMM_C grid computation
   // (If uncommented, it will be set to double)
   #define SET_FPOINT_G_DOUBLE
   
   //#define HALF_FIXED // XXX EXPERIMENT : !!! Requires _FPOINT_G_=float
   //#define LIGHT_FIXED //use F.P only for dists_sq calcs....
   #define Q_DISTS 28 // Number of bits in fraction for dists and cutoffs
      //XXX: consts shouldbe created from above def Q_DISTS 
   #define A_N  0xF0000000
   #define N_05 0x08000000
   #define N_1  0x10000000

   //
   // Fixed point macros
   typedef int _FIXED_;
   
   #define FLOAT2FIXED(num_f) (_FIXED_)(num_f * N_1) 
 
   #define _FPOINT_S_ double     // Datatype for the MM  part 
   
   //[AUTO SELECTION FROM OPTIONS]
   #ifdef SET_FPOINT_G_DOUBLE
      #define _FPOINT_G_ double  // Datatype for the grid part
   #else
      #define _FPOINT_G_ float   // Datatype for the grid part
   #endif
   
   // Choose kernels, according to data types
   
   #ifdef SET_FPOINT_G_DOUBLE
      #define KERNEL_FILE_MC                 "monte_carlo_d.cl"
      #define KERNEL_FILE_QMMM_C             "qmmm_c_d_optD.cl"
      #define KERNEL_FILE_QMMM_C_RED_COARSE  "qmmm_c_red_coarse_d_new.cl"
      #define KERNEL_FILE_CLOSURE            "closure_d_new.cl"
      #define KERNEL_FILE_CLOSURE_FES        "closure_d_fes.cl"
      #define KERNEL_FILE_CLOSURE_FEP        "fep_closure_d_new.cl"
      #define KERNEL_FILE_CLOSURE_FEP_FES    "fep_closure_fes.cl"
      #define KERNEL_FILE_CLOSURE_DUAL       "dual_density_closure_d.cl"
   #else
    
      #if defined(HALF_FIXED) && defined(LIGHT_FIXED)
            #define KERNEL_FILE_MC  "monte_carlo_nfi_light.cl"
      #else
         #define KERNEL_FILE_MC  "monte_carlo_f.cl"
      #endif
      
      #ifdef LEGACY_MULTI_DEVICE
         #define KERNEL_FILE_QMMM_C   "qmmm_c_f_optD_gvar.cl" //optE is worse than optD ; _fe_ is  experimental half float half fixed point
      #else
         #ifdef HALF_FIXED
            #ifdef LIGHT_FIXED
               #define KERNEL_FILE_QMMM_C "qmmm_c_nfi_light.cl"
            #else
               #define KERNEL_FILE_QMMM_C "qmmm_c_nfi_optD.cl"
            #endif
         #else
            #define KERNEL_FILE_QMMM_C "qmmm_c_f_optD.cl" //optD is the last stable here
         #endif
      #endif

      #define KERNEL_FILE_QMMM_C_RED_COARSE  "qmmm_c_red_coarse_f.cl"
      
      #if defined(HALF_FIXED) && defined(LIGHT_FIXED)
         #define KERNEL_FILE_CLOSURE            "closure_nfi_light.cl"
      #else
         #define KERNEL_FILE_CLOSURE            "closure_f.cl"
      #endif

   #endif
   
   
   #define KERNEL_FILE_MM_VDWC            "mm_vdwc_d.cl"
   #define KERNEL_FILE_MM_VDWC_RED_COARSE "mm_vdwc_red_coarse_d.cl"
   #define KERNEL_FILE_QMMM_TASKS         "qmmm_tasks_d.cl"
   #define KERNEL_FILE_QMMM_TASKS_FEP     "fep_qmmm_tasks_d.cl"

   // [AUTOMATIC, DONT EDIT] Macros
   
      //macro for unique_lock mutex barrier

   #define ULOCK_BARRIER(m_mutex,m_cond,m_count,m_max,f_msg,msg) unique_lock<mutex> ulock(*m_mutex); *m_count += 1;m_cond->notify_all();while (*m_count<m_max){m_cond->wait(ulock);}if(f_msg==1)cout<<msg;ulock.unlock(); 

   #ifdef SET_VERBOSE_LVL_WARN                
   #define VERBOSE_LVL_WARN(out) std::cout << out;
   #else
   #define VERBOSE_LVL_WARN(out)
   #endif   

   #ifdef SET_VERBOSE_LVL_ALIVE                
   #define VERBOSE_LVL_ALIVE(out) std::cout << out;
   #else
   #define VERBOSE_LVL_ALIVE(out)
   #endif   
   
   #ifdef SET_VERBOSE_LVL_INIT                
   #define VERBOSE_LVL_INIT(out) std::cout << out;
   #else
   #define VERBOSE_LVL_INIT(out)
   #endif     
     
   #ifdef SET_VERBOSE_LVL_SETUP                  
   #define VERBOSE_LVL_SETUP(out) std::cout <<out<< std::endl;
   #else
   #define VERBOSE_LVL_SETUP(out)
   #endif   

   #ifdef SET_VERBOSE_LVL_UDEBUG                 
   #define VERBOSE_LVL_UDEBUG(out) std::cout <<out<< std::endl;
   #else
   #define VERBOSE_LVL_UDEBUG(out)
   #endif                                 
      
   #ifdef SET_VERBOSE_LVL_UDEBUG2                
   #define VERBOSE_LVL_UDEBUG2(out) std::cout <<out<< std::endl;
   #else
   #define VERBOSE_LVL_UDEBUG2(out)
   #endif  
   
   #ifdef SET_VERBOSE_LVL_UDEBUG3                
   #define VERBOSE_LVL_UDEBUG3(out) std::cout << out;
   #else
   #define VERBOSE_LVL_UDEBUG3(out)
   #endif  

   #ifdef SET_VERBOSE_LVL_TRANSFERS            
   #define VERBOSE_LVL_TRANSFERS(out) std::cout <<out;
   #else
   #define VERBOSE_LVL_TRANSFERS(out)
   #endif  
   
   #ifdef SET_VERBOSE_LVL_THREADS            
   #define VERBOSE_LVL_THREADS(out) std::cout <<out;
   #else
   #define VERBOSE_LVL_THREADS(out)
   #endif  
   
   #ifdef SET_VERBOSE_LVL_SYNC 
   #define VERBOSE_LVL_SYNC(out) std::cout <<out;
   #else
   #define VERBOSE_LVL_SYNC(out)
   #endif  
   
   #ifdef SET_VERBOSE_LVL_SETUP2
   #define VERBOSE_LVL_SETUP2(out) std::cout <<out;
   #else
   #define VERBOSE_LVL_SETUP2(out)
   #endif  

   // Macros for profilling
   #ifdef BASE_PROFILING            
   #define PROFILE_FIRST(number_steps,t_start) if((number_steps)%PROF_EVERY==0) t_start = PAPI_get_real_usec();
   #define PROFILE(number_steps,t_acc,t_end,t_start,nacc) if((number_steps)%PROF_EVERY==0){t_end = PAPI_get_real_usec();t_acc+=(t_end-t_start); nacc++;}
   #else
   #define PROFILE(number_steps,t_acc,t_end,t_start,nacc)
   #define PROFILE_FIRST(number_steps,t_start)
   #endif
   
#endif //OPTIONS_H_INCLUDED
