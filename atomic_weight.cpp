#include <iostream>
#include <string>
#include <map>

#include "atomic_weight.h"

Atom_weight::~Atom_weight()
{
}

Atom_weight::Atom_weight()
{

   mass["LP"]= 0.0;  // Lone Pair for TIP5P
   mass["M"] = 0.0;  // M for TIP3P
   mass["CH"] = 13.01864; // OPLS-AA C+H
   mass["H"]= 1.00794;
   mass["He"]= 4.002602;
   mass["Li"]= 6.941;
   mass["Be"]= 9.012182;
   mass["B"]= 10.811;
   mass["C"]= 12.0107;
   mass["N"]= 14.0067;
   mass["O"]= 15.9994;
   mass["F"]= 18.9984032;
   mass["Ne"]= 20.1797;
   mass["Na"]= 22.98976928;
   mass["Mg"]= 24.3050;
   mass["Al"]= 26.9815386;
   mass["Si"]= 28.0855;
   mass["P"]= 30.973762;
   mass["S"]= 32.065;
   mass["Cl"]= 35.453;
   mass["Ar"]= 39.948;
   mass["K"]= 39.0983;
   mass["Ca"]= 40.078;
   mass["Sc"]= 44.955912;
   mass["Ti"]= 47.867;
   mass["V"]= 50.9415;
   mass["Cr"]= 51.9961;
   mass["Mn"]= 54.938045;
   mass["Fe"]= 55.845;
   mass["Co"]= 58.933195;
   mass["Ni"]= 58.6934;
   mass["Cu"]= 63.546;
   mass["Zn"]= 65.38;
   mass["Ga"]= 69.723;
   mass["Ge"]= 72.64;
   mass["As"]= 74.92160;
   mass["Se"]= 78.96;
   mass["Br"]= 79.904;
   mass["Kr"]= 83.798;
   mass["Rb"]= 85.4678;
   mass["Sr"]= 87.62;
   mass["Y"]= 88.90585;
   mass["Zr"]= 91.224;
   mass["Nb"]= 92.90638;
   mass["Mo"]= 95.96;
   mass["Tc"]= 98.0;
   mass["Ru"]= 101.07;
   mass["Rh"]= 102.90550;
   mass["Pd"]= 106.42;
   mass["Ag"]= 107.8682;
   mass["Cd"]= 112.411;
   mass["In"]= 114.818;
   mass["Sn"]= 118.710;
   mass["Sb"]= 121.760;
   mass["Te"]= 127.60;
   mass["I"]= 126.90447;
   mass["Xe"]= 131.293;
   mass["Cs"]= 132.9054519;
   mass["Ba"]= 137.327;
   mass["La"]= 138.90547;
   mass["Ce"]= 140.116;
   mass["Pr"]= 140.90765;
   mass["Nd"]= 144.242;
   mass["Pm"]= 145.0;
   mass["Sm"]= 150.36;
   mass["Eu"]= 151.964;
   mass["Gd"]= 157.25;
   mass["Tb"]= 158.92535;
   mass["Dy"]= 162.500;
   mass["Ho"]= 164.93032;
   mass["Er"]= 167.259;
   mass["Tm"]= 168.93421;
   mass["Yb"]= 173.054;
   mass["Lu"]= 174.9668;
   mass["Hf"]= 178.49;
   mass["Ta"]= 180.94788;
   mass["W"]= 183.84;
   mass["Re"]= 186.207;
   mass["Os"]= 190.23;
   mass["Ir"]= 192.217;
   mass["Pt"]= 195.084;
   mass["Au"]= 196.966569;
   mass["Hg"]= 200.59;
   mass["Tl"]= 204.3833;
   mass["Pb"]= 207.2;
   mass["Bi"]= 208.98040;
   mass["Po"]= 209.0;
   mass["At"]= 210.0;
   mass["Rn"]= 222.0;
   mass["Fr"]= 223.0;
   mass["Ra"]= 226.0;
   mass["Ac"]= 227.0;
   mass["Th"]= 232.03806;
   mass["Pa"]= 231.03588;
   mass["U"]= 238.02891;
   mass["Np"]= 237.0;
   mass["Pu"]= 244.0;
   mass["Am"]= 243.0;
   mass["Cm"]= 247.0;
   mass["Bk"]= 247.0;
   mass["Cf"]= 251.0;
   mass["Es"]= 252.0;
   mass["Fm"]= 257.0;
   mass["Md"]= 258.0;
   mass["No"]= 259.0;
   mass["Lr"]= 262.0;
   mass["Rf"]= 265.0;
   mass["Db"]= 268.0;
   mass["Sg"]= 271.0;
   mass["Bh"]= 272.0;
   mass["Hs"]= 270.0;
   mass["Mt"]= 276.0;
   mass["Ds"]= 281.0;
   mass["Rg"]= 280.0;
   mass["Cn"]= 285.0;
   mass["Uut"]= 284.0;
   mass["Uuq"]= 289.0;
   mass["Uup"]= 288.0;
   mass["Uuh"]= 293.0;
   mass["Uus"]= 292.0;
   mass["Uuo"]= 294.0;
}


double Atom_weight::find_mass(std::string atom_name) {

   double atom_mass;

   atom_mass = mass.find(atom_name)->second;

   return atom_mass;
}
