/**
 *
 * File: OCLmanager_FES.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-10-06
 *
 */

#include <string>

#include "OCLmanager.h"
#include "OCLmanager_FES.h"
#include "OCLDevice_FES.h"
#include "options.h"


OCLmanager_FES::OCLmanager_FES (
      int n_pre_steps,
      int nkernel,
      int ndevices,
      int nchains,
      const vvs &systems,
      const OPLS_Periodic* opls,
      Run_Periodic* run,
      vvg &grids,
      int config_data_size,
      int energy_data_size,
      _FPOINT_S_ temperature,
      double theta_max,
      double stepmax,
      const vvd &energies_qm,
      _FPOINT_S_ energy_qm_gp,
      const vvd &energies_mm,
      const vvd &energies_vdw_qmmm,
      const vvd &energies_old,
      Pipe_Host **hosts,
      std::string basename)
   : OCLmanager (n_pre_steps, nkernel, ndevices, nchains, systems, opls,
      run, grids, config_data_size, energy_data_size, temperature, theta_max,
      stepmax, energies_qm, energy_qm_gp, energies_mm, energies_vdw_qmmm, energies_old,
      hosts, basename)
{
   #ifdef SET_FPOINT_G_DOUBLE
      m_kernel_file_closure = KERNEL_FILE_CLOSURE_FES;
   #else
      m_kernel_file_closure = "not-existing-kernel";
   #endif
}



OCLmanager_FES::~OCLmanager_FES(){
}



/**
 * Does not work with legacy multi option.
 */
OCLDevice* OCLmanager_FES::create_device (const int i)
{
   return new OCLDevice_FES (
         m_captured_device[i],
         m_captured_context[i],
         &v_ignite_isReady[i],//Sync variables
         &v_ignite_mutex[i],  //Sync variables
         &v_ignite_slave[i],  //Sync variables
         &ignite_master,      //Sync variables
         m_n_pre_steps,
         m_nkernel,
         m_nchains_per_device,     //Max chains per device
         m_systems[0][0],     // devices get all the same "const" system
         m_opls,
         m_run,
         m_temperature,
         m_ndevices,
         has_extra_energy_file);
}

