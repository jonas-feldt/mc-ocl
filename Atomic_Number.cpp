/*
 * File:   Atomic_Number.cpp
 * Author: Jonas Feldt
 *
 * Version: 2013-08-20
 */

#include <string>
#include <map>

#include "Atomic_Number.h"



Atomic_Number::Atomic_Number () {
   z["LP"]= 0; // Lone Pair for TIP5P
   z["M"]=  0; // M for TIP3P
   z["CH"]= 7; // OPLS-UA C+H
   z["H"]=  1;
   z["He"]= 2;
   z["Li"]= 3;
   z["Be"]= 4;
   z["B"]=  5;
   z["C"]=  6;
   z["N"]=  7;
   z["O"]=  8;
   z["F"]=  9;
   z["Ne"]= 10;
   z["Na"]= 11;
   z["Mg"]= 12;
   z["Al"]= 13;
   z["Si"]= 14;
   z["P"]=  15;
   z["S"]=  16;
   z["Cl"]= 17;
   z["Ar"]= 18;
   z["K"]=  19;
   z["Ca"]= 20;
   z["Sc"]= 21;
   z["Ti"]= 22;
   z["V"]=  23;
   z["Cr"]= 24;
   z["Mn"]= 25;
   z["Fe"]= 26;
   z["Co"]= 27;
   z["Ni"]= 28;
   z["Cu"]= 29;
   z["Zn"]= 30;
   z["Ga"]= 31;
   z["Ge"]= 32;
   z["As"]= 33;
   z["Se"]= 34;
   z["Br"]= 35;
   z["Kr"]= 36;
   z["Rb"]= 37;
   z["Sr"]= 38;
   z["Y"]=  39;
   z["Zr"]= 40;
   z["Nb"]= 41;
   z["Mo"]= 42;
   z["Tc"]= 43;
   z["Ru"]= 44;
   z["Rh"]= 45;
   z["Pd"]= 46;
   z["Ag"]= 47;
   z["Cd"]= 48;
   z["In"]= 49;
   z["Sn"]= 50;
   z["Sb"]= 51;
   z["Te"]= 52;
   z["I"]=  53;
   z["Xe"]= 54;
   z["Cs"]= 55;
   z["Ba"]= 56;
   z["La"]= 57;
   z["Ce"]= 58;
   z["Pr"]= 59;
   z["Nd"]= 60;
   z["Pm"]= 61;
   z["Sm"]= 62;
   z["Eu"]= 63;
   z["Gd"]= 64;
   z["Tb"]= 65;
   z["Dy"]= 66;
   z["Ho"]= 67;
   z["Er"]= 68;
   z["Tm"]= 69;
   z["Yb"]= 70;
   z["Lu"]= 71;
   z["Hf"]= 72;
   z["Ta"]= 73;
   z["W"]=  74;
   z["Re"]= 75;
   z["Os"]= 76;
   z["Ir"]= 77;
   z["Pt"]= 78;
   z["Au"]= 79;
   z["Hg"]= 80;
   z["Tl"]= 81;
   z["Pb"]= 82;
   z["Bi"]= 83;
   z["Po"]= 84;
   z["At"]= 85;
   z["Rn"]= 86;
   z["Fr"]= 87;
   z["Ra"]= 88;
   z["Ac"]= 89;
   z["Th"]= 90;
   z["Pa"]= 91;
   z["U"]=  92;
   z["Np"]= 93;
   z["Pu"]= 94;
   z["Am"]= 95;
   z["Cm"]= 96;
   z["Bk"]= 97;
   z["Cf"]= 98;
   z["Es"]= 99;
   z["Fm"]= 100;
   z["Md"]= 101;
   z["No"]= 102;
   z["Lr"]= 103;
   z["Rf"]= 104;
   z["Db"]= 105;
   z["Sg"]= 106;
   z["Bh"]= 107;
   z["Hs"]= 108;
   z["Mt"]= 109;
   z["Ds"]= 110;
   z["Rg"]= 111;
   z["Cn"]= 112;
   z["Uut"]= 113;
   z["Uuq"]= 114;
   z["Uup"]= 115;
   z["Uuh"]= 116;
   z["Uus"]= 117;
   z["Uuo"]= 118;
}



Atomic_Number::~Atomic_Number () {
}



/**
 * Returns the atomic number of the element.
 * @param element
 * @return atomic number Z
 */
int Atomic_Number::get_z (std::string element) {
   return z.find (element)->second;
}

