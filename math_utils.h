#ifndef MATH_UTILS_H_INCLUDED
#define MATH_UTILS_H_INCLUDED

class Rnd_gen{

public:
   
   Rnd_gen(int newseed);
   ~Rnd_gen();
   
   void seed(int newseed);
   int int_rand();
   double uniform_double();
   double uniform_double_a_b(double a, double b);
   int uniform_int_a_b(int a, int b);
   
private:
   int m_current;
};

double boltzmann_factor(double temp_K, double delta_energy);
double length (double *v);

#endif // MATH_UTILS_H_INCLUDED
