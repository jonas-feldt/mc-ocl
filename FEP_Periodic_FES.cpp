/*
 * File:   FEP_Periodic_FES.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-10-30
 */

#include "FEP_Periodic_FES.h"
#include "FEP_OCLmanager_FES.h"



FEP_Periodic_FES::~FEP_Periodic_FES ()
{
}




FEP_OCLmanager* FEP_Periodic_FES::create_manager (
               int n_pre_steps, 
               int nkernel, 
               int ndevices, 
               int nchains, 
               const vvs &systemsA,
               System_Periodic* systemB,
               OPLS_Periodic* opls, 
               OPLS_Periodic* oplsB,
               Run_Periodic* run, 
               vvg &gridsA,
               vvg &gridsB,
               int config_data_size, 
               int energy_data_size, 
               _FPOINT_S_ temperature,
               double theta_max,
               double stepmax,   
               vvd &energies_qmA,
               vvd &energies_qmB,
               _FPOINT_S_ energy_qm_gp,
               vvd &energies_mmA,
               vvd &energies_vdw_qmmmA,
               vvd &energies_vdw_qmmmB,
               vvd &energies_oldA,
               vvd &energies_oldB,
               Pipe_Host **hosts,
               std::string basename)
{
   return new FEP_OCLmanager_FES(
         n_pre_steps, 
         nkernel, 
         ndevices, 
         nchains, 
         systemsA,
         systemB,
         opls, 
         oplsB,
         run, 
         gridsA,
         gridsB,
         config_data_size, 
         energy_data_size, 
         temperature,
         theta_max,
         stepmax,   
         energies_qmA,
         energies_qmB,
         energy_qm_gp,
         energies_mmA,
         energies_vdw_qmmmA,
         energies_vdw_qmmmB,
         energies_oldA,
         energies_oldB,
         hosts,
         basename);
}
