/*
 * File:    Abstract_Switcher.h
 * Author:  Jonas Feldt
 *
 */

#ifndef ABSTRACT_SWITCHER_H
#define ABSTRACT_SWITCHER_H

#include <cstdio>

class OPLS;

class Abstract_Switcher {

   public:

      virtual void switch_params (const int unsigned frame, OPLS *opls, FILE *fout) = 0;
      virtual ~Abstract_Switcher();

};

inline Abstract_Switcher::~Abstract_Switcher() {};

#endif /* ABSTRACT_SWITCHER_H */
