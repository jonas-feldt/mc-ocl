/**
 *
 * File: OCLDevice_FES.h
 * Author: Jonas Feldt
 *
 * Version: 2015-11-05
 *
 */

#ifndef OCLDEVICE_FES_H_INCLUDED
#define OCLDEVICE_FES_H_INCLUDED

#define __CL_ENABLE_EXCEPTIONS

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

#include <string>

#include "options.h"

class OCLmanager;
class mutex;
class condition_variable;
class System_Periodic;
class Run_Periodic;
class OPLS_Periodic;

class OCLDevice_FES : public OCLDevice {

public:

   OCLDevice_FES (
             cl::Device device,
             cl::Context *context,
             int *ignite_isReady,
             std::mutex *ignite_mutex,
             std::condition_variable *ignite_slave,
             std::condition_variable *ignite_master,
             int n_pre_steps,
             int nkernel,
             int max_chains,
             System_Periodic* system,
             const OPLS_Periodic* opls,
             Run_Periodic* run,
             _FPOINT_S_ temperature,
             int num_devices,
             bool extra_energy_file);

   virtual ~OCLDevice_FES() override;

   virtual void setup_pmc(std::string kernel_file_closure) override;

protected:

   cl::Buffer buffer_fes;

   double factor_fes;

};

#endif //OCLDEVICE_FES_H_INCLUDED



