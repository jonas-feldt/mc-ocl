/**
 *
 * File: qmmm_c_f_optB.cl
 * Author: Sebastiao Miranda
 *
 * Version: 2014-02-25
 *
 */
#pragma OPENCL EXTENSION cl_khr_fp64: enable

#define E2_ANG_TO_KJMOL 1389.354867249

//TODO: Check impact of "inline" in the number of registers
inline double compute_minimal_distance_sq (double rxi, double ryi, double rzi, double rxj, double ryj, double rzj) {
   
   double diffx = rxi - rxj;
   double diffy = ryi - ryj;
   double diffz = rzi - rzj;

   //TODO: Checkout if this code is efficient.
   diffx -=round(diffx);
   diffy -=round(diffy);
   diffz -=round(diffz);
   
   double distsq = diffx * diffx + diffy * diffy + diffz * diffz;

   return distsq;
}

__kernel
void qmmm_c(

   // Local Memory
   __local double *scratch,

   // Kernel Scalar Arguments
   // (number of atoms for this work-item)
   int m_grid_points,
   
   // MM molecule old x,y,z
   __global double *old_x,
   __global double *old_y,
   __global double *old_z,
   __global double *charges,

    // Changed MM molecule
   __global int      *changed_molecule,
   __global int      *changed_molecule_size,
   __global double    *ch_x,
   __global double    *ch_y,
   __global double    *ch_z,
   
   // Global buffers with GRID data
   __global double   *grid_rx,
   __global double   *grid_ry,
   __global double   *grid_rz,
   __global double   *grid_charge,
   __global int     *grid_size,
   
   // Work Group energy result
   __global double   *result_wg_energy,
   
   // Run constants
   __constant double *cutoff_coulomb_rec_sq,
   __constant double *cutoff_coulomb_rec,
   __constant double *cutoff,

   // System Constants
   __constant int   *mol2atom0
   
   ){
   
   __private int    k                 ;
   __private int    global_id         ;
   __private int    local_id          ;
   __private int    local_size        ; 
   __private int    grid_point        ;

   __private double  dist_new          ;
   __private double  dist_old          ;  
   __private double  dist_new_r        ;
   __private double  dist_old_r        ;   
   __private double  energy = 0.0      ;
   __private double  qs                ;
   __private double  this_grid_rx      ;
   __private double  this_grid_ry      ;
   __private double  this_grid_rz      ; 
   __private double  this_grid_charge  ;
   __private double  this_cutoff       ;
   
   __private double  this_ch_old_rx    ;
   __private double  this_ch_old_ry    ;
   __private double  this_ch_old_rz    ;
   __private double  this_ch_old_charge;
    
   // Molecule atoms
   __private int start_atom_i          ;
   __private int end_atom              ;
   
   // Get id's
   local_id  = get_local_id(0);
   global_id  = get_global_id(0);
   local_size = get_local_size(0);
   
   //printf("QMMM-C:%d %d %d",global_id,*grid_size,m_grid_points);

   //XXXXXXXXXXXXXXXXXXXXXXXXXXXX
   // if(global_id==0){
   
      // printf("changed_molecule = %d\n",*changed_molecule);
      // for(int j=0;j<*changed_molecule_size;j++){   
         // printf("x[%d] %lf\n",j, ch_x    [j]);
         // printf("y[%d] %lf\n",j, ch_y    [j]);
         // printf("z[%d] %lf\n",j, ch_z    [j]);
         // printf("c[%d] %lf\n",j, charges [start_atom_i+j]);
      // }
   
   // }
   //XXXXXXXXXXXXXXXXXXXXXXXXXXXX
   
   // If this is a valid work-item
   if(global_id < (*grid_size)/m_grid_points){

      // Find Out atoms belonging to the molecule
      start_atom_i = mol2atom0[*changed_molecule]; 

      //For each grid point of this work-item
      for(int i=0;i<m_grid_points;i++){
    
         grid_point = global_id*m_grid_points+i;
         //collect grid data to private memory
         this_grid_rx    = grid_rx    [grid_point];
         this_grid_ry    = grid_ry    [grid_point];
         this_grid_rz    = grid_rz    [grid_point];
         this_grid_charge= grid_charge[grid_point];
       
         this_cutoff     = *cutoff;
         
         // Loop through each changed atom in molecule
         for(int j=0;j<*changed_molecule_size;j++){   
     
            this_ch_old_rx       = old_x    [start_atom_i+j]; 
            this_ch_old_ry       = old_y    [start_atom_i+j]; 
            this_ch_old_rz       = old_z    [start_atom_i+j];
            this_ch_old_charge   = charges  [start_atom_i+j];
            
            dist_new = compute_minimal_distance_sq(ch_x[j],     ch_y[j],     ch_z[j],
                                                   this_grid_rx,this_grid_ry,this_grid_rz);
                                        
            dist_old = compute_minimal_distance_sq(this_ch_old_rx, this_ch_old_ry, this_ch_old_rz,
                                                   this_grid_rx,   this_grid_ry,   this_grid_rz);
                                        
            
            dist_new_r = sqrt(dist_new);
            dist_old_r = sqrt(dist_old);
            if (dist_new_r < this_cutoff && dist_old_r < this_cutoff) {
   
               qs = -this_ch_old_charge*this_grid_charge; //this_ch_old_charge is vector ff_charges in the serial code

               energy += qs * (rsqrt(dist_new) - rsqrt(dist_old) +
                          (*cutoff_coulomb_rec_sq) * (dist_new_r - dist_old_r));
                          
            }else if (dist_new_r < this_cutoff && dist_old_r >= this_cutoff) {     
               
               qs = -this_ch_old_charge*this_grid_charge; //this_ch_old_charge is vector ff_charges in the serial code
            
               energy += qs * (rsqrt(dist_new) - (*cutoff_coulomb_rec) +
                          (*cutoff_coulomb_rec_sq) * (dist_new_r - this_cutoff));
              
            }else if (dist_old_r < this_cutoff) { // dist_new >= cutoff && dist_old < cutoff
             
               qs = -this_ch_old_charge*this_grid_charge; //this_ch_old_charge is vector ff_charges in the serial code

               energy -= qs * (rsqrt(dist_old) - (*cutoff_coulomb_rec) +
                       (*cutoff_coulomb_rec_sq) * (dist_old_r - this_cutoff ));

            }
         }
         // Take grid charge into account   
      }
   }//else, wont contribute to reduction: energy is 0 by default.
     
   //Write private result to local memory
   scratch[local_id] = energy*E2_ANG_TO_KJMOL;

   // ENERGY G
   //printf("QMMM-C:%d e = %e\n",global_id, energy*E2_ANG_TO_KJMOL);
   
   barrier(CLK_LOCAL_MEM_FENCE);
   
   //Now all work-items of the same work group must accumulate their energy.
   for(int offset = local_size/2; offset > 0 ;offset >>= 1){
      if(local_id < offset){
         scratch[local_id] = scratch[local_id + offset] + scratch[local_id];
      }
      barrier(CLK_LOCAL_MEM_FENCE);
   }

   //Work item with id 0 writes the final result of the work-group
   if (local_id == 0) {
      result_wg_energy[get_group_id(0)] = scratch[0];
   } 
   
}
	
