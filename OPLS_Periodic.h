/*
 * File:   OPLS_Periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2015-04-09
 */

#ifndef OPLS_PERIODIC_H
#define	OPLS_PERIODIC_H

#include <vector>
#include <string>

#include "OPLS.h"
#include "typedefs.h"

class System_Periodic;
class Run_Periodic;

class OPLS_Periodic : public OPLS {
public:

   const double box_dimension;

   const double cutoff_coulomb;
   const double cutoff_coulomb_rec; // 1/R_c
   const double cutoff_coulomb_rec_sq; // (1/R_c)^2
   const double sqrt_box_dimension;
   
   const double cutoff_vdw;

   OPLS_Periodic(
         const System *system,
         Run_Periodic *run);

   OPLS_Periodic (const OPLS_Periodic &orig);

   ~OPLS_Periodic() override;

   double energy_vdw_qmmm (DistanceCall GetDistance) const override;
   double energy_vdw_qmmm (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const override;
   double energy_vdw_qmmm (System *mm_system, System *qm_system) override;

   const double* get_normal_charges() const override;
   
   void set_sigma (
         const unsigned int index,
         const double value) override;

   void set_charge (
         const unsigned int index,
         const double value) override;

   void print (std::string outfile) override;


protected:
   double energy_coulomb(DistanceCall GetDistance) const override;
   double energy_coulomb(DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const override;

   double energy_vdw (DistanceCall GetDistance) const override;
   double energy_vdw (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const override;

   double energy_coulomb_mm (DistanceCall GetDistance) const override;
   double energy_coulomb_mm (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const override;

   double energy_vdw_mm (DistanceCall GetDistance) const override;
   double energy_vdw_mm (DistanceCall GetDistance, DistanceCall GetDistance_old, int changed_molecule) const override;

   const std::vector<double> normal_charges;
};

#endif	/* OPLS_PERIODIC_H */

