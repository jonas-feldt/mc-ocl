/*
 * File:   RDF_Angle_Periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2015-05-12
 */

#ifndef RDF_ANGLE_PERIODIC_H
#define	RDF_ANGLE_PERIODIC_H

#include "RDF_Periodic.h"


class RDF_Angle_Periodic : public RDF_Periodic {
public:
   RDF_Angle_Periodic(std::string trajectory_file,
         int first_ID, int second_ID, double delta, int max_bin, int first_frame,
         int last_frame, std::string output_file, double box_dim,
         const unsigned int num_chains, const double delta_angle,
         const int max_bin_angle, const int id);
   virtual ~RDF_Angle_Periodic() override;

protected:
   virtual void analyze() override;
   virtual void finalize() override;
   virtual void save_output() override;


private:
   int id;
   double delta_angle;
   int max_bin_angle;
   long **histogram;
   double **normalized;

};

#endif	/* RDF_PERIODIC_H */

