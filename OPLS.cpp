/**
 *
 * File: OPLS.cpp
 * Author: Jonas Feldt, João Oliveira
 *
 * Version: 2015-04-09
 *
 */

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <algorithm>

#include "OPLS.h"
#include "options.h"

#include "System.h"
#include "geom_utils.h"
#include "Constants.h"
#include "typedefs.h"

#define COULOMB_CONST 332.063806190589  // in Ang kcal/mol


/**
 * Constructor
 *
 * @param system properties of the class System
 * @param number_of_lines of the parameter file
 * @param atyp_param atom types from force field parameter file
 * @param epsilon_param epsilon in kcal/mol from force field parameter file
 * @param sigma_param sigma in Angs from force field parameter file
 * @param charge_param charges from force field parameter file
 *
 */
OPLS::OPLS (
      const System *system,
      std::vector<int> &atyp_param,
      std::vector<double> &epsilon_param,
      std::vector<double> &sigma_param,
      std::vector<double> &charge_param,
      unsigned int solute_molecules)
   :  natom (system->natom),
      molecule(system->molecule, system->molecule + system->natom),
      molecule2atom(system->molecule2atom),
      system (system),
      solute_molecules (solute_molecules),
      start_solvent (solute_molecules > 0 ? molecule2atom[solute_molecules - 1].atom_id[molecule2atom[solute_molecules - 1].natoms - 1] + 1 : 0)
{

   epsilon  = new double[system->natom];
   sigma    = new double[system->natom];
   charge.resize(system->natom);

   for (unsigned int i = 0; i < system->natom; i++) {
      bool found = false;
      for (unsigned int k = 0; k < atyp_param.size(); k++) {
         if (system->atom_type[i] == atyp_param[k]) {
            epsilon[i] = epsilon_param[k];
            sigma  [i] = sigma_param  [k];
            charge [i] = charge_param [k];
            found = true;
            break; // break inner loop
         }
      }

      if (!found) {
         std::cerr << "ERROR: Could not find parameters for atom " << i << "." << std::endl; 
         exit (EXIT_FAILURE);
      }
   }
}



/**
 * Copy constructor
 */
OPLS::OPLS (const OPLS &orig)
   : charge (orig.charge),
   natom (orig.natom),
   molecule (orig.molecule),
   molecule2atom (orig.molecule2atom),
   solute_molecules (orig.solute_molecules),
   start_solvent (orig.start_solvent)
{
   epsilon = new double[natom];
   sigma   = new double[natom];
   for (int i = 0; i < natom; ++i) {
      epsilon[i] = orig.epsilon[i];
      sigma  [i] = orig.sigma  [i];
   }
}



/**
 * Destructor
 */
OPLS::~OPLS () {
   delete[] epsilon;
   delete[] sigma;
}




/**
 * Calculates the total energy of the system
 *
 * @param distances distances matrix
 */
double OPLS::energy (DistanceCall GetDistance) const {
   double energy = 0.0;
   energy += energy_vdw     (GetDistance);
   energy += energy_coulomb (GetDistance);
   return energy;

}



/**
 * Calculates the change of the total energy of the system.
 *
 * @param distances distances matrix
 * @param distances_old old distances matrix
 * @param changed_molecule id of the changed molecule
 */
double OPLS::energy (DistanceCall GetDistance, DistanceCall GetDistance_old,
        int changed_molecule) const
{
   double energy = 0.0;
   energy += energy_vdw     (GetDistance, GetDistance_old, changed_molecule);
   energy += energy_coulomb (GetDistance, GetDistance_old, changed_molecule);
   return energy;

}




/**
 * Calculates the van der Waals energy of the total system
 *
 * @param distances matrix for distances
 *
 */
double OPLS::energy_vdw (DistanceCall GetDistance) const
{
   double energy = 0.0;
   for (int i = 0; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++) {
         if (molecule[i] != molecule[j]) { // TODO 1,4-VdW...

            double dist = GetDistance (i, j);
            double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            double sigmas = sigma[i] * sigma[j];
            double t = sigmas / (dist * dist);
            double V6 = t * t * t;
            double V12 = V6 * V6;
            energy += sqrt_epsilon * (V12 - V6);
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}



/**
 * Calculates the change of the Van der Waals energy of the system.
 *
 * @param distances distance matrix
 * @param distances_old old distance matrix
 * @param changed_molecule id of the moved molecule
 */
double OPLS::energy_vdw (
      DistanceCall GetDistance,
      DistanceCall GetDistance_old,
      int changed_molecule) const
 {
   double energy = 0.0;
   for (int i = 0; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++) {
         if        (molecule[i] != changed_molecule
                 && molecule[j] != changed_molecule) continue;

         if (molecule[i] != molecule[j]) { // TODO 1,4-VdW...

            double dist = GetDistance (i, j);
            double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            double sigmas = sigma[i] * sigma[j];
            double dist_o = GetDistance_old (i, j);

            double t = sigmas / (dist * dist);
            double V6 = t * t * t;
            double V12 = V6 * V6;

            double t_o = sigmas / (dist_o * dist_o);
            double V6_o = t_o * t_o * t_o;
            double V12_o = V6_o * V6_o;

            energy += sqrt_epsilon * (V12 - V12_o - V6 + V6_o);
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}





/**
 * Calculates the Coulombic energy of the total system
 *
 * @param distances matrix of distances
 * @return energy in kcal/mol
 *
 */
double OPLS::energy_coulomb (DistanceCall GetDistance) const
{
   double energy = 0.0;
   for (int i = 0; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++){

         if (molecule[i] != molecule[j]) {
            double dist = GetDistance (i, j); // XXX 1,4-Coulomb?
            double charges = charge[i] * charge[j];
            energy += charges / dist;
         }
      }
   }
   energy *= COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}







/**
 * Calculates the van der Waals interaction between the
 * QM molecule and the MM molecules.
 *
 * @param dists
 * @return vdW energy in kJ/mol!
 */
double OPLS::energy_vdw_qmmm (DistanceCall GetDistance) const
{
   double energy = 0.0;
   for (unsigned int i = 0; i < start_solvent; i++) { // loop over QM atoms
      for (int j = start_solvent; j < natom; j++) {
         const double dist = GetDistance (i, j);
         const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
         const double sigmas = sigma[i] * sigma[j];
         const double t = sigmas / (dist * dist);
         const double V6 = t * t * t;
         const double V12 = V6 * V6;
         energy += sqrt_epsilon * (V12 - V6);
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}




/**
 * Calculates the van der Waals interaction between the
 * QM molecule of the QM system and the MM molecules of the MM system.
 *
 * @param mm_system takes the MM part of this system
 * @param qm_system uses only the QM molecule
 * @return vdW energy in kJ/mol!
 */
double OPLS::energy_vdw_qmmm (System *mm_system, System *qm_system) {
   double energy = 0.0;
   for (unsigned int i = 0; i < start_solvent; i++) { // loop over qm mol
      for (unsigned int j = start_solvent; j < mm_system->natom; j++) { // loop over mm mol's
         const double dist = compute_distance (
                qm_system->rx[i], qm_system->ry[i], qm_system->rz[i],
                mm_system->rx[j], mm_system->ry[j], mm_system->rz[j]);
         const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
         const double sigmas = sigma[i] * sigma[j];
         const double t = sigmas / (dist * dist);
         const double V6 = t * t * t;
         const double V12 = V6 * V6;
         energy += sqrt_epsilon * (V12 - V6);
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}




/**
 * Calculates the change of the van der Waals interaction between the
 * QM molecule and the changed MM molecule.
 *
 * @param dists_new
 * @param dists_old
 * @param changed_molecule
 * @return change of the vdW energy in kJ/mol!
 */
double OPLS::energy_vdw_qmmm (
      DistanceCall GetDistance,
      DistanceCall GetDistance_old,
      int changed_molecule) const
{
   double energy = 0.0;
   for (unsigned int i = 0; i < start_solvent; i++) { // loop over QM atoms
      for (int pos = 0; pos < molecule2atom[changed_molecule].natoms; pos++) {
         const int j = molecule2atom[changed_molecule].atom_id[pos];

         const double dist = GetDistance (i, j);
         const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
         const double sigmas = sigma[i] * sigma[j];
         const double dist_o = GetDistance_old (i, j);

         const double t = sigmas / (dist * dist);
         const double V6 = t * t * t;
         const double V12 = V6 * V6;

         const double t_o = sigmas / (dist_o * dist_o);
         const double V6_o = t_o * t_o * t_o;
         const double V12_o = V6_o * V6_o;

         energy += sqrt_epsilon * (V12 - V12_o - V6 + V6_o);
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}




/**
 * Calculates the Coulombic energy of the system
 *
 * @param distances distance matrix 
 * @param distances_old old distance matrix
 * @param changed_molecule id of the moved molecule
 */
double OPLS::energy_coulomb (
        DistanceCall GetDistance,
        DistanceCall GetDistance_old,
        int changed_molecule) const
{
   double energy = 0;

   for (int i = 0; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++) {
         if        (molecule[i] != changed_molecule
                 && molecule[j] != changed_molecule) continue;

         if (molecule[i] != molecule[j]) { // XXX 1,4-Coulomb?

            double dist = 1.0 / GetDistance (i, j) - 1.0 / GetDistance_old (i, j);
            double charges = charge[i] * charge[j];
            energy += charges * dist;
         }
      }
   }
   energy *= COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}





/**
 * Calculates the electrostatic energy for the MM part.
 *
 * @param distances matrix of distances
 * @return energy in kcal/mol
 *
 */
double OPLS::energy_coulomb_mm (DistanceCall GetDistance) const
{
   double energy = 0.0;
   for (int i = start_solvent; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++){

         if (molecule[i] != molecule[j]) { // XXX 1,4-Coulomb
            const double dist = GetDistance (i, j);
            const double charges = charge[i] * charge[j];
            energy += charges / dist;
         }
      }
   }
   energy *= COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}



/**
 * Calculates the Coulombic and the van der Waals energy only for the MM system
 *
 * @param distances matrix of distances
 * @return energy in kJ/mol
 *
 */
double OPLS::energy_mm (DistanceCall GetDistance) const
{
   double energy = 0.0;
   energy += energy_vdw_mm    (GetDistance);
   energy += energy_coulomb_mm(GetDistance);
   return energy;
}



/**
 * Calculates the change of the Coulomb and van der Waals energy only for the MM system
 *
 * @param distances matrix of distances
 * @return change of energy in kJ/mol
 *
 */
double OPLS::energy_mm (
      DistanceCall GetDistance,
      DistanceCall GetDistance_old,
      int changed_molecule) const
{
   double energy = 0.0;
   energy += energy_vdw_mm    (GetDistance, GetDistance_old, changed_molecule);
   energy += energy_coulomb_mm(GetDistance, GetDistance_old, changed_molecule);
   return energy;
}





/**
 * Calculates the van der Waals energy of the MM system
 *
 * @param distances matrix of distances
 * @return energy in kcal/mol (because params are in kcal/mol)
 *
 */
double OPLS::energy_vdw_mm (DistanceCall GetDistance) const
{
   double energy = 0.0;
   for (int i = start_solvent; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++) {
         if (molecule[i] != molecule[j]) // TODO 1,4-VdW...
         {

            const double dist = GetDistance (i, j);
            const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            const double sigmas = sigma[i] * sigma[j];
            const double t = sigmas / (dist * dist);
            const double V6 = t * t * t;
            const double V12 = V6 * V6;
            energy += sqrt_epsilon * (V12 - V6);
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}




/**
 * Calculates the change of the coulomb interaction in the MM part.
 *
 * @param dists_new
 * @param dists_old
 * @param changed_molecule
 * @return
 */
double OPLS::energy_coulomb_mm (
        DistanceCall GetDistance,
        DistanceCall GetDistance_old,
        int changed_molecule) const
{
   double energy = 0.0;
   for (int i = start_solvent; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++) {
         if        (molecule[i] != changed_molecule
                 && molecule[j] != changed_molecule) continue;

         if (molecule[i] != molecule[j]) { // XXX 1,4-Coulomb?

            const double dist = 1.0 / GetDistance (i, j) - 1.0 / GetDistance_old (i, j);
            const double charges = charge[i] * charge[j];
            energy += charges * dist;
         }
      }
   }
   energy *= COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}





/**
 * Calculates the change of the VDW energy in the MM part.
 *
 * @param dists_new
 * @param dists_old
 * @param changed_molecule
 * @return change of energy in kcal/mol
 */
double OPLS::energy_vdw_mm (
      DistanceCall GetDistance,
      DistanceCall GetDistance_old,
      int changed_molecule) const
{
   double energy = 0.0;
   for (int i = start_solvent; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++) {
         if   (molecule[i] != changed_molecule
            && molecule[j] != changed_molecule) continue;

         if (molecule[i] != molecule[j]) { // XXX 1,4-VdW...

            const double dist_new = GetDistance (i, j);
            const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            const double sigmas = sigma[i] * sigma[j];
            const double dist_old = GetDistance_old (i, j);

            const double t = sigmas / (dist_new * dist_new);
            const double V6 = t * t * t;
            const double V12 = V6 * V6;

            const double t_o = sigmas / (dist_old * dist_old);
            const double V6_o = t_o * t_o * t_o;
            const double V12_o = V6_o * V6_o;

            energy += sqrt_epsilon * (V12 - V12_o - V6 + V6_o);
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}




const double* OPLS::get_normal_charges () const
{
   return charge.data();
}



/**
 * Sets the new value.
 */
void OPLS::set_sigma (
      const unsigned int index,
      const double value)
{
   sigma[index] = value;
}



/**
 * Sets the new value.
 */
void OPLS::set_charge (
      const unsigned int index,
      const double value)
{
   charge[index] = value;
}




void OPLS::print (std::string outfile)
{
   FILE *fout;
   fout = fopen (outfile.c_str(), "a");
   fprintf (fout, "\nOPLS Parameters\n\n");
   fprintf (fout, "%8s %-2s %5s %15s %15s %15s\n", "id", "E", "type", "sigma", "epsilon", "charge");
   fprintf (fout, "-----------------------------------------------------------------\n");
   for (int i = 0; i < natom; i++)
   {
      fprintf (fout, "%8u %-2s %5i %15.6f %15.6f %15.6f\n",
            (i + 1), system->atom_name[i].c_str(), system->atom_type[i], sigma[i], epsilon[i], charge[i]);
   }
   fclose (fout);
}





