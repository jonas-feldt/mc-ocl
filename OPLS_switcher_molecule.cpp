/*
 * File:    OPLS_switcher_molecule.cpp
 * Author:  Jonas Feldt
 *
 */


#include <vector>
#include <utility>
#include <iostream>

#include "OPLS_switcher_molecule.h"
#include "OPLS_Switcher.h"
#include "System.h"
#include "OPLS.h"
#include "Run.h"



/**
 */
OPLS_switcher_molecule::OPLS_switcher_molecule (
      const System *system, Run *run, const std::vector<double> &factors)
   : molecule (run->fes_switch_molecule),
   molecule2atom (system->molecule2atom),
   system_types (system->atom_type, system->atom_type + system->natom),
   atom_types (run->atom_types),
   epsilons (run->epsilons),
   sigmas (run->sigmas),
   charges (run->charges),
   factors (factors)
{
}



OPLS_switcher_molecule::~OPLS_switcher_molecule() {
}





/**
 * Computes new parameters for this frame and replaces them in-place
 * in opls.
 */
void OPLS_switcher_molecule::switch_params (
      const unsigned int frame,
      OPLS *opls,
      FILE *fout)
{
   fprintf (fout, "\nOPLS VdW Parameter Switch\n\n");

   const double factor = factors[frame];

   // loops over the molecule
   for (int i = 0; i < molecule2atom[molecule].natoms; ++i) {
      int atom = molecule2atom[molecule].atom_id[i];

      // find the original parameters for this atom
      int param = 0;
      for (unsigned int k = 0; k < atom_types.size(); ++k) {
         if (system_types[atom] == atom_types[k]) {
            param = k;
            break;
         }
      }

      opls->epsilon[atom] = factor * epsilons[param];
      opls->set_sigma  (atom, factor * sigmas  [param]);
      opls->set_charge (atom, factor * charges [param]);

      fprintf (fout, "%d %d -> %d factor %f\n",
            i, system_types[atom], OPLS_Switcher::DUMMY_TYPE, factor);
   }
}





