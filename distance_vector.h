/*
 * File: distance_vector.h
 * Author: Jonas Feldt
 *
 * Version: 2017-10-07
 *
 * Distances of the system stored in matrix form.
 */

#ifndef DISTANCE_VECTOR_H
#define DISTANCE_VECTOR_H

#include <vector>

#include "distance_interface.h"

class System;
struct Molecule_db;

class DistanceVector : public DistanceInterface
{

   public:

      ~DistanceVector () override;
      void SetContext (System *system) override;
      DistanceVector *clone () const override;

      void ComputeDistances () override;
      void ComputeDistances (unsigned int molecule) override;

      void accept (DistanceInterface *other, unsigned int molecule) override;

      DistanceCall GetDelegate() const override;
      double GetDistance (unsigned int i, unsigned int j) const;

   protected:

      unsigned int dim;
      double *x;
      double *y;
      double *z;
      std::vector<Molecule_db> *mol2atom;

      double *dists = nullptr;

};


#endif // DISTANCE_VECTOR_H
