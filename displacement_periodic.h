/*
 * File:   displacement_periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2017-08-14
 */

#pragma once

#include <string>

#include "TrajectoryAnalyzerPeriodic.h"

class DisplacementPeriodic : public TrajectoryAnalyzerPeriodic
{
   public:

      DisplacementPeriodic (
           std::string trajectory_file, int first_frame, int last_frame,
           std::string output_file, const double box_dim, const unsigned int
           num_chains);
      ~DisplacementPeriodic () override;

   protected:

      void analyze () override;
      void finalize () override;
      void save_output () override;

   private:

      bool has_ref = false;
      double *refx = nullptr;
      double *refy = nullptr;
      double *refz = nullptr;


      double *result;
      int step = 0;
};


