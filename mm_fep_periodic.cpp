/*
 * File:   mm_fep_periodic.cpp
 * Author: Jonas Feldt
 * 
 * Version: 2017-07-18
 */

#include <iostream>
#include <future>
#include <random>
#include <string>
#include <fstream>
#include <cmath>
#include <functional>

#include "mm_fep_periodic.h"
#include "jonas_io_utils.h"
#include "System_Periodic.h"
#include "Run_Periodic.h"
#include "OPLS_Periodic.h"
#include "OPLS_Periodic_Softcore.h"
#include "ChainResults.h"
#include "FEP_ChainResults.h"
#include "geom_utils.h"
#include "typedefs.h"
#include "Constants.h"
#include "distance_interface.h"
#include "Rotate_Translate_Stepper.h"

#include "options.h" // Header with all run options.


// TODO does not worked for a charged molecule!!!


/**
 * Constructor
 */
MMFEPPeriodic::MMFEPPeriodic (
      const std::string m_base_name,
      const Run_Periodic *run,
      const OPLS_Periodic *oplsA,
      const OPLS_Periodic *oplsB,
      const int num_devices,
      const int num_chains)
 : m_base_name (m_base_name),
   oplsA (oplsA),
   oplsB (oplsB),
   num_devices (num_devices),
   num_chains (num_chains)
{
   // works on a copy to not modify the original run object
   m_run = new Run_Periodic(*run);
}



/**
 * Destructor
 */
MMFEPPeriodic::~MMFEPPeriodic () {
   delete m_run;
}





/**
 * Runs the MM MC simulation.
 *
 * @param systems
 *
 * @return a vector of vectors of the final systems (=systems[num_devices][num_chains])
 *
 */
vvs MMFEPPeriodic::run (vvs &systems)
{
   vvd energies_oldA (systems.size(), vd(systems.begin()->size()));
   vvd energies_oldB (systems.size(), vd(systems.begin()->size()));

   for (unsigned int i = 0; i < m_run->num_devices; ++i) {
      for (unsigned int j = 0; j < m_run->num_chains; ++j) {
         System_Periodic *system = systems[i][j];
         energies_oldA[i][j] = oplsA->energy (system->GetDistance);
         energies_oldB[i][j] = oplsB->energy (system->GetDistance);
         VERBOSE_LVL_INIT("\tMM energy A: " << energies_oldA[i][j] << std::endl);
         VERBOSE_LVL_INIT("\tMM energy B: " << energies_oldB[i][j] << std::endl);
      }
   }

   return run (systems, energies_oldA, energies_oldB);
}




/**
 * Runs the MM MC simulation.
 *
 * @param system
 *
 * @return a vector of vectors of the final systems (=systems[n_device][n_chain])
 */
vvs MMFEPPeriodic::run (System_Periodic *system)
{

   // computes initial MM, QMMM energies
   double energy_oldA = oplsA->energy (system->GetDistance);
   double energy_oldB = oplsB->energy (system->GetDistance);

   VERBOSE_LVL_INIT("[INIT] System MM atoms=" << system->natom << std::endl);    
   VERBOSE_LVL_INIT("\tMM energy A: " << energy_oldA << std::endl);
   VERBOSE_LVL_INIT("\tMM energy B: " << energy_oldB << std::endl);

   vvd energies_oldA (num_devices, vd(num_chains));
   vvd energies_oldB (num_devices, vd(num_chains));
   vvs systems;

   for (unsigned int i = 0; i < m_run->num_devices; ++i) {
      vs tmpS;
      tmpS.reserve(m_run->num_chains);
      for (unsigned int j = 0; j < m_run->num_chains; ++j) {
         tmpS.push_back(system);
         energies_oldA[i][j] = energy_oldA;
         energies_oldB[i][j] = energy_oldB;
      }
      systems.push_back(tmpS);
   }

   return run (systems, energies_oldA, energies_oldB);
}




/**
 * Runs the PMC simulation.
 *
 * @param system
 * @param energies_old
 *
 * @return a vector of vectors of the final systems (=systems[num_devices][num_chains])
 */
vvs MMFEPPeriodic::run (
      vvs &systems, 
      const vvd &energies_oldA,
      const vvd &energies_oldB)
{
   // Adjust maxmoves according to number of chains
   m_run->maxmoves /= (num_devices * num_chains);

   VERBOSE_LVL_WARN("[INIT] Set Maxmoves per Chain: " << m_run->maxmoves << std::endl);
   VERBOSE_LVL_INIT("[INIT] Making directory " << m_base_name << "..."<< std::endl);
   make_directory_jf (m_base_name);

   // seeds PRNG
   random_engine engine;
   std::uniform_int_distribution<int> dist (
         std::numeric_limits<int>::min(),
         std::numeric_limits<int>::max());
   if (not m_run->has_seed_path) engine.seed(m_run->seed);

   // Starts num_devices * num_chains tasks
   std::vector<std::vector<std::future<std::tuple<ChainResults*, System_Periodic*>>>> futures;
   futures.reserve(systems.size());
   int index = 0;
   for (unsigned int i = 0; i < systems.size(); ++i) {

      std::vector<std::future<std::tuple<ChainResults*, System_Periodic*>>> tmp;
      tmp.reserve(systems.begin()->size());
      for (unsigned int j = 0; j < systems.begin()->size(); ++j) {

         int seed = 0;
         if (not m_run->has_seed_path) seed = dist(engine);

         std::packaged_task<std::tuple<ChainResults*, System_Periodic*> ()> task(std::bind(
                &MMFEPPeriodic::run_chain, this, systems[i][j], index, seed,
                m_base_name, energies_oldA[i][j], energies_oldB[i][j]));
         tmp.push_back(task.get_future());
         std::thread(move(task)).detach();
         index++;
      }
      futures.push_back(std::move(tmp));
   }

   // collects results
   vvs final_systems (m_run->num_devices, vs(m_run->num_chains));
   std::vector<ChainResults*> results;
   for (unsigned int i = 0; i < systems.size(); ++i) {
      for (unsigned int j = 0; j < systems.begin()->size(); ++j) {
         std::tuple<ChainResults*, System_Periodic*> result = futures[i][j].get();
         results.push_back (std::get<0> (result));
         final_systems[i][j] = std::get<1> (result); 
      }
   }

   ChainResults::print_results (m_base_name + ".out", results);

   for (ChainResults *result : results) delete result;

   return final_systems;
}




std::tuple<ChainResults *, System_Periodic*> MMFEPPeriodic::run_chain(
      System_Periodic *system_start,
      const int chain_id,
      const int seed,
      std::string basename,
      double energy_oldA,
      double energy_oldB)
{

   // TODO check not to overwrite

   // Prints the header for the energy file
   FILE *energy_file = nullptr;
   FILE *exen_file = nullptr;
   std::string xyz_file;
   if (m_run->print_every != 0) {
      energy_file = std::fopen (
            (m_base_name + "-energy-" + std::to_string (chain_id) + ".out").c_str(), "w");
      fprintf (energy_file, "%10s %10s %20s %10s %20s %10s %10s %10s %10s\n",
           "Steps", "Accepted", "E_mm", "dE_mm", "mE_mm", "sE_mm", "dE", "mdG", "sdG"); 
      xyz_file = m_base_name + "-" + std::to_string (chain_id) + ".xyz";
   }
   if (m_run->print_energy != 0) {
      exen_file = std::fopen (
            (m_base_name + "-exen-" + std::to_string (chain_id) + ".out").c_str(), "w");
      fprintf (exen_file, "%10s %10s %20s %10s %20s %10s %10s %10s %10s\n",
           "Steps", "Accepted", "E_mm", "dE_mm", "mE_mm", "sE_mm", "dE", "mdG", "sdG"); 
   }

   // Inits systems for the simulation
   System_Periodic *system_old = new System_Periodic (*system_start);
   System_Periodic *system     = new System_Periodic (*system_start);

   // Inits variables
   double e_oldA        = energy_oldA;
   double e_oldB        = energy_oldB;
   double free_energy   = std::exp(
         -(e_oldB - e_oldA) / (m_run->temperature * BOLTZMANN_KB));
   double old_mean      = 0.0;
   double old_variance  = 0.0;
   double free_mean     = 0.0;
   double free_variance = 0.0;
   int accepted_steps   = 0;

   // Inits random engine
   random_engine rnd_engine;
   if (m_run->has_seed_path) {
      std::string file = m_run->seed_path + "/" + std::to_string(chain_id) + ".random";
      VERBOSE_LVL_INIT("[INIT] reading file " << file << std::endl);
      std::ifstream ifile (file, std::ios::binary);
      ifile >> rnd_engine;
      VERBOSE_LVL_INIT("[INIT] loading random state " << rnd_engine() << std::endl);
   } else {
      rnd_engine.seed(seed);
   }

   // TODO first molecule is still frozen
   //
   auto stepper = Rotate_Translate_Stepper {&rnd_engine, system, m_run};

   // MM MC steps
   for (unsigned int step = 0; step < m_run->maxmoves; ++step) {

      stepper.step(system, oplsA);
      double deA = stepper.dE (system, system_old, oplsA);

      if (stepper.is_accepted()) {

         const double deB = stepper.dE (system, system_old, oplsB);
         e_oldA += deA;
         e_oldB += deB;
         free_energy = std::exp(-(e_oldB - e_oldA) / (m_run->temperature * BOLTZMANN_KB));

         accepted_steps++;
         stepper.accept (system, system_old);

      } else {

         stepper.reject (system, system_old);
         deA = 0.0;
      }

      // Init Method
      if (step == 0) {

         old_mean  = e_oldA; 
         free_mean = free_energy;
         old_variance  = 0.0; 
         free_variance = 0.0;

      } else {
        
         const double new_mean      = old_mean  + (e_oldA       - old_mean ) / (step + 1);
         const double new_free_mean = free_mean + (free_energy - free_mean) / (step + 1);

         const double new_variance = old_variance  + (e_oldA - old_mean) * (e_oldA - new_mean);
         const double new_free_var = free_variance +
            (free_energy - free_mean) * (free_energy - new_free_mean);
      
         old_mean      = new_mean; 
         free_mean     = new_free_mean;

         old_variance  = new_variance; 
         free_variance = new_free_var; 
      }

      // IO
      const double dG = -BOLTZMANN_KB * m_run->temperature * log(free_mean);
      const double sExp = sqrt(free_variance / (step + 1));
      const double sdG = BOLTZMANN_KB * m_run->temperature * sExp / free_mean;
      if (m_run->print_every != 0 && (step % m_run->print_every) == 0) {
         fprintf (energy_file, "%10i %10i %20.3f %10.5f %20.3f %10.5f %10.5f %10.5f %10.5f\n",
               step, accepted_steps, e_oldA, deA,
               old_mean, std::sqrt(old_variance / (step + 1)),
               e_oldB - e_oldA, dG, sdG);
         system->save_xyz (xyz_file);
      }
      if (m_run->print_energy != 0 && (step % m_run->print_energy) == 0) {
         fprintf (exen_file, "%10i %10i %20.3f %10.5f %20.3f %10.5f %10.5f %10.5f %10.5f\n",
               step, accepted_steps, e_oldA, deA,
               old_mean, std::sqrt(old_variance / (step + 1)),
               e_oldB - e_oldA, dG, sdG);
      }

   } // end MC steps

   // saves random number generator state
   std::ofstream ofile (basename + "/" + std::to_string(chain_id) + ".random", std::ios::binary);
   ofile << rnd_engine; 


   delete system_old;
   
   if (m_run->print_every  != 0) std::fclose (energy_file);
   if (m_run->print_energy != 0) std::fclose (exen_file  );


   const double dG = -BOLTZMANN_KB * m_run->temperature * log(free_mean);
   const double sExp = sqrt(free_variance / m_run->maxmoves);
   const double sdG = BOLTZMANN_KB * m_run->temperature * sExp / free_mean;

   ChainResults *results = new FEP_ChainResults (
         old_mean, 0.0, dG,
         std::sqrt(old_variance / m_run->maxmoves), 0.0, sdG,
         m_run->stepmax * m_run->dimension_box, m_run->maxmoves, accepted_steps);

   return std::make_tuple (results, const_cast<System_Periodic*> (system));
}

















