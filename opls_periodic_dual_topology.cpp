/*
 * File:   opls_periodic_dual_topology.cpp
 * Author: Jonas Feldt
 *
 * Version: 2017-07-19
 */


#include "opls_periodic_dual_topology.h"
#include "OPLS_Periodic_Softcore.h"
#include "Run_Periodic.h"
#include "Constants.h"
#include "System.h" // for Molecule_db


OPLSPeriodicDualTopology::OPLSPeriodicDualTopology (
   const System *system,
   Run_Periodic *run,
   const unsigned int n,
   const unsigned int m,
   const double alpha,
   const double lambda)
   :  OPLS_Periodic_Softcore (system, run, n, m, alpha, lambda),
      lambda (lambda),
      reference (run->fep_reference_molecule),
      target (run->fep_target_molecule),
      s1r (std::pow(1.0 - lambda, n)),
      s2r (alpha * std::pow(lambda, m))
{
}


double OPLSPeriodicDualTopology::energy (DistanceCall GetDistance) const
{
   double energy = 0.0;
   energy += energy_mm (GetDistance);
   energy += EnergyVDWQMMMTopology (GetDistance, reference, s1r, s2r) +
         (1.0 - lambda) * EnergyCoulombQMMMTopology (GetDistance, reference);
   energy += EnergyVDWQMMMTopology (GetDistance, target, s1, s2) +
         lambda * EnergyCoulombQMMMTopology (GetDistance, target);
   return energy;
}




double OPLSPeriodicDualTopology::energy (
      DistanceCall GetDistance,
      DistanceCall GetDistanceOld,
      int changed_molecule) const
{
   double energy = 0.0;
   energy += energy_mm (GetDistance, GetDistanceOld, changed_molecule);
   energy += EnergyVDWQMMMTopology (GetDistance, reference, GetDistanceOld, changed_molecule, s1r, s2r) +
      (1.0 - lambda) * EnergyCoulombQMMMTopology (GetDistance, reference, GetDistanceOld, changed_molecule);
   energy += EnergyVDWQMMMTopology (GetDistance, target, GetDistanceOld, changed_molecule, s1, s2) +
         lambda * EnergyCoulombQMMMTopology (GetDistance, target, GetDistanceOld, changed_molecule);
   return energy;
}




double OPLSPeriodicDualTopology::EnergyVDWQMMMTopology (
      DistanceCall GetDistance, unsigned int topology,
      DistanceCall GetDistanceOld, unsigned int changed_molecule,
      const double s1l, const double s2l) const
{

   const auto &topology_m2a = molecule2atom[topology];
   const auto &changed_m2a = molecule2atom[changed_molecule];

   double energy = 0.0;
   const int dim = topology_m2a.natoms;
   for (int pos_solute = 0; pos_solute < dim; pos_solute++) {
      const int i = topology_m2a.atom_id[pos_solute];

      for (int pos_solvent = 0; pos_solvent < changed_m2a.natoms; pos_solvent++) {
         const int j = changed_m2a.atom_id[pos_solvent];
         
         const double dist_new = GetDistance (i, j);
         const double dist_old = GetDistanceOld (i, j);
         if (dist_new < cutoff_vdw) {

            const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            const double sigmas = sigma[i] * sigma[j];
            const double t = dist_new * dist_new / sigmas;
            const double V6 = t * t * t;
            const double t1 = 1.0 / (s2l + V6);

            if (dist_old < cutoff_vdw) {
               const double t_o = dist_old * dist_old / sigmas;
               const double V6_o = t_o * t_o * t_o;
               const double t1_o = 1.0 / (s2l + V6_o);
               energy += s1l * sqrt_epsilon *
                  (std::pow(t1, 2) - t1 -
                   std::pow(t1_o, 2) + t1_o);
            } else { // dist < cutoff && dist_old > cutoff
               energy += s1l * sqrt_epsilon *
                  (std::pow(t1, 2) - t1);
            }
         } else if (dist_old < cutoff_vdw) { // dist_new > cutoff && dist_old < cutoff
            // subtracts the old value
            const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            const double sigmas = sigma[i] * sigma[j];
            const double t_o = dist_old * dist_old / sigmas;
            const double V6_o = t_o * t_o * t_o;
            const double t1_o = 1.0 / (s2l + V6_o);
            energy -= s1l * sqrt_epsilon *
               (std::pow(t1_o, 2) - t1_o);

            // dist_new > cutoff && dist_old > cutoff --> always 0
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}



double OPLSPeriodicDualTopology::EnergyCoulombQMMMTopology (
      DistanceCall GetDistance, unsigned int topology,
      DistanceCall GetDistanceOld, unsigned int changed_molecule) const
{

   const auto &topology_m2a = molecule2atom[topology];
   const auto &changed_m2a = molecule2atom[changed_molecule];

   double energy = 0.0;
   const int dim = topology_m2a.natoms;
   for (int pos_solute = 0; pos_solute < dim; pos_solute++) {
      const int i = topology_m2a.atom_id[pos_solute];

      for (int pos_solvent = 0; pos_solvent < changed_m2a.natoms; pos_solvent++) {
         const int j = changed_m2a.atom_id[pos_solvent];
         
         const double dist = GetDistance (i, j);
         const double dist_old = GetDistanceOld (i, j);
         if (dist < cutoff_coulomb) {
            const double qs = charge[i] * charge[j];
            if (dist_old < cutoff_coulomb) {
               energy += qs * (1.0 / dist - 1.0 / dist_old +
                       cutoff_coulomb_rec_sq * (dist - dist_old));
            } else { // dist < cutoff && dist_old > cutoff
               energy += qs * (1.0 / dist - cutoff_coulomb_rec +
                       cutoff_coulomb_rec_sq * (dist - cutoff_coulomb));
            }
         } else if (dist_old < cutoff_coulomb) { // dist > cutoff && dist_old < cutoff
            const double qs = charge[i] * charge[j];   // subtracts old value
            energy -= qs * (1.0 / dist_old - cutoff_coulomb_rec +
                       cutoff_coulomb_rec_sq * (dist_old - cutoff_coulomb));

            // dist < cutoff && dist_old < cutoff --> always 0
         }
      }
   }
   energy *= COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}






double OPLSPeriodicDualTopology::EnergyVDWQMMMTopology (
      DistanceCall GetDistance, unsigned int topology,
      const double s1l, const double s2l) const
{
   double energy = 0.0;
   const int dim = molecule2atom[topology].natoms;
   for (int pos = 0; pos < dim; pos++) {
      const int i = molecule2atom[topology].atom_id[pos];
      for (int j = start_solvent; j < natom; j++) {
         
         const double dist = GetDistance (i, j);
         if (dist < cutoff_vdw) {
            const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            const double sigmas = sigma[i] * sigma[j];
            const double t = dist * dist / sigmas;
            const double V6 = t * t * t;
            const double t1 = 1.0 / (s2l + V6);
            energy += s1l * sqrt_epsilon *
               (std::pow(t1, 2) - t1);
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}



double OPLSPeriodicDualTopology::EnergyCoulombQMMMTopology (
      DistanceCall GetDistance, unsigned int topology) const
{
   double energy = 0.0;
   const int dim = molecule2atom[topology].natoms;
   for(int pos = 0; pos < dim; pos++){
      int i = molecule2atom[topology].atom_id[pos];
      for (int j = start_solvent; j < natom; j++) {
         
         const double dist = GetDistance (i, j);
         if (dist < cutoff_coulomb) {
            double qs = charge[i] * charge[j];
            energy += qs * (1.0 / dist - cutoff_coulomb_rec +
                    cutoff_coulomb_rec_sq * (dist - cutoff_coulomb));
         }
      }
   }
   energy *= COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}


