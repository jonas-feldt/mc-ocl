/*
 * File:   System.cpp
 * Author: João Oliveira, Jonas Feldt
 *
 * Version: 2017-07-10
 */

#ifndef SYSTEM_H
#define SYSTEM_H

#include <string>
#include <vector>

#include "typedefs.h"

class DistanceInterface;


/**
 * Structure that allows to easily find atoms of a molecule.
 */
typedef struct Molecule_db {
   int natoms;          // Number of atoms
   int *atom_id;        // array with IDs of atoms

   Molecule_db () {}

   Molecule_db (const Molecule_db &other) {
      natoms = other.natoms;
      atom_id = new int[other.natoms];
      for (int i = 0; i < natoms; ++i) {
         atom_id[i] = other.atom_id[i];
      }
   }

   ~Molecule_db () {delete[] atom_id;}
} Molecule_db;

class System {
public:

   const unsigned int natom;
   double *rx;
   double *ry;
   double *rz;
   int *atom_type;
   std::string *atom_name;
   double *atom_mass;
   int *atomic_numbers;
   int *n_12;
   int (*connect)[4];
   int *molecule;
   int nmol_total;
   std::vector<Molecule_db> molecule2atom;

   virtual void print();

   void UpdateDistances();
   void UpdateDistances(int id_molecule);

   virtual void save_all (std::string file);
   virtual void save_all (std::string file, std::string title);
   virtual void save_xyz (std::string file);

   virtual std::vector<double*> get_normal_coords();

   virtual void set_coordinates (double *x, double *y, double *z);

   System(unsigned int natom, double rx[], double ry[], double rz[], int
         atom_type[], std::string atom_name[], int n_12[], int connect[][4],
         DistanceInterface *distances);
   System(System& other);
   System(System& other, int mol);
   virtual ~System();

   DistanceCall GetDistance;
   DistanceInterface *distances;

protected:

   void create_molecules();
   void set_neighbors(int atom, int id);


};

#endif // SYSTEM_H
