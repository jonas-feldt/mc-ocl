/**
 *
 * File: OCLKernel.h
 * Author: Sebastiao Miranda
 *
 * Version: 2014-04-09
 *
 */

#ifndef OCLKERNEL_H_INCLUDED
#define OCLKERNEL_H_INCLUDED

//#define __NO_STD_VECTOR to use cl::vector instead
#define __CL_ENABLE_EXCEPTIONS
#include<string>
#include<iostream>

#include "options.h"

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

class OCLDevice;

class OCLKernel{

public:
    
   OCLKernel(std::string kernelName,std::string fileName, OCLDevice *device);
   ~OCLKernel();
   
   // Setup Global and local ranges
   void setRanges(int global, int local);
   
   // Compile kernel for a device
   int make_kernel(std::string flags);
   
   // Returns the preferred work group size for a kernel
   int get_pref_wg_mult_1d();

   // Glocal and local ranges
   int m_int_globalRange;
   int m_int_localRange;
   
   cl::NDRange m_globalRange;
   cl::NDRange m_localRange;
   
   // Program object
   cl::Program m_program;
   
   // Kernel object
   cl::Kernel m_kernel;
   
   // File and Kernel name
   std::string m_kernelName;
   std::string m_fileName;
      
   //Ptr to ocl device
   OCLDevice *m_device;
   
protected:
   
private:
    
};

#endif //OCLKERNEL_H_INCLUDED

