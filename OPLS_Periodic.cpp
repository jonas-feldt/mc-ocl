/*
 * File:   OPLS_Periodic.cpp
 * Author: Jonas Feldt
 *
 * Version: 2015-04-09
 */

#include <math.h>
#include <iostream>
#include <stdio.h>
#include <algorithm>

#include "OPLS.h"
#include "OPLS_Periodic.h"
#include "Run_Periodic.h"
#include "System.h"
#include "options.h"
#include "geom_utils.h"
#include "Constants.h"
#include "typedefs.h"

#define COULOMB_CONST 332.063806190589 // in Angs.kcal/mol


/**
 * Constructor
 *
 * @param system properties of the class System
 * @param run 
 */
OPLS_Periodic::OPLS_Periodic (
      const System *system,
      Run_Periodic *run)
:  OPLS(system, run->atom_types, run->epsilons, run->sigmas, run->charges, run->solute_molecules),
   box_dimension (run->dimension_box),
   cutoff_coulomb (run->cutoff_coulomb),
   cutoff_coulomb_rec (1.0 / cutoff_coulomb),
   cutoff_coulomb_rec_sq (cutoff_coulomb_rec * cutoff_coulomb_rec),
   sqrt_box_dimension (sqrt (box_dimension)),
   cutoff_vdw (run->cutoff_vdw),
   normal_charges (charge)
{
   // converts force field parameters to reduced units
   for (unsigned int i = 0; i < system->natom; i++) {
      sigma[i] /= box_dimension;
   }
   for (unsigned int i = 0; i < system->natom; i++) {
      charge[i] /= sqrt_box_dimension;
   }
}




/**
 * Copy constructor
 */
OPLS_Periodic::OPLS_Periodic (const OPLS_Periodic &orig)
   : OPLS (orig),
   box_dimension (orig.box_dimension),
   cutoff_coulomb (orig.cutoff_coulomb),
   cutoff_coulomb_rec (orig.cutoff_coulomb_rec),
   cutoff_coulomb_rec_sq (orig.cutoff_coulomb_rec_sq),
   sqrt_box_dimension (orig.sqrt_box_dimension),
   cutoff_vdw (orig.cutoff_vdw),
   normal_charges (orig.normal_charges)
{
}



OPLS_Periodic::~OPLS_Periodic () {
}




/**
 * Calculates the Van der Waals energy of the system
 *
 * @param distances distance matrix
 */
double OPLS_Periodic::energy_vdw (DistanceCall GetDistance) const
{
   double energy = 0.0;
   for (int i = 0; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++) {
         if (molecule[i] != molecule[j]) { // TODO 1,4-VdW...

            double dist = GetDistance (i, j);
            if (dist < cutoff_vdw) {
               double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
               double sigmas = sigma[i] * sigma[j];
               double t = sigmas / (dist * dist);
               double V6 = t * t * t;
               double V12 = V6 * V6;
               energy += sqrt_epsilon * (V12 - V6);
            }
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}



/**
 * Calculates the change of the Van der Waals energy of the system.
 *
 * @param distances distance matrix
 * @param distances_old old distance matrix
 * @param changed_molecule id of the moved molecule
 */
double OPLS_Periodic::energy_vdw (
      DistanceCall GetDistance,
      DistanceCall GetDistance_old,
      int changed_molecule) const
{
   double energy = 0.0;
   for (int pos = 0 ; pos < molecule2atom[changed_molecule].natoms ; pos++ ){
      int i = molecule2atom[changed_molecule].atom_id[pos];
      for (int j = 0; j < natom; j++) {

         if (molecule[i] != molecule[j]) { // TODO 1,4-VdW...

            double dist = GetDistance (i, j);
            double dist_o = GetDistance_old (i, j);
            if (dist < cutoff_vdw) {

               double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
               double sigmas = sigma[i] * sigma[j];
               double t = sigmas / (dist * dist);
               double V6 = t * t * t;
               double V12 = V6 * V6;

               if (dist_o < cutoff_vdw) {
                  double t_o = sigmas / (dist_o * dist_o);
                  double V6_o = t_o * t_o * t_o;
                  double V12_o = V6_o * V6_o;
                  energy += sqrt_epsilon * (V12 - V12_o - V6 + V6_o);
               } else { // dist < cutoff && dist_old > cutoff
                  energy += sqrt_epsilon * (V12 - V6);
               }
            } else if (dist_o < cutoff_vdw) { // dist > cutoff && dist_old < cutoff
               // subtracts the old value
               double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
               double sigmas = sigma[i] * sigma[j];
               double t_o = sigmas / (dist_o * dist_o);
               double V6_o = t_o * t_o * t_o;
               double V12_o = V6_o * V6_o;
               energy -= sqrt_epsilon * (V12_o - V6_o);

               // dist > cutoff && dust_old > cutoff --> always 0
            }
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}





/**
 * Calculates the Coulombic energy of the system
 *
 * @param distance matrix
 */
double OPLS_Periodic::energy_coulomb (DistanceCall GetDistance) const
{
   double energy = 0.0;
   for (int i = 0; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++){

         if (molecule[i] != molecule[j]) { // XXX 1,4-Coulomb?

            double dist = GetDistance (i, j);
            if (dist < cutoff_coulomb) {
               double qs = charge[i] * charge[j];
               energy += qs * (1.0 / dist - cutoff_coulomb_rec +
                       cutoff_coulomb_rec_sq * (dist - cutoff_coulomb));
            }
         }
      }
   }
   energy *= COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}



/**
 * Calculates the Coulombic energy of the system
 *
 * @param distance matrix
 * @param distances_old old distance matrix
 * @param changed_molecule id of the moved molecule
 */
double OPLS_Periodic::energy_coulomb (
        DistanceCall GetDistance,
        DistanceCall GetDistance_old,
        int changed_molecule) const
{
   double energy = 0.0;
   for (int pos = 0 ; pos < molecule2atom[changed_molecule].natoms ; pos++ ){
      int i = molecule2atom[changed_molecule].atom_id[pos];
      for (int j = 0; j < natom; j++) {

         if (molecule[i] != molecule[j]) { // XXX 1,4-Coulomb?

            double dist = GetDistance (i, j);
            double dist_old = GetDistance_old (i, j);
            if (dist < cutoff_coulomb) {
               double qs = charge[i] * charge[j];
               if (dist_old < cutoff_coulomb) {
                  energy += qs * (1.0 / dist - 1.0 / dist_old +
                          cutoff_coulomb_rec_sq * (dist - dist_old));
               } else { // dist < cutoff && dist_old > cutoff
                  energy += qs * (1.0 / dist - cutoff_coulomb_rec +
                          cutoff_coulomb_rec_sq * (dist - cutoff_coulomb));
               }
            } else if (dist_old < cutoff_coulomb) { // dist > cutoff && dist_old < cutoff
               double qs = charge[i] * charge[j];   // subtracts old value
               energy -= qs * (1.0 / dist_old - cutoff_coulomb_rec +
                          cutoff_coulomb_rec_sq * (dist_old - cutoff_coulomb));

               // dist < cutoff && dist_old < cutoff --> always 0
            }
         }
      }
   }
   energy *= COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}






/**
 * Calculates the Coulomb energy of the MM part.
 *
 * @param distance matrix
 * @return energy in kcal/mol
 */
double OPLS_Periodic::energy_coulomb_mm (DistanceCall GetDistance) const
{
   double energy = 0.0;
   for (int i = start_solvent; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++){
         if (molecule[i] == molecule[j]) continue; // TODO 1,4-Coulomb?

         const double dist = GetDistance (i, j);
         
         if (dist < cutoff_coulomb) {
            const double qs = charge[i] * charge[j];
            energy += qs * (1.0 / dist - cutoff_coulomb_rec +
                    cutoff_coulomb_rec_sq * (dist - cutoff_coulomb));
         }
      }
   }
   energy *= COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}



/**
 * Calculates the change of the coulomb interaction in the MM part.
 *
 * @param dists_new
 * @param dists_old
 * @param changed_molecule
 * @return
 */
double OPLS_Periodic::energy_coulomb_mm (
      DistanceCall GetDistance,
      DistanceCall GetDistance_old,
      int changed_molecule) const
{
   double energy = 0.0;
   for(int pos = 0 ; pos < molecule2atom[changed_molecule].natoms ; pos++ ){
      int i = molecule2atom[changed_molecule].atom_id[pos];
      for (int j = start_solvent; j < natom; j++) {

         if (molecule[i] != molecule[j]) { // XXX 1,4-Coulomb?

            const double dist = GetDistance (i, j);
            const double dist_old = GetDistance_old (i, j);
            if (dist < cutoff_coulomb) {
               const double qs = charge[i] * charge[j];
               if (dist_old < cutoff_coulomb) {
                  energy += qs * (1.0 / dist - 1.0 / dist_old +
                          cutoff_coulomb_rec_sq * (dist - dist_old));
               } else { // dist < cutoff && dist_old > cutoff
                  energy += qs * (1.0 / dist - cutoff_coulomb_rec +
                          cutoff_coulomb_rec_sq * (dist - cutoff_coulomb));
               }
            } else if (dist_old < cutoff_coulomb) { // dist > cutoff && dist_old < cutoff
               const double qs = charge[i] * charge[j];   // subtracts old value
               energy -= qs * (1.0 / dist_old - cutoff_coulomb_rec +
                          cutoff_coulomb_rec_sq * (dist_old - cutoff_coulomb));

               // dist > cutoff && dist_old > cutoff --> always 0
            }
         }
      }
   }
   energy *= COULOMB_CONST * KCAL_TO_KJ;
   return energy;
}





/**
 * Calculates the van der Waals energy of the MM part.
 *
 * @param distances distance matrix
 * @return energy in kcal/mol
 */
double OPLS_Periodic::energy_vdw_mm (DistanceCall GetDistance) const
 {
   double energy = 0.0;
   for (int i = start_solvent; i < natom - 1; i++) {
      for (int j = i + 1; j < natom; j++) {
         if (molecule[i] == molecule[j]) continue; // TODO 1,4-VdW...

         
         const double dist = GetDistance (i, j);
         if (dist < cutoff_vdw) {
            const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            const double sigmas = sigma[i] * sigma[j];
            const double t = sigmas / (dist * dist);
            const double V6 = t * t * t;
            const double V12 = V6 * V6;
            energy += sqrt_epsilon * (V12 - V6);
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}



/**
 * Calculates the van der Waals energy of the MM system
 *
 * @param distances matrix of distances
 * @return energy in kcal/mol (because params are in kcal/mol)
 *
 */
double OPLS_Periodic::energy_vdw_mm (DistanceCall GetDistance,
                                     DistanceCall GetDistance_old,
                                     int changed_molecule) const
{
   double energy = 0.0;
   for(int pos = 0 ; pos < molecule2atom[changed_molecule].natoms ; pos++ ){
      const int i = molecule2atom[changed_molecule].atom_id[pos];
      for (int j = start_solvent; j < natom; j++) {

         if (molecule[i] != molecule[j]) { // TODO 1,4-VdW...

            const double dist = GetDistance (i, j);
            const double dist_o = GetDistance_old (i, j);
            if (dist < cutoff_vdw) {

               const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
               const double sigmas = sigma[i] * sigma[j];
               const double t = sigmas / (dist * dist);
               const double V6 = t * t * t;
               const double V12 = V6 * V6;

               if (dist_o < cutoff_vdw) {
                  const double t_o = sigmas / (dist_o * dist_o);
                  const double V6_o = t_o * t_o * t_o;
                  const double V12_o = V6_o * V6_o;
                  energy += sqrt_epsilon * (V12 - V12_o - V6 + V6_o);
               } else { // dist < cutoff && dist_old > cutoff
                  energy += sqrt_epsilon * (V12 - V6);
               }
            } else if (dist_o < cutoff_vdw) { // dist > cutoff && dist_old < cutoff
               // subtracts the old value
               const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
               const double sigmas = sigma[i] * sigma[j];
               const double t_o = sigmas / (dist_o * dist_o);
               const double V6_o = t_o * t_o * t_o;
               const double V12_o = V6_o * V6_o;
               energy -= sqrt_epsilon * (V12_o - V6_o);

               // dist > cutoff && dust_old > cutoff --> always 0
            }
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}




/**
 * Calculates the van der Waals interaction between the QM molecule and
 * the MM molecules.
 *
 * @param dists
 * @return vdW energy in kJ/mol
 */
double OPLS_Periodic::energy_vdw_qmmm (DistanceCall GetDistance) const
{

   double energy = 0.0;
   for (unsigned int i = 0; i < start_solvent; i++){
      for (int j = start_solvent; j < natom; j++) {
         
         const double dist = GetDistance (i, j);
         if (dist < cutoff_vdw) {
            const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            const double sigmas = sigma[i] * sigma[j];
            const double t = sigmas / (dist * dist);
            const double V6 = t * t * t;
            const double V12 = V6 * V6;
            energy += sqrt_epsilon * (V12 - V6);
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}




/**
 * Calculates the van der Waals interaction between the
 * QM molecule of the QM system and the MM molecules of the MM system.
 *
 * @param mm_system takes the MM part of this system
 * @param qm_system uses only the QM molecule 
 * @return vdW energy in kJ/mol!
 */
double OPLS_Periodic::energy_vdw_qmmm (System *mm_system, System *qm_system)
{
   double energy = 0.0;
   for (unsigned int i = 0; i < start_solvent; i++) { // loop over qm mol
      for (unsigned int j = start_solvent; j < mm_system->natom; j++) { // loop over mm mol's
         const double dist = compute_minimal_distance (
               qm_system->rx[i], qm_system->ry[i], qm_system->rz[i],
               mm_system->rx[j], mm_system->ry[j], mm_system->rz[j]);

         if (dist < cutoff_vdw) {
            const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            const double sigmas = sigma[i] * sigma[j];
            const double t = sigmas / (dist * dist);
            const double V6 = t * t * t;
            const double V12 = V6 * V6;
            energy += sqrt_epsilon * (V12 - V6);
         }
      }
   }
   energy *= 4 * KCAL_TO_KJ;
   return energy;
}




/**
 * Calculates the change of the van der Waals interaction between the
 * QM molecule and the changed MM molecule.
 *
 * @param dists_new
 * @param dists_old
 * @param changed_molecule
 * @return change of the vdW energy in kJ/mol
 */
double OPLS_Periodic::energy_vdw_qmmm (
        DistanceCall GetDistance,
        DistanceCall GetDistance_old,
        int changed_molecule) const
{
   double energy = 0.0;
   for (int pos = 0; pos < molecule2atom[changed_molecule].natoms; pos++) {
      const int i = molecule2atom[changed_molecule].atom_id[pos];
      for (unsigned int j = 0; j < start_solvent; j++) { // loop over qm mol

         const double dist_new = GetDistance (i, j);
         const double dist_old = GetDistance_old (i, j);
         if (dist_new < cutoff_vdw) {

            const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            const double sigmas = sigma[i] * sigma[j];
            const double t = sigmas / (dist_new * dist_new);
            const double V6 = t * t * t;
            const double V12 = V6 * V6;

            if (dist_old < cutoff_vdw) {
               const double t_o = sigmas / (dist_old * dist_old);
               const double V6_o = t_o * t_o * t_o;
               const double V12_o = V6_o * V6_o;
               energy += sqrt_epsilon * (V12 - V12_o - V6 + V6_o);
            } else { // dist_new < cutoff && dist_old > cutoff
               energy += sqrt_epsilon * (V12 - V6);
            }
         } else if (dist_old < cutoff_vdw) { // dist_new > cutoff && dist_old < cutoff
            // subtracts the old value
            const double sqrt_epsilon = sqrt (epsilon[i] * epsilon[j]);
            const double sigmas = sigma[i] * sigma[j];
            const double t_o = sigmas / (dist_old * dist_old);
            const double V6_o = t_o * t_o * t_o;
            const double V12_o = V6_o * V6_o;
            energy -= sqrt_epsilon * (V12_o - V6_o);

            // dist_new > cutoff && dist_old > cutoff --> always 0
         }
      }
   }
   energy *= 4.0 * KCAL_TO_KJ;
   return energy;
}



const double* OPLS_Periodic::get_normal_charges () const
{
   return normal_charges.data();
}


/**
 * Sets the new value and converts it to 'periodic units'.
 */
void OPLS_Periodic::set_sigma (
      const unsigned int index,
      const double value)
{
   sigma[index] = value / box_dimension;
}



/**
 * Sets the new value and converts it to 'periodic units'.
 */
void OPLS_Periodic::set_charge (
      const unsigned int index,
      const double value)
{
   charge[index] = value / sqrt_box_dimension;
}



void OPLS_Periodic::print (std::string outfile)
{
   FILE *fout;
   fout = fopen (outfile.c_str(), "a");
   fprintf (fout, "\nOPLS Parameters\n\n");
   fprintf (fout, "%8s %-2s %5s %15s %15s %15s\n", "id", "E", "type", "sigma", "epsilon", "charge");
   fprintf (fout, "-----------------------------------------------------------------\n");
   for (int i = 0; i < natom; i++)
   {
      fprintf (fout, "%8u %-2s %5i %15.6f %15.6f %15.6f\n",
            (i + 1), system->atom_name[i].c_str(), system->atom_type[i],
            sigma[i] * box_dimension, epsilon[i],
            charge[i] * sqrt_box_dimension);
   }
   fclose (fout);
}
