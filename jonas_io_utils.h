/**
 *
 * File: jonas_io_utils.h
 * Author: Jonas Feldt, João Oliveira
 *
 * Version: 2015-01-06
 *
 */

#ifndef JONAS_IO_UTILS_H_INCLUDED
#define JONAS_IO_UTILS_H_INCLUDED

#include <string>

class System;
class OPLS;
class Run;

void make_directory_jf(std::string folder_name);

void write_molecule_qm_jf(std::string molpro_template, std::string folder_name, std::string molpro_inp, System *system);

void write_system (
      std::string molpro_template,
      std::string folder_name,
      std::string base_name,
      System *system);

void put_wfu_to_input_ref_jf(std::string folder_name, std::string molpro_inps, int count, std::string wavefunction_name, int which_wfu);
void remove_wf_files_jf (std::string base_name, int counter);

void change_hf_jf(std::string folder_name, std::string molpro_inps, int count);
void put_cube (std::string folder_name, std::string base_name , int count);

void put_latt_to_input_jf(std::string folder_name, std::string molpro_inps, int count);
void put_run_to_input_jf(std::string folder_name, std::string base_name, const Run *run);
void put_latt_to_input_name_jf(std::string folder_name, std::string molpro_inps, std::string latt_name);
void put_dummy_latt_to_input_jf(std::string folder_name, std::string molpro_inps);
void write_latt_file_name_jf(std::string folder_name, std::string latt_name, System *system, const double *charges);
void write_latt_pmc_file_jf(std::string folder_name, int count, int idmol, System *system, System *copy, OPLS *opls);
void write_dummy_latt_file_jf(std::string folder_name);

void molpro_jf(std::string folder_name, std::string input_name, int count);
// default for backward compatibility reasons
void molpro_lattice(std::string folder_name, std::string input_name, std::string molpro, int cores);
double read_energy_molpro_jf(std::string folder_name, std::string output_name, int state = 1);
double read_excitation_molpro (std::string folder_name, std::string base_name, int count);

#endif // JONAS_IO_UTILS_H_INCLUDED
