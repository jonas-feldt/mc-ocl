/**
 *
 * File: FEP_OCLmanager.h
 * Author: Jonas Feldt 
 *
 * Version: 2015-01-06
 *
 */
 
#ifndef FEP_OCLMANAGER_H_INCLUDED
#define FEP_OCLMANAGER_H_INCLUDED

#include <string>

#include "OCLmanager.h"
#include "typedefs.h"
#include "options.h"

class System_Periodic;
class Run_Periodic;
class Pipe_Host;
class Charge_Grid;
class OCLDevice;

class FEP_OCLmanager : public OCLmanager {

public:
         
   FEP_OCLmanager(
               int n_pre_steps, 
               int nkernel, 
               int ndevices, 
               int nchains, 
               const vvs &systemsA,
               System_Periodic* systemB,
               OPLS_Periodic* opls, 
               OPLS_Periodic* oplsB,
               Run_Periodic* run, 
               vvg &gridsA,
               vvg &gridsB,
               int config_data_size, 
               int energy_data_size, 
               _FPOINT_S_ temperature,
               double theta_max,
               double stepmax,   
               vvd &energies_qmA,
               vvd &energies_qmB,
               _FPOINT_S_ energy_qm_gp,
               vvd &energies_mmA,
               vvd &energies_vdw_qmmmA,
               vvd &energies_vdw_qmmmB,
               vvd &energies_oldA,
               vvd &energies_oldB,
               Pipe_Host **hosts,
               std::string basename                      
               );
      
   virtual ~FEP_OCLmanager();
   
   // Inits Opencl, Opencl Devices and Chains
   virtual void init(); 
   
   //Pointers to MM data and Grid and other Stuff
   System_Periodic *m_systemB;
   vvg m_gridsB;
   const OPLS_Periodic *m_oplsB;
   
   vvd m_energies_qmB       ;
   vvd m_energies_vdw_qmmmB ;
   vvd m_energies_oldB      ;

protected:

   virtual void final_output();
   virtual OCLDevice *create_device (const int i) override;
};

#endif //FEP_OCLMANAGER_H_INCLUDED
