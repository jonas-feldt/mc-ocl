
#include <cstdlib>
#include <iostream>
#include <cmath>

#include "Rotate_Translate_Stepper.h"
#include "System.h"
#include "OPLS.h"
#include "typedefs.h"
#include "geom_utils.h"
#include "Run.h"
#include "distance_interface.h"
#include "preferential_rt_stepper.h"


PreferentialRTStepper::PreferentialRTStepper (
      random_engine *rnd_engine,
      System *system,
      Run *run)
   : Rotate_Translate_Stepper (rnd_engine, system, run)
{
   if (run->solute_molecules == 1) {
      std::cerr << "ERROR: Needs exactly one solute molecule for preferential sampling" << std::endl;
      std::exit (EXIT_FAILURE);
   }

   // computes the initial weights
   center_mass (0, system, sx, sy, sz);
   weights = new double[system->nmol_total - 1];
   weights_sum = 0.0;
   for (int i = 1; i < system->nmol_total; ++i) {
      double x, y, z;
      center_mass (i, system, x, y, z);
      weights[i - 1] = 1.0 / compute_minimal_distance (x, y, z, sx, sy, sz);
      weights_sum += weights[i - 1];
   }
} 


void PreferentialRTStepper::step (System *system, const OPLS *opls)
{
   // choose with fixed 10% chance a solute move, otherwise a weighted solvent move
   if (dist_boltzman (*rnd_engine) < 0.1)
   {
      random_molecule = 0;
   } else {
      // interval method to choose molecule i with propability weights[i] / weights_sum
      std::uniform_real_distribution<_FPOINT_S_> dist_weight {0.0, weights_sum};
      double random = dist_weight (*rnd_engine);
      double sum = 0.0;
      for (int i = 1; i < system->nmol_total; ++i) {
         sum += weights[i - 1];
         if (random < sum) {
            random_molecule = i;
            break;
         }
      }
   }

   const auto &m2a = system->molecule2atom;
   const auto dim = m2a[random_molecule].natoms;
   const auto ids = m2a[random_molecule].atom_id;

   // rotates the molecule
   double comx, comy, comz;
   center_mass (random_molecule, system, comx, comy, comz);
   double theta = (*dist_theta) (*rnd_engine);
   double rnd1;
   double rnd2;
   double s_rnd_12;
   do {
      rnd1 = dist_two (*rnd_engine);
      rnd2 = dist_two (*rnd_engine);
      s_rnd_12 = rnd1 * rnd1 + rnd2 * rnd2;
   } while (s_rnd_12 >= 1.0); //Condition s_random_12 = random_1² + random_2² < 1

   // TODO not very efficient
   // translates to origin
   for(int pos = 0 ; pos < dim; ++pos){
      int atom = ids[pos];
      translation (1.0, -comx, -comy, -comz, 
            system->rx, system->ry, system->rz, atom);
   }

   // rotates
   for(int pos = 0 ; pos < dim; ++pos){
      // TODO double calculations inside this function for every atom
      int atom = ids[pos];
      rotate (theta, rnd1, rnd2, s_rnd_12,
            system->rx[atom], system->ry[atom], system->rz[atom]);
   }

   // TODO not very efficient
   // translates back
   for(int pos = 0 ; pos < dim; ++pos){
      int atom = ids[pos];
      translation (1.0, comx, comy, comz, 
            system->rx, system->ry, system->rz, atom);
   }

   // translation
   do {
      rnd1 = dist_two (*rnd_engine);
      rnd2 = dist_two (*rnd_engine); 
      s_rnd_12 = rnd1 * rnd1 + rnd2 * rnd2;
   } while (s_rnd_12 >= 1.0);

   double trans_x = 2 * rnd1 * sqrt (1 - s_rnd_12);
   double trans_y = 2 * rnd2 * sqrt (1 - s_rnd_12);
   double trans_z = 1 - 2 * s_rnd_12;

   double random_step = (*dist_step) (*rnd_engine); 
   for(int pos = 0 ; pos < dim; ++pos){
      int atom = ids[pos];
      translation (random_step, trans_x, trans_y, trans_z, 
            system->rx[atom], system->ry[atom], system->rz[atom]);
   }

   system->UpdateDistances (random_molecule);

   if (random_molecule != 0) {
      // compute the new weight for the acceptance criteria
      comx += random_step * trans_x;
      comy += random_step * trans_y;
      comz += random_step * trans_z;
      weight_new = 1.0 / compute_minimal_distance (comx, comy, comz, sx, sy, sz);
      weights_sum_new = weights_sum - weights[random_molecule - 1] + weight_new;
   }
}



bool PreferentialRTStepper::is_accepted ()
{
   // in case of solute move no weights are needed
   if (random_molecule == 0) {
      if (de < 0 || dist_boltzman (*rnd_engine) < boltzmann_factor (temperature, de)) {
         return true;
      }
      return false;
   }

   // solvent moves with weights
   if (de < 0 ||
         dist_boltzman (*rnd_engine) < (weight_new * weights_sum) / (weights_sum_new * weights[random_molecule - 1]) * 
         boltzmann_factor (temperature, de))
   {
      return true;
   }
   return false;
}


void PreferentialRTStepper::accept (System *system, System *system_old)
{
   Rotate_Translate_Stepper::accept (system, system_old);

   // updates weight and sum of weights
   if (random_molecule != 0) {
      weights[random_molecule - 1] = weight_new;
      weights_sum = weights_sum_new;
   }
}




