/**
 *
 * File: jonas_io_utils.cpp
 * Author: Jonas Feldt, João Oliveira
 *
 * Version: 2015-02-12
 *
 */

#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <fstream>
#include <time.h>
#include <iomanip>
#include <sys/stat.h>
#include <sstream>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <ext/stdio_filebuf.h>

#include "System.h"
#include "Run.h"
#include "OPLS.h"
#include "io_utils.h"
#include "Run_Periodic.h"
#include "System_Periodic.h"
#include "OPLS_Periodic.h"
#include "options.h"

#define HARTREE_TO_KJMOL 2625.5

using namespace std;



/**
 * Creates a folder
 *
 * @param folder_name name of the folder to be created
 */
void make_directory_jf (string folder_name) {
   mkdir (folder_name.c_str (), S_IRWXU | S_IRGRP | S_IXGRP); // creates directory with a given folder name
}



/**
 * Puts the geometry of the QM molecule inside the molpro input
 *
 * @param molpro_template string that contains the template
 * @param folder_name name of the folder where all the molpro inputs will be created
 * @param base_name base name for the input file
 * @param system object were the information of the system are saved
 */
void write_molecule_qm_jf (string molpro_template, string folder_name,
        string base_name, System *system) {

   int natom         = system->natom;
   string *atom_name = system->atom_name;
   const auto coords = system->get_normal_coords ();
   double *x = coords[0];
   double *y = coords[1];
   double *z = coords[2];
   int *molecule     = system->molecule;

   istringstream stream (molpro_template);
   ofstream file_out;
   file_out.open ((folder_name + "/" + base_name + ".inp").c_str ());

   // TODO use available informations
   int atoms_in_molecule = 0; // counts number of atoms in molecule 0
   for (int i = 0; i < natom; i++) {
      if (molecule[i] == 0) {
         atoms_in_molecule++;
      }
   }

   string all_line;
   while (getline (stream, all_line)) {
      if ( all_line.find ("geometry") == 0 ) {
         file_out << "geometry" << endl;
         file_out << atoms_in_molecule << endl;
         file_out << endl;
         for (int i = 0; i < natom; i++) {
            if (molecule[i] == 0) {
               file_out << atom_name[i] << "\t";
               file_out << scientific << setprecision (15);
               file_out << x[i] << "\t";
               file_out << y[i] << "\t";
               file_out << z[i] << endl;
            }
         }
      } else {
         file_out << all_line << endl;
      }
   }
}



void write_system (
      string molpro_template,
      string folder_name,
      string base_name,
      System *system) {

   int natom         = system->natom;
   string *atom_name = system->atom_name;
   const auto coords = system->get_normal_coords ();
   double *x = coords[0];
   double *y = coords[1];
   double *z = coords[2];

   istringstream stream (molpro_template);
   ofstream file_out;
   file_out.open ((folder_name + "/" + base_name + ".inp").c_str ());

   string all_line;
   while (getline (stream, all_line)) {
      if ( all_line.find ("geometry") == 0 ) {
         file_out << "geometry" << endl;
         file_out << natom << endl;
         file_out << endl;
         for (int i = 0; i < natom; i++) {
            file_out << atom_name[i] << "\t";
            file_out << scientific << setprecision (15);
            file_out << x[i] << "\t";
            file_out << y[i] << "\t";
            file_out << z[i] << endl;
         }
      } else {
         file_out << all_line << endl;
      }
   }
}



/**
 * Creates lattice file and writes the point charges for all molecules with an
 * ID != 0 (all MM molecules).
 *
 * @param folder_name name of the folder where all the molpro inputs will be created
 * @string latt_name base name for the lattice file
 * @param count counter that will be added to file name
 * @param system object were the information of the system are saved
 * @param charges array with charges 
 */
void write_latt_file_name_jf(
      string folder_name, string latt_name, System *system,
      const double *charges)
{
   int natom      = system->natom;
   const auto coords = system->get_normal_coords ();
   double *x = coords[0];
   double *y = coords[1];
   double *z = coords[2];
   int *molecule  = system->molecule;

   int number_charges = natom - system->molecule2atom[0].natoms;

   ofstream lat_file;
   lat_file.open ((folder_name + "/" + latt_name + ".lat").c_str (), ios::trunc);
   if (lat_file.is_open ()) {
      lat_file << "Charge Points" << endl;
      lat_file << number_charges << endl;
      lat_file << scientific << setprecision (15);
      for (int i = 0; i < natom; i++) {
         if (molecule[i] != 0) {
            lat_file << setw (25) << x[i];
            lat_file << setw (25) << y[i];
            lat_file << setw (25) << z[i];
            lat_file << setw (25) << charges[i];
            lat_file << " 0" << endl;
         }
      }
      lat_file.close ();
   } else {
      cerr << "ERROR CAN'T CREATE LATTICE FILE" << endl;
      fflush(stderr);
      exit (EXIT_FAILURE);
   }
}




/**
 * Creates a dummy lattice file.
 *
 * @param folder_name name of the folder where all the molpro inputs will be created
 */
void write_dummy_latt_file_jf (string folder_name) {
   ofstream lat_file;
   lat_file.open ((folder_name + "/dummy.lat").c_str ());
   if (lat_file.is_open ()) {
      lat_file  << "Point Charges" << endl;
      lat_file  << "0" << endl;
      lat_file.close ();
   } else {
      cout << " ERROR CAN'T CREATE DUMMY LATTICE FILE" << endl;
      fflush(stderr);
      exit (EXIT_FAILURE);
   }
}




/**
 * Puts a selected wave function inside a molpro input
 *
 * @param folder_name name of the folder where all the molpro inputs will be created
 * @param base_name name of the molpro input
 * @param count counter that will be added to file name
 * @param wfu_counter wavefunction file
 */
void put_wfu_to_input_ref_jf(string folder_name, string file_name, int count, string wavefunction_name, int wfu_counter){

   stringstream count_string;
   count_string << count;
   stringstream wfu;
   wfu << wfu_counter;

   string temp_name = folder_name + "/temp_file_" + count_string.str () + ".inp";
   string new_name = folder_name + "/" + file_name + "_" + count_string.str () + ".inp";


   ifstream file_in (new_name.c_str ());
   ofstream file_out;
   file_out.open (temp_name.c_str ());
   file_out.precision (10);

   string all_line;
   while (getline (file_in, all_line)) {
      if ( all_line.find ("file,2,") == 0 ) {
         file_out << "file,2," + wavefunction_name + "_" + wfu.str () + ".wfu" << endl;
      } else {
         file_out << all_line << endl;
      }
   }

   file_in.close ();
   file_out.close ();

   int result = rename ( temp_name.c_str () , new_name.c_str () );
   if ( result != 0 ) {
      perror ( "ERROR RENAME FILE IN PUT_WFU_TO_INPUT_REF FUNCTION" );
   }
}



/**
 * Puts the name of a lattice file inside of a Molpro input.
 *
 * @param folder_name name of the folder where all the Molpro inputs will be created
 * @param base_name base name for files
 * @param latt_name base name of the lattice file
 * @param count counter that will be added to file name
 */
void put_latt_to_input_name_jf (
      string folder_name, string base_name, string latt_name) {

   string temp_name = folder_name + "/temp_file.inp";
   string input_name = folder_name + "/" + base_name + ".inp";
   string all_line;

   ifstream file_in (input_name.c_str ());
   ofstream file_out;
   file_out.precision (10);
   file_out.open (temp_name.c_str ());

   while (getline (file_in, all_line)) {
      if (all_line.find ("lattice,infile=") == 0) {
         file_out << "lattice,infile=" + latt_name + ".lat" << endl;
      } else {
         file_out << all_line << endl;
      }
   }

   file_in.close ();
   file_out.close ();

   int result = rename (temp_name.c_str (), input_name.c_str ());
   if ( result != 0 ) {
      perror ( "ERROR RENAMING FILE IN put_latt_to_input_name()" );
   }
}



/**
 * Puts dummy lattice file inside a molpro input.
 *
 * @param folder_name name of the folder where all the molpro inputs will be created
 * @param base_name name of the molpro input
 * @param count counter that will be added to file name
 */
void put_dummy_latt_to_input_jf (string folder_name, string file_name) {

   string temp_name = folder_name + "/temp_file" + ".inp";
   string new_name = folder_name + "/" + file_name + ".inp";

   ifstream file_in (new_name.c_str ());
   ofstream file_out;
   file_out.open (temp_name.c_str ());
   file_out.precision (10);

   string all_line;
   while (getline (file_in, all_line)) {
      if ( all_line.find ("lattice,infile=") == 0 ) {
         file_out << "lattice,infile=dummy.lat" << endl;
      } else {
         file_out << all_line << endl;
      }
   }

   file_in.close ();
   file_out.close ();

   int result = rename ( temp_name.c_str () , new_name.c_str () );
   if ( result != 0 ) {
      perror ( "ERROR CAN'T PUT DUMMY LATT FILE IN INPUT" );
   }
}



/**
 * Changes HF in order not to update the wavefunction.
 *
 * @param folder_name name of the folder where all the molpro inputs will be created
 * @param base_name base name for all files
 * @param count counter that will be added to file name
 */
void change_hf_jf (string folder_name, string base_name , int count) {

   stringstream ss;
   ss << count;

   string temp_name = folder_name + "/temp_file_" + ss.str () + ".inp";
   string new_name = folder_name + "/" + base_name + "_" + ss.str () + ".inp";
   ifstream file_in (new_name.c_str ());
   ofstream file_out;
   file_out.open (temp_name.c_str ());
   file_out.precision (10);

   string all_line;
   while (getline (file_in, all_line)) {
      if ( all_line.find ("hf") == 0 ) {
         file_out << "{hf;maxit,0}" << endl;
      } else {
         file_out << all_line << endl;
      }
   }

   file_in.close ();
   file_out.close ();

   int result = rename ( temp_name.c_str () , new_name.c_str () );
   if ( result != 0 ) {
      perror ( "Error renaming input file" );
   }
}




/**
 * Put the command to write the cube file.
 *
 * @param folder_name name of the folder where all the molpro inputs will be created
 * @param base_name base name for all files
 * @param count counter that will be added to file name
 */
void put_cube (string folder_name, string base_name , int count) {

   stringstream ss;
   ss << count;

   string temp_name = folder_name + "/temp_file_" + ss.str () + ".inp";
   string new_name = folder_name + "/" + base_name + "_" + ss.str () + ".inp";
   ifstream file_in (new_name.c_str ());
   ofstream file_out;
   file_out.open (temp_name.c_str ());
   file_out.precision (10);

   string all_line;
   while (getline (file_in, all_line)) {
      file_out << all_line << endl;
   }
   file_out << "{cube,a,-1,100,100,100" << endl;
   file_out << "density,2100.2;step,0.3,0.3,0.3}" << endl;

   file_in.close ();
   file_out.close ();

   int result = rename ( temp_name.c_str () , new_name.c_str () );
   if ( result != 0 ) {
      perror ( "Error renaming input file" );
   }
}





/**
 * Runs version of MOLPRO with linked user.F.
 *
 * @param folder_name name of the folder where all the molpro inputs will be created
 * @param input_name name of the molpro input
 * @param molpro executable of molpro
 */
void molpro_lattice (string folder_name, string input_name, string molpro, int cores) {
   string input = "cd " + folder_name + ";" + molpro + " --no-xml-output -n" + to_string(cores) + " " + input_name + ".inp";
   VERBOSE_LVL_INIT("Call to system: " << input << endl);
   int e = system (input.c_str ());
   if (e == -1) {
      cerr << "Error calling system: " << input << endl;
      fflush(stderr);
      exit(EXIT_FAILURE);
   }
}




/**
 * Reads ground state energy from a molpro output file.
 *
 * @param folder_name name of the folder where all the molpro inputs will be created
 * @param base_name base name for all files
 * @return energy from the QM calculation in kJ/mol
 */
double read_energy_molpro_jf (string folder_name, string file, int state) {

   string output = folder_name + "/" + file + ".out";
   ifstream qn_out (output.c_str ());

   double energy_qm = 0.0;
   bool found = false;
   string all_line;
   while (getline (qn_out, all_line, '\n')) {
      if ( all_line.find (string(" ENERGY(") + std::to_string(state).c_str() + ")") == 0 ) {
         char *cstr = new char [all_line.length () + 1];
         strcpy (cstr, all_line.c_str ()); // TODO change in context of that buffer problem...
         char *p = strtok (cstr, " \t\n");
         int number = 0;
         while (p != 0) {
            if (number == 2) {
               energy_qm = atof (p);
            }
            p = strtok (NULL, " \t\n");
            number += 1;
         }
         delete[] cstr;
         if (p) free (p);
         found = true;
         break;
      }
   }
   qn_out.close ();

   if (!found) {
      cerr << "Error reading the energy from " << output << endl;
      fflush(stderr);
      exit(EXIT_FAILURE);      
   }
   return energy_qm * HARTREE_TO_KJMOL;
}



/**
 * Reads first excitation energy from a molpro output file.
 *
 * @param folder_name name of the folder where all the molpro inputs will be created
 * @param base_name base name for all files
 * @param count counter that will be added to file name
 * @return energy from the QM calculation in eV
 */
double read_excitation_molpro (string folder_name, string base_name, int count) {

   stringstream ss;
   ss << count;
   string output = folder_name + "/" + base_name + "_" + ss.str () + ".out";
   ifstream qn_out (output.c_str ());

   double energy_qm = 0.0;
   bool found = false;
   string all_line;
   while (getline (qn_out, all_line, '\n')) {
      if ( all_line.find (" EX") == 0 ) {
         char *cstr = new char [all_line.length () + 1];
         strcpy (cstr, all_line.c_str ()); // TODO change in context of that buffer problem...
         char *p = strtok (cstr, " ");
         int number = 0;
         while (p != 0) {
            if (number == 2) {
               energy_qm = atof (p);
            }
            p = strtok (NULL, " ");
            number += 1;
         }
         delete[] cstr;
         if (p) free (p);
         found = true;
         break;
      }
   }
   qn_out.close ();

   if (!found) {
      cerr << "Error reading the excitation energy from " << output << endl;
      fflush(stderr);
      exit(EXIT_FAILURE);      
   }
   return energy_qm;
}




/**
 * Deletes previous WFU. This deletes the previous wfu from the /scr/$USER and
 * /home/$USER/wfu, which are the paths which one normally has when one
 * uses MOLPRO.
 *
 * @param base_name base name of this job
 * @param counter postfix for file names
 *
 */
void remove_wf_files_jf (string base_name, int counter) {
   stringstream counter_s;
   counter_s << counter;
   string user = getenv ("USER");
   string tmp = "/scr/" + user + "/" + base_name + "_" + counter_s.str () + ".wfu";
   remove (tmp.c_str ());
   tmp = "/home/" + user + "/wfu/" + base_name + "_" + counter_s.str () + ".wfu";
   remove (tmp.c_str ());
}
