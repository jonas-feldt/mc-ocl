/*
 * File:   test_c_interface.h
 * Author: Jonas Feldt
 * Version: 2013-08-19
 *
 */

#ifndef C_INTEGRALS_H
#define	C_INTEGRALS_H

#ifdef	__cplusplus
extern "C" {
#endif

   void init_binom();

   void read_data(FILE *fp, long int ngrp, long int npsh, long int *inshell,
           long int *infuns, long int *ioffs, double *ixpss, long int *igpoffs,
           double *icentres, long int *ilmins, long int *ilmaxs,
           long int *incarts, long int *ncont, double *ccs, long int *ccoffs);

   void read_density(double *density, FILE *file, long int nbas);

   void basis_pt_charges(long int nbas, long int ngrp, long int npsh,
           long int *inshell, long int *infuns, long int *ioffs, double *ixpss,
           long int *igpoffs, double *icentres, long int *ilmins,
           long int *ilmaxs, long int *incarts, long int nq, double *qq,
           double *rq, double *a, long int *ncont, double *ccs,
           long int *ccoffs);


#ifdef	__cplusplus
}
#endif

#endif	/* C_INTEGRALS_H */

