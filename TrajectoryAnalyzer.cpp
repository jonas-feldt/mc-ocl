/*
 * File:   TrajectoryAnalyzer.cpp
 * Author: Jonas Feldt
 *
 * Version: 2014-02-17
 */

#include <fstream>
#include <string>

#include "TrajectoryAnalyzer.h"
#include "io_utils.h"


/*
 * Assumes the same number of frames in every chain!
 */
TrajectoryAnalyzer::TrajectoryAnalyzer (
        std::string trajectory_file, int first_frame, int last_frame,
        std::string output_file, const unsigned int num_chains)
   : first_frame (first_frame),
   trajectory_file (trajectory_file),
   output_file (output_file),
   system (nullptr),
   num_chains (num_chains)
{
   steps_file = last_frame - first_frame + 1;
   steps = steps_file * num_chains; 
}


void TrajectoryAnalyzer::read_system (std::string tinker_file)
{
   system = read_tinker_system (tinker_file);
}


TrajectoryAnalyzer::~TrajectoryAnalyzer ()
{
   if (system != nullptr) delete system;
}



void TrajectoryAnalyzer::run ()
{
   int num = system->natom;
   std::string line;

   for (unsigned int i = 0; i < num_chains; ++i) {
      std::string tmp = trajectory_file + "_" + std::to_string(i);
      std::ifstream trajectory (tmp.c_str());
      for (int i = 0; i < first_frame - 1; ++i) { // neglects first_frame systems
         for (int j = 0; j < num; ++j) {
            std::getline (trajectory, line); // discard lines
         }
      }

      for (int i = 0; i < steps_file; ++i) {
         read_frame (trajectory, system);
         analyze ();
      }
   }

   finalize();
   save_output ();
}
