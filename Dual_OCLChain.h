/**
 *
 * File: Dual_OCLChain.h
 * Author: Jonas Feldt
 *
 * Version: 2015-03-11
 *
 */

#ifndef DUAL_OCLCHAIN_H_INCLUDED
#define DUAL_OCLCHAIN_H_INCLUDED

#define __CL_ENABLE_EXCEPTIONS
#include <string>
#include <iostream>
#include <fstream>
#include <thread>
#include <math.h>

#include "options.h"
#include "math_utils.h"
#include "OCLChain.h"

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include <CL/cl.hpp>
#endif

class Pipe_Host;
class Charge_Grid;
class System_Periodic;

class Dual_OCLChain : public OCLChain {

public:

   Dual_OCLChain(
         System_Periodic *systemA,
         Charge_Grid *gridA,
         Charge_Grid *gridB,
         Pipe_Host *hostA,
         _FPOINT_S_ energy_qmA,
         _FPOINT_S_ energy_qmB,
         _FPOINT_S_ energy_qm_gp,
         _FPOINT_S_ energy_mm,
         _FPOINT_S_ energy_vdw_qmmm,
         _FPOINT_S_ energy_oldA,
         _FPOINT_S_ energy_oldB,
         int n_pre_steps,
         int n_config_data_size,
         int n_energy_data_size,
         int seed,
         double stepmax,
         std::string base_name,
         int num_means,
         int n_energy_save_params,
         std::string print_s,
         bool extra_energy_file,
         bool has_seed_state,
         std::string state_file,
         const double dimension_box);

   virtual ~Dual_OCLChain() override;

   //Setup buffers for chain
   virtual void setup_buffers() override;

   //Write grid into the device's chain bufers
   virtual void write_grid(int inner_step, bool reduce,
         double dimension_box, double cutoff_coulomb) override;

   //Write init energies
   virtual void write_init_energies() override;

   //Update reference energy
   virtual void update_reference_energy(int inner_step) override;

   virtual ChainResults *get_results (const int steps) override;

   // Pointers to host side objects
   Charge_Grid *m_gridB  ;

   // Buffers that refer only to one chain

   //Grid data
   cl::Buffer m_grid_rxB    ;
   cl::Buffer m_grid_ryB    ;
   cl::Buffer m_grid_rzB    ;
   cl::Buffer m_grid_chargeB;
   cl::Buffer m_grid_sizeB  ;

   //buffers for energies
   cl::Buffer m_qmmm_c_energyB;

   //Host provided energies
   cl::Buffer m_qm_energyB; // Also, updated by OpenCL device

   //Persistent OpenCL Device data
   cl::Buffer m_energy_oldB;

protected:

   virtual void chain_loop() override;

   // Host side energy variables respecting to this chain
   // (host side _FPOINT_S_'s)
   _FPOINT_S_ m_f_energy_qmB       ;
   _FPOINT_S_ m_f_energy_oldB      ;

   //
   // Events
   //

   // Global Grid Buffers Write
   cl::Event m_EVENT_grid_rxB;
   cl::Event m_EVENT_grid_ryB;
   cl::Event m_EVENT_grid_rzB;
   cl::Event m_EVENT_grid_chargeB;
   cl::Event m_EVENT_grid_sizeB;

   // Energy qm
   cl::Event m_EVENT_qm_energyB;
   cl::Event m_EVENT_energy_oldB;
};

#endif //DUAL_OCLCHAIN_H_INCLUDED

