/*
 * File:    OPLS_Switcher.h
 * Author:  Jonas Feldt
 *
 */

#ifndef OPLS_SWITCHER_H
#define OPLS_SWITCHER_H

#include <vector>
#include <utility>
#include <cstdio>

#include "Abstract_Switcher.h"

class OPLS;
class System;
class Run;

class OPLS_Switcher : public Abstract_Switcher {

   public:
      // dummy type FES
      static const int DUMMY_TYPE;

      OPLS_Switcher (const System *system, Run *run);

      virtual ~OPLS_Switcher();

      virtual void switch_params (const int unsigned frame, OPLS *opls, FILE *fout) override;

   protected:

      virtual double compute_factor (const unsigned int frame) = 0;

      const int natom;
      const std::vector<int> system_types;

      const std::vector<int> atom_types;
      const std::vector<double> epsilons;
      const std::vector<double> sigmas;
      const std::vector<double> charges;

      const double num_frames;
      std::vector<std::pair<int, int>> types;
   

};

#endif /* OPLS_SWITCHER_H */
