/*
 * File:    OPLS_Switcher_linear.h
 * Author:  Jonas Feldt
 *
 * Version: 2015-04-14
 */

#ifndef OPLS_SWITCHER_LINEAR_H
#define OPLS_SWITCHER_LINEAR_H

#include "OPLS_Switcher.h"

class OPLS_Switcher_linear : public OPLS_Switcher {

   public:

      using OPLS_Switcher::OPLS_Switcher; // redeclare constructor

   protected:

      virtual double compute_factor (const unsigned int frame) override;

};

#endif // OPLS_SWITCHER_LINEAR_H
