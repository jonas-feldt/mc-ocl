/**
 *
 * File: Run_Periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2015-02-18
 *
 */

#ifndef RUN_PERIODIC_H
#define RUN_PERIODIC_H

#include <string>

#include "Run.h"

class Run_Periodic : public Run {

public:

   double dimension_box;
   double cutoff_coulomb;
   double cutoff_vdw;

   Run_Periodic(int maxmoves, double stepmax, double temperature,
           double theta_max, int wf_updates, int adjust_max_step,
           int print_confs, double cutoff_coulomb, double cutoff_vdw,
           double dimension_box, int print_energy,
           const std::string grid_input,
           const std::string pipe_input,
           int seed, bool has_seed_path, std::string seed_path);
   Run_Periodic() = default;
   Run_Periodic(const Run_Periodic&) = default;

   void set_dimension_box (const double dimension);

   virtual void print();
   virtual void fprint(std::string file);
   bool is_sane(std::string outfile);

protected:
private:
};

#endif // RUN_PERIODIC_H
