/**
 *
 * File: ChainResults.cpp
 * Author: Jonas Feldt
 *
 * Version: 2014-10-22
 *
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <cmath>

#include "ChainResults.h"

#define WIDTH 15
#define SMALL 3



/**
 * Constructor
 *
 */
ChainResults::ChainResults (
      double mean_energy, double mean_interaction,
      double std_dev_energy, double std_dev_interaction,
      double stepmax, int steps, int accepted_steps)
   : mean_energy (mean_energy), mean_interaction (mean_interaction),
   std_dev_energy (std_dev_energy), std_dev_interaction (std_dev_interaction),
   stepmax (stepmax), steps (steps), accepted_steps (accepted_steps)
{
}




/*
 * Destructor
 */
ChainResults::~ChainResults ()
{
}



void ChainResults::print_results (std::string file, std::vector<ChainResults*> results)
{
   std::ofstream out (file, std::ofstream::app);

   // title
   out << std::endl;
   out << "Results for " << results.size() << " chain";
   if (results.size() > 1) out << "s"; // chain or chains 
   out << std::endl;
   out << std::endl;

   // header for the table
   out << std::setw(WIDTH + SMALL + 2)  << "Energy";
   out << std::setw(WIDTH) << "+/-";
   out << std::setw(WIDTH) << "E_int";
   out << std::setw(WIDTH) << "+/-";
   out << std::setw(WIDTH) << "Steps";
   out << std::setw(WIDTH) << "Accepted";
   out << std::setw(WIDTH) << "Stepmax" << std::endl;
   out << std::setw(SMALL + 2 + 7 * WIDTH) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

   // variables for the combined results
   double energy        = 0.0;
   double sigma_energy  = 0.0;
   double eint          = 0.0;
   double sigma_eint    = 0.0;
   int steps            = 0;
   int accepted_steps   = 0;

   // table
   int index = 0;
   for (auto it = results.begin(); it != results.end(); ++it, ++index) {
      ChainResults *r = (*it);
      double dSteps = (double) r->steps;
      // sums up
      if (results.size() > 1) {
         energy   += r->mean_energy      * dSteps;
         eint     += r->mean_interaction * dSteps;
         sigma_energy += (pow(r->std_dev_energy     , 2.0) + pow(r->mean_energy     , 2.0)) * dSteps;
         sigma_eint   += (pow(r->std_dev_interaction, 2.0) + pow(r->mean_interaction, 2.0)) * dSteps;
         steps          += r->steps;
         accepted_steps += r->accepted_steps;
      }

      // print row of the table
      out << std::setw(SMALL) << index << " |"; // index

      out << std::scientific << std::setprecision(6);
      out << std::setw(WIDTH) << r->mean_energy; // energy
      out << std::setw(WIDTH) << r->std_dev_energy;

      out << std::setw(WIDTH) << r->mean_interaction; // E_int
      out << std::setw(WIDTH) << r->std_dev_interaction;

      out << std::setw(WIDTH) << r->steps; // steps
      out << std::setw(WIDTH - 2) << std::fixed << std::setprecision(1);
      out << (double) r->accepted_steps / dSteps * 100.0 << " %";

      out << std::setw(WIDTH) << std::setprecision(6) << r->stepmax << std::endl; // stepmax 
   }

   if (results.size() > 1) {
      out << std::setw(SMALL + 2 + 7 * WIDTH) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      // prints summed up row of the table
      out << std::setw(SMALL + 2) << " |"; // no index

      double dSteps = (double) steps;
      out << std::scientific << std::setprecision(6);
      energy /= dSteps;
      out << std::setw(WIDTH) <<  energy; // energy
      out << std::setw(WIDTH) << sqrt(sigma_energy / dSteps - pow(energy, 2.0));

      eint /= dSteps;
      out << std::setw(WIDTH) << eint; // eint
      out << std::setw(WIDTH) << sqrt(sigma_eint / dSteps - pow(eint, 2.0));

      out << std::setw(WIDTH) << steps; // steps
      out << std::setw(WIDTH - 2) << std::fixed << std::setprecision(1);
      out << (double) accepted_steps / dSteps * 100.0 << " %";

      out << std::setw(WIDTH) << "" << std::endl; // empty field
   }
}



