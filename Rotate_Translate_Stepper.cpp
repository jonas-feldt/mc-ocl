
#include <cstdlib>
#include <iostream>
#include <cmath>

#include "Rotate_Translate_Stepper.h"
#include "System.h"
#include "OPLS.h"
#include "typedefs.h"
#include "geom_utils.h"
#include "Run.h"
#include "options.h"
#include "distance_interface.h"


Rotate_Translate_Stepper::Rotate_Translate_Stepper (
      random_engine *rnd_engine,
      System *system,
      Run *run)
   : stepmax (run->stepmax),
     thetamax (run->theta_max * (M_PI / 180.0)),
     rnd_engine (rnd_engine),
     temperature (run->temperature),
     adjust_step (run->adjust_max_step),
     adjust_theta (run->adjust_max_theta ),
     acceptance_ratio (run->acceptance_ratio)
{
   dist_molecule = new std::uniform_int_distribution<int> (run->solute_molecules, system->nmol_total - 1);
   dist_theta = new std::uniform_real_distribution<double> ( 0.0, thetamax);
   dist_step = new std::uniform_real_distribution<_FPOINT_S_> ( 0.0, stepmax);
} 


Rotate_Translate_Stepper::~Rotate_Translate_Stepper ()
{
   delete dist_molecule;
   delete dist_theta;
   delete dist_step;
}



void Rotate_Translate_Stepper::step (System *system, const OPLS *opls)
{
   random_molecule = (*dist_molecule) (*rnd_engine);

   const auto &m2a = system->molecule2atom;
   const auto dim = m2a[random_molecule].natoms;
   const auto ids = m2a[random_molecule].atom_id;

   // rotates the molecule
   double comx, comy, comz;
   center_mass (random_molecule, system, comx, comy, comz);
   double theta = (*dist_theta) (*rnd_engine);
   double rnd1;
   double rnd2;
   double s_rnd_12;
   do {
      rnd1 = dist_two (*rnd_engine);
      rnd2 = dist_two (*rnd_engine);
      s_rnd_12 = rnd1 * rnd1 + rnd2 * rnd2;
   } while (s_rnd_12 >= 1.0); //Condition s_random_12 = random_1² + random_2² < 1

   // TODO not very efficient
   // translates to origin
   for(int pos = 0 ; pos < dim; ++pos){
      int atom = ids[pos];
      translation (1.0, -comx, -comy, -comz, 
            system->rx, system->ry, system->rz, atom);
   }

   // rotates
   for(int pos = 0 ; pos < dim; ++pos){
      // TODO double calculations inside this function for every atom
      int atom = ids[pos];
      rotate (theta, rnd1, rnd2, s_rnd_12,
            system->rx[atom], system->ry[atom], system->rz[atom]);
   }

   // TODO not very efficient
   // translates back
   for(int pos = 0 ; pos < dim; ++pos){
      int atom = ids[pos];
      translation (1.0, comx, comy, comz, 
            system->rx, system->ry, system->rz, atom);
   }

   // translation
   do {
      rnd1 = dist_two (*rnd_engine);
      rnd2 = dist_two (*rnd_engine); 
      s_rnd_12 = rnd1 * rnd1 + rnd2 * rnd2;
   } while (s_rnd_12 >= 1.0);

   double trans_x = 2 * rnd1 * sqrt (1 - s_rnd_12);
   double trans_y = 2 * rnd2 * sqrt (1 - s_rnd_12);
   double trans_z = 1 - 2 * s_rnd_12;

   double random_step = (*dist_step) (*rnd_engine); 
   for(int pos = 0 ; pos < dim; ++pos){
      int atom = ids[pos];
      translation (random_step, trans_x, trans_y, trans_z, 
            system->rx[atom], system->ry[atom], system->rz[atom]);
   }

   system->UpdateDistances (random_molecule);
}



double Rotate_Translate_Stepper::dE (
      System *system,
      System *system_old,
      const OPLS *opls)
{
   // sets de for following calls!
   de = opls->energy (system->GetDistance,
         system_old->GetDistance, random_molecule);
   return de;
}



void Rotate_Translate_Stepper::accept (System *system, System *system_old)
{
   adj_acc_steps++;
   adj_steps++;

   copy_coordinates (system, system_old, random_molecule);
   system_old->distances->accept (system->distances, random_molecule);
}



void Rotate_Translate_Stepper::reject (System *system, System *system_old)
{
   adj_steps++;

   copy_coordinates (system_old, system, random_molecule);
   system->distances->accept (system_old->distances, random_molecule);
}


bool Rotate_Translate_Stepper::is_accepted ()
{
   if (de < 0 ||
         dist_boltzman (*rnd_engine) < boltzmann_factor (temperature, de))
   {
      return true;
   }
   return false;
}


void Rotate_Translate_Stepper::update (int step)
{
   if (adjust_step != 0 && step != 0 && (step % adjust_step) == 0)
   {
      double tmp = stepmax;
      stepmax = adjust_stepmax (adj_steps, adj_acc_steps, stepmax, acceptance_ratio);
      if ((std::abs (tmp - stepmax) / tmp) > 0.05) // more than 5% diff
      {
         adj_steps = 0;
         adj_acc_steps = 0;
      }
      delete dist_step;
      dist_step = new std::uniform_real_distribution<_FPOINT_S_> (0.0, stepmax);
   } else if (adjust_theta != 0 && step != 0 && (step % adjust_theta) == 0)
   {
      double tmp = thetamax;
      thetamax = adjust_stepmax (adj_steps, adj_acc_steps, thetamax, acceptance_ratio);
      if ((std::abs (tmp - thetamax) / tmp) > 0.05) // more than 5% diff
      {
         adj_steps = 0;
         adj_acc_steps = 0;
      }
      delete dist_theta;
      dist_theta = new std::uniform_real_distribution<double> (0.0, thetamax);
   }
}


