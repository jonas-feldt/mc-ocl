/**
 *
 * File: OCLmanager.cpp
 * Author: Sebastiao Miranda, Jonas Feldt
 *
 * Version: 2015-02-16
 *
 */
 
#include <math.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <atomic>
#include <random>

#ifdef BASE_PROFILING
   #include <iomanip>
   #include "papi.h"
#endif

#include "OCLmanager.h"
#include "options.h"

#define NUM_MEANS 2 // number of means


OCLmanager::OCLmanager( int n_pre_steps, 
                        int nkernel, 
                        int ndevices, 
                        int nchains,
                        const vvs &systems,
                        const OPLS_Periodic* opls, 
                        Run_Periodic* run, 
                        vvg &grids,
                        int config_data_size, 
                        int energy_data_size, 
                        _FPOINT_S_ temperature,
                        double theta_max,
                        double stepmax,   
                        const vvd &energies_qm,
                        _FPOINT_S_ energy_qm_gp,
                        const vvd &energies_mm,
                        const vvd &energies_vdw_qmmm,
                        const vvd &energies_old,
                        Pipe_Host **hosts,
                        std::string   basename)
{
   
   m_ndevices           = ndevices;
   m_nchains_per_device = nchains;
   m_config_data_size   = config_data_size;
   m_energy_data_size   = energy_data_size;

   if (run->print_energy != 0)   has_extra_energy_file = true;
   else                          has_extra_energy_file = false;
 
   m_systems   = systems;
   m_opls      = opls;
   m_run       = run;
   m_grids     = grids;
   m_hosts     = hosts;
   m_basename  = basename;
   m_stepmax   = stepmax;
   m_n_pre_steps = n_pre_steps;
   m_temperature = temperature;

   // chain shared energies 
   m_energy_qm_gp      = energy_qm_gp      ;  

   // chain specific energies
   m_energies_qm       = energies_qm       ;
   m_energies_mm       = energies_mm       ;
   m_energies_vdw_qmmm = energies_vdw_qmmm ;
   m_energies_old      = energies_old      ;      
   
   m_captured_context  = new cl::Context*[m_ndevices] ;
   m_captured_device   = new cl::Device [m_ndevices]  ;

   v_ignite_isReady = new int[m_ndevices]                      ;
   v_ignite_mutex   = new std::mutex[m_ndevices]               ;
   v_ignite_slave   = new std::condition_variable[m_ndevices]  ;
   
   #ifdef LEGACY_MULTI_DEVICE 
      m_bal = new double[m_ndevices];
   #endif

   for (int a = 0; a < m_ndevices; a++){
      v_ignite_isReady[a] = 0;
   }    
}



OCLmanager::~OCLmanager(){
   
   // delete first devices which finishs/joins chains
   for (int i = 0; i < m_ndevices; ++i) {
      delete m_devices[i];
      delete m_captured_context[i];
   }

   // save random state
   for (int i = 0; i < m_nchains; ++i) {
      m_chains[i]->save_random_state(m_basename + "/" + std::to_string(i) + ".random");
   }

   // afterwards delete chains
   for (auto it = m_chains.begin(); it != m_chains.end(); ++it) {
      delete (*it);
   }

   delete[] m_captured_context;
   delete[] m_captured_device ;

   delete[] v_ignite_isReady  ;
   delete[] v_ignite_mutex    ;
   delete[] v_ignite_slave    ;


   #ifdef LEGACY_MULTI_DEVICE                       
      delete[] v_bcast_qmmm_c     ; 
      delete[] v_bcast_mm         ; 
      delete[] v_bcast_qmmm_vdw   ; 
      delete[] v_bcast_qmmm_c_nucl; 
      
      delete p_barrier_mutex; 
      delete p_barrier_cond ; 
      delete p_barrier_count; 
      delete[] m_bal;
   #endif
}

// Init Opencl platforms, devices, etc
void OCLmanager::init(){ 
   
 
   // Either launch m_nchains_per_device per device   
   // or m_ndevies for 1 chain, in which case
   // multiple clones of that chain are created
   #ifdef LEGACY_MULTI_DEVICE
      m_nchains = m_ndevices;
   #else
      m_nchains = m_nchains_per_device * m_ndevices;
   #endif

   try{
      
      // Query machine for available hardware
      device_query();
      
      // Print some info associated with the device
      print_device_status();

      // seeds PRNG
      int seed = 0;
      std::string seed_file;
      random_engine engine;
      std::uniform_int_distribution<int> dist (
            std::numeric_limits<int>::min(),
            std::numeric_limits<int>::max());

      if (not m_run->has_seed_path) {
         seed_file = "";
         engine.seed(m_run->seed);
         #ifdef LEGACY_MULTI_DEVICE
            // only a single seed for all devices
            seed = dist(engine);
         #endif
      } else {
         seed_file = m_run->seed_path;
      }
      
      // Create chain objects
      // (Correspond to one path of the state space exploration tree)
      int index = 0;
      for (unsigned int i = 0; i < m_systems.size(); ++i) {
         for (unsigned int j = 0; j < m_systems.begin()->size(); ++j) {

            // seeds for chains
            #ifndef LEGACY_MULTI_DEVICE
               if (not m_run->has_seed_path) {
                  seed = dist(engine);
               }
            #endif


            m_chains.push_back(
               new OCLChain(
                  m_systems[i][j]          , //The Chain makes a copy of *m_system
                  m_grids[i][j]            , //The Chain makes a copy of *m_grids
                  m_hosts[index]           , //Each chain is associated with a pipe host
                  m_energies_qm[i][j]      ,
                  m_energy_qm_gp           , 
                  m_energies_mm[i][j]      ,
                  m_energies_vdw_qmmm[i][j],
                  m_energies_old[i][j]     ,
                  m_n_pre_steps            ,
                  m_config_data_size       ,   
                  m_energy_data_size       ,   
                  seed                     , 
                  m_stepmax                ,
                  m_basename               ,
                  NUM_MEANS                ,
                  N_ENERGY_SAVE_PARAMS     ,
                  PRINT_S                  ,
                  has_extra_energy_file    ,
                  m_run->has_seed_path     ,
                  seed_file + "/" + std::to_string(index) + ".random",
                  m_run->dimension_box
               )
            );
            index++;
         }
      }
      // Create opencl device managers and add chains to them
      //
      // Either create <m_ndevices> devices for 1-chain parallelism 
      // (each with a cloned chain),
      // OR create one devce per chain.
      #ifdef LEGACY_MULTI_DEVICE

	 //Barrier Variables
         v_bcast_qmmm_c       = new _FPOINT_G_[m_ndevices];
         v_bcast_mm           = new _FPOINT_S_[m_ndevices];
         v_bcast_qmmm_vdw     = new _FPOINT_S_[m_ndevices];
         v_bcast_qmmm_c_nucl  = new _FPOINT_S_[m_ndevices];
         
         p_barrier_mutex = new std::mutex();
         p_barrier_cond  = new std::condition_variable(); 
         p_barrier_count = new int();
         *p_barrier_count = 0;
         m_barrier_max   = m_ndevices;   
         
         //Balancing Vector
         //for (int i = 0; i < m_ndevices; i++){
            //m_bal[i]=(double)1/(double)m_ndevices;
            
            VERBOSE_LVL_SETUP2("xxx:: WARNING: MANUAL BALANCING SEED SETUP: "<<std::endl);
            m_bal[0]=0.5;
            m_bal[1]=0.5;
            VERBOSE_LVL_SETUP2("[SETU] m_bal["<<0<<"]="<<m_bal[0]<<std::endl);
            VERBOSE_LVL_SETUP2("[SETU] m_bal["<<1<<"]="<<m_bal[1]<<std::endl);
         //}
   
         for (int i = 0 ; i < m_ndevices ; i++){   
            
           int dev_index=i;
           #ifdef ALT_DEV0 // Flag to allow using second device as device 0 (master in LEGACY_MULTI)
              if (i==0 and m_ndevices>1){
                 dev_index = 1;
              }else if (i==1 and m_ndevices>1){
                 dev_index = 0;
              }
           #endif

            m_devices.push_back(
               new OCLDevice(
                     m_captured_device[dev_index],
                     m_captured_context[dev_index],
                     &v_ignite_isReady[i],//Sync variables
                     &v_ignite_mutex[i],  //for master-device comn
                     &v_ignite_slave[i],  //
                     &ignite_master,      //

                     v_bcast_qmmm_c,     //Sync variables
                     v_bcast_mm,         //Sync variables
                     v_bcast_qmmm_vdw,   //Sync variables
                     v_bcast_qmmm_c_nucl,//Sync variables
                     
                     p_barrier_mutex,     //for device-to-device barrier 
                     p_barrier_cond,      // 
                     p_barrier_count,     //
                     m_barrier_max,       //
                     m_bal,               // balancing vector                  
                     #ifdef BALANCING
                        this,
                     #endif
                     m_n_pre_steps, 
                     m_nkernel,
                     m_nchains_per_device,// Max chains per device 
                     m_system,
                     m_opls, 
                     m_run, 
                     m_temperature,
                     m_ndevices,
                     has_extra_energy_file)
                  );  
                  
            // Add cloned chain to device and make association
            m_devices[i]->add_chain(m_chains[i]);            
            m_chains[i]->assoc_device(m_devices[i],0);
           
            // Startup device 
            m_devices[i]->prepare_randomness();
            m_devices[i]->setup_pmc(m_kernel_file_closure);
            m_devices[i]->first_writes();
         }

      #else

         for (int i = 0 ; i < m_ndevices ; i++)
         {
            m_devices.push_back(create_device (i));
                  
            // Add chains to device and make association
            for(int j = 0; j < m_nchains_per_device; j++){
               m_devices[i]->add_chain(m_chains[i * m_nchains_per_device + j]);            
               m_chains[i * m_nchains_per_device + j]->assoc_device(m_devices[i],j);
            }
            
            // Startup device
            m_devices[i]->prepare_randomness();
            m_devices[i]->setup_pmc(m_kernel_file_closure);
            m_devices[i]->first_writes();

            #ifdef LEGACY_HETERO_DEVICE
               m_devices[i]->energy_qm         =  m_energy_qm         ;
               m_devices[i]->energy_qm_gp      =  m_energy_qm_gp      ; 
               m_devices[i]->energy_mm         =  m_energy_mm         ;
               m_devices[i]->energy_vdw_qmmm   =  m_energy_vdw_qmmm   ;
               m_devices[i]->energy_old        =  m_energy_old        ;
           #endif
         }   
      #endif
 
      //
      // Launch Chain Threads (for pipe comn)
      //
      
      #ifdef WILL_WF_ADJUST
         for (int i = 0; i < m_nchains ; i++)
            m_chains[i]->launch_thread();
      #endif
      
      // Launch Device Threads (for device comn)
      //
      for (int i = 0; i < m_ndevices ; i++)
         m_devices[i]->launch_thread();
       
   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLmanager:: Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
}

void OCLmanager::device_query(){
   
   int index0=0, index1=0,capturedDevices=0; 
   try{

      int avoid = m_run->skip_first;
      //Query for platforms
      cl::Platform::get(&m_platforms);
      std::string retval;
      std::cout << "Platform listing:" << std::endl;
      for(std::vector<cl::Platform>::iterator it = m_platforms.begin(); it != m_platforms.end(); ++it) {  
      
         it->getInfo(CL_PLATFORM_NAME,&retval);
         std::cout << index0 <<": "<< retval << std::endl;
         std::vector<cl::Device> curr_devices;
         
         it->getDevices(CL_DEVICE_TYPE_ALL, &curr_devices);
         std::cout <<"\tDevices:"<< std::endl;
         
         index1=0;
         for(std::vector<cl::Device>::iterator it1 = curr_devices.begin(); it1 != curr_devices.end(); ++it1) {    
            
            it1->getInfo(CL_DEVICE_VENDOR,&retval);
            std::cout << retval << std::endl;
            index1++;

            if(((retval.find(std::string(TARGET_VENDOR)) != std::string::npos) or
                     std::string(TARGET_VENDOR)==std::string("ANY")) and
                  (retval.find(std::string(TARGET_VENDOR_AVOID)) == std::string::npos)){
               it1->getInfo(CL_DEVICE_NAME,&retval);
               std::cout <<"\t"<< index1 <<": "<< retval << std::endl;
               if(avoid>0){avoid--;continue;} 
               if(capturedDevices<m_ndevices){
                  cl::Context* curr_context = new cl::Context(*it1);
                  m_captured_context[capturedDevices] =curr_context;
                  m_captured_device[capturedDevices]  =(*it1);
                  std::cout <<"\t\t Created context for this device" << std::endl;
                  capturedDevices++;
               }
            }
         }
         index0++;		
      }
      
      if(m_ndevices != capturedDevices){
         std::cout<<"OCLmanager::device_query Error: Wrong number of devices"<<std::endl;
         throw;
      }
      
   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLmanager::device_query Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }      
}
void OCLmanager::print_device_status(){

   try{
      
      #ifdef SET_VERBOSE_LVL_SETUP

         std::cout << "CL_DEVICE_MAX_MEM_ALLOC_SIZE:" << std::endl;
         int d=0;
         for(std::vector<OCLDevice*>::iterator it = m_devices.begin(); it != m_devices.end(); ++it){
            unsigned long long ret;
            (*it)->m_device.getInfo(CL_DEVICE_MAX_MEM_ALLOC_SIZE,&ret); 
            std::cout << "\tDevice "<<d<<": "<<ret/(1024*1024)<< " Mb"<<std::endl;
            d++;
         }
         
         std::cout << "CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE :" << std::endl;
         d=0;
         for(std::vector<OCLDevice*>::iterator it = m_devices.begin(); it != m_devices.end(); ++it){
            unsigned long long ret;
            (*it)->m_device.getInfo(CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,&ret); 
            std::cout << "\tDevice "<<d<<": "<<ret/(1024)<< " Kb"<<std::endl;
            d++;
         }
         
         std::cout << "CL_DEVICE_MAX_CONSTANT_ARGS :" << std::endl;
         d=0;
         for(std::vector<OCLDevice*>::iterator it = m_devices.begin(); it != m_devices.end(); ++it){
            unsigned int ret;
            (*it)->m_device.getInfo(CL_DEVICE_MAX_CONSTANT_ARGS,&ret); 
            std::cout << "\tDevice "<<d<<": "<<ret<<std::endl;
            d++;
         }

         std::cout << "CL_DEVICE_LOCAL_MEM_SIZE :" << std::endl;
         d=0;
         for(std::vector<OCLDevice*>::iterator it = m_devices.begin(); it != m_devices.end(); ++it){
            unsigned long long ret;
            (*it)->m_device.getInfo(CL_DEVICE_LOCAL_MEM_SIZE,&ret); 
            std::cout << "\tDevice "<<d<<": "<<ret/(1024)<< " Kb"<<std::endl;
            d++;
         }
         
      #endif
   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLmanager::print_device_status Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }      
}
void OCLmanager::run(){

   // ::init() has to be called before this

   // Ignite PMC Cycles
   //
   for (int i = 0; i < m_ndevices ; i++){    
      std::unique_lock<std::mutex> ulock(v_ignite_mutex[i]);
      
         while(v_ignite_isReady[i] == 1){
            VERBOSE_LVL_SYNC("[SYNC] OCLmanager is waiting for thread " << i << " ..." << std::endl);
            ignite_master.wait(ulock); 
         }
                                                
         v_ignite_isReady[i] = 1;
         v_ignite_slave[i].notify_all();    
      
      ulock.unlock();
   }
   
   // At the end of K PMC Cycles, rebalance execution
   // via chain stealing
   // TODO
   //
   
   // Wait for every device (chain group) to finish all PMC Cycles
   //
   for (int i = 0; i < m_ndevices ; i++){    
      m_devices[i]->join_thread();
      VERBOSE_LVL_SYNC("[SYNC] OCLmanager, Device Thread "<<i <<" joined."<< std::endl);
   } 
   
   // End of all PMC Cycles
   //
   
   final_output();
}



/**
 * Gathers results from devices and prints them.
 */
void OCLmanager::final_output() {

   std::vector<ChainResults*> results;

   #ifdef LEGACY_MULTI_DEVICE
      // In this mode, only the first device needs to output
      results = m_devices[0]->get_results();
   #else
      for (int i = 0; i < m_ndevices ; i++){    
         std::vector<ChainResults*> tmp = m_devices[i]->get_results();
         results.insert(results.end(), tmp.begin(), tmp.end());
      } 
   #endif

   ChainResults::print_results(m_basename + ".out", results);

   // clean up results
   for (auto it = results.begin(); it != results.end(); ++it) {
      delete *it;
   }
}




/*
 * Returns the final systems of all devices and all chains.
 * The systems have to be deleted if no longer needed. 
 *
 * @return systems[num_dev][num_chains] 
 */
std::vector<std::vector<System_Periodic*>> OCLmanager::get_final_systems() {
   std::vector<std::vector<System_Periodic*>> systems;
   for (auto it = m_devices.begin(); it != m_devices.end(); ++it) {
      systems.push_back((*it)->get_final_systems());
   }
   return systems;
}



void OCLmanager::final_profile(){
   // TIME_PROBE profiling and .py output
   //
   #ifdef TIME_PROBE
      //Write python script:
 
      std::cout << "OCLmanager: Outputing .py with global time data" << std::endl; 

      ofstream out_py;
      out_py.open ("time_probe.py");
      //out.precision (3);
      //out.setf (std::ios::fixed, ios::floatfield);
      int index;
      if(out_py.is_open ()) {
      
         //header
               
         out_py<<"import matplotlib.pyplot as plt"<<std::endl;
         out_py<<"fig, ax = plt.subplots()"<<std::endl;
     
         int ypos= (m_nchains*2)*m_ndevices+m_ndevices;  

         double bias = m_devices[0]->v_micro_probe[0]; 
         #define NORMSEC(unum) ((unum-bias)/1000000)
         #define SEC(unum0) ((unum0)/1000000)

         for(std::vector<OCLDevice*>::iterator it0 = m_devices.begin(); it0 != m_devices.end(); ++it0){       
             
            int index;

            out_py<<"ax.broken_barh([";//<<std::endl;
            index=0;
            for(auto it1 = ((*it0)->v_micro_probe).begin();
               it1 != ((*it0)->v_micro_probe).end(); 
               ++index,++it1){
               
               // PMC cycle time
               //   
               
               if(index%2==0)
                  out_py<<"("<<std::setprecision (30)<<NORMSEC(*it1);
               else if (index!=(((*it0)->v_micro_probe).size()-1))
                  out_py<<","<<std::setprecision (30)<<SEC((*it1)-(*(it1-1)))<<"),";
               else
                  out_py<<","<<std::setprecision (30)<<SEC((*it1)-(*(it1-1)))<<")";
              
            }
            out_py<<"] , ("<<ypos<<", 1),facecolors='blue',linewidth=0.25)"<<std::endl;
            ypos--; 
 
            for(auto it1 = ((*it0)->m_chains).begin(); it1 != ((*it0)->m_chains).end(); ++it1){
               out_py<<"ax.broken_barh([";//<<std::endl;
               index=0;
               for(auto it2 = ((*it1)->v_micro_probe).begin();
                  it2 != ((*it1)->v_micro_probe).end(); 
                  ++index,++it2){
                 
                  // Molpro time
                  // 
                  if(index%2==0)
                     out_py<<"("<<std::setprecision (30)<<NORMSEC(*it2);
                  else if (index!=(((*it1)->v_micro_probe).size()-1))
                     out_py<<","<<std::setprecision (30)<<SEC((*it2)-(*(it2-1)))<<"),";
                  else
                     out_py<<","<<std::setprecision (30)<<SEC((*it2)-(*(it2-1)))<<")";                
                  
               }
               out_py<<"] , ("<<ypos<<", 1),facecolors='IndianRed',linewidth=0.25)"<<std::endl;
               ypos--; 
            }
            ypos+=m_nchains;//reset hpos
            for(auto it1 = ((*it0)->m_chains).begin(); it1 != ((*it0)->m_chains).end(); ++it1){
               
               out_py<<"ax.broken_barh([";//<<std::endl;
               index=0;
               for(auto it2 = ((*it1)->m_host->v_micro_probe).begin();
                  it2 != ((*it1)->m_host->v_micro_probe).end(); 
                  ++index,++it2){

                  // Grid Read time
                  // 
                  if(index%2==0)
                     out_py<<"("<<std::setprecision (30)<<NORMSEC(*it2);
                  else if (index!=((((*it1)->m_host->v_micro_probe)).size()-1))
                     out_py<<","<<std::setprecision (30)<<SEC((*it2)-(*(it2-1)))<<"),";
                  else
                     out_py<<","<<std::setprecision (30)<<SEC((*it2)-(*(it2-1)))<<")";   
                  
               }
               out_py<<"] , ("<<ypos<<", 1),facecolors='LimeGreen',linewidth=0.25)"<<std::endl;
               ypos--;
            }
         }
         out_py<<"ax.grid(True)"<<std::endl;
         out_py<<"ax.set_xticks([10*f for f in range(46)],minor=False)"<<std::endl;
         out_py<<"                                                    "<<std::endl;
         out_py<<"labels = ax.get_xticklabels()                       "<<std::endl;
         out_py<<"for label in labels:                                "<<std::endl;
         out_py<<"   label.set_fontsize(8)                            "<<std::endl;
         out_py<<"   label.set_rotation(90)                           "<<std::endl;
         
         
         out_py<<"plt.savefig(\"probe.pdf\")"<<std::endl;

         out_py.close ();
      } else {
         std::cout << " ERROR CAN'T OPEN PY OUTPUT PROBE FILE" << std::endl;
         exit (EXIT_FAILURE);
      }
      
   #endif
  
   // CSV output with balancing data
   //
   
   #ifdef LEGACY_MULTI_DEVICE
   #ifdef BALANCING_PROBE
   #ifdef BALANCING   
      
      std::cout << "OCLmanager: Outputing .csv with balancing data" << std::endl; 
   
      ofstream out;
      out.open ("out_probe_csv.csv");
      //out.precision (3);
      //out.setf (std::ios::fixed, ios::floatfield);

      if(out.is_open ()) {
         out << "iter,t0,t1"<< std::endl;
               
         for (int i = 0; i < m_run->maxmoves / BALANCING_PROBE; i++){   
            out<<i<<",";         
            for (int j = 0; j < m_ndevices ; j++){    
               out<<m_devices[j]->m_tpmc_probe[i] - m_devices[j]->m_tulock_probe[i]<<","; 
            }   
            out<<"\n";            
         }      
            
         out.close ();
      } else {
         std::cout << " ERROR CAN'T OPEN CSV BALANCING PROBE FILE" << std::endl;
         exit (EXIT_FAILURE);
      }
      
   #endif   
   #endif   
   #endif  
}




/**
 * Does not work with legacy multi option.
 */
OCLDevice* OCLmanager::create_device (const int i)
{
   return new OCLDevice (
         m_captured_device[i],
         m_captured_context[i],
         &v_ignite_isReady[i],//Sync variables
         &v_ignite_mutex[i],  //Sync variables
         &v_ignite_slave[i],  //Sync variables
         &ignite_master,      //Sync variables
         m_n_pre_steps, 
         m_nkernel,
         m_nchains_per_device,     //Max chains per device 
         m_systems[0][0],     // devices get all the same "const" system
         m_opls, 
         m_run, 
         m_temperature,
         m_ndevices,
         has_extra_energy_file);
}
