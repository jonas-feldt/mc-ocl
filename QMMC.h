/*
 * File:   QMMC.h
 * Author: Jonas Feldt
 *
 * Version: 2015-11-30
 */

#ifndef QMMC_H
#define	QMMC_H

#include <string>

#include "typedefs.h"

class Run;
class System;
class ChainResults;

/**
 * QM Metropolis MC.
 */
class QMMC {
public:

   QMMC(const std::string base_name,
        const Run *run,
        const int num_devices,
        const int num_chains);

   virtual ~QMMC();

   std::vector<std::vector<System*>> run(System *system);
   std::vector<std::vector<System*>> run(std::vector<std::vector<System*>> &systems);

private:

   const std::string m_base_name;
   Run *m_run;
   const int num_devices;
   const int num_chains;

   std::vector<std::vector<System*>> run (
         std::vector<std::vector<System*>> &systems,
         const vvd &energies_old);

   std::tuple<ChainResults *, System *> run_chain(
         System *system,
         const int chain_id,
         const int seed,
         std::string basename,
         double energy_old);

   double run_molpro (
      std::string filename,
      System *system) const;

};

#endif	/* QMMC_H */

