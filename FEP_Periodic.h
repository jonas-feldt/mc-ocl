/*
 * File:   FEP_Periodic.h
 * Author: Jonas Feldt
 *
 * Version: 2015-01-06
 */

#ifndef FEP_PERIODIC_H
#define	FEP_PERIODIC_H

#include <string>

#include "typedefs.h"
#include "options.h"

class System_Periodic;
class Run_Periodic;
class OPLS_Periodic;
class Run;
class FEP_OCLmanager;
class Pipe_Host;

/**
 * Free Energy Pertubation theory which uses PMC with periodic boundary
 * conditions...
 *
 * TODO
 */
class FEP_Periodic {
public:
   FEP_Periodic(Run_Periodic *run);
   virtual ~FEP_Periodic();
   vvs run(vvs &systemsA, System_Periodic *systemB,
         Run_Periodic *run,
         OPLS_Periodic *opls, OPLS_Periodic *oplsB,
         std::string traject_file,
         int num_devices, int num_chains);
   vvs run(vvs &systemsA, System_Periodic *systemB,
         Run_Periodic *run,
         OPLS_Periodic *opls, OPLS_Periodic *oplsB,
         std::string traject_file,
         int num_devices, int num_chains, double energy_qm_gp);

   double get_energy_qm_gp();

protected:

   virtual FEP_OCLmanager *create_manager(
               int n_pre_steps, 
               int nkernel, 
               int ndevices, 
               int nchains, 
               const vvs &systemsA,
               System_Periodic* systemB,
               OPLS_Periodic* opls, 
               OPLS_Periodic* oplsB,
               Run_Periodic* run, 
               vvg &gridsA,
               vvg &gridsB,
               int config_data_size, 
               int energy_data_size, 
               _FPOINT_S_ temperature,
               double theta_max,
               double stepmax,   
               vvd &energies_qmA,
               vvd &energies_qmB,
               _FPOINT_S_ energy_qm_gp,
               vvd &energies_mmA,
               vvd &energies_vdw_qmmmA,
               vvd &energies_vdw_qmmmB,
               vvd &energies_oldA,
               vvd &energies_oldB,
               Pipe_Host **hosts,
               std::string basename);

private:
   Run_Periodic *m_run;

   double qm_lattice_updates_wf (std::string base_name, int device, int chain,
           System_Periodic *systemB, System_Periodic *systemA,
           OPLS_Periodic *opls, const Run *run);

   double m_energy_qm_gp;
};

#endif	/* FEP_PERIODIC_H */

