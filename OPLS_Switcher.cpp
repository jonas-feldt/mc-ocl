/*
 * File:    OPLS_Switcher.cpp
 * Author:  Jonas Feldt
 *
 */


#include <vector>
#include <utility>
#include <iostream>

#include "OPLS_Switcher.h"
#include "System.h"
#include "OPLS.h"
#include "Run.h"


// init dummy type
const int OPLS_Switcher::DUMMY_TYPE = -1;


/**
 * The lifetime of opls is tied to the switcher.
 */
OPLS_Switcher::OPLS_Switcher (
      const System *system, Run *run)
   : natom (system->natom),
   system_types (system->atom_type, system->atom_type + system->natom),
   atom_types (run->atom_types),
   epsilons (run->epsilons),
   sigmas (run->sigmas),
   charges (run->charges),
   num_frames (static_cast<double>(run->fep_num_frames)),
   types (run->fep_switch_pairs)
{
}



OPLS_Switcher::~OPLS_Switcher() {
}





/**
 * Computes new parameters for this frame and replaces them in-place
 * in opls.
 */
void OPLS_Switcher::switch_params (
      const unsigned int frame,
      OPLS *opls,
      FILE *fout)
{
   if (types.size() == 0) return; // constant parameters

   fprintf (fout, "\nOPLS VdW Parameter Switch\n\n");

   double factor = compute_factor (frame); // get factor
   for (std::pair<int, int> &pair : types) {

      // find atoms that have to be changed
      for (int i = 0; i < natom; ++i) {
         if (system_types[i] == pair.first) {

            // find old and new parameters
            int oldk = 0;
            int newk = 0;
            for (unsigned int k = 0; k < atom_types.size(); ++k) {
               if (atom_types[k] == pair.first) {
                  oldk = k;
               } else if (atom_types[k] == pair.second) {
                  newk = k;
               }
            }

            // special case for FES dummy parameter
            if (pair.second == DUMMY_TYPE) {
               opls->epsilon[i] = factor * epsilons[oldk];
               opls->set_sigma  (i, factor * sigmas  [oldk]);
               opls->set_charge (i, factor * charges [oldk]);
            } else { // computes and sets new parameters
               opls->epsilon[i] = factor * epsilons[oldk] +
                                    (1.0 - factor) * epsilons[newk];
               opls->set_sigma  (i, factor * sigmas  [oldk] +
                                    (1.0 - factor) * sigmas  [newk]);
               opls->set_charge (i, factor * charges [oldk] +
                                    (1.0 - factor) * charges [newk]);
            }

            fprintf (fout, "%d %d -> %d factor %f\n",
                  i, pair.first, pair.second, factor);
         }
      } 
   }
}





