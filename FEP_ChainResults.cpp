/**
 *
 * File: FEP_ChainResults.cpp
 * Author: Jonas Feldt
 *
 * Version: 2014-10-22
 *
 */

#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <cmath>

#include "ChainResults.h"
#include "FEP_ChainResults.h"

#define WIDTH 15
#define SMALL 3



/**
 * Constructor
 *
 */
FEP_ChainResults::FEP_ChainResults (
      double mean_energy, double mean_interaction, double mean_free_energy,
      double std_dev_energy, double std_dev_interaction, double std_dev_free_energy,
      double stepmax, int steps, int accepted_steps)
   : ChainResults (
         mean_energy, mean_interaction,
         std_dev_energy, std_dev_interaction,
         stepmax, steps, accepted_steps),
   mean_free_energy (mean_free_energy),
   std_dev_free_energy (std_dev_free_energy)
{
}




/*
 * Destructor
 */
FEP_ChainResults::~FEP_ChainResults ()
{
}



void FEP_ChainResults::print_results (std::string file, std::vector<FEP_ChainResults*> results)
{
   if (results.size() == 0) return;

   std::ofstream out (file, std::ofstream::app);

   // title
   out << std::endl;
   out << "Results for " << results.size() << " chain";
   if (results.size() > 1) out << "s"; // chain or chains 
   out << std::endl;
   out << std::endl;

   // header for the table
   out << std::setw(WIDTH + SMALL + 2)  << "Free E.";
   out << std::setw(WIDTH) << "+/-";
   out << std::setw(WIDTH) << "Energy";
   out << std::setw(WIDTH) << "+/-";
   out << std::setw(WIDTH) << "E_int";
   out << std::setw(WIDTH) << "+/-";
   out << std::setw(WIDTH) << "Steps";
   out << std::setw(WIDTH) << "Accepted";
   out << std::setw(WIDTH) << "Stepmax" << std::endl;
   out << std::setw(SMALL + 2 + 9 * WIDTH) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

   // variables for the combined results
   double free_energy         = 0.0;
   double sigma_free_energy   = 0.0;
   double energy              = 0.0;
   double sigma_energy        = 0.0;
   double eint                = 0.0;
   double sigma_eint          = 0.0;
   int steps          = 0;
   int accepted_steps = 0;

   // table
   int index = 0;
   for (auto it = results.begin(); it != results.end(); ++it, ++index) {
      FEP_ChainResults *r = (*it);
      double dSteps = (double) r->steps;

      // sums up
      if (results.size() > 1) {
         free_energy += r->mean_free_energy * dSteps; 
         energy      += r->mean_energy      * dSteps; 
         eint        += r->mean_interaction * dSteps; 
         sigma_energy      += (pow(r->std_dev_energy     , 2.0) + pow(r->mean_energy     , 2.0)) * dSteps;
         sigma_eint        += (pow(r->std_dev_interaction, 2.0) + pow(r->mean_interaction, 2.0)) * dSteps;
         sigma_free_energy += (pow(r->std_dev_free_energy, 2.0) + pow(r->mean_free_energy, 2.0)) * dSteps;
         steps          += r->steps;
         accepted_steps += r->accepted_steps;
      }

      // print row of the table
      out << std::setw(SMALL) << index << " |"; // index

      out << std::scientific << std::setprecision(6);
      out << std::setw(WIDTH) << r->mean_free_energy; // free_energy
      out << std::setw(WIDTH) << r->std_dev_free_energy;

      out << std::setw(WIDTH) << r->mean_energy; // energy
      out << std::setw(WIDTH) << r->std_dev_energy;

      out << std::setw(WIDTH) << r->mean_interaction; // E_int
      out << std::setw(WIDTH) << r->std_dev_interaction;

      out << std::setw(WIDTH) << r->steps; // steps
      out << std::setw(WIDTH - 2) << std::fixed << std::setprecision(1);
      out << (double) r->accepted_steps / dSteps * 100.0 << " %";

      out << std::setw(WIDTH) << std::setprecision(6) << r->stepmax << std::endl; // stepmax 
   }

   if (results.size() > 1) {

      double ch_free_energy       = 0.0;
      double ch_energy            = 0.0;
      double ch_eint              = 0.0;
      double ch_accepted_steps    = 0.0;

      // compute means
      double dSteps = (double) steps;
      double percent_steps= accepted_steps / dSteps;
      energy      /= dSteps;
      eint        /= dSteps;
      free_energy /= dSteps;

      // second sweep to calculate std devs
      for (auto it = results.begin(); it != results.end(); ++it) {
         FEP_ChainResults *r = (*it);
         ch_free_energy       += pow(r->mean_free_energy - free_energy   , 2);
         ch_energy            += pow(r->mean_energy      - energy        , 2); 
         ch_eint              += pow(r->mean_interaction - eint          , 2); 
         ch_accepted_steps    += pow((r->accepted_steps / (double) r->steps) - percent_steps, 2);
      }
      double tmp = results.size() * (results.size() - 1);
      ch_free_energy    /= tmp;
      ch_energy         /= tmp;
      ch_eint           /= tmp;
      ch_accepted_steps /= tmp;
      ch_free_energy    = sqrt(ch_free_energy   );
      ch_energy         = sqrt(ch_energy        );
      ch_eint           = sqrt(ch_eint          );
      ch_accepted_steps = sqrt(ch_accepted_steps);


      // seperator line
      out << std::setw(SMALL + 2 + 9 * WIDTH) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      // prints summed up row of the table
      out << std::setw(SMALL + 2) << " |"; // no index

      out << std::scientific << std::setprecision(6);
      out << std::setw(WIDTH) << free_energy; // free_energy
      out << std::setw(WIDTH) << sqrt(sigma_free_energy / dSteps - pow(free_energy, 2.0));

      out << std::setw(WIDTH) << energy; // energy 
      out << std::setw(WIDTH) << sqrt(sigma_energy / dSteps - pow(energy, 2.0));

      out << std::setw(WIDTH) << eint; // eint
      out << std::setw(WIDTH) << sqrt(sigma_eint / dSteps - pow(eint, 2.0));

      out << std::setw(WIDTH) << steps; // steps
      out << std::setw(WIDTH - 2) << std::fixed << std::setprecision(1);
      out << (double) accepted_steps / dSteps * 100.0 << " %";

      out << std::setw(WIDTH) << "" << std::endl; // empty field


      // prints std devs between chains
      out << std::setw(SMALL + 2) << " |"; // no index

      out << std::scientific << std::setprecision(6);
      out << std::setw(WIDTH) << ch_free_energy; // free_energy
      out << std::setw(WIDTH) << ""; 

      out << std::setw(WIDTH) << ch_energy; // energy 
      out << std::setw(WIDTH) << ""; 

      out << std::setw(WIDTH) << ch_eint; // eint
      out << std::setw(WIDTH) << ""; 

      out << std::setw(WIDTH) << ""; // steps
      out << std::setw(WIDTH - 2) << std::fixed << std::setprecision(1);
      out << ch_accepted_steps * 100.0 << " %";

      out << std::setw(WIDTH) << "" << std::endl; // empty field
   }
}



