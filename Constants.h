/*
 * File:   Constants.h
 * Author: Jonas Feldt
 *
 * Version: 2014-09-26
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H

#include <math.h>

#define BOHR_TO_ANG 0.529177209 // Molpro 2012
#define ANG_TO_BOHR 1.0/BOHR_TO_ANG
#define E2_ANG_TO_KJMOL 1389.354867249
#define BOLTZMANN_KB 8.3144621 / 1000.0
#define KCAL_TO_KJ 4.183999734824774
#define RAD_TO_DEGREE 180.0 / M_PI
#define COULOMB_CONST 332.063806190589  // in Ang kcal/mol


#endif	/* CONSTANTS_H */

