/**
 *
 * File: OCLChain.cpp
 * Author: Sebastiao Miranda, Jonas Feldt
 *
 * Version: 2015-03-12
 *
 */

#include <iostream>
#include <iomanip>
#include <atomic>

#include "OCLChain.h"
#include "OCLDevice.h"
#include "ChainResults.h"
#include "Constants.h"
#include "typedefs.h"

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

#ifdef BASE_PROFILING
   #include <iomanip>
   #include "papi.h"
#endif


int OCLChain::lastChain = 0;

OCLChain::OCLChain(  System_Periodic *system,
                     Charge_Grid *grid,
                     Pipe_Host *host,
                     _FPOINT_S_ energy_qm,
                     _FPOINT_S_ energy_qm_gp,
                     _FPOINT_S_ energy_mm,
                     _FPOINT_S_ energy_vdw_qmmm,
                     _FPOINT_S_ energy_old,
                     int n_pre_steps,
                     int config_data_size,
                     int energy_data_size,
                     int seed,
                     double stepmax,
                     std::string base_name,
                     int num_means,
                     int n_energy_save_params,
                     std::string print_s,
                     bool extra_energy_file,
                     bool has_random_state,
                     std::string state_file,
                     const double dimension_box)
   :  has_extra_energy_file(extra_energy_file),
      num_means (num_means),
      m_n_energy_save_params (n_energy_save_params),
      m_print_s (print_s),
      m_dimension_box (dimension_box)
{

   // Profiling variables
   #ifdef BASE_PROFILING
      t_wlattice=0;
      t_wlattice_end=0;
      t_wlattice_start=0;
      n_wlattice=0;

      t_getgrid=0;
      t_getgrid_end=0;
      t_getgrid_start=0;
      n_getgrid=0;
   #endif

   // Chain's system and grid
   m_system = new System_Periodic(*system);
   m_grid   = new Charge_Grid(*grid);

   // Chain's energy
   m_f_energy_qm         = energy_qm         ;
   m_f_energy_qm_gp      = energy_qm_gp      ;
   m_f_energy_mm         = energy_mm         ;
   m_f_energy_vdw_qmmm   = energy_vdw_qmmm   ;
   m_f_energy_old        = energy_old        ;

   // Object for pipe comn
   m_host = host;

   // Random generation
   if (has_random_state) {
      load_random_state (state_file, rnd_engine);
   } else {
      rnd_engine.seed(seed);
   }
   m_stepmax = stepmax;
   m_n_pre_steps = n_pre_steps;

   m_config_data_size = config_data_size;
   m_energy_data_size = energy_data_size;
   // Random Number lists
   m_v_random_molecule     = new int       [m_n_pre_steps];
   m_v_rand_rotate_theta   = new _FPOINT_S_[m_n_pre_steps];
   m_v_q0                  = new _FPOINT_S_[m_n_pre_steps];
   m_v_q1                  = new _FPOINT_S_[m_n_pre_steps];
   m_v_q2                  = new _FPOINT_S_[m_n_pre_steps];
   m_v_q3                  = new _FPOINT_S_[m_n_pre_steps];
   m_v_random_trans_num    = new _FPOINT_S_[m_n_pre_steps];
   m_v_rand_trans_ex       = new _FPOINT_S_[m_n_pre_steps];
   m_v_rand_trans_ey       = new _FPOINT_S_[m_n_pre_steps];
   m_v_rand_trans_ez       = new _FPOINT_S_[m_n_pre_steps];
   m_v_random_boltzman_acc = new _FPOINT_S_[m_n_pre_steps];

   // Buffer for collecting accepted steps
   m_acc_vector   = new _FPOINT_S_[(m_n_energy_save_params * sizeof(_FPOINT_S_)+
                                    m_system->natom*3*sizeof(_FPOINT_S_))] ;

   if (has_extra_energy_file) {
      m_acc_energies = new _FPOINT_S_[(m_n_energy_save_params * sizeof(_FPOINT_S_))];
      m_extra_energy_file = fopen((base_name + "_exen.out" + "_" + std::to_string(lastChain)).c_str(), "w");
   } else m_acc_energies = nullptr; // allows delete[]

   VERBOSE_LVL_INIT("[INIT] Chain" << lastChain<<", energy_file:"<<  (base_name + "_energy.out" + "_" + std::to_string(lastChain)) << std::endl);
   // preparations for IO
   m_trajectory  = base_name + ".xyz" + "_" + std::to_string(lastChain);
   m_energy_file = fopen((base_name + "_energy.out" + "_" + std::to_string(lastChain)).c_str(), "w");
   m_output_file = fopen((base_name + ".out").c_str(), "a");
   m_output_string = base_name + ".out";

  // TODO write this somewhere up in the hierarchy (PMC/FEP)?
   #ifndef STANDALONE
      if (lastChain == 0) fprintf(m_output_file, "Energy of molecule 0 in gas-phase %15.5f\n",
              energy_qm_gp);
   #endif

   // TODO should go partly to FEP
   if (m_n_energy_save_params == 8) {
      fprintf (m_energy_file, "%10s %10s %20s %20s %10s %10s %10s %10s\n", "step",
              "accepted", "E_pert", "E_mm", "E_inter", "e_charges", "chqmmmvdw",
              "change_mm");
      if (has_extra_energy_file)
         fprintf (m_extra_energy_file, "%10s %10s %20s %20s %10s %10s %10s %10s\n", "step",
                 "accepted", "E_pert", "E_mm", "E_inter", "e_charges", "chqmmmvdw",
                 "change_mm");

   } else if (m_n_energy_save_params == 11) {
      fprintf (m_energy_file, "%10s %10s %20s %20s %10s %10s %20s %20s %10s %20s %10s\n",
            "step",
            "accepted",
            "E(PMC)",
            "E(MM)",
            "E(Int)",
            "E(C-QM3)",
            "E(PMC-B)",
            "<E(PMC)>",
            "<E(Int)>",
            "<E(PMC-B)>",
            "E-E(B)");

      if (has_extra_energy_file)
         fprintf (m_extra_energy_file, "%10s %10s %20s %20s %10s %10s %20s %20s %10s %20s %10s\n",
               "step",
               "accepted",
               "E(PMC)",
               "E(MM)",
               "E(Int)",
               "E(C-QM3)",
               "E(PMC-B)",
               "<E(PMC)>",
               "<E(Int)>",
               "<E(PMC-B)>",
               "E-E(B)");

   } else {
      fprintf (m_energy_file, "%10s %10s %20s %20s %10s %10s %10s %10s %20s %20s %10s %10s %10s\n", "step",
              "accepted", "E_pert", "E_mm", "E_int", "ECQM3t-r", "EvdwQM3t-r",
              "dEMM", "E_pertB", "m(E_pert)", "m(E_int)", "m(dG)", "ddE");
      if (has_extra_energy_file)
         fprintf (m_extra_energy_file, "%10s %10s %20s %20s %10s %10s %10s %10s %20s %20s %10s %10s %10s\n", "step",
                 "accepted", "E_pert", "E_mm", "E_int", "ECQM3t-r", "EvdwQM3t-r",
                 "dEMM", "E_pertB", "m(E_pert)", "m(E_int)", "m(dG)", "ddE");

   }

   m_chain_index = lastChain;
   lastChain++;
}



OCLChain::~OCLChain(){

   // system is not deleted but returned back
   // XXX not really clean, if it's not returned
   // then it's never deleted
   delete[] m_v_random_molecule    ;
   delete[] m_v_rand_rotate_theta  ;
   delete[] m_v_q0                 ;
   delete[] m_v_q1                 ;
   delete[] m_v_q2                 ;
   delete[] m_v_q3                 ;
   delete[] m_v_random_trans_num   ;
   delete[] m_v_rand_trans_ex      ;
   delete[] m_v_rand_trans_ey      ;
   delete[] m_v_rand_trans_ez      ;
   delete[] m_v_random_boltzman_acc;
   delete[] m_acc_vector           ;
   delete[] m_acc_energies         ;
   delete m_grid                   ;

   #ifndef SET_FPOINT_G_DOUBLE
      delete[] v_rx_f;
      delete[] v_ry_f;
      delete[] v_rz_f;
   #endif
}



void OCLChain::launch_thread(){
    m_thread = std::thread(&OCLChain::chain_loop, this);
}

void OCLChain::join_thread(){
    m_thread.join();
}

void OCLChain::chain_loop(){

   while (true){

      // Wait for wf request
      //
      std::unique_lock<std::mutex> ulock(*m_wf_mutex_launch);
         while(*m_wf_flag_launch == 0){
            VERBOSE_LVL_SYNC("[SYNC] Chain thread "<<m_chain_index<<" is waiting for wfu request" << std::endl);
            m_wf_slave_launch->wait(ulock);
         }
         if (*m_wf_flag_launch == -1) return; // die, unique_lock unlocks mutex
         *m_wf_flag_launch = 0;
      ulock.unlock();

      //Execute wf request (pipe comn)
      //
      VERBOSE_LVL_SYNC("[SYNC] Chain thread "<<m_chain_index<<" will issue wfu to wf process" << std::endl);

      MICRO_PROBE(v_micro_probe);//Time stamp before wfu

      // Write System to pipe
      //

      PROFILE_FIRST(0,t_wlattice_start);
      m_host->write_lattice_pipe (m_system, m_device->m_opls);
      PROFILE (0,t_wlattice,t_wlattice_end,t_wlattice_start,n_wlattice);

      // Read Grid from pipe
      // (also wait for wf process to terminate the wfu)

      PROFILE_FIRST(0,t_getgrid_start);
      m_f_energy_qm = m_host->read_grid_pipe (m_grid);
      PROFILE (0,t_getgrid,t_getgrid_end,t_getgrid_start,n_getgrid);

      MICRO_PROBE(v_micro_probe);//Time stamp after wfu

      // Alert master the pipe comn is over
      //
      std::unique_lock<std::mutex> ulock2(*m_wf_mutex_finish);
         *m_wf_flag_finish = 1;
         m_wf_master_finish->notify_all();
      ulock2.unlock();

   }

}

void OCLChain::assoc_device(OCLDevice *device, int chain_index){

   m_device=device;
   m_queue=m_device->m_queue;

   m_wf_flag_launch = &(m_device->v_wf_flag_launch[chain_index]);
   m_wf_flag_finish = &(m_device->v_wf_flag_finish[chain_index]);

   m_wf_mutex_finish = &(m_device->v_wf_mutex_finish[chain_index]);
   m_wf_mutex_launch = &(m_device->v_wf_mutex_launch[chain_index]);

   m_wf_slave_launch = &(m_device->v_wf_slave_launch[chain_index]);
   m_wf_master_finish = &(m_device->v_wf_master_finish[chain_index]);

}

void OCLChain::setup_buffers(){
   // Find largest MM molecule and set the size of these buffers accordingly
   max_mol_atom=-1;
   for(int i = 1; i < m_system->nmol_total; i++)
      if(m_system->molecule2atom[i].natoms > max_mol_atom)
         max_mol_atom = m_system->molecule2atom[i].natoms;

   VERBOSE_LVL_SETUP2("[SETU] Chain "<<m_chain_index<<" Size of changed MM buffers: "<<max_mol_atom<<std::endl);

   // System Reference
   m_old_x =                  cl::Buffer(*(m_device->m_p_context),      //Device context
                                  CL_MEM_READ_WRITE,                     //Read and Write(for updating ref)
                                  (m_system->natom)*sizeof(_FPOINT_S_)); //Number of atoms

   m_old_y =                  cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (m_system->natom)*sizeof(_FPOINT_S_));

   m_old_z =                  cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (m_system->natom)*sizeof(_FPOINT_S_));

   #ifndef SET_FPOINT_G_DOUBLE
   m_old_x_f =                cl::Buffer(*(m_device->m_p_context),      //Device context
                                  CL_MEM_READ_WRITE,                     //Read and Write(for updating ref)
                                  (m_system->natom)*sizeof(_FPOINT_G_)); //Number of atoms

   m_old_y_f =                cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (m_system->natom)*sizeof(_FPOINT_G_));

   m_old_z_f =                cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (m_system->natom)*sizeof(_FPOINT_G_));
   #endif

   // Molecule changed in the MC step (int)
   m_changed_molecule =       cl::Buffer (*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  sizeof(int));

   m_changed_molecule_size =  cl::Buffer (*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  sizeof(int));


   //x,y,z info of the molecules
   m_ch_x =                   cl::Buffer (*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (max_mol_atom)*sizeof(_FPOINT_S_));

   m_ch_y =                   cl::Buffer (*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (max_mol_atom)*sizeof(_FPOINT_S_));

   m_ch_z =                   cl::Buffer (*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (max_mol_atom)*sizeof(_FPOINT_S_));

   //float version if float-mode for qmmm_c is on
   #ifndef SET_FPOINT_G_DOUBLE

   m_ch_x_f =                   cl::Buffer (*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (max_mol_atom)*sizeof(_FPOINT_G_));

   m_ch_y_f =                   cl::Buffer (*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (max_mol_atom)*sizeof(_FPOINT_G_));

   m_ch_z_f =                   cl::Buffer (*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (max_mol_atom)*sizeof(_FPOINT_G_));
   #endif

   //The QM grid x,y,z


   m_grid_rx =                cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY,              // Read Only is from device's prespective
                                  MAX_GRID_SIZE_MULT*(m_grid->dim)*sizeof(_FPOINT_G_));

   m_grid_ry =                cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY,
                                  MAX_GRID_SIZE_MULT*(m_grid->dim)*sizeof(_FPOINT_G_));

   m_grid_rz =                cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY,
                                  MAX_GRID_SIZE_MULT*(m_grid->dim)*sizeof(_FPOINT_G_));

   m_grid_charge =            cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY,
                                  MAX_GRID_SIZE_MULT*(m_grid->dim)*sizeof(_FPOINT_G_));

   m_grid_size =              cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY,
                                  sizeof(int));

   m_sqrt_box_dim =           cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY,
                                  sizeof(_FPOINT_S_));


   //(For m_KERNEL_mm_vdwc/_red kernels)
   m_mm_vdwc_energy_vdw =         cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  (m_device->m_KERNEL_mm_vdwc->m_int_globalRange/       //Global/Local is size of first reduction
                                  m_device->m_KERNEL_mm_vdwc->m_int_localRange)*sizeof(_FPOINT_S_));

   m_mm_vdwc_energy_col =         cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                 (m_device->m_KERNEL_mm_vdwc->m_int_globalRange/       //Global/Local is size of first reduction
                                  m_device->m_KERNEL_mm_vdwc->m_int_localRange)*sizeof(_FPOINT_S_));

   m_minors_energy =              cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  3*sizeof(_FPOINT_S_));//includes mm_vdwc, qmmm_c_nucl and qmmm_vdw
   #ifdef LEGACY_MULTI_DEVICE
   if(m_device->m_bal[m_device->m_device_index]!=0){//If m_bal is 0, no need for this buffer XXX: grid buffers could also be removed....
   #endif
      m_qmmm_c_energy  =         cl::Buffer(*(m_device->m_p_context),
                                     CL_MEM_READ_WRITE,
                                     (FIX_THIS_MULT*(m_device->m_KERNEL_qmmm_c->m_int_globalRange)/       //Global/Local is size of first reduction
                                     m_device->m_KERNEL_qmmm_c->m_int_localRange)*sizeof(_FPOINT_G_));

   #ifdef LEGACY_MULTI_DEVICE
   }else{
      m_qmmm_c_energy  =         cl::Buffer(*(m_device->m_p_context),
                                     CL_MEM_READ_WRITE,
                                     2*sizeof(_FPOINT_G_));//Dummy size just to be a vector, and use position 0
   }
   #endif

   //Host provided Energies
   //This one will be also updtated by OpenCL device
   m_qm_energy =              cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  sizeof(_FPOINT_S_));



   m_energy_qm_gp =           cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY,
                                  sizeof(_FPOINT_S_));

   // Init to 0
   _FPOINT_S_ zero = 0;
   m_energy_old =             cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                  sizeof(_FPOINT_S_),
                                  &zero);

   m_energy_vdw_qmmm =        cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  sizeof(_FPOINT_S_));

   m_energy_mm =              cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  sizeof(_FPOINT_S_));

   m_old_means =              cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  num_means*sizeof(double));

   m_old_variance =           cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  num_means*sizeof(double));


   // Init to 0
   int zero_i = 0;
   m_number_accepted =        cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                  sizeof(int),
                                  &zero_i);

      //Buffer to save accepted configs
   m_accepted_config_data =   cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_WRITE,
                                  m_config_data_size*sizeof(_FPOINT_S_));

   if (has_extra_energy_file) {
      m_accepted_energy_data =   cl::Buffer(*(m_device->m_p_context),
                                     CL_MEM_READ_WRITE,
                                     m_energy_data_size*sizeof(_FPOINT_S_));
   }

     // random lists

   m_random_molecule=         cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(int),
                                  m_v_random_molecule);

   m_rand_rotate_theta=       cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(_FPOINT_S_),
                                  m_v_rand_rotate_theta);

   m_q0=                      cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(_FPOINT_S_),
                                  m_v_q0);

   m_q1=                      cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(_FPOINT_S_),
                                  m_v_q1);

   m_q2=                      cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(_FPOINT_S_),
                                  m_v_q2);

   m_q3=                      cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(_FPOINT_S_),
                                  m_v_q3);

   m_random_trans_num=        cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(_FPOINT_S_),
                                  m_v_random_trans_num);


   m_rand_trans_ex=           cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(_FPOINT_S_),
                                  m_v_rand_trans_ex);

   m_rand_trans_ey=           cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(_FPOINT_S_),
                                  m_v_rand_trans_ey);

   m_rand_trans_ez=           cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(_FPOINT_S_),
                                  m_v_rand_trans_ez);

   m_random_boltzman_acc=     cl::Buffer(*(m_device->m_p_context),
                                  CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR,
                                  m_n_pre_steps*sizeof(_FPOINT_S_),
                                  m_v_random_boltzman_acc);


}




/**
 * Generates all random numbers for one PMC cycle which will be used on the 
 * OpenCL Device.
 */
void OCLChain::prepare_randomness(double theta_max){

   /**
   * Equivalent random calls in Reference PMC CYCLE:
   *
   * int random_molecule = random_gen_mol->uniform_int_a_b (1, nmol_total - 1);
   * rotate_random_molecule (theta_max, random_molecule, copy, random_gen_mol);
   * translate_random_molecule (stepmax, random_molecule, copy, random_gen_mol);
   */

   // (starts at 1 to avoid QM molecule)
   std::uniform_int_distribution<int> dist_molecule            (   1, m_system->nmol_total - 1);
   std::uniform_real_distribution<double> dist_theta           ( 0.0, theta_max * (M_PI / 180.0));
   std::uniform_real_distribution<double> dist_two             (-1.0, 1.0);
   std::uniform_real_distribution<_FPOINT_S_> dist_step       ( 0.0, m_stepmax);
   std::uniform_real_distribution<_FPOINT_S_> dist_boltzman   ( 0.0, 1.0);

   // m_n_pre_steps is the number of elements this lists will hold.
   // It corresponds to the maximum number of steps the device can run autonomously
   for(int i = 0; i < m_n_pre_steps; ++i){

      /***** Generate random molecule *****/
      m_v_random_molecule[i] = dist_molecule(rnd_engine);

      /***** Generate Rotation randomness *****/
      double random_1    = 0.0;
      double random_2    = 0.0;
      double s_random_12 = 0.0;
      double theta = dist_theta (rnd_engine); 
      do {
         random_1 = dist_two (rnd_engine); 
         random_2 = dist_two (rnd_engine);
         s_random_12 = random_1 * random_1 + random_2 * random_2;
      } while (s_random_12 >= 1.0); //Condition s_random_12 = random_1² + random_2² < 1

      double ex = 2.0 * random_1 * sqrt (1.0 - s_random_12);
      double ey = 2.0 * random_2 * sqrt (1.0 - s_random_12);
      double ez = 1.0 - 2.0 * s_random_12;

      m_v_q0[i] = (_FPOINT_S_)      cos (theta / 2.0);
      m_v_q1[i] = (_FPOINT_S_) ex * sin (theta / 2.0);
      m_v_q2[i] = (_FPOINT_S_) ey * sin (theta / 2.0);
      m_v_q3[i] = (_FPOINT_S_) ez * sin (theta / 2.0);

      m_v_rand_rotate_theta[i] = (_FPOINT_S_)theta;

      /***** Generate Translation randomness *****/
      double random_3    = 0.0;
      double random_4    = 0.0;
      double s_random_34 = 0.0;
      do {
         random_3 = dist_two (rnd_engine);
         random_4 = dist_two (rnd_engine); 
         s_random_34 = random_3 * random_3 + random_4 * random_4;
      } while (s_random_34 >= 1.0);

      m_v_rand_trans_ex[i] = (_FPOINT_S_) 2 * random_3 * sqrt (1 - s_random_34);
      m_v_rand_trans_ey[i] = (_FPOINT_S_) 2 * random_4 * sqrt (1 - s_random_34);
      m_v_rand_trans_ez[i] = (_FPOINT_S_) 1 - 2 * s_random_34;

      m_v_random_trans_num[i] = dist_step (rnd_engine); 

      /***** Generate Boltzman randomness *****/
      m_v_random_boltzman_acc[i] = dist_boltzman (rnd_engine); 
   }

   return;
}



void OCLChain::write_grid(
      int memory_offset,
      bool reduce,
      double dimension_box,
      double cutoff_coulomb){

   if (reduce) m_grid->reduced_coords(dimension_box, cutoff_coulomb);

   try{

      std::vector<cl::Event> write_grid_dependencies;

      if (memory_offset > 0) {
         write_grid_dependencies.push_back(m_device->m_EVENT_closure[memory_offset]);
      }

      cl_int blocking;
      #ifdef FORCE_GRID_CL_TRUE
         blocking = CL_TRUE;
      #else
         blocking = CL_FALSE;
      #endif

      //The "offset" in the enqueue function is in respect to the target buffer! Kept to 0
      //The origin vector offset is  + m_assoc_device*(grid_size) - Comment only applies for old muli-device

      VERBOSE_LVL_TRANSFERS("[TRAN] Chain "<<m_chain_index<<" will write grid do opencldevice"<<std::endl);

      #ifdef HALF_FIXED

         // Convert Grid Coordinates To Fixed Point
         // XXX XXX : Attention : This could be moved to another loop (like pipe-read)
         // This has to happen every grid update, make sure of that !

         _FIXED_* x_q = new _FIXED_[m_grid->dim];
         _FIXED_* y_q = new _FIXED_[m_grid->dim];
         _FIXED_* z_q = new _FIXED_[m_grid->dim];

         for (int v=0; v<m_grid->dim; v++){
            x_q[v] = FLOAT2FIXED(m_grid->x[v]);
            y_q[v] = FLOAT2FIXED(m_grid->y[v]);
            z_q[v] = FLOAT2FIXED(m_grid->z[v]);
         }

         m_queue->enqueueWriteBuffer(  m_grid_rx,                       //Buffer
                                       blocking,                        //blocking
                                       0,                               //offset
                                       (m_grid->dim)*sizeof(_FIXED_),   //#grid points
                                       x_q,                             //origin vector
                                       &write_grid_dependencies,        //no dependencies
                                       &m_EVENT_grid_rx);               //finish event

         m_queue->enqueueWriteBuffer(  m_grid_ry,                       //Buffer
                                       blocking,                        //blocking
                                       0,                               //offset
                                       (m_grid->dim)*sizeof(_FIXED_),   //#grid points
                                       y_q,                             //origin vector
                                       &write_grid_dependencies,        //no dependencies
                                       &m_EVENT_grid_ry);               //finish event

         m_queue->enqueueWriteBuffer(  m_grid_rz,                       //Buffer
                                       blocking,                        //blocking
                                       0,                               //offset
                                       (m_grid->dim)*sizeof(_FIXED_),   //#grid points
                                       z_q,                             //origin vector
                                       &write_grid_dependencies,        //no dependencies
                                       &m_EVENT_grid_rz);               //finish event

      #else

         m_queue->enqueueWriteBuffer(  m_grid_rx,                       //Buffer
                                       blocking,                        //blocking
                                       0,                               //offset
                                       (m_grid->dim)*sizeof(_FPOINT_G_),//#grid points
                                       m_grid->x,                       //origin vector
                                       &write_grid_dependencies,        //no dependencies
                                       &m_EVENT_grid_rx);               //finish event

         m_queue->enqueueWriteBuffer(  m_grid_ry,                       //Buffer
                                       blocking,                        //blocking
                                       0,                               //offset
                                       (m_grid->dim)*sizeof(_FPOINT_G_),//#grid points
                                       m_grid->y,                       //origin vector
                                       &write_grid_dependencies,        //no dependencies
                                       &m_EVENT_grid_ry);               //finish event

         m_queue->enqueueWriteBuffer(  m_grid_rz,                       //Buffer
                                       blocking,                        //blocking
                                       0,                               //offset
                                       (m_grid->dim)*sizeof(_FPOINT_G_),//#grid points
                                       m_grid->z,                       //origin vector
                                       &write_grid_dependencies,        //no dependencies
                                       &m_EVENT_grid_rz);               //finish event
      #endif

      m_queue->enqueueWriteBuffer(  m_grid_charge,                   //Buffer
                                    blocking,                        //blocking
                                    0,                               //offset
                                    (m_grid->dim)*sizeof(_FPOINT_G_),//#grid points
                                    m_grid->charges,                 //origin vector
                                    &write_grid_dependencies,        //no dependencies
                                    &m_EVENT_grid_charge);           //finish event

      m_queue->enqueueWriteBuffer(  m_grid_size,                     //Buffer
                                    blocking,                        //blocking
                                    0,                               //offset
                                    sizeof(int),                     //#grid points
                                    &m_grid->dim,                    //origin vector
                                    &write_grid_dependencies,        //no dependencies
                                    &m_EVENT_grid_size);             //finish event

      m_queue->enqueueWriteBuffer(  m_sqrt_box_dim,                  //Buffer
                                    blocking,                        //blocking
                                    0,                               //offset
                                    sizeof(_FPOINT_S_),              //#grid points
                                    &m_grid->sqrt_box_dim,            //origin vector
                                    &write_grid_dependencies,        //no dependencies
                                    &m_EVENT_sqrt_box_dim);          //finish event
      /*#ifdef HALF_FIXED
         delete x_q;
         delete y_q;
         delete z_q;
      #endif*/

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLChain::write_grid Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }

   // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_grid_rx       );
   m_device->m_mc_dependencies.push_back(m_EVENT_grid_ry       );
   m_device->m_mc_dependencies.push_back(m_EVENT_grid_rz       );
   m_device->m_mc_dependencies.push_back(m_EVENT_grid_size     );
   m_device->m_mc_dependencies.push_back(m_EVENT_grid_charge   );
   m_device->m_mc_dependencies.push_back(m_EVENT_sqrt_box_dim  );
}

void OCLChain::write_init_energies(){

   cl_int blocking = CL_FALSE;

   try{

      m_queue->enqueueWriteBuffer(  m_qm_energy,                //Buffer
                                    blocking,                   //blocking defined above
                                    0,                          //offset
                                    sizeof(_FPOINT_S_),         //1 value
                                    &m_f_energy_qm,              //origin vector
                                    NULL,                       //no dependencies
                                    &m_EVENT_qm_energy);        //finish event

      m_queue->enqueueWriteBuffer(  m_energy_qm_gp,             //Buffer
                                    blocking,                   //blocking defined above
                                    0,                          //offset
                                    sizeof(_FPOINT_S_),         //1 value
                                    &m_f_energy_qm_gp,           //origin vector
                                    NULL,                       //no dependencies
                                    &m_EVENT_energy_qm_gp);     //finish event

      m_queue->enqueueWriteBuffer(  m_energy_mm,                //Buffer
                                    blocking,                   //blocking defined above
                                    0,                          //offset
                                    sizeof(_FPOINT_S_),         //1 value
                                    &m_f_energy_mm,              //origin vector
                                    NULL,                       //no dependencies
                                    &m_EVENT_energy_mm);        //finish event

      m_queue->enqueueWriteBuffer(  m_energy_vdw_qmmm,          //Buffer
                                    blocking,                   //blocking defined above
                                    0,                          //offset
                                    sizeof(_FPOINT_S_),         //1 value
                                    &m_f_energy_vdw_qmmm,       //origin vector
                                    NULL,                       //no dependencies
                                    &m_EVENT_energy_vdw_qmmm);  //finish event

      m_queue->enqueueWriteBuffer(  m_energy_old,               //Buffer
                                    blocking,                   //blocking defined above
                                    0,                          //offset
                                    sizeof(_FPOINT_S_),         //1 value
                                    &m_f_energy_old,             //origin vector
                                    NULL,                       //no dependencies
                                    &m_EVENT_energy_old);       //finish event

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLChain::write_init_energies Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }

   // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_qm_energy        );
   m_device->m_mc_dependencies.push_back(m_EVENT_energy_qm_gp     );
   m_device->m_mc_dependencies.push_back(m_EVENT_energy_mm        );
   m_device->m_mc_dependencies.push_back(m_EVENT_energy_vdw_qmmm  );
   m_device->m_mc_dependencies.push_back(m_EVENT_energy_old       );

}

void OCLChain::write_system_lattice(){
   try{

      cl_int blocking;
      #ifdef FORCE_LATTICE_CL_TRUE
         blocking = CL_TRUE;
      #else
         blocking = CL_FALSE;
      #endif

      //The "offset" in the enqueue function is in respect to the target buffer! Kept to 0

      m_queue->enqueueWriteBuffer(  m_old_x,                              //Buffer
                                    blocking,                             //blocking
                                    0,                                    //offset
                                    (m_system->natom)*sizeof(_FPOINT_S_),//#atoms
                                    m_system->rx,                         //origin vector
                                    NULL,                                 //no dependencies
                                    &m_EVENT_old_x);                      //finish event


      m_queue->enqueueWriteBuffer(  m_old_y,                              //Buffer
                                    blocking,                             //blocking
                                    0,                                    //offset
                                    (m_system->natom)*sizeof(_FPOINT_S_),//#atoms
                                    m_system->ry,                         //origin vector
                                    NULL,                                 //no dependencies
                                    &m_EVENT_old_y);                      //finish event

      m_queue->enqueueWriteBuffer(  m_old_z,                              //Buffer
                                    blocking,                             //blocking
                                    0,                                    //offset
                                    (m_system->natom)*sizeof(_FPOINT_S_),//#atoms
                                    m_system->rz,                         //origin vector
                                    NULL,                                 //no dependencies
                                    &m_EVENT_old_z);                      //finish event
      #ifndef SET_FPOINT_G_DOUBLE

      #if defined(HALF_FIXED) && defined(LIGHT_FIXED)

         // Convert Grid Coordinates To Fixed Point
         // XXX XXX : Attention : This could be moved to another loop (like pipe-read)
         // This has to happen every grid update, make sure of that !

         _FIXED_* x_q = new _FIXED_[m_system->natom];
         _FIXED_* y_q = new _FIXED_[m_system->natom];
         _FIXED_* z_q = new _FIXED_[m_system->natom];

         for (int v=0; v<m_system->natom; v++){
            x_q[v] = FLOAT2FIXED(m_system->rx[v]);
            y_q[v] = FLOAT2FIXED(m_system->ry[v]);
            z_q[v] = FLOAT2FIXED(m_system->rz[v]);
         }

         m_queue->enqueueWriteBuffer(  m_old_x_f,                           //Buffer
                                       blocking,                            //blocking
                                       0,                                   //offset
                                       (m_system->natom)*sizeof(_FPOINT_G_),//#atoms
                                       x_q,                                 //origin vector
                                       NULL,                                //no dependencies
                                       NULL);                               //finish event


         m_queue->enqueueWriteBuffer(  m_old_y_f,                           //Buffer
                                       blocking,                            //blocking
                                       0,                                   //offset
                                       (m_system->natom)*sizeof(_FPOINT_G_),//#atoms
                                       y_q,                                 //origin vector
                                       NULL,                                //no dependencies
                                       NULL);                               //finish event

         m_queue->enqueueWriteBuffer(  m_old_z_f,                           //Buffer
                                       blocking,                            //blocking
                                       0,                                   //offset
                                       (m_system->natom)*sizeof(_FPOINT_G_),//#atoms
                                       z_q,                                 //origin vector
                                       NULL,                                //no dependencies
                                       NULL);                               //finish event
      #else

         v_rx_f = new float[m_system->natom];
         v_ry_f = new float[m_system->natom];
         v_rz_f = new float[m_system->natom];

         for(unsigned int a=0; a < m_system->natom; a++){
            v_rx_f[a] = (float) m_system->rx[a];
            v_ry_f[a] = (float) m_system->ry[a];
            v_rz_f[a] = (float) m_system->rz[a];
         }

         m_queue->enqueueWriteBuffer(  m_old_x_f,                           //Buffer
                                       blocking,                            //blocking
                                       0,                                   //offset
                                       (m_system->natom)*sizeof(_FPOINT_G_),//#atoms
                                       v_rx_f,                              //origin vector
                                       NULL,                                //no dependencies
                                       NULL);                               //finish event


         m_queue->enqueueWriteBuffer(  m_old_y_f,                           //Buffer
                                       blocking,                            //blocking
                                       0,                                   //offset
                                       (m_system->natom)*sizeof(_FPOINT_G_),//#atoms
                                       v_ry_f,                              //origin vector
                                       NULL,                                //no dependencies
                                       NULL);                               //finish event

         m_queue->enqueueWriteBuffer(  m_old_z_f,                           //Buffer
                                       blocking,                            //blocking
                                       0,                                   //offset
                                       (m_system->natom)*sizeof(_FPOINT_G_),//#atoms
                                       v_rz_f,                              //origin vector
                                       NULL,                                //no dependencies
                                       NULL);                               //finish event
      #endif
      #endif

      //(Charges are written once in constant memory...)

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLChain::write_system_lattice Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }

   // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_old_x);
   m_device->m_mc_dependencies.push_back(m_EVENT_old_y);
   m_device->m_mc_dependencies.push_back(m_EVENT_old_z);
}
void OCLChain::get_reference_system(int memory_offset){

   std::vector<cl::Event> read_event_dependencies;

   read_event_dependencies.push_back(m_device->m_EVENT_closure[memory_offset]);

   VERBOSE_LVL_TRANSFERS("[TRAN] Chain "<<m_chain_index<<" will get curr xyz reference"<<std::endl);
   try{
      m_queue->enqueueReadBuffer( m_old_x,
                                  CL_TRUE,
                                  0,
                                  m_system->natom*sizeof(_FPOINT_S_),
                                  m_system->rx,
                                  &read_event_dependencies,
                                  &m_EVENT_read_ref_x);

      m_queue->enqueueReadBuffer( m_old_y,
                                  CL_TRUE,
                                  0,
                                  m_system->natom*sizeof(_FPOINT_S_),
                                  m_system->ry,
                                  &read_event_dependencies,
                                  &m_EVENT_read_ref_y);

      m_queue->enqueueReadBuffer( m_old_z,
                                  CL_TRUE,
                                  0,
                                  m_system->natom*sizeof(_FPOINT_S_),
                                  m_system->rz,
                                  &read_event_dependencies,
                                  &m_EVENT_read_ref_z);

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLChain::get_reference_system Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }

   // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_read_ref_x);
   m_device->m_mc_dependencies.push_back(m_EVENT_read_ref_y);
   m_device->m_mc_dependencies.push_back(m_EVENT_read_ref_z);
}




int OCLChain::get_current_number_accepted(int memory_offset) {

   std::vector<cl::Event> read_event_dependencies;

   read_event_dependencies.push_back(m_device->m_EVENT_closure[memory_offset]);

   VERBOSE_LVL_TRANSFERS("[TRAN] Chain "<<m_chain_index<<" will get curr acc steps"<<std::endl);

   int curr_acc = 0;

   try{

      // LOG 29.7.14: Removed the current_jump method, because now m_number_accepted is a
      // single dedicated buffer.
      // Compute current memory jump
      //int current_jump = ((inner_step/(m_device->m_run->print_every))%m_run->n_save_systems)*
      //                   (m_n_energy_save_params+m_system->natom*3)*sizeof(_FPOINT_S_);

      m_queue->enqueueReadBuffer(  m_number_accepted,
                                   CL_TRUE,
                                   0,//current_jump + sizeof(_FPOINT_S_)
                                   sizeof(int),
                                   &curr_acc,
                                   &read_event_dependencies,
                                   &m_EVENT_read_number_accepted);

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLDevice::get_current_number_accepted Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }

   m_curr_acc = curr_acc;

   // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_read_number_accepted);

   return curr_acc;
}

void OCLChain::update_lists(double theta_max, int memory_offset){


   int blocking = CL_FALSE;

   // Generate more numbers
   prepare_randomness(theta_max);

   // Setup dependencies
   std::vector<cl::Event> update_dependecies;

   update_dependecies.push_back(m_device->m_EVENT_closure[memory_offset]);

   try{

   // Write them to gpu

   m_queue->enqueueWriteBuffer( m_random_molecule,        //Buffer
                                blocking,                 //blocking
                                0,                        //offset
                                m_n_pre_steps*sizeof(int),//#numbers
                                m_v_random_molecule,      //origin vector
                                &update_dependecies,      //no dependencies
                                &m_EVENT_random_molecule);//finish event

   m_queue->enqueueWriteBuffer( m_rand_rotate_theta,
                                blocking,
                                0,
                                m_n_pre_steps*sizeof(_FPOINT_S_),
                                 m_v_rand_rotate_theta,
                                &update_dependecies,
                                &m_EVENT_rand_rotate_theta);

   m_queue->enqueueWriteBuffer( m_q0,
                                blocking,
                                0,
                                m_n_pre_steps*sizeof(_FPOINT_S_),
                                m_v_q0,
                                &update_dependecies,
                                &m_EVENT_q0);

   m_queue->enqueueWriteBuffer( m_q1,
                                blocking,
                                0,
                                m_n_pre_steps*sizeof(_FPOINT_S_),
                                m_v_q1,
                                &update_dependecies,
                                &m_EVENT_q1);

   m_queue->enqueueWriteBuffer( m_q2,
                                blocking,
                                0,
                                m_n_pre_steps*sizeof(_FPOINT_S_),
                                m_v_q2,
                                &update_dependecies,
                                &m_EVENT_q2);

   m_queue->enqueueWriteBuffer( m_q3,
                                blocking,
                                0,
                                m_n_pre_steps*sizeof(_FPOINT_S_),
                                m_v_q3,
                                &update_dependecies,
                                &m_EVENT_q3);

   m_queue->enqueueWriteBuffer( m_random_trans_num,
                                blocking,
                                0,
                                m_n_pre_steps*sizeof(_FPOINT_S_),
                                m_v_random_trans_num,
                                &update_dependecies,
                                &m_EVENT_random_trans_num);

   m_queue->enqueueWriteBuffer( m_rand_trans_ex,
                                blocking,
                                0,
                                m_n_pre_steps*sizeof(_FPOINT_S_),
                                m_v_rand_trans_ex,
                                &update_dependecies,
                                &m_EVENT_rand_trans_ex);

   m_queue->enqueueWriteBuffer( m_rand_trans_ey,
                                blocking,
                                0,
                                m_n_pre_steps*sizeof(_FPOINT_S_),
                                m_v_rand_trans_ey,
                                &update_dependecies,
                                &m_EVENT_rand_trans_ey);

   m_queue->enqueueWriteBuffer( m_rand_trans_ez,
                                blocking,
                                0,
                                m_n_pre_steps*sizeof(_FPOINT_S_),
                                m_v_rand_trans_ez,
                                &update_dependecies,
                                &m_EVENT_rand_trans_ez);

   m_queue->enqueueWriteBuffer( m_random_boltzman_acc,
                                blocking,
                                0,
                                m_n_pre_steps*sizeof(_FPOINT_S_),
                                m_v_random_boltzman_acc,
                                &update_dependecies,
                                &m_EVENT_random_boltzman_acc);

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLChain::update_lists Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }


      // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_random_molecule    );
   m_device->m_mc_dependencies.push_back(m_EVENT_rand_rotate_theta  );
   m_device->m_mc_dependencies.push_back(m_EVENT_q0                 );
   m_device->m_mc_dependencies.push_back(m_EVENT_q1                 );
   m_device->m_mc_dependencies.push_back(m_EVENT_q2                 );
   m_device->m_mc_dependencies.push_back(m_EVENT_q3                 );
   m_device->m_mc_dependencies.push_back(m_EVENT_random_trans_num   );
   m_device->m_mc_dependencies.push_back(m_EVENT_rand_trans_ex      );
   m_device->m_mc_dependencies.push_back(m_EVENT_rand_trans_ey      );
   m_device->m_mc_dependencies.push_back(m_EVENT_rand_trans_ez      );
   m_device->m_mc_dependencies.push_back(m_EVENT_random_boltzman_acc);

}

void OCLChain::update_reference_energy(int memory_offset){

   std::vector<cl::Event> update_reference_dependencies;
   update_reference_dependencies.push_back(m_device->m_EVENT_closure[memory_offset]);

   cl_int blocking=CL_TRUE;

   try{

      // Get reference's mm and vdw_qmmm
      //

      m_queue->enqueueReadBuffer(m_energy_vdw_qmmm,
                                 blocking,
                                 0,
                                 sizeof(_FPOINT_S_),
                                 &m_f_energy_vdw_qmmm,
                                 &update_reference_dependencies,
                                 &m_EVENT_get_energy_vdw_qmmm);

      m_queue->enqueueReadBuffer(m_energy_mm,
                                 blocking,
                                 0,
                                 sizeof(_FPOINT_S_),
                                 &m_f_energy_mm,
                                 &update_reference_dependencies,
                                 &m_EVENT_get_energy_mm);

      m_f_energy_old = m_f_energy_qm + m_f_energy_vdw_qmmm + m_f_energy_mm;

      // Write Updated Energies
      //

      VERBOSE_LVL_TRANSFERS("[TRAN] Chain "<<m_chain_index<<" will update m_f_energy_qm  = "<<m_f_energy_qm<<std::endl);
      VERBOSE_LVL_TRANSFERS("[TRAN] Chain "<<m_chain_index<<" will update m_f_energy_old = "<<m_f_energy_old<<std::endl);

      m_queue->enqueueWriteBuffer(  m_qm_energy,         //Buffer
                                    blocking,            //blocking defined above
                                    0,                   //offset
                                    sizeof(_FPOINT_S_),  //1 value
                                    &m_f_energy_qm,         //origin vector
                                    &update_reference_dependencies,
                                    &m_EVENT_qm_energy); //finish event

      m_queue->enqueueWriteBuffer(  m_energy_old,        //Buffer
                                    blocking,            //blocking defined above
                                    0,                   //offset
                                    sizeof(_FPOINT_S_),  //1 value
                                    &m_f_energy_old,        //origin vector
                                    &update_reference_dependencies,
                                    &m_EVENT_energy_old); //finish event

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLDevice::write_new_energy Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }

   // Setup dependencies for next ocldevice monte carlo

   m_device->m_mc_dependencies.push_back(m_EVENT_get_energy_vdw_qmmm );
   m_device->m_mc_dependencies.push_back(m_EVENT_get_energy_mm       );
   m_device->m_mc_dependencies.push_back(m_EVENT_qm_energy           );
   m_device->m_mc_dependencies.push_back(m_EVENT_energy_old          );

}
void OCLChain::save_acc_systems(int memory_offset, int n_reads, int n_reads_energies){

   #ifdef BASE_PROFILING
   double t_fsave_acc=0,t_fsave_end=0,t_fsave_start=0;
   int    n_fsave=0;
   #endif

   std::vector<cl::Event> read_event_dependencies;

   read_event_dependencies.push_back(m_device->m_EVENT_closure[memory_offset]);

   VERBOSE_LVL_TRANSFERS("\n[CHAIN] SAVE_ACC_SYSTEMS: Enqueueing Read Buffers to read system xyz (number: "<<n_reads<<" )"<<std::endl);

   for(int k=0; k < n_reads; k++){

      m_queue->enqueueReadBuffer(  m_accepted_config_data,
                                   CL_TRUE,

                                   k*(m_n_energy_save_params * sizeof(_FPOINT_S_)+
                                      m_system->natom*3*sizeof(_FPOINT_S_)),

                                     (m_n_energy_save_params * sizeof(_FPOINT_S_)+
                                      m_system->natom*3*sizeof(_FPOINT_S_)),

                                   m_acc_vector,
                                   &read_event_dependencies,
                                   &m_EVENT_read_config_data);

      // Print to energy file
      if(k==0) PROFILE_FIRST(0,t_fsave_start);

      // TODO move to FEP 
      if (m_n_energy_save_params == 8) {
         fprintf (m_energy_file,
                  m_print_s.c_str(),
                  (int)m_acc_vector[0],
                  (int)m_acc_vector[1],
                  (_FPOINT_S_)m_acc_vector[2],
                  (_FPOINT_S_)m_acc_vector[3],
                  (_FPOINT_S_)m_acc_vector[4],
                  (_FPOINT_S_)m_acc_vector[5],
                  (_FPOINT_S_)m_acc_vector[6],
                  (_FPOINT_S_)m_acc_vector[7]);

      } else if (m_n_energy_save_params == 11) {
         fprintf (m_energy_file,
                  m_print_s.c_str(),
                  (int)m_acc_vector[0],
                  (int)m_acc_vector[1],
                  (_FPOINT_S_)m_acc_vector[ 2],
                  (_FPOINT_S_)m_acc_vector[ 3],
                  (_FPOINT_S_)m_acc_vector[ 4],
                  (_FPOINT_S_)m_acc_vector[ 5],
                  (_FPOINT_S_)m_acc_vector[ 6],
                  (_FPOINT_S_)m_acc_vector[ 7],
                  (_FPOINT_S_)m_acc_vector[ 8],
                  (_FPOINT_S_)m_acc_vector[ 9],
                  (_FPOINT_S_)m_acc_vector[10]);

      } else {
         const double free_energy = -BOLTZMANN_KB * m_device->m_run->temperature * log((_FPOINT_S_)m_acc_vector[11]);

         fprintf (m_energy_file,
                  m_print_s.c_str(),
                  (int)       m_acc_vector[0],
                  (int)       m_acc_vector[1],
                  (_FPOINT_S_)m_acc_vector[2],
                  (_FPOINT_S_)m_acc_vector[3],
                  (_FPOINT_S_)m_acc_vector[4],
                  (_FPOINT_S_)m_acc_vector[5],
                  (_FPOINT_S_)m_acc_vector[6],
                  (_FPOINT_S_)m_acc_vector[7],
                  (_FPOINT_S_)m_acc_vector[8],
                  (_FPOINT_S_)m_acc_vector[9],
                  (_FPOINT_S_)m_acc_vector[10],
                  free_energy,
                  (_FPOINT_S_)m_acc_vector[12]);
      }

      // XXX: This is a bit hardcoded
      int xyz_offset = m_n_energy_save_params;

      // Print to trajectory 
      //
      // XXX open only once before the k-loop
      std::ofstream out;
      out.open (m_trajectory.c_str(), std::ios::app);
      out.precision (3);
      out.setf (std::ios::fixed, std::ios::floatfield);

      if(out.is_open ()) {
         out << "\t" << m_system->natom << std::endl;
         out <<  std::endl;
         for (unsigned int i = 0; i < m_system->natom; i++) {
            out << m_system->atom_name[i] << "\t";
            out << m_acc_vector[xyz_offset+3*i  ] * ((System_Periodic*)(m_system))->box_dimension << "\t";
            out << m_acc_vector[xyz_offset+3*i+1] * ((System_Periodic*)(m_system))->box_dimension << "\t";
            out << m_acc_vector[xyz_offset+3*i+2] * ((System_Periodic*)(m_system))->box_dimension << "\t";
            out << std::endl;
         }
         out.close ();
      } else {
         std::cout << " ERROR CAN'T OPEN OUTPUT FILE" << std::endl;
         exit (EXIT_FAILURE);
      }
   }


   if (has_extra_energy_file) {
      VERBOSE_LVL_TRANSFERS("\n[CHAIN] SAVE_ACC_SYSTEMS: Enqueueing Read Buffers to read energies (number: "<< n_reads_energies <<" )"<<std::endl);


      for(int k=0; k < n_reads_energies; k++){

         // XXX a single huge read might be faster
         m_queue->enqueueReadBuffer(  m_accepted_energy_data,
                                      CL_TRUE,
                                      k*m_n_energy_save_params * sizeof(_FPOINT_S_),
                                        m_n_energy_save_params * sizeof(_FPOINT_S_),
                                      m_acc_energies,
                                      &read_event_dependencies,
                                      &m_EVENT_read_config_data); // can overwrite out event

         // TODO move to FEP 
         if (m_n_energy_save_params == 8) {

            fprintf (m_extra_energy_file,
                     m_print_s.c_str(),
                     (int)       m_acc_energies[0],
                     (int)       m_acc_energies[1],
                     (_FPOINT_S_)m_acc_energies[2],
                     (_FPOINT_S_)m_acc_energies[3],
                     (_FPOINT_S_)m_acc_energies[4],
                     (_FPOINT_S_)m_acc_energies[5],
                     (_FPOINT_S_)m_acc_energies[6],
                     (_FPOINT_S_)m_acc_energies[7]);

         } else if (m_n_energy_save_params == 11) {
            fprintf (m_extra_energy_file,
                     m_print_s.c_str(),
                     static_cast<int> (m_acc_energies[0]),
                     static_cast<int> (m_acc_energies[1]),
                     static_cast<_FPOINT_S_> (m_acc_energies[ 2]),
                     static_cast<_FPOINT_S_> (m_acc_energies[ 3]),
                     static_cast<_FPOINT_S_> (m_acc_energies[ 4]),
                     static_cast<_FPOINT_S_> (m_acc_energies[ 5]),
                     static_cast<_FPOINT_S_> (m_acc_energies[ 6]),
                     static_cast<_FPOINT_S_> (m_acc_energies[ 7]),
                     static_cast<_FPOINT_S_> (m_acc_energies[ 8]),
                     static_cast<_FPOINT_S_> (m_acc_energies[ 9]),
                     static_cast<_FPOINT_S_> (m_acc_energies[10]));
         } else {

            const double free_energy = -BOLTZMANN_KB * m_device->m_run->temperature * log((_FPOINT_S_)m_acc_energies[11]);
            fprintf (m_extra_energy_file,
                     m_print_s.c_str(),
                     (int)       m_acc_energies[0],
                     (int)       m_acc_energies[1],
                     (_FPOINT_S_)m_acc_energies[2],
                     (_FPOINT_S_)m_acc_energies[3],
                     (_FPOINT_S_)m_acc_energies[4],
                     (_FPOINT_S_)m_acc_energies[5],
                     (_FPOINT_S_)m_acc_energies[6],
                     (_FPOINT_S_)m_acc_energies[7],
                     (_FPOINT_S_)m_acc_energies[8],
                     (_FPOINT_S_)m_acc_energies[9],
                     (_FPOINT_S_)m_acc_energies[10],
                     free_energy,
                     (_FPOINT_S_)m_acc_energies[12]);
         }
      }
   }


   PROFILE (0,t_fsave_acc,t_fsave_end,t_fsave_start,n_fsave);

   #ifdef BASE_PROFILING
   std::cout<<std::endl;
   if(n_fsave!=0){

         std::cout << "[Measu after save: ]t_fsave_acc:              "
                   << std::setprecision (15)
                   << t_fsave_acc/(n_fsave*1000)
                   << " ms"
                   << "("
                   << n_fsave
                   <<" samples)"
                   << std::endl;
   }
   #endif
   // Setup dependencies for next ocldevice monte carlo
   m_device->m_mc_dependencies.push_back(m_EVENT_read_config_data);

   // Info: The last system is in m_acc_vector
}




void OCLChain::print_profiling(){
   #ifdef BASE_PROFILING
   std::cout << "Pipe times For Chain "<<m_chain_index << std::endl;

   if(n_wlattice!=0){
         std::cout << "t_wlattice:               "
                   << std::setprecision (15)
                   << t_wlattice/(n_wlattice*1000)
                   << " ms"
                   << "("
                   << n_wlattice
                   <<" samples)"
                   << std::endl;
   }
   if(n_getgrid!=0){
         std::cout << "t_getgrid:                "
                   << std::setprecision (15)
                   << t_getgrid/(n_getgrid*1000)
                   << " ms"
                   << "("
                   << n_getgrid
                   <<" samples)"
                   << std::endl;
   }

   #ifdef WILL_WF_ADJUST
      m_host->print_profiling();
   #endif

   #endif

}



/**
 * Returns the final results.
 */
ChainResults *OCLChain::get_results (const int steps){

   const int number_accepted = get_current_number_accepted(0); // use 0 as dummy

   double old_means[num_means];
   double old_variance[num_means];

   get_means(num_means, old_means, old_variance);
   m_queue->finish();

   //compute std dev
   const double sigma_energy      = sqrt(old_variance[MEAN_ENERGY     ] / m_device->m_maxmoves);
   const double sigma_interaction = sqrt(old_variance[MEAN_INTERACTION] / m_device->m_maxmoves);

   fclose (m_energy_file); // XXX the scope of these fopen/close close can be reduced
   fclose (m_output_file);
   if (has_extra_energy_file) fclose (m_extra_energy_file);

   return new ChainResults(
         old_means[MEAN_ENERGY], old_means[MEAN_INTERACTION],
         sigma_energy          , sigma_interaction,
         m_stepmax * m_dimension_box, steps, number_accepted);
}



/**
 * Reads the means and their variances from the device. Saves the
 * results in the provided arrays.
 *
 * @param dim number of means
 * @param means storage for means
 * @param variances storage for variances
 */
void OCLChain::get_means(int dim, double *means, double *variances) {
   try{
      m_queue->finish(); //sanity sync, probably not needed
      m_queue->enqueueReadBuffer(     m_old_means,
                                      CL_TRUE,
                                      0,
                                      dim * sizeof(double),
                                      means,
                                      NULL,
                                      NULL);

      m_queue->enqueueReadBuffer(     m_old_variance,
                                      CL_TRUE,
                                      0,
                                      dim * sizeof(double),
                                      variances,
                                      NULL,
                                      NULL);

   }catch(cl::Error error){//openCL error catching
      std::cout<<"OCLChain::get_means Error: "<<error.what()<<"("<<error.err()<<")"<<std::endl;
      throw error;
   }
}


void OCLChain::save_random_state (std::string file) {
   std::ofstream ofile (file, std::ios::binary);
   ofile << rnd_engine; 
}


void OCLChain::load_random_state (std::string file, random_engine &engine) {
   std::cout << "reading file " << file << std::endl;
   std::ifstream ifile (file, std::ios::binary);
   ifile >> engine;
   std::cout << "loading random state " << engine() << std::endl;
}
