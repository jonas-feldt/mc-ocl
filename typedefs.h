/*
 * File:   typedefs.h
 * Author: Jonas Feldt
 *
 * Version: 2014-11-03
 */

#ifndef TYPEDEFS
#define TYPEDEFS

#include <vector>
#include <random>

#include "options.h"
#include "Delegate.h"

class System_Periodic;
class Charge_Grid;

// some typedefs to simplify double iterations
// vectors:
typedef std::vector<std::vector<System_Periodic*>> vvs; // double
typedef std::vector<std::vector<Charge_Grid*    >> vvg;
typedef std::vector<std::vector<_FPOINT_S_      >> vvd;

typedef std::vector<System_Periodic*> vs;               // single
typedef std::vector<Charge_Grid*    > vg;
typedef std::vector<_FPOINT_S_      > vd;

// 64-bit Mersenne Twister by Matsumoto and Nishimura, 2000
typedef std::mersenne_twister_engine<std::uint_fast64_t, 64, 312, 156, 31,
      0xb5026f5aa96619e9, 29,
      0x5555555555555555, 17,
      0x71d67fffeda60000, 37,
      0xfff7eee000000000, 43, 6364136223846793005> random_engine;

typedef SA::delegate<double (unsigned int, unsigned int)> DistanceCall;

#endif // TYPEDEFS
