/*
 * File:   TrajectoryAnalyzer.h
 * Author: Jonas Feldt
 *
 * Version: 2014-02-17
 */

#ifndef TRAJECTORYANALYZER_H
#define	TRAJECTORYANALYZER_H

#include "System.h"


class TrajectoryAnalyzer {

   public:
      TrajectoryAnalyzer(
           std::string trajectory_file, int first_frame, int last_frame,
           std::string output_file, const unsigned int num_chains);
      virtual ~TrajectoryAnalyzer();
      virtual void run();
      virtual void read_system(std::string tinker_file);

   protected:
      virtual void analyze() = 0;
      virtual void finalize() = 0;
      virtual void save_output() = 0;
   
      int first_frame;
      int steps;
      std::string trajectory_file;
      std::string output_file;
      System *system;
      unsigned int num_chains;
      int steps_file;

};

#endif	/* TRAJECTORYANALYZER_H */

