/*
 * File:   System_Periodic.cpp
 * Author: Jonas Feldt
 *
 * Version: 2013-04-19
 */

#include <math.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>

#include "options.h"
#include "System_Periodic.h"
#include "geom_utils.h"
#include "distance_interface.h"


/**
 * Constructor
 *
 *
 * @param natom number of atoms
 * @param rx coordinates
 * @param ry coordinates
 * @param rz coordinates
 * @param atom_type force field atom types
 * @param atom_name name of elements
 * @param n_12 number of bonded neighbors
 * @param connect indices of bonded neighbors
 * @param box_dimension dimension of box
 */
System_Periodic::System_Periodic (unsigned int natom, double rx[], double ry[],
        double rz[], int atom_type[], std::string atom_name[], int n_12[],
        int connect[][4], double box_dimension, DistanceInterface *distances)
: System (natom, rx, ry, rz, atom_type, atom_name, n_12, connect, distances),
box_dimension (box_dimension)
{
   // converts coordinates to reduced units
   for (unsigned int i = 0; i < natom; ++i) {
      rx[i] /= box_dimension;
   }
   for (unsigned int i = 0; i < natom; ++i) {
      ry[i] /= box_dimension;
   }
   for (unsigned int i = 0; i < natom; ++i) {
      rz[i] /= box_dimension;
   }

   normal_x = new double[natom];
   normal_y = new double[natom];
   normal_z = new double[natom];

   // updates distances
   distances->ComputeDistances ();
}



System_Periodic::~System_Periodic () {
   delete[] normal_x;
   delete[] normal_y;
   delete[] normal_z;
}





/*
 * Copy constructor
 *
 * @param other to be copied system
 */
System_Periodic::System_Periodic (System_Periodic &other) 
   : System (other), box_dimension(other.box_dimension)
{
   normal_x = new double[natom];
   normal_y = new double[natom];
   normal_z = new double[natom];
}




/*
 * Copy constructor, which makes a copy only of the specified molecule.
 *
 * @param other to be copied system
 * @param mol to be copied molecule
 */
System_Periodic::System_Periodic (System_Periodic &other, int mol)
   : System (other, mol), box_dimension(other.box_dimension)
{
   normal_x = new double[natom];
   normal_y = new double[natom];
   normal_z = new double[natom];
}




/**
 * Checks if the input structure is physically/chemically correct.
 *
 * @param cutoff_coulomb the cutoff for coulomb in reduced units
 * @param cutoff_vdw the cutoff for vdw in reduced units
 * @param outfile main output file
 * @return true if sane, false otherwise
 *
 */
bool System_Periodic::is_sane(double cutoff_coulomb, double cutoff_vdw) {

   // TODO check the minimal distance to all neighboured images
   return true;

   // checks the minimal distance between the QM molecule and it's image
   // which should be larger than the cutoffs
   double min = 1.0; 
   for (int i = 0; i < molecule2atom[0].natoms; ++i) {
      int first = molecule2atom[0].atom_id[i];
      for (int j = 0; j < molecule2atom[0].natoms; ++j) {
         int second = molecule2atom[0].atom_id[j];
         // XXX can I use here the distances and what does this actually do?
         double dist = compute_minimal_distance (
               rx[first], ry[first], rz[first],
               rx[second] + 1.0, ry[second], rz[second]);
         if (dist < min) min = dist;
      }
   }
   if (min < cutoff_vdw) { 
      fprintf (stderr, "ERROR: The minimal distance between the QM part and its image is smaller than the cutoff for the VdW interactions.\n");
      fprintf (stderr, "       %8.3f < %8.3f\n", min * box_dimension, cutoff_vdw * box_dimension);
      fprintf (stderr, "       Decrease the cutoff or/and increase the box dimension.\n");
      return false;
   }
   if (min < cutoff_coulomb) {
      fprintf (stderr, "ERROR: The minimal distance between the QM part and its image is smaller than the cutoff for the Coulomb interactions.\n");
      fprintf (stderr, "       %8.3f < %8.3f\n", min * box_dimension, cutoff_coulomb * box_dimension);
      fprintf (stderr, "       Decrease the cutoff or/and increase the box dimension.\n");
      return false;
   } 
   return true;
}



void System_Periodic::print () {

   printf("Number of atoms: %8i\n", natom);

   for (unsigned int i = 0; i < natom; i++) {
      printf ("    %4i   %4s  %10.7f   %10.7f  %10.7f  %6i  Connections %i \n",
              i, atom_name[i].c_str(), rx[i] * box_dimension, ry[i] * box_dimension,
              rz[i] * box_dimension, atom_type[i], n_12[i]);
   }
   
   std::cout << "molecules " << nmol_total << std::endl;
   for (int i = 0; i < nmol_total; ++i) {
      std::cout << i << " " << molecule2atom[i].natoms << std::endl;
      for (int j = 0; j < molecule2atom[i].natoms; ++j) {
         std::cout << j << " " << molecule2atom[i].atom_id[j] << std::endl;
         if (molecule2atom[i].atom_id[j] > static_cast<int> (natom)) exit(EXIT_FAILURE); // XXX avoid cast
      }
   }

   std::cout << "atoms " << natom << std::endl;
   for (unsigned int i = 0; i < natom; ++i) {
      std::cout << i << " " << molecule[i] << std::endl;
   } 


   //printf("Distances\n");
   //printf("    ");
   //for (int i = 0; i < natom; ++i) {
   //   printf("%10i", i);
   //}
   //std::cout << std::endl;
   //for (int i = 0; i < natom; ++i) {
   //   printf("%4i", i);
   //   for (int j = 0; j < natom; ++j) {
   //      printf("%10.5f", distances[i][j] * box_dimension);
   //   }
   //   std::cout << std::endl;
   //}
}



/**
 * Appends the informations about the system in tinker file format to the given
 * file.
 *
 * @param file name of output file
 */
void System_Periodic::save_all (std::string file) {

   std::ofstream out;
   out.open (file.c_str(), std::ios::app);
   out << std::scientific << std::setprecision(15);
   const auto &coords = get_normal_coords ();
   double *x = coords[0];
   double *y = coords[1];
   double *z = coords[2];

   if (out.is_open ()) {
      out << "\t" << natom << std::endl;
      for (unsigned int i = 0; i < natom; i++) {
         out << i << "\t";
         out << atom_name[i] << "\t";
         out << std::setw(25) << x[i] << "\t";
         out << std::setw(25) << y[i] << "\t";
         out << std::setw(25) << z[i] << "\t";
         out << atom_type[i] << "\t";
         for (int j = 0; j < n_12[i]; ++j) {
            out << (connect[i][j] + 1) << "\t";
         }
         out << std::endl;
      }
      out.close ();
   } else {
      std::cerr << " ERROR CAN'T OPEN OUTPUT FILE" << std::endl;
      exit (EXIT_FAILURE);
   }
}




/**
 * Appends the informations about the system in xyz file format to the given
 * file.
 *
 * @param file name of output file
 */
void System_Periodic::save_xyz (std::string file) {

   std::ofstream out;
   out.open (file.c_str(), std::ios::app);
   out.precision (3);
   out.setf (std::ios::fixed, std::ios::floatfield);

   if (out.is_open ()) {
      out << "\t" << natom << std::endl;
      out << std::endl;
      for (unsigned int i = 0; i < natom; i++) {
         out << atom_name[i] << "\t";
         out << rx[i] * box_dimension << "\t";
         out << ry[i] * box_dimension << "\t";
         out << rz[i] * box_dimension << "\t";
         out << std::endl;
      }
      out.close ();
   } else {
      std::cerr << " ERROR CAN'T OPEN OUTPUT FILE" << std::endl;
      exit (EXIT_FAILURE);
   }
}



std::vector<double*> System_Periodic::get_normal_coords ()
{
   for (int i = 0; i < nmol_total; ++i) {
      const auto &mol = molecule2atom[i];
      double cx, cy, cz;
      center_mass(i, this, cx, cy, cz);
      normal_coordinate_helper (mol, cx, rx, normal_x);
      normal_coordinate_helper (mol, cy, ry, normal_y);
      normal_coordinate_helper (mol, cz, rz, normal_z);
   }

   return std::vector<double*> {normal_x,
                                normal_y,
                                normal_z};
}




void System_Periodic::wrap (unsigned int molecule)
{
   // translate molecule into origin
   move_into_center_mass(molecule, this);

   // wrap rest of the system
   double *tx = new double[natom];
   double *ty = new double[natom];
   double *tz = new double[natom];
   for (int i = 0; i < nmol_total; ++i) {
      const auto &mol = molecule2atom[i];
      double cx, cy, cz;
      center_mass(i, this, cx, cy, cz);
      // overwrites the original rx, ry, rz
      normal_coordinate_helper (mol, cx, rx, tx);
      normal_coordinate_helper (mol, cy, ry, ty);
      normal_coordinate_helper (mol, cz, rz, tz);
   }

   for (unsigned int i = 0; i < natom; ++i) {
      rx[i] = tx[i] / box_dimension;
      ry[i] = ty[i] / box_dimension;
      rz[i] = tz[i] / box_dimension;
   }

   delete[] tx;
   delete[] ty;
   delete[] tz;
}


void System_Periodic::normal_coordinate_helper (const Molecule_db &mol, double com, double *r, double *n) {

   int cell = int (com);
   double c = com - cell;
   // now transform all atoms according to coms
   for (int i = 0; i < mol.natoms; ++i) {
      const unsigned int atom = mol.atom_id[i];
      double t = r[atom]- cell;
      if (c < -0.5) t += 1.0; // -0.5 to 1.0
      else if (c >  0.5) t -= 1.0; // -0.5 to 0.5
      n[atom] = t * box_dimension; // -box_dim / 2 to box_dim / 2
   }
}




/**
 * Translates the molecule by the given vector in normal coordinates.
 */
void System_Periodic::translate (double x, double y, double z, int mol) {
   x /= box_dimension;
   y /= box_dimension;
   z /= box_dimension;
   for (int pos = 0; pos < molecule2atom[mol].natoms; ++pos) {
      int i = molecule2atom[mol].atom_id[pos];
      rx[i] += x;
      ry[i] += y;
      rz[i] += z;
   }
}

