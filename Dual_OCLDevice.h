/**
 *
 * File: Dual_OCLDevice.h
 * Author: Jonas Feldt
 *
 * Version: 2017-02-13
 *
 */

#ifndef DUAL_OCLDEVICE_H_INCLUDED
#define DUAL_OCLDEVICE_H_INCLUDED

#define __CL_ENABLE_EXCEPTIONS

#include <condition_variable>

#ifdef PORTABLE_CL_WRAPPER
   #include "cl12cpp.h"
#else
   #include<CL/cl.hpp>
#endif

#include "OCLDevice.h"

class System_Periodic;
class OPLS_Periodic;
class Run_Periodic;
class OCl_Kernel;


class Dual_OCLDevice : public OCLDevice {

public:

   Dual_OCLDevice(
             cl::Device device,
             cl::Context *context,
             int *ignite_isReady,
             std::mutex *ignite_mutex,
             std::condition_variable *ignite_slave,
             std::condition_variable *ignite_master,
             int n_pre_steps,
             int nkernel,
             int max_chains,
             System_Periodic* system,
             const OPLS_Periodic* opls,
             Run_Periodic* run,
             _FPOINT_S_ temperature,
             int ndevices,
             bool extra_energy_file);

   virtual ~Dual_OCLDevice();

   //// Device object code

   //Sets up Kernels and buffers for PMC Cycle
   virtual void setup_pmc(std::string kernel_file_closure) override;

   //Member OpenCL Kernels
   OCLKernel *m_KERNEL_qmmm_cB;               //Coulomb qmmm (GRID)
   OCLKernel *m_KERNEL_qmmm_c_red_coarseB;    //Coarser version of reduction

   // Kernel Event vectors
   cl::Event *m_EVENT_qmmm_cB;
   cl::Event *m_EVENT_qmmm_c_red_coarseB;

protected:

   //// Pmc logic code

   //Change arguments to switch chain
   virtual void setChainArgs(int chain) override;

   //Launches a PMC Cycle step
   virtual void launch_pmc_cycle(int inner_step, int outer_step, int memory_offset) override;

};

#endif //DUAL_OCLDEVICE_H_INCLUDED
