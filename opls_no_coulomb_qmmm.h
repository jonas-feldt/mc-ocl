/*
 * File: opls_no_coulomb_qmmm.h
 * Author: Jonas Feldt
 *
 * Version: 2017-07-18
 *
 * Template based decorator which turns off the QM/MM
 * Coulomb interaction.
 */

#pragma once

#include "typedefs.h"

template <class T>
class OPLSNoCoulombQMMM : public T
{
   public:

      using T::T;
      
      double energy (DistanceCall GetDistance) const override
      {
         double energy = 0.0;
         energy += T::energy_vdw (GetDistance);
         energy += T::energy_coulomb_mm (GetDistance);
         return energy;
      }

      double energy (DistanceCall GetDistance,
                     DistanceCall GetDistanceOld,
                     int changed_molecule) const override
      {
         double energy = 0.0;
         energy += T::energy_vdw (GetDistance, GetDistanceOld, changed_molecule);
         energy += T::energy_coulomb_mm (GetDistance, GetDistanceOld, changed_molecule);
         return energy;
      }

};


#include "opls_periodic_dual_topology.h"

// XXX this seems to work but I would prefer to refactor dual topology to use
// the normal energy_coulomb functions maybe or so...
template <>
class OPLSNoCoulombQMMM <OPLSPeriodicDualTopology> : public OPLSPeriodicDualTopology
{
   public:

      using OPLSPeriodicDualTopology::OPLSPeriodicDualTopology;
      
      double energy (DistanceCall GetDistance) const override
      {
         double energy = 0.0;
         energy += energy_mm (GetDistance);
         energy += EnergyVDWQMMMTopology (GetDistance, reference, s1r, s2r);
         energy += EnergyVDWQMMMTopology (GetDistance, target, s1, s2);
         return energy;
      }




      double energy (DistanceCall GetDistance, DistanceCall GetDistanceOld,
            int changed_molecule) const override
      {
         double energy = 0.0;
         energy += energy_mm (GetDistance, GetDistanceOld, changed_molecule);
         energy += EnergyVDWQMMMTopology (GetDistance, reference, GetDistanceOld, changed_molecule, s1r, s2r);
         energy += EnergyVDWQMMMTopology (GetDistance, target, GetDistanceOld, changed_molecule, s1, s2);
         return energy;
      }

};
