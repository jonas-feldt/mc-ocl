/**
 *
 * File: qmmm_c_f_optB.cl
 * Author: Sebastiao Miranda
 *
 * Version: 2014-02-25
 *
 */
#pragma OPENCL EXTENSION cl_khr_fp64: enable

#define E2_ANG_TO_KJMOL 1389.354867249

typedef long LONG;
typedef int _FIXED_;

#define FLOAT2FIXED(num_f) (_FIXED_)(num_f * N_1) 
#define FIXED2FLOAT(num_f) ((float)(num_f))/N_1 

inline _FIXED_ compute_minimal_distance_sq (_FIXED_ rxi_i, _FIXED_ ryi_i, _FIXED_ rzi_i, _FIXED_ rxj_i, _FIXED_ ryj_i, _FIXED_ rzj_i) {
 
   _FIXED_ dx_i = rxi_i - rxj_i;
   _FIXED_ dy_i = ryi_i - ryj_i;
   _FIXED_ dz_i = rzi_i - rzj_i;

   //equivalent to -=round()
   dx_i -= (dx_i + N_05)&A_N; //>>N)<<N;   
   dy_i -= (dy_i + N_05)&A_N; //>>N)<<N;   
   dz_i -= (dz_i + N_05)&A_N; //>>N)<<N;   
   
   _FIXED_ dx_sq_i =  ( (LONG)dx_i * (LONG)dx_i )>>N;
   _FIXED_ dy_sq_i =  ( (LONG)dy_i * (LONG)dy_i )>>N;
   _FIXED_ dz_sq_i =  ( (LONG)dz_i * (LONG)dz_i )>>N;
  
   //Faster yet less precise version of the above 3lines  
   //
   //_FIXED_ dx_sq_i =  ( (dx_i>>(N/2)) * (dx_i>>(N/2)) );
   //_FIXED_ dy_sq_i =  ( (dy_i>>(N/2)) * (dy_i>>(N/2)) );
   //_FIXED_ dz_sq_i =  ( (dz_i>>(N/2)) * (dz_i>>(N/2)) );
   
   return dx_sq_i+dy_sq_i+dz_sq_i;
}
__kernel
void qmmm_c(

   // Local Memory
   __local float *scratch,

   // Kernel Scalar Arguments
   // (number of atoms for this work-item)
   int m_grid_points,
   
   // MM molecule old x,y,z
   __global _FIXED_ *old_x,
   __global _FIXED_ *old_y,
   __global _FIXED_ *old_z,
   __global float *charges,

    // Changed MM molecule
   __global int      *changed_molecule,
   __global int      *changed_molecule_size,
   __global _FIXED_    *ch_x,
   __global _FIXED_    *ch_y,
   __global _FIXED_    *ch_z,
   
   // Global buffers with GRID data
   __global _FIXED_  *grid_rx,
   __global _FIXED_  *grid_ry,
   __global _FIXED_  *grid_rz,
   __global float    *grid_charge,
   __global int      *grid_size,
   
   // Work Group energy result
   __global float    *result_wg_energy,
   
   // Run constants
   __constant float *cutoff_coulomb_rec_sq,
   __constant float *cutoff_coulomb_rec,
   __constant float *cutoff,

   // System Constants
   __constant int   *mol2atom0,
   
   // Squared cutoff for faster branching
   __constant _FIXED_ *cutoff_sq
   
   ){
   
   __private int    k                 ;
   __private int    global_id         ;
   __private int    local_id          ;
   __private int    local_size        ; 
   __private int    grid_point        ;

   __private _FIXED_  dist_new        ;
   __private _FIXED_  dist_old        ;    
   __private float  dist_new_r        ;
   __private float  dist_old_r        ;   
   __private float  energy = 0.0      ;
   __private float  qs                ;
   __private _FIXED_  this_grid_rx      ;
   __private _FIXED_  this_grid_ry      ;
   __private _FIXED_  this_grid_rz      ; 
   __private float  this_grid_charge  ;
   __private _FIXED_  this_cutoff       ;
   
   __private _FIXED_  this_ch_old_rx    ;
   __private _FIXED_  this_ch_old_ry    ;
   __private _FIXED_  this_ch_old_rz    ;
   __private float  this_ch_old_charge;
    
   // Molecule atoms
   __private int start_atom_i          ;
   __private int end_atom              ;
   
   // Get id's
   local_id  = get_local_id(0);
   global_id  = get_global_id(0);
   local_size = get_local_size(0);
   
   //printf("QMMM-C:%d %d %d",global_id,*grid_size,m_grid_points);

   // If this is a valid work-item
   if(global_id < (*grid_size)/m_grid_points){

      // Find Out atoms belonging to the molecule
      start_atom_i = mol2atom0[*changed_molecule]; 

      //For each grid point of this work-item
      for(int i=0;i<m_grid_points;i++){
    
         grid_point = global_id*m_grid_points+i;
         //collect grid data to private memory
         this_grid_rx    = grid_rx    [grid_point];
         this_grid_ry    = grid_ry    [grid_point];
         this_grid_rz    = grid_rz    [grid_point];
         this_grid_charge= grid_charge[grid_point];
       
         this_cutoff     = *cutoff_sq;
         
         // Loop through each changed atom in molecule
         for(int j=0;j<*changed_molecule_size;j++){   
     
            this_ch_old_rx       = old_x    [start_atom_i+j]; 
            this_ch_old_ry       = old_y    [start_atom_i+j]; 
            this_ch_old_rz       = old_z    [start_atom_i+j];
            this_ch_old_charge   = charges  [start_atom_i+j];
            
            dist_new = compute_minimal_distance_sq(ch_x[j],     ch_y[j],     ch_z[j],
                                                   this_grid_rx,this_grid_ry,this_grid_rz);
                                        
            dist_old = compute_minimal_distance_sq(this_ch_old_rx, this_ch_old_ry, this_ch_old_rz,
                                                   this_grid_rx,   this_grid_ry,   this_grid_rz);
                                        
            if (dist_new < this_cutoff && dist_old < this_cutoff) {
   
               float dist_new_f = FIXED2FLOAT(dist_new);
               float dist_old_f = FIXED2FLOAT(dist_old);
   
               dist_new_r = sqrt(dist_new_f);
               dist_old_r = sqrt(dist_old_f);
   
               qs = -this_ch_old_charge*this_grid_charge; //this_ch_old_charge is vector ff_charges in the serial code

               energy += qs * (rsqrt(dist_new_f) - rsqrt(dist_old_f) +
                          (*cutoff_coulomb_rec_sq) * (dist_new_r - dist_old_r));
                          
            }else if (dist_new < this_cutoff && dist_old >= this_cutoff) {     
               
               float dist_new_f = FIXED2FLOAT(dist_new);
               dist_new_r = sqrt(dist_new_f);
                              
               qs = -this_ch_old_charge*this_grid_charge; //this_ch_old_charge is vector ff_charges in the serial code
            
               energy += qs * (rsqrt(dist_new_f) - (*cutoff_coulomb_rec) +
                          (*cutoff_coulomb_rec_sq) * (dist_new_r - *cutoff));
              
            }else if (dist_old < this_cutoff) { // dist_new >= cutoff && dist_old < cutoff
              
               float dist_old_f = FIXED2FLOAT(dist_old);
               dist_old_r = sqrt(dist_old_f);
               
               qs = -this_ch_old_charge*this_grid_charge; //this_ch_old_charge is vector ff_charges in the serial code

               energy -= qs * (rsqrt(dist_old_f) - (*cutoff_coulomb_rec) +
                       (*cutoff_coulomb_rec_sq) * (dist_old_r - *cutoff ));

            }
         }
         // Take grid charge into account   
      }
   }//else, wont contribute to reduction: energy is 0 by default.
     
   //Write private result to local memory
   scratch[local_id] = energy*E2_ANG_TO_KJMOL;
   
   barrier(CLK_LOCAL_MEM_FENCE);
   
   //Now all work-items of the same work group must accumulate their energy.
   for(int offset = local_size/2; offset > 0 ;offset >>= 1){
      if(local_id < offset){
         scratch[local_id] = scratch[local_id + offset] + scratch[local_id];
      }
      barrier(CLK_LOCAL_MEM_FENCE);
   }

   //Work item with id 0 writes the final result of the work-group
   if (local_id == 0) {
      result_wg_energy[get_group_id(0)] = scratch[0];
   } 
   
}
	
