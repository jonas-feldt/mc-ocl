/*
 * File:   RDF_Periodic.cpp
 * Author: Jonas Feldt
 *
 * Version: 2014-02-18
 */

#include <iostream>
#include <math.h>

#include "RDF_Periodic.h"
#include "io_utils.h"
#include "options.h"
#include "geom_utils.h"
#include "typedefs.h"



RDF_Periodic::RDF_Periodic(std::string trajectory_file,
         int first_ID, int second_ID, double delta, int max_bin, int first_frame,
         int last_frame, std::string output_file, double box_dim, const unsigned int num_chains)
   : TrajectoryAnalyzerPeriodic(trajectory_file, first_frame, last_frame, output_file, box_dim, num_chains),
   first (first_ID),
   second (second_ID),
   delta (delta),
   max_bin (max_bin)
{
   histogram = new long[max_bin];
   for (int i = 0.0; i < max_bin; ++i) {
      histogram[i] = 0.0;
   }
   normalized = new double[max_bin];
   this->delta = delta / box_dim;
}



RDF_Periodic::~RDF_Periodic () {
   delete[] histogram;
   delete[] normalized;
}


void RDF_Periodic::analyze () {

   const int num = system->natom;
   const int *atom_types = system->atom_type;
   const int *molecules  = system->molecule;
   DistanceCall GetDistance = system->GetDistance;

   for (int i = 0; i < num; ++i) {
      if (atom_types[i] != first) continue;          // wrong id
      for (int j = 0; j < num; ++j) {
         if (i == j) continue;                       // same atom
         if (atom_types[j] != second) continue;      // wrong id
         if (molecules[i] == molecules[j]) continue; // same molecule
         const double dist = GetDistance (i, j);
         const int bin = static_cast<int> (dist / delta);
         if (bin >= max_bin) continue;               // distance too large
         histogram[bin] += 1;
      }
   }
}


/**
 * Creates the normalized distribution function for periodic systems.
 */
void RDF_Periodic::finalize () {
   // counts number of pairs
   int pairs = 0;
   int num = system->natom;
   int *atom_types = system->atom_type;
   int *molecules = system->molecule;
   for (int i = 0; i < num; ++i) {
      if (atom_types[i] != first)          continue; // wrong id
      for (int j = 0; j < num; ++j) {
         if (i == j) continue;                       // same atom
         if (atom_types[j] != second)      continue; // wrong id
         if (molecules[i] == molecules[j]) continue; // same molecule
         pairs += 1;
      }
   }

   double delta_norm = delta * box_dim;
   for (int i = 0; i < max_bin; ++i) {
      double vol = pow(delta_norm * (i + 1), 3.0) - pow(delta_norm * i, 3.0);
      double n = (double) histogram[i] / (double) pairs / (double) steps;
      double v = (4.0 / 3.0 * M_PI * vol) / pow (box_dim, 3.0);
      normalized[i] = n / v;
   }
}



void RDF_Periodic::save_output () {
   if (file_exists(output_file)) {
      std::cerr << "Output file already exists: " << output_file << std::endl;
      return;
   }

   FILE *out = fopen(output_file.c_str(), "w");
   fprintf(out, "%10s %10s %10s %10s\n", "bin", "distance", "histogram",
           "normalized");
   double delta_norm = delta * box_dim;
   double d_half = delta_norm / 2.0;
   for (int i = 0; i < max_bin; ++i) {         // loop over bins
      fprintf(out, "%10i %10.3f %10li %10.4f\n", i, (i * delta_norm) + d_half,
              histogram[i], normalized[i]);
   }
   fclose(out);
}
