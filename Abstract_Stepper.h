
/*
 * File:   Abstract_Stepper.h
 * Author: Jonas Feldt
 *
 * Version: 2017-05-15
 */

#ifndef ABSTRACT_STEPPER_H
#define	ABSTRACT_STEPPER_H

class System;
class OPLS;

class Abstract_Stepper
{
   public:

      virtual void step (System *system, const OPLS *opls) = 0;
      virtual double dE (System *system, System *system_old, const OPLS *opls) = 0;
      virtual bool is_accepted () = 0;
      
      virtual void accept (System *system, System *system_old) = 0;
      virtual void reject (System *system, System *system_old) = 0;
      virtual void update (int step) {};
};

#endif	/* ABSTRACT_STEPPER_H */

